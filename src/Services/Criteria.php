<?php
/*
-----------------------------------------------------------
FILE NAME: Criteria.class.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  This calss is used to handle GET, POST and PUT method of "criteria" service.
GET method is used to retrieve information of criteria group(s)
POST method is used to create an criteria group
PUT method is used to modify an criteria group

This file is initial version
Create on 07/15/2016
Written by liaom@miamioh.edu
*/

namespace MiamiOH\FacultyElections\Services;

class Criteria extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    // ************************************************************************
    // **********Helper functions that were called by the frame work***********
    // **********and create internal datasource and configuration objects******
    // ************************************************************************
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     *
     * GET:
     *     get information of criteria(s), which represent Pool table in DB
     *
     * Parameter(s):
     *     none
     *
     * Return(s):
     *     @return criteriaId: number
     *     @return criteriaName: string
     *     @return criteriaOption: array
     * 			@return id: number
     * 			@return name: string
     * */
    public function getCriteria()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        // base sql query, retrieve information of all criteria
        $sql_query = "
        	select 
        		facelectmgr.criteria.criteria_id,
        		facelectmgr.criteria.criteria_name,
        		facelectmgr.criteria_option.criteria_option_id,
        		facelectmgr.criteria_option.criteria_option_name 
        	from facelectmgr.criteria
        	left join facelectmgr.criteria_option
        	on facelectmgr.criteria.criteria_id = facelectmgr.criteria_option.criteria_id
        	order by facelectmgr.criteria_option.criteria_option_id
        ";

        //===========================================================
        //==================== execute sql query ====================
        //===========================================================

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $results = $dbh->queryall_array($sql_query);
        } catch (Exception $e) {
            throw new \Exection('Error: retrieve data from database:\n' . $e->getMessage());
        }

        //============================================================
        //==================== Set Return Payload ====================
        //============================================================

        foreach ($results as $row) {
            $payload[$row['criteria_id']]['criteriaId'] = $row['criteria_id'];
            $payload[$row['criteria_id']]['criteriaName'] = $row['criteria_name'];
            $payload[$row['criteria_id']]['criteriaOption'][$row['criteria_option_id']] = $row['criteria_option_name'];
        }

        $payload = array_values($payload);

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setTotalObjects(count($payload));

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }
}
