<?php
/*
-----------------------------------------------------------
FILE NAME: common.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this class contains some helper function which is shared by all WS

Create on 09/06/2016
Written by liaom@miamioh.edu
*/

class Common
{
    private $dbh;

    function __construct($database, $dataSource_name)
    {
        $this->dbh = $database->getHandle($dataSource_name);
    }

    public function sql_select($sql, $size = 1000)
    {
        return $this->sql_queryall_array($sql, $size);
    }

    public function sql_update($sql)
    {
        $this->sql_execute($sql);
    }

    public function sql_delete($sql)
    {
        $this->sql_execute($sql);
    }

    public function sql_insert($sql)
    {
        $this->sql_execute($sql);
    }

    private function sql_queryall_array($sql, $size = 1000)
    {
        $results = array();

        for ($i = 0, $j = $i + $size; true; $i += $size, $j = $i + 2 * $size) {
            $_sql = "
			SELECT * FROM
				(
					SELECT a.*, rownum r__ FROM
						(
							select * from ({$sql}) order by rownum desc
						) a
					WHERE rownum < {$j}
				)
			WHERE r__ >= {$i}
			";

            $res = $this->dbh->queryall_array($_sql);
            if (count($res) == 0) {
                break;
            }
            $results = array_merge($results, $res);
        }

        return $results;
    }

    private function sql_execute($sql)
    {
        $this->dbh->prepare($sql)->execute();
    }
}