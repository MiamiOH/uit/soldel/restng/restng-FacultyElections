<?php
/*
-----------------------------------------------------------
FILE NAME: Notification.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  The Participant Service is designed to get, update and insert participant 
records to banner FACELECTMGR.ELECTION_MEMBER Table. 

Authorization: authentication and authorization are handled by authorization token. The Service will treat
phone data as public but when people insert new records, the consumer app would need update right of person
project in Authman

INPUT:
PARAMETERS: Election ID, Voter Type, Acceptance Type and Unique ID

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

06/XX/2016              SCHMIDEE
Description:  Initial Program

07/19/2016              PATELAH
Description:  Modified the code to include test cases for nomineesNotResponded and switch case to pull configMgr
values for nomineesNotResponded as notificationType.

08/04/2016              PATELAH
Description:  Modified the code to include the test case for the Semi-Finalist winner to receive notification.

----------------------------------------------------------------------------------------------------------------
 */

namespace MiamiOH\FacultyElections\Services;

class Notification extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
        $this->config = $this->configuration->getConfiguration('FacultyElections',
            'EmailText');
    }

    public function setValidation($validation)
    {
        $this->validation = $validation;
    }

    /*
     * Post Notification Information (POST)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/StudentFinancialAid view Access Required)
     * electionId: Election ID of elections that participant information needs to be
     *		      retrived
     * uniqueId: Unique ID of people that participant information needs to be retrived
     * type: Type of participant information to retrive.
     *
     * Return: Returns an array of all participant information of the elections or users
     *         requested. The type filter maybe used to retrive more specific records.
     */
    public function createNotification()
    {

        //log
        $this->log->debug('  FacultyElection:createNotification() was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $inputData = $request->getData();

        ////////////////////////
        //validation of inputs//
        ////////////////////////

        //check to be sure data was received
        if (is_array($inputData) && count($inputData) == 0) {
            throw new \Exception('No data was received.');
        }

        //electionId is required.

        $electionId = $inputData['electionId'] ?? '';
        if ($electionId === '') {
            throw new \Exception('ElectionId is required.');
        }

        //verify that the electionId is a number
        if (preg_match('/^\d+$/', $electionId) == 0) {
            throw new \Exception($electionId . ' is not a valid electionId value.');
        }

        //if any of the other fields are set, pull in the values
        $toPool = $inputData['toPool'] ?? '';
        $to = $inputData['to'] ?? '';
        $notificationType = $inputData['notificationType'] ?? '';
        $subject = $inputData['subject'] ?? '';
        $body = $inputData['body'] ?? '';

        //verify that one of the to and toPool values is set
        if ($toPool === '' && $to === '') {
            throw new \Exception('Either To or ToPool is required.');
        }

        //verify that either notificationType is set or subject and body are set
        if (($notificationType === '' && $subject === '') || ($notificationType === '' && $body === '') || ($notificationType === '' && $subject === '' && $body === '')) {
            throw new \Exception('NotificationType or subject and body is required.');
        }

        //verify the toPool value if it exists.
        if ($toPool !== '' && $toPool !== 'voters' && $toPool !== 'nominees' && $toPool !== 'all' && $toPool !== 'nomineesNotResponded'
            && $toPool !== 'voternotvotedinsfr' && $toPool !== 'voternotvotedinfr' && $toPool !== 'semi-finalist') {
            throw new \Exception($toPool . ' is not a valid toPool value.');
        }

        //verify the to value if it exists.
        if ($to !== '') {
            try {
                $toArray = explode(',', $to);
                foreach ($toArray as $emailAddress) {
                    $matchResult = preg_match('/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/',
                        $emailAddress);
                    if ($matchResult !== 1) {
                        throw new \Exception($emailAddress . ' is not a valid to value.');
                    }
                }
            } catch (\Exception $e) {
                throw new \Exception($to . ' is not a valid to value.');
            }
        }

        //verify that the notificationType is valid if it is set
        if ($notificationType !== '' &&
            $notificationType !== 'ballotConfirmation' &&
            $notificationType !== 'nominationRound' &&
            $notificationType !== 'semiFinalistsRound' &&
            $notificationType !== 'finalistsRound' &&
            $notificationType !== 'results' &&
            $notificationType !== 'nomineesNotResponded' &&
            $notificationType !== 'voternotvotedinsfr' &&
            $notificationType !== 'voternotvotedinfr' &&
            $notificationType !== 'semi-finalist') {
            throw new \Exception($notificationType . ' is not a valid notificationType value.');
        }

        ////////////////////////////////////////////////
        //Generate all the emails that need to be sent//
        ////////////////////////////////////////////////

        //These are the fields we'll need to actually send the notifications
        $notificationSubject = '';
        $notificationBody = '';
        $includeAdmins = true;

        //If the notificationType is set, retrieve the subject and body
        switch ($notificationType) {
            case 'ballotConfirmation':
                $notificationSubject = $this->config['Ballot_Confirmation_Subject'];
                $notificationBody = $this->config['Ballot_Confirmation_Body'];
                $includeAdmins = false;
                break;
            case 'results':
                $notificationSubject = $this->config['Results_Subject'];
                $notificationBody = $this->config['Results_Body'];
                break;
            case 'nominationRound':
                $notificationSubject = $this->config['Nomination_Round_Subject'];
                $notificationBody = $this->config['Nomination_Round_Body'];
                break;
            case 'semiFinalistsRound':
                $notificationSubject = $this->config['SemiFinalists_Round_Subject'];
                $notificationBody = $this->config['SemiFinalists_Round_Body'];
                break;
            case 'finalistsRound':
                $notificationSubject = $this->config['Finalists_Round_Subject'];
                $notificationBody = $this->config['Finalists_Round_Body'];
                break;
            case 'nomineesNotResponded':
                $notificationSubject = $this->config['Nominee_Not_Responded_Subject'];
                $notificationBody = $this->config['Nominee_Not_Responded_Body'];
                break;
            case 'voternotvotedinsfr':
                $notificationSubject = $this->config['Voter_Not_Responded_Subject'];
                $notificationBody = $this->config['Voter_Not_Responded_Body'];
                break;
            case 'voternotvotedinfr':
                $notificationSubject = $this->config['Voter_Not_Responded_Subject'];
                $notificationBody = $this->config['Voter_Not_Responded_Body'];
                break;
            case 'semi-finalist':
                $notificationSubject = $this->config['Semi_Finalist_Winner_Subject'];
                $notificationBody = $this->config['Semi_Finalist_Winner_Body'];
                break;
            default:
                break;
        }

        //Override the notificationType's subject if one was given by the consumer
        if ($subject !== '') {
            $notificationSubject = $subject;
        }

        //Override the notificationType's body if one was given by the consumer
        if ($body !== '') {
            $notificationBody = $body;
        }

        //Get election data for the given election (will be used to replace stuff)
        try {
            $electionData = $this->callResource('facultyElections.election.get',
                array('options' => array('electionId' => $electionId)))->getPayload()[0];
        } catch (\Exception $e) {
            throw new \Exception($electionId . ' is not a valid electionId value.');
        }

        //Replace any pre-defined values within the subject and body text
        $stringsToReplace = array(
            "[ELECTION_NAME]" => $electionData['electionName'],
            "[NUMBER_FINALISTS]" => $electionData['numOfWinners']
        );
        $notificationBody = strtr($notificationBody, $stringsToReplace);
        $notificationSubject = strtr($notificationSubject, $stringsToReplace);

        //Now that we've got the contents of the email, let's verify we can send it to someone
        if ($toPool !== '') {
            $participantResponse = '';

            switch ($toPool) {
                case 'all':
                    //call the participant service with just an electionId so that all participants are returned.
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get',
                        array('options' => array('electionId' => $electionId))
                    )->getPayload();
                    break;
                case 'voters':
                    //call the participant service with the electionId and type=voter
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'type' => 'voter'
                            )
                        )
                    )->getPayload();
                    break;
                case 'nominees':
                    //call the participant service with the electionId and type=nominee
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'type' => 'nominee'
                            )
                        )
                    )->getPayload();
                    break;

                case 'nomineesNotResponded':
                    // call the participant service with electionId and acceptance=default
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'acceptance' => 'default'
                            )
                        )
                    )->getPayload();
                    break;

                case 'voternotvotedinsfr':
                    // call the participant service with electionId and type=voternotvotedinsfr
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'type' => 'voternotvotedinsfr'
                            )
                        )
                    )->getPayload();
                    break;

                case 'voternotvotedinfr':
                    // call the participant service with electionId and type=voternotvotedinsfr
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'type' => 'voternotvotedinfr'
                            )
                        )
                    )->getPayload();
                    break;

                case 'semi-finalist':
                    // call the participant service with electionId and type=voternotvotedinsfr
                    $participantResponse = $this->callResource(
                        'facultyElections.participant.get', array(
                            'options' => array(
                                'electionId' => $electionId,
                                'type' => 'semi-finalist',
                                'acceptance' => 'default,accepted'
                            )
                        )
                    )->getPayload();
                    break;

                default:
                    break;
            }

            //If there were no participants and no semifinalists identified, throw an exception because there is no one to send notifications to
            if ($participantResponse === '' || count($participantResponse) == 0) {
                throw new \Exception('No election participants were identified with the pool \'' . $toPool . '\'.');
            } else {
                $notificationToList = array();
                foreach ($participantResponse as $p) {
                    $notificationToList[] = $p['uniqueId'] . '@miamioh.edu';
                }

            }

        } else {
            $notificationToList = explode(',', $to);
        }

        if ($includeAdmins) {
            //Include Admins in Emails Sent
            $adminUniqueId = explode(',', $this->config['adminUniqueId']);
            //loop through the configmgr ids and add them to $notificationToList
            foreach ($adminUniqueId as $config) {
                $address = $config . '@miamioh.edu';
                //check to see if that email address is already in the list
                if (!in_array($address, $notificationToList)) {
                    $notificationToList[] = $address;
                }
            }
        }

        //Remove any Duplicates
        $notificationToList = array_unique($notificationToList);

        //make the calls to the notification service.
        foreach ($notificationToList as $toEmailAddress) {
            $dataArray = array(
                'data' => array(
                    'dataType' => 'model',
                    'data' => array(
                        'toAddr' => $toEmailAddress,
                        'fromAddr' => $this->config['From_Email'],
                        'subject' => $notificationSubject,
                        'body' => $notificationBody
                    )
                )
            );
            try {
                $responseFromNotificationService = $this->callResource('notification.v1.email.message.create',
                    $dataArray)->getPayload();
            } catch (\Exception $e) {
                throw new \Exception('Could not send ballot confirmation email (User: ' . $this->getApiUser()->getUsername() . '). Error: ' . $e->getMessage());
            }
            if (isset($responseFromNotificationService['error'])) {
                throw new \Exception('Could not send ballot confirmation email (User: ' . $this->getApiUser()->getUsername() . '). Error: ' . $responseFromNotificationService['data']['message']);
            }
        }
        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        return $response;
    }
}
