<?php
/*
-----------------------------------------------------------
FILE NAME: Election.class.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  This calss is used to handle GET, POST and PUT method of "election" service.
GET method is used to retrieve information of election(s)
POST method is used to create an election
PUT method is used to modify an election

This file is initial version
Create on 05/26/2016
Last modify on 06/10/2016
Written by liaom@miamioh.edu
*/

namespace MiamiOH\FacultyElections\Services;


use Illuminate\Support\Carbon;



class Election extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    // *************************************************************************
    // **********Helper functions that were called by the frame work************
    // **********and create internal datasource and configuration objects*******
    // *************************************************************************
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     *
     * GET:
     *		Get information of election(s),
     *			1, basic info: e.g. electionName, electionId ......
     * 			2, voting rounds info (if exists), e.g. roundTypeName, startDate ......
     *
     * Parameter(s):
     *		@param electionId: list
     *		@param electionStatus: enum('close', 'open')
     * 		@param uniqueId: single, string
     *
     * Return(s):
     *		@return electionId: number,
     *		@return electionName: string,
     *		@return numOfWinners: number,
     *		@return numOfSemiFinalists: number,
     *		@return electionStatus: number,
     *		@return isOpen: number,
     *		@return voterPoolId: number,
     *		@return createUser: string,
     *		@return updateUser: string,
     *		@return dateCreated: string,				//using format: MM/DD/YYYY HH24:MM:SS
     *		@return dateUpdated: string,				//using format: MM/DD/YYYY HH24:MM:SS
     *		@return voterPoolName: string,
     *		@return nomineePoolId: number,
     *		@return nomineePoolName: string,
     *		@return votingRounds:
     *			@return roundTypeName: string
     *			@return id: number,
     *			@return startDate: string,		//using format: MM/DD/YYYY HH24:MM:SS
     *			@return endDate: string,		//using format: MM/DD/YYYY HH24:MM:SS
     *			@return status: string,
     *			@return createUser: string,
     *			@return updateUser: string,
     *			@return dateCreated: string,	//using format: MM/DD/YYYY HH24:MM:SS
     *			@return dateUpdated: string,	//using format: MM/DD/YYYY HH24:MM:SS
     * */
    public function getElection()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        //getting if using paging
        if ($request->isPaged()) {
            $offset = $request->getOffset();
            $limit = $request->getLimit();
        }

        //log: input stream
        $this->log->debug('  FacultyElection.Election.getElection(), inputStream: ' . print_r($options,
                true));

        //check if all parameters are valid
        $ruleArray = array(
            'electionId' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
            'uniqueId' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'electionStatus' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );
        $this->checkParameters($ruleArray, $options, 'GET');

        // base sql query, retrieve information of all elections
        $sql_query = "
          select 
            facelectmgr.election.election_id, 
            election_name, 
            number_of_winners, 
            number_of_semi_finalists, 
            election_status_name,
            division_filter_value,
            department_filter_value,
            campus_filter_value,
            election_is_open, 
            voter_pool_id," . (isset($options['uniqueId']) ? "member_type_name, nomination_acceptance, nominee_status," : "") . "
            nominee_pool_id, 
            DIVISION_FILTER_VALUE,
            DEPARTMENT_FILTER_VALUE,
            CAMPUS_FILTER_VALUE,
            facelectmgr.pool.pool_name,
            facelectmgr.pool.pool_id,
            facelectmgr.election.create_user,
            facelectmgr.election.update_user,
            to_char(facelectmgr.election.date_created, 'DD-MON-YYYY HH24:MI:SS') as date_created,
            to_char(facelectmgr.election.date_updated, 'DD-MON-YYYY HH24:MI:SS') as date_updated 
          from facelectmgr.election 
          join facelectmgr.election_status 
          on facelectmgr.election.election_status_id = facelectmgr.election_status.election_status_id
          join facelectmgr.pool on  facelectmgr.pool.pool_id=facelectmgr.election.voter_pool_id or facelectmgr.pool.pool_id=facelectmgr.election.nominee_pool_id
        " . (isset($options['uniqueId']) ? "
          join facelectmgr.election_member on facelectmgr.election_member.election_id = facelectmgr.election.election_id
          join facelectmgr.member_type on facelectmgr.election_member.member_type_id = facelectmgr.member_type.member_type_id 
        " : "");

        $sql_cond = '';
        if (isset($options['electionId']) || isset($options['electionStatus']) || isset($options['uniqueId'])) {
            $sql_cond = ' where ';
        }

        // paring electionId to WHERE clause
        if (isset($options['electionId'])) {
            if (is_array($options['electionId'])) {
                $sql_cond .= 'election_id in (';
                foreach ($options['electionId'] as $v) {
                    $sql_cond .= "{$v},";
                }
                //remove the extra comma at the end of the pidms
                $sql_cond = substr($sql_cond, 0, -1);
                $sql_cond .= ') ';
            } else {
                $sql_cond .= "election_id= {$options['electionId']} ";
            }
        }

        // paring electionStatus to WHERE clause
        if (isset($options['electionStatus'])) {
            if (isset($options['electionId'])) {
                $sql_cond .= ' and ';
            }
            if ($options['electionStatus'] == 'open') {
                $sql_cond .= "election_is_open=1";
            } elseif ($options['electionStatus'] == 'close') {
                $sql_cond .= "election_is_open=0";
            }
        }

        if (isset($options['uniqueId'])) {
            if (isset($options['electionStatus']) || isset($options['electionId'])) {
                $sql_cond .= ' and ';
            }

            $sql_cond .= "uniqueid='{$options['uniqueId']}'";
        }

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            //log: sql query
            $this->log->debug("  FacultyElection.Election.getElection(), sqlQuery: {$sql_query}{$sql_cond}");
            //print_r($sql_query . $sql_cond);exit();
            $results = $dbh->queryall_array($sql_query . $sql_cond);
            //print_r($results);exit();
        } catch (Exception $e) {
            throw new \Exection('Error: retrieve data from database:\n' . $e->getMessage());
        }

        //log: sql execute successfully
        $this->log->debug('  FacultyElection.Election.getElection(), sql query execute successfully');

        $tmp = array();
        foreach ($results as $row) {
            if ($row['pool_id'] == $row['voter_pool_id']) {
                $row['voter_pool_name'] = $row['pool_name'];
                if ($row['voter_pool_id'] != $row['nominee_pool_id']) {
                    unset($row['voter_pool_id']);
                }
            }
            if ($row['pool_id'] == $row['nominee_pool_id']) {
                $row['nominee_pool_name'] = $row['pool_name'];
                if ($row['voter_pool_id'] != $row['nominee_pool_id']) {
                    unset($row['nominee_pool_id']);
                }
            }
            unset($row['pool_id']);
            unset($row['pool_name']);
            foreach ($row as $key => $v) {
                $tmp[$row['election_id']][$key] = $v;
            }
        }

        $tmp = array_values($tmp);

        foreach ($tmp as $row) {
            $votingRoundsReq = $this->callResource('facultyElections.votingRound.get',
                array('options' => array('electionId' => $row['election_id'])));
            $votingRounds = $votingRoundsReq->getPayload();

            array_push($payload, $this->nameConverter($row, $votingRounds));
        }

        $response->setTotalObjects(count($payload));

        if (isset($offset) && isset($limit)) {
            $payload = array_slice($payload, $offset, $limit);
        }

        //log: return value was set to payload
        $this->log->debug('  FacultyElection.Election.getElection(), outputStream: ' . (print_r($payload,
                true)));

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * POST:
     *     create an election
     *
     * Parameters(s)
     *     @param electionName: single - required
     *     @param nomineePoolId: single
     *     @param numOfWinners: single
     *     @param numOfSemiFinalists: single
     *     @param voterPoolId: single
     *
     * Return(s)
     *     @return electionId: number
     * */
    public function createElection()
    {
        //log: function start
        $this->log->debug('  FacultyElection.Election.createElection() was called.');

        //init: get body array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        //log: input stream
        $this->log->debug('  FacultyElection.Election.createElection(), inputStream: ' . (print_r($body,
                true)));

        $sql_query = "
            insert into facelectmgr.election(
                election_id,
                election_name,
                nominee_pool_id,
                election_status_id,
                number_of_winners,
                number_of_semi_finalists,
                division_filter_value,
                department_filter_value,
                campus_filter_value,
                create_user,
                update_user,
                date_created,
                date_updated,
                voter_pool_id
            ) values(FACELECTMGR.FEM_SEQUENCE.NEXTVAL,?,?,(select election_status_id from facelectmgr.election_status where facelectmgr.election_status.election_status_name='New'),?,?,?,?,?,?,?,sysdate,sysdate,?)
        ";

        $ruleArr = array(
            'electionName' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'nomineePoolId' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'voterPoolId' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'numOfWinners' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'numOfSemiFinalists' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'divisionFilterValue' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'departmentFilterValue' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'createUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );

        $this->checkParameters($ruleArr, $body, 'POST');


        //make sure that numOfSemiFinalists > numOfWinners
        if ($body['numOfWinners'] >= $body['numOfSemiFinalists']) {
            throw new \Exception('Error: number of winners must be less than number of semi-finalists');
        }

        //log: pass checking parameters
        $this->log->debug('  FacultyElection.Election.createElection(), parameters checking pass');

        //set param to default value if they are not set by user
        if (!isset($body['nomineePoolId'])) {
            $body['nomineePoolId'] = null;
        }
        if (!isset($body['voterPoolId'])) {
            $body['voterPoolId'] = null;
        }
        if (!isset($body['divisionFilterValue'])) {
            $body['divisionFilterValue'] = null;
        }
        if (!isset($body['departmentFilterValue'])) {
            $body['departmentFilterValue'] = null;
        }
        if (!isset($body['numOfWinners'])) {
            $body['numOfWinners'] = 0;
        }
        if (!isset($body['numOfSemiFinalists'])) {
            $body['numOfSemiFinalists'] = 0;
        }

        if (!isset($body['createUser'])) {
            //get user's uniqueId from token
            $createUser = $this->getApiUser()->getUsername();
        } else {
            $createUser = $body['createUser'];
        }

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $sql_body = array(
                $body['electionName'],
                $body['nomineePoolId'],
                $body['numOfWinners'],
                $body['numOfSemiFinalists'],
                $body['divisionFilterValue'],
                $body['departmentFilterValue'],
                null,
                $createUser,
                $createUser,
                $body['voterPoolId']
            );

            //log: sql query
            $this->log->debug("  FacultyElection.Election.createElection(), sqlQuery: {$sql_query} @@withData@@ " . (print_r($sql_body,
                    true)));

            $dbh->perform($sql_query, $sql_body);
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if (strpos($err, 'parent key not found')) {
                $lPos = strpos($err, 'FACELECTMGR.') + 12;
                $rPos = strpos($err, '_FK) violated - parent key not found');
                $key = substr($err, $lPos, $rPos - $lPos);
                throw new \Exception("Error: {$key} not found");
            } elseif (strpos($err, 'unique constraint')) {
                throw new \Exception("Error: electionName({$body['electionName']}) already exist");
            } else {
                throw new \Exception($err);
            }
        }

        //log: sql execute successfully
        $this->log->debug('  FacultyElection.Election.createElection(), sql query execute successfully');

        //get electionId of last created election
        $sql_query = "select election_id from facelectmgr.election where election_name = '{$body['electionName']}'";
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $results = $dbh->queryall_array($sql_query);
        } catch (Exception $e) {
            throw new \Exection('Error: fail to get electionId after insertion:\n' . $e->getMessage());
        }
        $payload['electionId'] = $results[0]['election_id'];


        //After the creation of the election record has taken place successfully, generate participants!
        $messages = $this->generateParticipants($payload['electionId'],
            $body['nomineePoolId'],
            $body['voterPoolId'],
            $body['numOfWinners'],
            $body['divisionFilterValue'],
            $body['departmentFilterValue']
        );

        if (count($messages) > 0) {
            $payload['messages'] = $messages;
        }

        //log: return value was set to payload
        $this->log->debug('  FacultyElection.Election.createElection(), outputStream: ' . print_r($payload,
                true));

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * PUT:
     *     modify an election
     *
     * Parameters(s)
     *     @param electionId: single - required
     *     @param nomineePoolId: single
     *     @param numOfWinners: single
     *     @param numOfSemiFinalists: single
     *     @param voterPoolId: single
     *     @param electionName: single
     *     @param electionStatus: single
     *
     * Return(s)
     *     none if succeed
     *     error if fail
     * */
    public function modifyElection()
    {
        //log: function start
        $this->log->debug('  FacultyElection.Election.modifyElection() was called.');

        //init: get body array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        //log: input stream
        $this->log->debug('  FacultyElection.Election.modifyElection(), inputStream: ' . (print_r($body,
                true)));

        $ruleArr = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'electionName' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'nomineePoolId' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'voterPoolId' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'electionStatus' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'numOfWinners' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'numOfSemiFinalists' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'updateUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );

        $this->checkParameters($ruleArr, $body, 'PUT');

        //retrieve data from GET method using electionId user provided
        $dataGET = $this->callResource('facultyElections.election.get',
            array('options' => array('electionId' => $body['electionId'])));
        $rtn = $dataGET->getPayload();

        if (count($rtn) == 0) {
            throw new \Exception('Error: electionId not found');
        }

        if (isset($body['numOfWinners']) && isset($body['numOfSemiFinalists']) && $body['numOfWinners'] > $body['numOfSemiFinalists']) {
            throw new \Exception('Error: number of winners must be less than or equal to number of semi-finalists');
        }

        if (!isset($body['updateUser'])) {
            //get user's uniqueId from token
            $updateUser = $this->getApiUser()->getUsername();
        } else {
            $updateUser = $body['updateUser'];
            unset($body['updateUser']);
        }

        //log: pass checking parameters
        $this->log->debug('  FacultyElection.Election.modifyElection(), parameters checking pass');

        $electionId = $body['electionId'];
        unset($body['electionId']);

        //base query
        $sql_query = "
            update facelectmgr.election set 
        ";
        //set user params: (optional) - electionName, electionStatus,
        //numOfWinners, numOfSemiFinalists, voterPoolId, nomineePoolId
        foreach ($body as $paramName => $paramValue) {
            if ($paramName == 'electionStatus') {
                continue;
            }
            if ($paramName == 'electionStatus' && !is_numeric($paramValue)) {
                $sql_query .= $this->paramName2db($paramName) . ' = case when exists (select facelectmgr.election_status.election_status_id from facelectmgr.election_status where facelectmgr.election_status.election_status_name = ?) then (select facelectmgr.election_status.election_status_id from facelectmgr.election_status where facelectmgr.election_status.election_status_name = ?) else -1 end,';
            } elseif (($paramName == 'nomineePoolId' || $paramName == 'voterPoolId') && !is_numeric($paramValue)) {
                $sql_query .= $this->paramName2db($paramName) . ' = case when exists (select facelectmgr.pool.pool_id from facelectmgr.pool where facelectmgr.pool.pool_name = ?) then (select facelectmgr.pool.pool_id from facelectmgr.pool where facelectmgr.pool.pool_name = ?) else -1 end,';
            } else {
                $sql_query .= $this->paramName2db($paramName) . ' = ?,';
            }
        }

        //===================================================================================
        //============================= update election status ==============================
        //===================================================================================


        if (!isset($body['electionStatus'])) {
            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                $votingRounds = $dbh->queryall_array("select facelectmgr.round_type.round_type_name, 
															to_char(start_date, 'DD-MON-YYYY HH24:MI:SS') as start_date, 
															to_char(end_date, 'DD-MON-YYYY HH24:MI:SS') as end_date  
													from facelectmgr.election_round 
													join facelectmgr.round_type on facelectmgr.round_type.round_type_id = facelectmgr.election_round.round_type_id
													where election_id = {$electionId}");
            } catch (\Exception $e) {

                throw new \Exception("Error: failed to get voting round information");
            }

            foreach ($votingRounds as $index => $votingRound) {
                $votingRound['start_date'] = $votingRound['start_date'] === null ? '' : strtotime($votingRound['start_date']);
                $votingRound['end_date'] = $votingRound['end_date'] === null ? '' : strtotime($votingRound['end_date']);
                $votingRounds[$votingRound['round_type_name']] = $votingRound;

                unset($votingRounds[$index]);
            }

            $body['electionStatus'] = $this->calcElectionStatusFromVotingRounds($votingRounds);
        }


        //set system params: update_user and date_updated
        $sql_query .= "update_user = '{$updateUser}', date_updated = sysdate, ";

        $sql_query .= "election_status_id = (select election_status_id from facelectmgr.election_status where election_status_name = '{$body['electionStatus']}') ";

        $sql_query .= "where election_id={$electionId}";

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $sql_arr = array();
            unset($body['electionStatus']);
            foreach ($body as $paramName => $paramValue) {
                if (in_array($paramName, [
                        'nomineePoolId',
                        'voterPoolId'
                    ]) && !is_numeric($paramValue)) {
                    array_push($sql_arr, $paramValue);
                }
                array_push($sql_arr, $paramValue);
            }

            //log: sql query
            $this->log->debug("  FacultyElection.Election.modifyElection(), sqlQuery: {$sql_query} @@withData@@ " . print_r($sql_arr,
                    true));

            $dbh->perform($sql_query, $sql_arr);
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if (strpos($err, 'parent key not found')) {
                $lPos = strpos($err, 'FACELECTMGR.') + 12;
                $rPos = strpos($err, '_FK) violated - parent key not found');
                $key = substr($err, $lPos, $rPos - $lPos);
                throw new \Exception("parent key not found, in '{$key}'");
            } else {
                throw new \Exception($err);
            }
        }
        //log: sql execute successfully
        $this->log->debug('  FacultyElection.Election.createElection(), sql query execute successfully');

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function calcElectionStatusFromVotingRounds($votingRounds)
    {
        if (isset($votingRounds['Self-Nomination Round'])) {

            if (
                $votingRounds['Self-Nomination Round']['start_date'] === ''
                || $votingRounds['Self-Nomination Round']['start_date'] > time()) {
                return 'New';
            }
            if ($votingRounds['Self-Nomination Round']['end_date'] === ''
                || $votingRounds['Self-Nomination Round']['end_date'] > time()) {
                return 'Self Nomination';
            }
        } else {
            return 'New';
        }
        if (isset($votingRounds['Semi-Finalists Round'])) {
            if (
                $votingRounds['Semi-Finalists Round']['start_date'] === ''
                || $votingRounds['Semi-Finalists Round']['start_date'] > time()) {
                return 'Self Nomination Verification';
            }
            if ($votingRounds['Semi-Finalists Round']['end_date'] === ''
                || $votingRounds['Semi-Finalists Round']['end_date'] > time()) {
                return 'Semi-Finalist Selection';
            }
        } else {
            return 'Self Nomination Verification';
        }
        if (isset($votingRounds['Finalists Round'])) {
            if ($votingRounds['Finalists Round']['start_date'] === ''
                || $votingRounds['Finalists Round']['start_date'] > time()) {
                return 'Semi-Finalist Verification';
            }
            if ($votingRounds['Finalists Round']['end_date'] === ''
                || $votingRounds['Finalists Round']['end_date'] > time()) {
                return 'Finalist Selection';
            }
            return 'Finalist Verification';
        } else {
            return 'Semi-Finalist Verification';
        }
    }


    /**
     * The generateParticipants private method is intended to be used when
     * participants should be generated for an election.  This includes nominee and
     * voter pools based on CriteriaGroup Ids.
     *
     * @param $electionId : id of an election
     * @param $nomineePoolId : id of the nominee pool nominees should be generated
     *     from
     * @param $voterPoolId : id of the voter pool voters should be nominated from
     * @param $numWinners : num of winners
     * @param $divisionFilter
     * @param $deptFilter
     *
     * Return(s)
     *      none if succeed
     *      message if warning
     *      error if fail
     *
     * @return array messages if any were collected during participant generation
     */
    private function generateParticipants(
        $electionId,
        $nomineePoolId,
        $voterPoolId,
        $numWinners,
        $divisionFilter,
        $deptFilter
    )
    {
        $messages = array();

        //get nominees
        $nomineeList = $this->getParticipantList($nomineePoolId, $divisionFilter, $deptFilter);

        //set up the nominee $participantNomineeData
        $participantNomineeData = array();
        $participantNomineeData['uniqueId'] = array();
        $participantNomineeData['electionId'] = $electionId;
        $participantNomineeData['type'] = 'nominee';
        $participantNomineeData['acceptance'] = 'default';
        $participantNomineeData['nomineeStatus'] = 'Nominee';
        //push in the employee uniqueIds

        $numNominees = 0;
        foreach ($nomineeList as $employee) {
            if (isset($employee['startDate']) && (Carbon::today()->greaterThanOrEqualTo(Carbon::parse($employee['startDate'])))) {
                $participantNomineeData['uniqueId'][] = strtolower($employee['uniqueId']);
                $numNominees++;
            }
        }

        $participantNomineeData['uniqueId'] = array_unique($participantNomineeData['uniqueId']); //remove any duplicates

        if ($numNominees > 0) {
            //call the participant service for nominees
            $this->callResource('facultyElections.participant.post',
                array('data' => $participantNomineeData));
        }
        if ($numNominees == $numWinners) {
            $messages[] = 'Number of nominees generated is equal to number of winners.';
        } else {
            if ($numNominees < $numWinners) {
                $messages[] = 'Number of nominees generated is less than the number of winners.';
            } else {
                if ($numNominees <= 0) {
                    $messages[] = 'No nominees were generated.';
                }
            }
        }

        //get voters
        $voterList = $this->getParticipantList($voterPoolId, $divisionFilter, $deptFilter);

        //set up the voter $participantVoterData
        $participantVoterData = array();
        $participantVoterData['uniqueId'] = array();
        $participantVoterData['electionId'] = $electionId;
        $participantVoterData['type'] = 'voter';
        //push in the employee uniqueIds
        $numVoters = 0;
        foreach ($voterList as $employee) {
            if (isset($employee['startDate']) && (Carbon::today()->greaterThanOrEqualTo(Carbon::parse($employee['startDate'])))) {
                $participantVoterData['uniqueId'][] = strtolower($employee['uniqueId']);
                $numVoters++;
            }
        }
        $participantVoterData['uniqueId'] = array_unique($participantVoterData['uniqueId']); //remove any duplicates

        if ($numVoters <= 0) {
            $messages[] = 'No voters were generated.';
        } else {
            //call the participant service for voters
            $this->callResource('facultyElections.participant.post',
                array('data' => $participantVoterData));
        }
        return $messages;

    }

    private function getParticipantList($poolId, $divisionFilter, $deptFilter)
    {
        $criteria = $this->callResource('facultyElections.criteriaGroup.get',
            array('options' => array('poolId' => $poolId)))->getPayload()[0]['criteria'];

        //loop through the nominee criteria that are set and apply them to the employee call
        $employeeOptions = array();
        $employeeOptions['limit'] = '500';
        $employeeOptions['offset'] = '1';
        if (isset($divisionFilter)) {
            $employeeOptions['standardizedDivisionCode'] = $divisionFilter;
        }
        if (isset($deptFilter)) {
            $employeeOptions['standardizedDepartmentCode'] = $deptFilter;
        }

        $gradLevelOptions = [];
        $tenureOptions = [];
        $rankCodeOptions = [];
        $staffClassficationCodeOptions = [];
        foreach ($criteria as $criterion) {
            if ($criterion['criteriaName'] == 'Grad Level') {
                $gradLevelOptions = [];
                foreach ($criterion['options'] as $option) {
                    $gradLevelOptions[] = $option['criteriaFilterValue'];
                }
            } elseif ($criterion['criteriaName'] == 'Group') {
                foreach ($criterion['options'] as $option) {
                    if ($option['criteriaOptionName'] == 'Tenured' || $option['criteriaOptionName'] == 'Tenure Track') {
                        $tenureOptions[] = $option['criteriaFilterValue'];
                    } elseif ($option['criteriaOptionName'] == 'Librarians' || $option['criteriaOptionName'] == 'Lecturers and Clinical Licensed') {
                        $rankCodeOptions[] = $option['criteriaFilterValue'];
                    } elseif ($option['criteriaOptionName'] == 'Staff'){
                        $staffClassficationCodeOptions[] = $option['criteriaFilterValue'];
                    } else {
                        throw new \Exception('Invalid group name.');
                    }
                }
            }
            //ignore the other criteria (old and invalid ones)
        }

        if(count($gradLevelOptions) > 0 ) {
            $employeeOptions['gradLevel'] = implode(',', $gradLevelOptions);
        }

        //make separate calls for tenure code and rank code, and merge the results
        $employeeListByTenureCode = [];
        if (count($tenureOptions) > 0) {
            $employeeOptions['tenureCode'] = implode(',', $tenureOptions);
            $employeeListByTenureCode = $this->getEmployeeList($employeeOptions);
        }
        $employeeListByRankCode = [];
        unset($employeeOptions['tenureCode']);
        if (count($rankCodeOptions) > 0) {
            $employeeOptions['rankCode'] = implode(',', $rankCodeOptions);
            $employeeListByRankCode = $this->getEmployeeList($employeeOptions);
        }
        $employeeListByClassificationCode = [];
        unset($employeeOptions['rankCode'], $employeeOptions['gradLevel']);
        if(count($staffClassficationCodeOptions) > 0 ) {
            $employeeOptions['classificationCode'] = implode(',', $staffClassficationCodeOptions);
            $employeeListByClassificationCode = $this->getEmployeeList($employeeOptions);
        }
        return array_merge($employeeListByTenureCode, $employeeListByRankCode, $employeeListByClassificationCode);

    }

    private function getDualApptmntDeptValidationArray()
    {
        $daDeptValidationStr = $this->configuration->getConfiguration('FacultyElections',
            'SystemConfigurations')['daDeptCodes'];
        $daDeptValidationMigrate = explode(',', $daDeptValidationStr);
        $daDeptValidationArray = array();
        foreach ($daDeptValidationMigrate as $m) {
            $t = explode('-', $m);
            $daDeptValidationArray[$t[0]] = $t[1];
        }

        return $daDeptValidationArray;
    }

    private function getDualAppmntDivValidationArray()
    {
        //Get the validation array for Divisions -> Dual Appointment codes from ConfigMgr:
        $daDivValidationStr = $this->configuration->getConfiguration('FacultyElections',
            'SystemConfigurations')['daDivCodes'];
        $daDivValidationMigrate = explode('|', $daDivValidationStr);
        $daDivValidationArray = array();
        foreach ($daDivValidationMigrate as $m) {
            $t = explode('-', $m);
            $daDivValidationArray[$t[0]] = explode(',', $t[1]);
        }

        return $daDivValidationArray;
    }

    /*
     * paramNameT->DatabaseName:
     *     used to translate paramName to attribute name in database
     *
     * parameter(s):
     *     @param paramName, name of parameter, string
     *
     * return:
     *     a string, represent name of database attribute
     * */
    private function paramName2db($paramName)
    {
        switch ($paramName) {
            case 'electionId':
                return 'election_id';
            case 'electionName':
                return 'election_name';
            case 'electionStatus':
                return 'election_status_id';
            case 'nomineePoolId':
                return 'nominee_pool_id';
            case 'voterPoolId':
                return 'voter_pool_id';
            case 'numOfWinners':
                return 'number_of_winners';
            case 'numOfSemiFinalists':
                return 'number_of_semi_finalists';
            case 'divisionFilterValue':
                return 'division_filter_value';
            case 'departmentFilterValue':
                return 'department_filter_value';
            default:
                throw new \Exception("Error: fail to covert param name, '{$paramName}' doesn't be defined");
        }
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param ruleArr, parameters rule(specify isRequired
     *         and isArray for all parameters), should be array
     *             e.g. $ruleArr = array(
     *                      'electionId' => array(
     *                           'isArray' => true,
     *                           'isRequired' => false,
     *                       ),
     *                       ......
     *                  );
     *     @param paramArr, parameters array send by user, should be an array
     *     @param method, method of request function, either 'GET', 'POST',
     *         'PUT' or 'DELETE'
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameters($ruleArr, $paramArr, $method)
    {
        foreach ($ruleArr as $name => $obj) {
            if ($obj['isRequired'] && !isset($paramArr[$name])) {
                throw new \Exception("Error: missing required parameters ({$name})");
            }

            if (isset($paramArr[$name])) {
                if (is_array($paramArr[$name]) && !$obj['isArray']) {
                    throw new \Exception("Error: invalid {$name}, it should be single");
                }
                $this->checkParameterValue($name, $paramArr[$name], $method);

                unset($paramArr[$name]);
            }
        }

        if (count($paramArr) != 0) {
            throw new \Exception('Error: too many parameters');
        }
    }

    /*
     * Parameters value checking controller: (only call by checkParameters())
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param paramName, name of parameter, should be string
     *     @param paramValue, value of parameter, either be single or array
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameterValue($paramName, $paramValue, $method)
    {
        if (!is_array($paramValue)) {
            $paramValue = array($paramValue);
        }
        if ($paramName == 'electionId'
            || $paramName == 'numOfWinners'
            || $paramName == 'numOfSemiFinalists'
            || $paramName == 'dateCreated'
            || $paramName == 'dateUpdated'
        ) {
            foreach ($paramValue as $v) {
                $this->checkParameterNumber($paramName, $v);
            }
        } elseif ($paramName == 'nomineePoolId'
            || $paramName == 'voterPoolId') {
            foreach ($paramValue as $v) {
                $this->checkParameterBothNumberAndString($paramName, $v);
            }
        } elseif ($paramName == 'electionName'
            || $paramName == 'uniqueId'
            || $paramName == 'updateUser'
        ) {
            foreach ($paramValue as $v) {
                $this->checkParameterName($v, $paramName);
            }
        } elseif ($paramName == 'electionStatus') {
            foreach ($paramValue as $v) {
                $this->checkParameterStatus($v, $method);
            }
        } else {
            if ($paramName == 'departmentFilterValue'
                || $paramName == 'divisionFilterValue') {
                foreach ($paramValue as $v) {
                    if (preg_match('/[^A-Za-z0-9 \,"]/', $v)) {
                        throw new \Exception("Error: invalid {$paramName}, at '{$v}''");
                    }
                }

            }
        }
    }

    //***********************************************************
    //*******************Check Functions Start*******************
    //***********************************************************
    private function checkParameterNumber($key, $value)
    {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!is_numeric($value)) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }

    private function checkParameterStatus($electionStatus, $method)
    {
        if (is_array($electionStatus)) {
            throw new \Exception('Error: invalid electionStatus, is array');
        }

        if (trim($electionStatus) == '') {
            throw new \Exception('Error: empty electionStatus');
        }

        if (strtolower($method) == 'get') {
            if ($electionStatus != 'open' && $electionStatus != 'close') {
                throw new \Exception("Error: invalid electionStatus, at '{$electionStatus}''");
            }
        } elseif (strtolower($method) == 'put') {
            $this->checkParameterBothNumberAndString('electionStatus',
                $electionStatus);
        }
    }

    private function checkParameterBothNumberAndString($paramName, $paramValue)
    {
        if (trim($paramValue) == '') {
            throw new \Exception("Error: empty {$paramName}");
        }

        if (!is_numeric($paramValue) && preg_match('/[^A-Za-z0-9 "]/',
                $paramValue)) {
            throw new \Exception("Error: invalid {$paramName}, at '{$paramValue}''");
        }
    }

    //should not be empty
    //should not exceed 30 bytes
    //should only contains: letter, digits, parentheses,
    // space, colon, dash, underscore
    private function checkParameterName($electionName, $parameterName)
    {
        //echo $parameterName."\n";
        if (trim($electionName) == '' && $parameterName != 'divisionFilterValue' && $parameterName != 'departmentFilterValue') {
            throw new \Exception("Error: empty $parameterName");
        }
        if (strlen($electionName) >= 30) {
            throw new \Exception("Error: $parameterName too long, at '{$electionName}'");
        }
        if (preg_match('/[^A-Za-z0-9() :\-_"]/', $electionName)) {
            throw new \Exception("Error: invalid $parameterName, at '{$electionName}'");
        }
    }

    //valid input should be greater than current time()
    private function checkParameterDate($date)
    {
        if (trim($date) == '') {
            throw new \Exception('Error: empty date');
        }
        if (!is_numeric($date) || $date < time()) {
            throw new \Exception("Error: invalid date, at '{$date}'");
        }
    }
    //***********************************************************
    //********************Check Functions End********************
    //***********************************************************

    /*
     * Database Name -> WS Name:
     *     translate database name to name used in web services
     *
     * parameter(s):
     *     @param row, a row of data return from database, array
     *         format: array(
     *                     'election_id' => '12',
     *                     'voter_pool_id' => '3',
     *                     ......
     *                 )
     *
     * return(s):
     *     an array, same as @param row, but key names have converted to WS name
     *         format: array(
     *                     'electionId' => '12',
     *                     'voterPoolId' => '3',
     *                     ......
     *                 )
     * */
    private function nameConverter($row, $votingRounds)
    {
        $rtn = array();
        foreach ($row as $k => $v) {
            $key = $this->nameConverterHelper($k);
            if (in_array($key, ['dateCreated', 'dateUpdated'])) {
                $v = $this->timeFormatConverter($v);
            }
            if ($key == 'participantType') {
                $v = strtolower($v);
            }
            if ($key == 'isOpen') {
                $v = ($v == "1" ? true : false);
            }
            $rtn[$key] = $v;
        }

        foreach ($votingRounds as $votingRound) {
            $rtn['votingRounds'][$votingRound['type']]['id'] = $votingRound['typeId'];

            if ($votingRound['startDate'] != '') {
                $rtn['votingRounds'][$votingRound['type']]['startDate'] = $this->timeFormatConverter($votingRound['startDate']);
            } else {
                $rtn['votingRounds'][$votingRound['type']]['startDate'] = 'none';
            }
            if ($votingRound['endDate'] != '') {
                $rtn['votingRounds'][$votingRound['type']]['endDate'] = $this->timeFormatConverter($votingRound['endDate']);
            } else {
                $rtn['votingRounds'][$votingRound['type']]['endDate'] = 'none';
            }

            if ($votingRound['startDate'] == '' || $votingRound['startDate'] > time()) {
                $rtn['votingRounds'][$votingRound['type']]['status'] = 'Not Started';
            } elseif ($votingRound['endDate'] != '' && $votingRound['endDate'] < time()) {
                $rtn['votingRounds'][$votingRound['type']]['status'] = 'Closed';
            } else {
                $rtn['votingRounds'][$votingRound['type']]['status'] = 'In Progress';
            }
            $rtn['votingRounds'][$votingRound['type']]['createUser'] = $votingRound['createUser'];
            $rtn['votingRounds'][$votingRound['type']]['updateUser'] = $votingRound['updateUser'];
            $rtn['votingRounds'][$votingRound['type']]['dateCreated'] = $this->timeFormatConverter($votingRound['dateCreated']);
            $rtn['votingRounds'][$votingRound['type']]['dateUpdated'] = $this->timeFormatConverter($votingRound['dateUpdated']);
        }

        return $rtn;
    }

    private function nameConverterHelper($name)
    {
        switch ($name) {
            case 'election_id':
                return 'electionId';
            case 'voter_pool_name':
                return 'voterPoolName';
            case 'nominee_pool_name':
                return 'nomineePoolName';
            case 'election_is_open':
                return 'isOpen';
            case 'election_status_name':
                return 'electionStatus';
            case 'voter_pool_id':
                return 'voterPoolId';
            case 'nominee_pool_id':
                return 'nomineePoolId';
            case 'create_user':
                return 'createUser';
            case 'update_user':
                return 'updateUser';
            case 'date_created':
                return 'dateCreated';
            case 'date_updated':
                return 'dateUpdated';
            case 'election_name':
                return 'electionName';
            case 'number_of_winners':
                return 'numOfWinners';
            case 'number_of_semi_finalists':
                return 'numOfSemiFinalists';
            case 'member_type_name':
                return 'participantType';
            case 'nomination_acceptance':
                return 'nominationAcceptance';
            case 'nominee_status':
                return 'nomineeStatus';
            case 'division_filter_value':
                return 'divisionFilterValue';
            case 'department_filter_value':
                return 'departmentFilterValue';
            case 'campus_filter_value':
                return 'campusFilterValue';
            default:
                throw new \Exception("Error: name not found, when translating db_name to ws_name, at '{$name}'");
        }
    }

    private function timeFormatConverter($datetime)
    {
        return date_format(date_create($datetime), "m/d/Y H:i:s");
    }

    private function getEmployeeList($optionsList)
    {
        //make an initial call to the employee service
        $employeeResponse = $this->callResource('employee.v1',
            array('options' => $optionsList));
        $employeePayload = $employeeResponse->getPayload();

        $limit = $employeeResponse->getRequest()->getLimit(); //how many come back at a time. will be 500.
        $offset = $employeeResponse->getRequest()->getOffset(); //which number do we start at.  will be 1 to start with
        $total = $employeeResponse->getTotalObjects(); //how many there are total.  ex: 789
        $numberRetrieved = $limit;

        while ($numberRetrieved < $total) {
            $offset += $limit;
            $optionsList['offset'] = $offset;
            $resp = $this->callResource('employee.v1',
                array('options' => $optionsList));
            $pay = $resp->getPayload();
            $employeePayload = array_merge($employeePayload, $pay);
            $numberRetrieved += $limit;
        }

        //get additional employees that has dual appointments in the given department/division

        //get the mapping tables from configMgr
        $daDeptValidationArray = $this->getDualApptmntDeptValidationArray();
        $daDivValidationArray = $this->getDualAppmntDivValidationArray();

        //Check to see if there was a department used.  If so, we have to check for any dual appointments
        //for that department.
        if (isset($optionsList['standardizedDepartmentCode'])) {
            $deptArray = explode(',', $optionsList['standardizedDepartmentCode']);
            $daDeptArray = array();
            foreach ($deptArray as $d) {
                $daCode = '';
                foreach ($daDeptValidationArray as $k => $v) {
                    if ($v == $d) {
                        $daCode = $k;
                    }
                }
                if ($daCode != '') {
                    $daDeptArray[] = $daCode;
                }
            }
            $daDeptList = implode(',', $daDeptArray);
            if ($daDeptList != '') {
                //keep the filters to select people with dual appointments
                unset($optionsList['standardizedDepartmentCode']);
                $options = array_merge($optionsList, ['dualAppointmentCode' => $daDeptList]);

                $daDeptEmployeesResp = $this->callResource('employee.v1',
                    array('options' => $options));
                $p = $daDeptEmployeesResp->getPayload();
                $employeePayload = array_merge($employeePayload, $p);
            }
        }

        //Check to see if there was a division used.  If so, we have to check for any dual appointments
        //for that division.
        if (isset($optionsList['standardizedDivisionCode'])) {
            $divArray = explode(',', $optionsList['standardizedDivisionCode']);
            $daCodes = array();
            foreach ($divArray as $i => $d) {
                foreach ($daDivValidationArray as $k => $v) {
                    if ($k == $d) { //we've found a match in the validation data for the division given.
                        //now, take the list of dual appointment codes for that division ($v) and add them into an array
                        foreach ($v as $index => $code) {
                            $daCodes[] = $code;
                        }
                    }
                }
            }
            $daCodesList = implode(',', $daCodes);
            if ($daCodesList != '') {
                //keep the filters to select people with dual appointments
                unset($optionsList['standardizedDivisionCode']);
                $options = array_merge($optionsList, ['dualAppointmentCode' => $daCodesList]);
                $daDivEmployeesResp = $this->callResource('employee.v1',
                    array('options' => $options));
                $p = $daDivEmployeesResp->getPayload();
                $employeePayload = array_merge($employeePayload, $p);
            }
        }
        return $employeePayload;
    }
}