<?php
/*
-----------------------------------------------------------
FILE NAME: VotingRound.class.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  This class is used to handle GET, POST, PUT method of "votingRound" service
GET method is used to retrieve information of a voting round
POST method is used to create a voting round
PUT method is used to modify a voting round

This file is initial version
Create on 05/18/2016
Last modify on 06/10/2016
Written by liaom@miamioh.edu
*/

namespace MiamiOH\FacultyElections\Services;

class VotingRound extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    // ************************************************************************
    // **********Helper functions that were called by the frame work***********
    // **********and create internal datasource and configuration objects******
    // ************************************************************************
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     *
     * GET: get information of voting round(s)
     *
     * Parameter(s):
     *     @param electionId: list - required
     *     @param type: number
     *
     * Return(s):
     *     @return votingRoundName: string
     *     @return startDate: date
     *     @return endDate: date
     *     @return createUser: string
     *     @return dateCreated: date
     *     @return dateUpdated: date
     * */
    public function getVotingRound()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        $sql_query = "
            select
                election_id,
                FACELECTMGR.round_type.round_type_name,
                FACELECTMGR.round_type.round_type_id,
                to_char(start_date, 'DD-MON-YYYY HH24:MI:SS') as start_date,
                to_char(end_date, 'DD-MON-YYYY HH24:MI:SS') as end_date,
                facelectmgr.election_round.create_user,
                facelectmgr.election_round.update_user,
                to_char(facelectmgr.election_round.date_created, 'DD-MON-YYYY HH24:MI:SS') as date_created,
                to_char(facelectmgr.election_round.date_updated, 'DD-MON-YYYY HH24:MI:SS') as date_updated 
            from FACELECTMGR.election_round
            join FACELECTMGR.round_type on FACELECTMGR.election_round.round_type_id = FACELECTMGR.round_type.round_type_id
        ";

        $ruleArr = array(
            'electionId' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
            'type' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
        );

        $this->checkParameters($ruleArr, $options, 'GET');

        //log: pass checking parameters
        $this->log->debug('  FacultyElection.VotingRound.getVotingRound(), parameters checking pass');

        $sql_cond = '';
        if (count($options) > 0) {
            $sql_cond = 'where ';
        }

        // paring electionId to WHERE clause
        //allow a list of electionId, e.g. election_id in (1, 2, 3, ...)
        if (isset($options['electionId'])) {
            if (is_array($options['electionId'])) {
                $sql_cond .= 'election_id in (';
                foreach ($options['electionId'] as $v) {
                    $sql_cond .= "{$v},";
                }
                //remove the extra comma at the end of the electionIds
                $sql_cond = substr($sql_cond, 0, -1);
                $sql_cond .= ') ';
            } else {
                $sql_cond .= "election_id={$options['electionId']}";
            }
        }

        //parsing type to WHERE clause
        //allow a list of type, e.g. round_type_id in (1, 2, 3, ...)
        if (isset($options['type'])) {
            if (isset($options['electionId'])) {
                $sql_cond .= ' and ';
            }
            if (is_array($options['type'])) {
                $sql_cond .= 'FACELECTMGR.election_round.round_type_id in (';
                foreach ($options['type'] as $v) {
                    $sql_cond .= "{$v},";
                }
                //remove the extra comma at the end of the electionIds
                $sql_cond = substr($sql_cond, 0, -1);
                $sql_cond .= ') ';
            } else {
                $sql_cond .= "FACELECTMGR.election_round.round_type_id={$options['type']}";
            }
        }

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            //log: sql query
            $this->log->debug("  FacultyElection.VotingRound.getVotingRound(), sqlQuery: {$sql_query}{$sql_cond}");

            $results = $dbh->queryall_array($sql_query . $sql_cond);
        } catch (Exception $e) {
            throw new \Exception('Error: retrieve data from database:\n' . $e->getMessage());
        }

        //log: sql execute successfully
        $this->log->debug('  FacultyElection.VotingRound.getVotingRound(), sql query execute successfully');

        //set response to $payload
        foreach ($results as $row) {
            array_push($payload, $this->nameConverter($row));
        }

        //log: return value was set to payload
        $this->log->debug('  FacultyElection.VotingRound.getVotingRound(), outputStream: ' . (print_r($payload,
                true)));

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * POST: create a voting round(s)
     *
     * Parameters(s)
     *     @param electionId: list - single
     *     @param type: list - single
     *     @param description: list - single
     *     @param startDate: list - single
     *     @param endDate: list - single
     *     @param createUser: string
     *
     * Return(s)
     *     none
     * */
    public function createVotingRound()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        $sql_query = "
            insert into FACELECTMGR.ELECTION_ROUND(
              election_id,
              round_type_id,
              description,
              create_user,
              update_user,
              start_date,
              end_date,
              date_created,
              date_updated
            ) values(?,?,?,?,?,to_date(?,'DD-MON-YYYY HH24:MI:SS'),to_date(?,'DD-MON-YYYY HH24:MI:SS'),sysdate,sysdate)
        ";

        $ruleArr = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'type' => array(
                'isArray' => false,
                // change to 'TRUE' if allow setup multiple rounds in one time
                'isRequired' => true,
            ),
            'description' => array(
                'isArray' => false,
                // change to 'TRUE' if allow setup multiple rounds in one time
                'isRequired' => false,
            ),
            'startDate' => array(
                'isArray' => false,
                // change to 'TRUE' if allow setup multiple rounds in one time
                'isRequired' => true,
            ),
            'endDate' => array(
                'isArray' => false,
                // change to 'TRUE' if allow setup multiple rounds at a time
                'isRequired' => false,
            ),
            'createUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );

        $this->checkParameters($ruleArr, $body, 'POST');

        // check if endDate is before startDate

        if (isset($body['startDate']) && isset($body['endDate']) && $body['startDate'] > $body['endDate']) {
            throw new \Exception('Error: start date > end date');
        }

        //translate start date and end date from timestamp to SQL time
        $startDate = isset($body['startDate']) ? date('d-M-Y H:i:s',
            $body['startDate']) : '';
        $endDate = isset($body['endDate']) ? date('d-M-Y H:i:s',
            $body['endDate']) : '';

        if (!isset($body['description'])) {
            $body['description'] = '';
        }

        if(!isset($body['createUser'])) {
            //get user's uniqueId from token
            $createUser = $this->getApiUser()->getUsername();
        } else {
            $createUser = $body['createUser'];
        }

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $sql_arr = array();

            array_push($sql_arr, $body['electionId']);
            if (is_numeric($body['type'])) {
                array_push($sql_arr, $body['type']);
            } else {
                $sql_get_type = "select round_type_id from facelectmgr.round_type where round_type_name='{$body['type']}'";
                try {
                    $type = $dbh->queryall_array($sql_get_type);
                    $body['type'] = $type[0]['round_type_id'];
                    array_push($sql_arr, $body['type']);
                } catch (\Exception $e2) {
                    throw new \Exception("Error: failed to get type");
                }
            }
            array_push($sql_arr, $body['description']);
            array_push($sql_arr, $createUser);
            array_push($sql_arr, $createUser);
            array_push($sql_arr, $startDate);
            array_push($sql_arr, $endDate);

            $dbh->perform($sql_query, $sql_arr);
        } catch (\Exception $e) {
            $err = $e->getMessage();

            if (strpos($err, 'parent key not found')) {
                $lPos = strpos($err, 'FACELECTMGR.') + 12;
                $rPos = strpos($err, '_FK) violated - parent key not found');
                $key = substr($err, $lPos, $rPos - $lPos);
                throw new \Exception("Error: {$key} not found");
            } elseif (strpos($err,
                'unique constraint (FACELECTMGR.ELECTION_ROUND_PK)')) {
                throw new \Exception('Error: round already exist');
            } else {
                throw new \Exception($err);
            }
        }

        //====================================================================
        //=================== insert voter to voter_status ===================
        //====================================================================

        //get all voters
        $sql_get_voter = "select uniqueid from facelectmgr.election_member join facelectmgr.member_type on facelectmgr.member_type.member_type_id=facelectmgr.election_member.member_type_id where facelectmgr.member_type.member_type_name='Voter' and election_id={$body['electionId']}";
        //check type
        $sql_check_type = "select round_type_name from facelectmgr.round_type where round_type_id={$body['type']}";
        try {
            $res = $dbh->queryall_array($sql_check_type);

            if ($res[0]['round_type_name'] !== 'Self-Nomination Round') {
                $res = $dbh->queryall_array($sql_get_voter);

                $sql_insert_voter = "
				insert into facelectmgr.voter_status(uniqueid, election_id, round_type_id, voted, create_user, update_user, date_created, date_updated)
				";

                for ($i = 0; $i < count($res); $i++) {
                    $str = "'{$res[$i]['uniqueid']}', {$body['electionId']}, {$body['type']}, 0, '{$createUser}', '{$createUser}', sysdate, sysdate from dual";
                    if ($i === 0) {
                        $str = ' select ' . $str;
                    } else {
                        $str = ' union all select ' . $str;
                    }
                    $sql_insert_voter .= $str;
                }

                if (count($res) != 0) {
                    $res = $dbh->queryall_array($sql_insert_voter);
                }
            }


        } catch (\Exception $e2) {
            if ($e2->getMessage() == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                //do nothing
            } else {
                throw new \Exception("Error: failed to insert voters");
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * PUT: modify a voting round
     *
     * Parameters(s)
     *     @param electionId: single - required
     *     @param type: single - required
     *     @param description: single
     *     @param startDate: single
     *     @param endDate: single
     *     @param updateUser: single
     *
     * Return(s)
     *     none
     * */
    public function modifyVotingRound()
    {
        //log: function start
        $this->log->debug('  FacultyElection.VotingRound.modifyVotingRound() was called');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        //log: input stream
        $this->log->debug('  FacultyElection.VotingRound.modifyVotingRound(), inputStream: ' . (print_r($body,
                true)));



        $ruleArr = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'type' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'description' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'startDate' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'endDate' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'updateUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),

        );

        $this->checkParameters($ruleArr, $body, 'PUT');

        //log: pass checking parameters
        $this->log->debug('  FacultyElection.VotingRound.modifyVotingRound(), parameters checking pass');

        $electionId = $body['electionId'];
        unset($body['electionId']);
        $type = $body['type'];
        unset($body['type']);

        //if type is not round_type_id
        if (!is_numeric($type)) {
            $type = "(case when exists (select facelectmgr.round_type.round_type_id from facelectmgr.round_type where facelectmgr.round_type.round_type_name = '{$type}') then (select facelectmgr.round_type.round_type_id from facelectmgr.round_type where facelectmgr.round_type.round_type_name = '{$type}') else -1 end)";
        }

        if(!isset($body['updateUser'])) {
            //get user id from token
            $updateUser = $this->getApiUser()->getUsername();
        } else {
            $updateUser = $body['updateUser'];
            unset($body['updateUser']);
        }

        //log: user access
        $this->log->debug("  FacultyElection.VotingRound.modifyVotingRound(), access user: {$updateUser}");

        //base query
        $sql_query = "
            update facelectmgr.election_round set 
        ";
        //set user params: (optional) - description, startDate, endDate
        foreach ($body as $paramName => $paramValue) {
            if (in_array($paramName, ['startDate', 'endDate'])) {
                $sql_query .= $this->paramName2db($paramName) . " = to_date(?,'DD-MON-YYYY HH24:MI:SS'),";
            } else {
                $sql_query .= $this->paramName2db($paramName) . " = ?,";
            }

        }
        //set system params: update_user and date_updated
        $sql_query .= "update_user = '{$updateUser}', date_updated = sysdate ";
        $sql_query .= "where election_id={$electionId} and round_type_id = " . $type;

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $sql_arr = array();
            foreach ($body as $paramName => $paramValue) {

                if (in_array($paramName, ['startDate', 'endDate'])) {
                    array_push($sql_arr, date("d-M-Y H:i:s", $paramValue));
                } else {
                    array_push($sql_arr, $paramValue);
                }

            }

            //log: sql query
            $this->log->debug("  FacultyElection.VotingRound.modifyVotingRound(), sqlQuery: {$sql_query}");

            $dbh->perform($sql_query, $sql_arr);
        } catch (\Exception $e) {
            if ($e->getMessage() == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                //do nothing
            } else {
                throw new \Exception($e->getMessage());
            }
        }

        //log: sql execute successfully
        $this->log->debug('  FacultyElection.VotingRound.modifyVotingRound(), sql query execute successfully');

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param ruleArr, parameters rule(specify isRequired
     *         and isArray for all parameters), should be array
     *             e.g. $ruleArr = array(
     *                      'electionId' => array(
     *                           'isArray' => true,
     *                           'isRequired' => false,
     *                       ),
     *                       ......
     *                  );
     *     @param paramArr, parameters array send by user, should be an array
     *     @param method, method of request function, either 'GET', 'POST',
     *         'PUT' or 'DELETE'
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameters($ruleArr, $paramArr, $method)
    {
        foreach ($ruleArr as $name => $obj) {
            if ($obj['isRequired'] && !isset($paramArr[$name])) {
                throw new \Exception("Error: missing required parameters ({$name})");
            }

            if (isset($paramArr[$name])) {
                if (is_array($paramArr[$name]) && !$obj['isArray']) {
                    throw new \Exception("Error: invalid {$name}, it should be single");
                }

                $this->checkParameterValue($name, $paramArr[$name], $method);

                unset($paramArr[$name]);
            }
        }

        if (count($paramArr) != 0) {
            throw new \Exception('Error: too much parameters');
        }
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param paramName, name of parameter, should be string
     *     @param paramValue, value of parameter, either be single or array
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameterValue($paramName, $paramValue, $method)
    {
        if (!is_array($paramValue)) {
            $paramValue = array($paramValue);
        }
        if ($paramName == 'electionId') {
            foreach ($paramValue as $v) {
                $this->checkParameterElectionId($v);
            }
        } elseif ($paramName == 'type') {
            foreach ($paramValue as $v) {
                $this->checkParameterRoundType($v);
            }
        } elseif ($paramName == 'description') {
            foreach ($paramValue as $v) {
                $this->checkParameterDescription($v);
            }
        } elseif ($paramName == 'startDate'
            || $paramName == 'endDate'
            || $paramName == 'dateCreated'
            || $paramName == 'dateUpdated'
        ) {
            foreach ($paramValue as $v) {
                $this->checkParameterDate($v);
            }
        }
    }

    //***********************************************************
    //*******************Check Functions Start*******************
    //***********************************************************
    private function checkParameterElectionId($electionId)
    {
        if (trim($electionId) == '') {
            throw new \Exception('Error: empty electionId');
        }
        if (!is_numeric($electionId)) {
            throw new \Exception("Error: invalid electionId, at '{$electionId}'");
        }
    }

    private function checkParameterRoundType($type)
    {
        if (trim($type) == '') {
            throw new \Exception('Error: empty type');
        }
        if (!is_numeric($type) && !in_array($type, [
                'Self-Nomination Round',
                'Semi-Finalists Round',
                'Finalists Round'
            ])) {
            throw new \Exception("Error: invalid type, at '{$type}'");
        }
    }

    private function checkParameterDescription($description)
    {
        if (strlen($description) >= 300) {
            throw new \Exception('Error: description too long, count: ' . strlen($description));
        }
        //*********************************************
        //************SQL Injection Checking************
        //*********************************************
    }

    private function checkParameterDate($date)
    {
        if (trim($date) == '') {
            throw new \Exception('Error: empty date');
        }
        if (!is_numeric($date)) {
            throw new \Exception("Error: invalid date, '{$date}'");
        }
    }
    //***********************************************************
    //********************Check Functions End********************
    //***********************************************************

    /*
     * paramNameT->DatabaseName:
     *     used to translate paramName to attribute name in database
     *
     * parameter(s):
     *     @param paramName, name of parameter, string
     *
     * return:
     *     a string, represent name of database attribute
     * */
    private function paramName2db($paramName)
    {
        switch ($paramName) {
            case 'description':
                return 'description';
            case 'startDate':
                return 'start_date';
            case 'endDate':
                return 'end_date';
            default:
                return '';
        }
    }

    /*
     * Database Name -> WS Name:
     *     translate database name to name used in web services
     *
     * parameter(s):
     *     @param row, a row of data return from database, array
     *         format: array(
     *                     'election_id' => '12',
     *                     'voter_pool_id' => '3',
     *                     ......
     *                 )
     *
     * return(s):
     *     an array, same as @param row, but key names have converted to WS name
     *         format: array(
     *                     'electionId' => '12',
     *                     'voterPoolId' => '3',
     *                     ......
     *                 )
     * */
    private function nameConverter($row)
    {
        $rtn = array();
        foreach ($row as $k => $v) {
            $rtn[$this->nameConverterHelper($k)] = $v;
        }

        return $rtn;
    }

    private function nameConverterHelper($name)
    {
        switch ($name) {
            case 'election_id':
                return 'electionId';
            case 'round_type_name':
                return 'type';
            case 'create_user':
                return 'createUser';
            case 'update_user':
                return 'updateUser';
            case 'date_created':
                return 'dateCreated';
            case 'date_updated':
                return 'dateUpdated';
            case 'start_date':
                return 'startDate';
            case 'end_date':
                return 'endDate';
            case 'round_type_id':
                return 'typeId';
            default:
                return '';
        }
    }
}
