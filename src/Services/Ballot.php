<?php

/*
	-----------------------------------------------------------
	FILE NAME: Ballot.class.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: PATEL AXHAY

	DESCRIPTION:  The Ballot service is currently designed for GET only

	INPUT:
	PARAMETERS: electionId, round_type_id, vote_rank

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:
	
	FACELECTMGR.VOTE
	FACELECTMGR.ROUND_TYPE
	

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	05/27/2016		PATELAH
	Description:	Initial Program
	
	06/02/2016      PATELAH
	Description:	Validation of the SQL query
	
	06/07/2016      PATELAH
	Description:	Code cleanup
	
	06/14/2016		MILLSE
	Description:	Added in the submitBallot() method and supporting helper methods
	
 */

namespace MiamiOH\FacultyElections\Services;

class Ballot extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    // GET(read/view) the Ballot Information
    public function getBallot()
    {

        //log
        $this->log->debug('Ballot service was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        //set the options for electionId or roundTypeName
        if (!$options ||
            (!isset($options['electionId']) || ($options['electionId']) == "")
            || (!isset($options['roundTypeName']) || ($options['roundTypeName']) == "")
        ) {
            throw new \Exception('Error: At least one election ID or roundTypeName must be specified.');
        }

        // Setting the electionId option
        if ($options && isset($options['electionId'])) {
            $electionIdParam = $options['electionId'];
        }

        // Setting the roundTypeName option
        if ($options && isset($options['roundTypeName'])) {
            $roundTypeNameParam = $options['roundTypeName'];
        }

        // If electionId and roundTypeName are set
        if (isset($electionIdParam) && isset($roundTypeNameParam)) {

            //Apply a regular expression to electionId
            if (preg_match('/^\\d*$/', $options['electionId']) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('ElectionId must be numeric.');
            }
            //Apply a regular expression to roundTypeName
            if (preg_match('/^[\w\- ]+$/', $options['roundTypeName']) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('RoundTypeName is invalid.');
            }

            //Build the payload to be returned
            $electionIdRecords = $this->buildGetPayloadForBallots(
            //get the ballots based on the given parameters
                $this->getBallotDataWithElectionId($electionIdParam,
                    $roundTypeNameParam),
                $electionIdParam,
                $roundTypeNameParam
            );

            //do more payload setup
            foreach ($electionIdRecords as $e => $v) { //add each array to the payload
                $payload[$e] = $v;
            }
        }

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;

    }

    // POST (submit/create) a Ballot
    public function submitBallot()
    {
        //log
        $this->log->debug('Ballot service (POST) was called.');

        //set up some variables for the method
        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();
        $payload = array();

        //Check to see if there was data submitted
        if (!isset($data) || $data == '' || !is_array($data) || count($data) <= 0) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Ballot must be provided.');
        }

        //Set up some variables to set as we churn through the input data
        $insertDataArray = array();
        $voteRanks = array();
        $givenRoundName = '';
        $givenElectionId = '';
//        $givenSendEmail = 'true'; //Default sendEmail to true;

        if (!isset($data['voter']) || $data['voter'] == '') {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Voter must be provided.');
        }

        $givenVoter = $data['voter'];

        //If the electionId is missing, throw an exception.
        if (!isset($data['electionId']) || $data['electionId'] == '') {
            throw new \MiamiOH\RESTng\Exception\BadRequest('ElectionId must be provided.');
        }

        //Save the electionId for later
        $givenElectionId = $data['electionId'];

        //If the roundName is missing, throw an exception.
        if (!isset($data['roundTypeName']) || $data['roundTypeName'] == '') {
            throw new \MiamiOH\RESTng\Exception\BadRequest('RoundTypeName must be provided.');
        }

        //Save the roundName for later
        $givenRoundName = $data['roundTypeName'];

        //If the votes are missing, throw an exception.
        if (!isset($data['votes']) || $data['votes'] == '') {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Votes must be provided.');
        }

        $givenVotes = $data['votes'];


//        //But if it is used, sendEmail must contain true or false. No exceptions!
//        if (isset($data['sendEmail']) && $data['sendEmail'] != 'true' && $data['sendEmail'] != 'false') {
//            throw new \MiamiOH\RESTng\Exception\BadRequest('If used, sendEmail must contain true or false.');
//        } else {
//            if (isset($data['sendEmail'])) { //so set it if it was set to 'true' or 'false'
//                $givenSendEmail = $data['sendEmail'];
//            }
//        }


        //Loop through the ranks/nominees given
        foreach ($givenVotes as $voteRank => $nomineeUniqueId) {
            //Make sure the voteRank and nominee are present for the vote
            if ((!isset($nomineeUniqueId) || $nomineeUniqueId == '') && $voteRank != '' && isset($voteRank)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Nominee uniqueid must be provided with ranked vote [$voteRank].");
            }
            if ((!isset($voteRank) || $voteRank == '') && (!isset($nomineeUniqueId) || $nomineeUniqueId == '')) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Rank and nominee must be provided for each vote.');
            }
            if ((!isset($voteRank) || $voteRank == '') && $nomineeUniqueId != '' && isset($nomineeUniqueId)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Rank must be provided with vote for [$nomineeUniqueId].");
            }

            //Make sure the voteRank is valid.  Must be numeric.
            if (preg_match('/^\\d*$/', $voteRank) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Rank [$voteRank] was invalid; must be numeric.");
            }

            //Make sure the nominee is valid.  Must be alpha numeric.
            if (preg_match('/^[[:alnum:]]{3,}$/', $nomineeUniqueId) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Nominee must be alpha-numeric [$nomineeUniqueId].");
            }

            //If we make it past all the tests above, add all the important info into an array.
            $insertDataNode = array();
            $insertDataNode['nominee'] = $nomineeUniqueId;
            $insertDataNode['electionId'] = $givenElectionId;
            $insertDataNode['roundType'] = $givenRoundName; //will be converted from name to an ID
            $insertDataNode['voteRank'] = $voteRank;
            $insertDataArray[] = $insertDataNode;

            //keep the voteRanks around for later validation
            $voteRanks[] = $voteRank;

        }

        //now that we have the insert data for each vote that is being cast on the ballot,
        //let's do a little validation

        //Check to be sure the given roundName is valid.
        if (preg_match('/^[\w\- ]+$/',
                $givenRoundName) == 0 || !$this->roundNameIsValid($givenRoundName)) {
            throw new \Exception('RoundTypeName is invalid.');
        }

        //Check to be sure the given electionId is valid.
        if (preg_match('/^\\d*$/',
                $givenElectionId) == 0 || !$this->electionIdIsValid($givenElectionId)) {
            throw new \Exception('ElectionId is invalid.');
        }

        //verify that the rank numbers are correct.
        $rankCount = count($voteRanks);
        sort($voteRanks);
        for ($i = 1; $i <= $rankCount; $i++) {
            if ($voteRanks[$i - 1] != $i) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Rankings submitted were inconsistent/invalid.");
            }
        }

        //verify the nomineeUniqueIds given are eligible for votes for the given election and round - else, exception
        foreach ($insertDataArray as $insertDataNode) {
            $nominee = $insertDataNode['nominee'];
            if (!$this->nomineeIsValid($insertDataNode['electionId'],
                $insertDataNode['roundType'], $nominee)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Nominee uniqueid [$nominee] was invalid.");
            }
        }

        //just before the insert, check to be sure this person is allowed to vote.
        if (!$this->isVoterAllowedToVote($givenElectionId, $givenRoundName,
            $givenVoter)) {
            throw new \Exception("Voter [$givenVoter] may not vote for this election/round.");
        }

        //send the $insertDataArray off to be inserted into the database
        $this->insertBallotIntoDatabase($insertDataArray, $givenVoter);
        //If the flag is set, send an email to the ballot submitter
        /*if($givenSendEmail == 'true'){
            $results = $this->callResource('facultyElections.Notification.post', array(
                'data' => array(
                'electionId' => $givenElectionId,
                'to' => $givenVoter.'@miamioh.edu',
                'notificationType' => 'ballotConfirmation'
            )));
        }*/
        //Send a success message!
        $payload['message'] = 'Ballot submission successful.';

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
    * Method to build the payload for returning ballot data
    */
    private function buildGetPayLoadForBallots($resultsArray, $outerKey, $innerKey)
    {
        //set up vars
        $returnArray = array();
        $ballotStagingArray = array();

        //Spin through the results and group by ballot_id.
        foreach ($resultsArray as $singleVote) {
            $ballotStagingArray[$singleVote['ballot_id']][] = array(
                'electionId' => $singleVote['election_id'],
                'roundTypeName' => $singleVote['round_type_name'],
                'voteRank' => $singleVote['vote_rank'],
                'nomineeUniqueid' => $singleVote['nominee_uniqueid'],
            );
        }

        //reform the array so that it removes the ballot_ids
        foreach ($ballotStagingArray as $ballotId => $ballot) {
            $returnArray[] = $ballot;
        }

        return $returnArray;

    }

    /*
    * This helper method is used to query the database for Ballots.
    */
    private function getBallotDataWithElectionId($eid, $rtn)
    {

        //Create the query
        $queryString = "
					    select
							  v.ballot_id,
							  v.election_id,
							  rt.round_type_name,
							  v.vote_rank,
							  v.nominee_uniqueid
							  from facelectmgr.vote v
							join facelectmgr.round_type rt
							  on (v.round_type_id = rt.round_type_id)
							where v.election_id = ?
							and rt.round_type_name = ?
							order by vote_rank asc";

        // execute sql query
        try {
            //get the database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //Execute the query and save the results
            $results = $dbh->queryall_array($queryString, $eid, $rtn);
        } catch (Exception $e) {
            //something went wrong with the query execution
            throw new \Exception('Error: retrieving ballot data from database:\n' . $e->getMessage());
        }

        return $results;
    }

    /*
    * This helper method returns true or false. Is the given nominee valid for the
    * given election and round?
    */
    private function nomineeIsValid($electionId, $roundTypeName, $nomineeUniqueId)
    {

        //default validity is false
        $isValid = false;

        //set up the query
        $query = "
			select
				em.uniqueid
			from
				facelectmgr.election_member em
				inner join facelectmgr.round_type rt on (em.nominee_status = rt.candidates)
			where
				em.election_id = ?
				and rt.round_type_name = ?
				and em.uniqueid = ?";

        try {
            //get the database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the query and save the results
            $results = $dbh->queryall_array($query, $electionId, $roundTypeName,
                strtolower($nomineeUniqueId));
        } catch (Exception $e) {
            //Something went wrong with query execution
            throw new \Exception('Error: Failure calling database to validate that nominee is eligible for election/round: ' . $e->getMessage());
        }

        //Check to see if anything was returned.  If so, they are valid.
        foreach ($results as $r) {
            if (isset($r['uniqueid']) && $r['uniqueid'] != '') {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /*
    * This helper method returns true or false. Is the given roundName valid?
    */
    private function roundNameIsValid($roundName)
    {

        //Default validity is false
        $isValid = false;

        //Set up the query
        $query = "
			select
				rt.round_type_name
			from
				facelectmgr.round_type rt
			where rt.round_type_name = ?";

        try {
            //get the database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the query and save the results
            $results = $dbh->queryall_array($query, $roundName);
        } catch (\Exception $e) {
            //something went wrong with query execution
            throw new \Exception('Error: Failure calling database to validate roundTypeName is valid: ' . $e->getMessage());
        }

        //Check to see if anything was returned.  If so, the roundName was valid.
        foreach ($results as $r) {
            if (isset($r['round_type_name']) && $r['round_type_name'] != '') {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /*
    * This method returns the roundTypeId when a roundName is given.
    */
    private function getRoundTypeIdFromName($roundName)
    {

        //default id is null
        $id = null;

        //set up the query
        $query = "
			select
				rt.round_type_id
			from
				facelectmgr.round_type rt
			where rt.round_type_name = ?";

        try {
            //get the database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the query and save the results
            $results = $dbh->queryall_array($query, $roundName);
        } catch (\Exception $e) {
            //something went wrong with query execution
            throw new \Exception('Error: Failure calling database to validate roundTypeName is valid: ' . $e->getMessage());
        }

        //if an id was returned, set the id
        foreach ($results as $r) {
            if (isset($r['round_type_id']) && $r['round_type_id'] != '') {
                $id = $r['round_type_id'];
            }
        }

        return $id;
    }

    /*
    * This helper method returns true or false.  Is this electionId a real electionId?
    */
    private function electionIdIsValid($electionId)
    {

        //default validity is false
        $isValid = false;

        //set up the query
        $query = "
			select
				e.election_id
			from
				facelectmgr.election e
			where e.election_id = ?";

        try {
            //get the database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the query and save the results
            $results = $dbh->queryall_array($query, $electionId);
        } catch (\Exception $e) {
            //something went wrong with query execution
            throw new \Exception('Error: Failure calling database to validate electionId is valid: ' . $e->getMessage());
        }

        //if an electionid was returned, it is valid
        foreach ($results as $r) {
            if (isset($r['election_id']) && $r['election_id'] != '') {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /*
    * This helper method inserts the given data into the database and then calls the
    * markVoterAsVoted method.
    */
    private function insertBallotIntoDatabase($data, $voter)
    {

        //get a ballotid
        $ballotIdQuery = "select facelectmgr.fem_sequence.nextval from dual";
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $results = $dbh->queryall_array($ballotIdQuery);
        } catch (\Exception $e) {
            throw new \Exception('Error: Failure calling database to get a new ballotId: ' . $e->getMessage());
        }
        $ballotId = $results[0]['nextval'];

        //set up the query
        $insertQuery = "
			insert into facelectmgr.vote 
				(ballot_id, nominee_uniqueid, election_id, round_type_id, vote_rank, date_created)
			values 
				(?, ?, ?, ?, ?, SYSDATE)";

        //get the roundTypeId from the name
        $rtId = $this->getRoundTypeIdFromName($data[0]['roundType']);

        //for each vote in the data array, insert a vote into the database.
        foreach ($data as $vote) {
            try {
                //get a database handle
                $dbh = $this->database->getHandle($this->datasource_name);
                //perform the insert
                $voteResults = $dbh->perform($insertQuery, $ballotId,
                    strtolower($vote['nominee']), $vote['electionId'], $rtId,
                    $vote['voteRank']);
            } catch (\Exception $e) {
                //something went wrong with the insert execution
                throw new \Exception("Error: Failure calling database to insert a vote. BallotId was $ballotId: " . $e->getMessage());
            }
        }

        //lastly update the database to show that the voter has voted for this election/round.
        $this->markVoterAsVoted($data[0]['electionId'], $rtId, $voter);

    }

    /*
    * This helper method returns true or false.  Is the given voter allowed to vote
    * for the given election and round?
    */
    private function isVoterAllowedToVote($electionId, $roundTypeName, $voter)
    {

        //default is true
        $allowedToVote = true;

        //set up the query
        $query = "
			select
				vs.uniqueid
			from facelectmgr.voter_status vs
			inner join facelectmgr.round_type rt on (vs.round_type_id = rt.round_type_id)
			where
				vs.election_id = ? and
				rt.round_type_name = ? and
				vs.uniqueid = ? and
				(vs.voted is null or vs.voted != 1)";

        try {
            //get a database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the query and save the results
            $results = $dbh->queryall_array($query, $electionId, $roundTypeName,
                strtolower($voter));
        } catch (Exception $e) {
            //something went wrong with query execution
            throw new \Exception('Error: Failure calling database to validate voter is eligible to vote for election/round: ' . $e->getMessage());
        }

        //if there were no results returned, they are not allowed to vote
        if (is_array($results) && count($results) <= 0) {
            $allowedToVote = false;
        }

        //check to be sure the results actually have a value in them.  Otherwise,
        //they are not allowed to vote
        foreach ($results as $r) {
            if (!isset($r['uniqueid']) || $r['uniqueid'] === '') {
                $allowedToVote = false;
            }
        }

        return $allowedToVote;
    }

    /*
    * This helper method updates a voter's record to show that they have voted for
    * a specific round in a specific election
    */
    private function markVoterAsVoted($electionId, $rtId, $voter)
    {

        //set up the query
        $updateQuery = "
			update facelectmgr.voter_status vs
			set vs.voted = 1
			where
				vs.uniqueid = ? and
				vs.election_id = ? and
				vs.round_type_id = ?
		";

        try {
            //get a database handle
            $dbh = $this->database->getHandle($this->datasource_name);
            //execute the update
            $results = $dbh->perform($updateQuery, strtolower($voter), $electionId,
                $rtId);
        } catch (Exception $e) {
            //something went wrong with the update execution
            throw new \Exception('Error: Failure calling database to update voter status: ' . $e->getMessage());
        }

    }

}
