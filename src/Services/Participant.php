<?php
/*
-----------------------------------------------------------
FILE NAME: DebtProjection.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  The Participant Service is designed to get, update and insert participant 
records to banner FACELECTMGR.ELECTION_MEMBER Table. 

Authorization: authentication and authorization are handled by authorization token. The Service will treat
phone data as public but when people insert new records, the consumer app would need update right of person
project in Authman

INPUT:
PARAMETERS: Election ID, Voter Type, Acceptance Type and Unique ID

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

06/15/2016              SCHMIDEE
Description:  Initial Program

 */

namespace MiamiOH\FacultyElections\Services;
use Illuminate\Support\Facades\Log;

//include 'common.php';

class Participant extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get Particpant Information (GET)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/StudentFinancialAid view Access Required)
     * electionId: Election ID of elections that participant information needs to be
     *		      retrived
     * uniqueId: Unique ID of people that participant information needs to be retrived
     * type: Type of participant information to retrive.
     *
     * Return: Returns an array of all participant information of the elections or users
     *         requested. The type filter maybe used to retrive more specific records.
     */
    public function getParticipant()
    {

        //log
        $this->log->debug('  FacultyElection:getParticipant() was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        $whereAddedToQuery = false;    //Flag if where has been added to Query yet.

        //Initial Query
        $query = 'select 
 			facelectmgr.election_member.election_id,
 			facelectmgr.election_member.uniqueid,
 			facelectmgr.election_member.create_user,
 			facelectmgr.election_member.update_user,
 			facelectmgr.election_member.date_created,
 			facelectmgr.election_member.date_updated,
 			facelectmgr.election_member.member_type_id,
 			facelectmgr.member_type.member_type_name,
 			facelectmgr.election_member.nomination_acceptance,
 			facelectmgr.election_member.nominee_status,
 			facelectmgr.voter_status.round_type_id,
 			facelectmgr.round_type.round_type_name,
 			facelectmgr.voter_status.voted
 		from FACELECTMGR.ELECTION_MEMBER ';

        $sql_join_stat = "
		join facelectmgr.member_type
		on facelectmgr.member_type.member_type_id = facelectmgr.election_member.member_type_id
		left join facelectmgr.voter_status 
		on facelectmgr.voter_status.election_id = facelectmgr.election_member.election_id
		and facelectmgr.voter_status.uniqueid = facelectmgr.election_member.uniqueid
		left join facelectmgr.round_type
		on facelectmgr.round_type.round_type_id = facelectmgr.voter_status.round_type_id 
		";

        $sql_where_stat = '';

        //Process Election ID Filter
        if (isset($options['electionId'])) {
            //Election ID Set
            if (count($options['electionId']) > 0 && preg_match('/^\\d+$/',
                    implode('', $options['electionId'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Election ID must be numeric.');
            }

            //Clean Election ID and Create Query
            $sql_where_stat .= ' where (facelectmgr.election_member.election_id in ( ' . implode(',',
                    $options['electionId']) . ' ))';
            $whereAddedToQuery = true;

        }

        //Process Type Filter
        if (isset($options['type'])) {
            $orNeeded = false;    //Flag if or is needed to be added to the Query.
            //Type Set
            if (count($options['type']) > 0 && preg_match('/^[\\w\\-]+$/',
                    implode('', $options['type'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Type must be Alpha Numeric.');
            }

            //Add where or and to query as needed.
            if (!$whereAddedToQuery) {
                $sql_where_stat .= ' where (';
                $whereAddedToQuery = true;
            } else {
                $sql_where_stat .= ' and (';
            }

            //Create Rest of the Query
            foreach (array_unique($options['type']) as $t) {
                $temp = strtolower($t);
                switch ($temp) {
                    case 'voter':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or';
                        }
                        $sql_where_stat .= ' (nominee_status is null and nomination_acceptance is null)';
                        $orNeeded = true;

                        break;
                    case 'semi-finalist':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nominee_status) like \'semi-finalist\')';
                        $orNeeded = true;
                        break;
                    case 'finalist':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nominee_status) like \'finalist\')';
                        $orNeeded = true;
                        break;
                    case 'winner':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nominee_status) like \'winner\')';
                        $orNeeded = true;
                        break;
                    case 'nominee':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nominee_status) like \'nominee\')';
                        $orNeeded = true;
                        break;
                    case 'voternotvotedinsfr':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= " ((nominee_status is null and nomination_acceptance is null) and (voted=0 and facelectmgr.round_type.round_type_id=(select facelectmgr.round_type.round_type_id from facelectmgr.round_type where facelectmgr.round_type.round_type_name='Semi-Finalists Round'))) ";
                        $orNeeded = true;
                        break;
                    case 'voternotvotedinfr':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= " ((nominee_status is null and nomination_acceptance is null) and (voted=0 and facelectmgr.round_type.round_type_id=(select facelectmgr.round_type.round_type_id from facelectmgr.round_type where facelectmgr.round_type.round_type_name='Finalists Round'))) ";
                        $orNeeded = true;
                        break;
                    default:
                        throw new \MiamiOH\RESTng\Exception\BadRequest("Error: Type '$t' is not valid.");
                        break;
                }

            }
            $sql_where_stat .= ")";
        }

        //Process Acceptance Filter
        if (isset($options['acceptance'])) {
            $orNeeded = false;

            //Acceptance Set
            if (count($options['acceptance']) > 0 && preg_match('/^[\\w\\-]+$/',
                    implode('', $options['acceptance'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Acceptance must be Alpha Numeric.');
            }

            //Add where or and to query as needed.
            if (!$whereAddedToQuery) {
                $sql_where_stat .= ' where (';
                $whereAddedToQuery = true;
            } else {
                $sql_where_stat .= ' and (';
            }

            //Create Rest of the Query
            foreach (array_unique($options['acceptance']) as $t) {
                $temp = strtolower($t);
                switch ($temp) {
                    case 'default':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nomination_acceptance) like \'default\')';
                        $orNeeded = true;
                        break;
                    case 'accepted':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nomination_acceptance) like \'accepted\')';
                        $orNeeded = true;
                        break;
                    case 'declined':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nomination_acceptance) like \'declined\')';
                        $orNeeded = true;
                        break;
                    case 'removed':
                        if ($orNeeded) {
                            $sql_where_stat .= ' or ';
                        }
                        $sql_where_stat .= ' (lower(nomination_acceptance) like \'removed\')';
                        $orNeeded = true;
                        break;
                    default:
                        throw new \MiamiOH\RESTng\Exception\BadRequest("Error: Acceptance '$t' is not valid.");
                        break;
                }

            }
            $sql_where_stat .= ")";
        }

        $uniqueIDFilter = false;    //Flag if there is a filter on Unique ID

        //Process Unique ID Filter
        if (isset($options['uniqueId'])) {
            //Election ID Set
            if (count($options['uniqueId']) > 0 && preg_match('/^[[:alnum:]]{3,}(,[[:alnum:]]{3,})*$/',
                    implode(',', $options['uniqueId'])) == 0) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Unique ID must be alpha numeric and at least 3 characters.');
            }
            //Add where or and to query as needed.
            if (!$whereAddedToQuery) {
                $sql_where_stat .= ' where (';
                $whereAddedToQuery = true;
            } else {
                $sql_where_stat .= ' and (';
            }

            //Create Rest of the Query
            $sql_where_stat .= ' lower(facelectmgr.election_member.uniqueid) in ( \'' . implode("','",
                    array_map('trim',
                        array_map('strtolower', $options['uniqueId']))) . '\'))';
            $whereAddedToQuery = true;
            $uniqueIDFilter = true;
        }

        $dbh = $this->database->getHandle($this->datasource_name);
        //echo $query . $sql_join_stat . $sql_where_stat;exit();
        $results = $dbh->queryall_array($query . $sql_join_stat . $sql_where_stat . ' order by facelectmgr.voter_status.round_type_id desc');
        //print_r($results);exit();
        //$common = new \Common($this->database, $this->datasource_name);
        //$results = $common->sql_select($query . $sql_join_stat . $sql_where_stat, 100);

        //print_r($results);exit();

//		//get all voters' uniqueId
//		$voterStatuses = array();
//		foreach ($results as $result) {
//			if ($result['nomination_acceptance'] == NULL) {
//				array_push($voterStatuses, array(
//					'uniqueId' => $result['uniqueid'],
//					'electionId' => $result['election_id'],
//				));
//			}
//		}

        //var_dump($results);
        $returnElements = $this->buildReturnElements($results);
        //var_dump($returnElements);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($returnElements);

        return $response;

    }

    /*
     * POST:
     *     insert election member into election_member table
     * 		(allow multiple uniqueId being inserted at same time)
     *
     * Parameters(s)
     *     @param uniqueId: list - required
     *     @param electionId: single - required
     *     @param type: - required
     *     @param acceptance: single, enum: 'default', 'accepted', 'declined', 'removed'
     *     @param nomineeStatus: single, enum: '', 'Nominee', 'Semi-Finalist', 'Finalist', 'Winner'
     *     @param createUser
     *
     * Return(s)
     *     none if succeed
     *     error if fail
     * */
    public function createParticipant()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();


        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'uniqueId' => array(
                'isArray' => true,
                'isRequired' => true,
            ),
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'type' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'acceptance' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'nomineeStatus' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'createUser' => array (
                'isArray' => false,
                'isRequired' => false,
            )
        );

        //if participant will be inserted is voter
        //we should not allow to use acceptance and nomineeStatus
        if ($body['type'] == 'voter' && (isset($body['acceptance']) || isset($body['nomineeStatus']))) {
            throw new \Exception('Error: setting acceptance or nomineeStatus for voter is not allowed');
        }

        $this->checkParameters($ruleArray, $body);

        if (!is_array($body['uniqueId'])) {
            $body['uniqueId'] = array($body['uniqueId']);
        }


        $checkNumOfPeople = $this->callResource('person.read.collection.v2',
            array('options' => array('uniqueId' => implode(',', $body['uniqueId']))))
            ->getPayload();



        //=============================================================
        //============ Check Election Exist and Isn't Close ===========
        //=============================================================

        $election = $this->callResource('facultyElections.election.get', array(
            'options' => array(
                'electionId' => $body['electionId']
            )
        ))->getPayload();

        if (count($election) == 0) {
            throw new \Exception("Error: election (ID: {$body['electionId']}) does not exist");
        }

        $election = $election[0];
        if ($election['electionStatus'] == 'Closed') {
            throw new \Exception("Error: election (ID: {$body['electionId']}) has been closed");
        }

        //=============================================================
        //============== Adding Multiple Insertion To SQL =============
        //=============================================================

        //set default value for acceptance and nomineeStatus if not been set yet
        if ($body['type'] == 'nominee') {
            if (!isset($body['acceptance'])) {
                $body['acceptance'] = 'default';
            }
            if (!isset($body['nomineeStatus'])) {
                $body['nomineeStatus'] = '';
            }
        } elseif ($body['type'] = 'voter') {
            $body['acceptance'] = '';
            $body['nomineeStatus'] = '';
        }

        // sql query to get member_type_id
        // from member_type table
        // using member_type_name
        if ($body['type'] == 'nominee') {
            $body['type'] = 'Nominee';
        } elseif ($body['type'] = 'voter') {
            $body['type'] = 'Voter';
        }

        if(!isset($body['createUser'])) {
            //get user's uniqueId from token
            $createUser = $this->getApiUser()->getUsername();
        } else {
            $createUser = $body['createUser'];
        }

        // prepare multiple insertion
        $sql_insert_participant = "
        insert into facelectmgr.election_member(uniqueid, election_id, member_type_id, nomination_acceptance, nominee_status, create_user, update_user, date_created, date_updated) 
        values(?,?,(select member_type_id from facelectmgr.member_type where member_type_name=?),?,?,?,?,sysdate,sysdate)
        ";

        //===========================================================
        //==================== execute sql query ====================
        //===========================================================

        try {


            $outerU = null;
            $dbh = $this->database->getHandle($this->datasource_name);
            $p = $dbh->prepare($sql_insert_participant);

            foreach ($body['uniqueId'] as $uniqueId) {
                $outerU = $uniqueId;
                $queryArgs = array(
                    $uniqueId,
                    $body['electionId'],
                    $body['type'],
                    $body['acceptance'],
                    $body['nomineeStatus'],
                    $createUser,
                    $createUser
                );
                $p->execute($queryArgs);
            }

        } catch (\Exception $e) {
            $err = $e->getMessage();
            if (strpos($err, 'unique constraint')) {
                throw new \Exception('Error: one or more participant you tried to add has already existed in this election');
            } else {
                throw new \Exception('Error: failed to insert participant to database:' . $outerU . '\n' . $err);
            }
        }

        //===========================================================
        //==== Insert Voter To voting_status If In Voting Round =====
        //===========================================================

        if (($election['electionStatus'] == 'Semi-Finalist Selection'
                || $election['electionStatus'] == 'Finalist Selection')
            && $body['type'] == 'Voter'
        ) {

            if ($election['electionStatus'] == 'Semi-Finalist Selection') {
                $roundTypeName = 'Semi-Finalists Round';
            } else {
                $roundTypeName = 'Finalists Round';
            }

            $roundTypeId = "(select round_type_id from facelectmgr.round_type where round_type_name='{$roundTypeName}')";

            $sql_insert_voter_status = "
			insert into facelectmgr.voter_status(uniqueid, election_id, round_type_id, voted, create_user, update_user, date_created, date_updated)
			values('{$uniqueId}', {$body['electionId']}, {$roundTypeId}, 0, '{$createUser}', '{$createUser}', sysdate, sysdate)
			";

            try {
                $dbh = $this->database->getHandle($this->datasource_name);

                $dbh->prepare($sql_insert_voter_status)->execute();
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     * PUT:
     *     modify an participant
     *
     * Parameters(s)
     *     @param electionId: single - required
     *     @param uniqueId: single - required
     *     @param type: single
     *     @param acceptance: single
     *     @param nomineeStatus: single
     *     @param updateUser: single, string
     *
     * Return(s)
     *     none if succeed
     *     error if fail
     * */
    public function modifyParticipant()
    {
        //init: get body array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        $ruleArr = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'uniqueId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'type' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'acceptance' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'nomineeStatus' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
            'updateUser' => array(
                'isArray' => false,
                'isRequired' => false,
            )
        );

        $this->checkParameters($ruleArr, $body, 'PUT');

        $electionId = $body['electionId'];
        unset($body['electionId']);

        $uniqueId = $body['uniqueId'];


        //get record from db for specified user
        $dataGET = $this->callResource('facultyElections.participant.get', array(
                'options' => array(
                    'electionId' => $electionId,
                    'uniqueId' => $uniqueId,
                    'type' => 'nominee'
                )
            )
        );
        $personalRecord = $dataGET->getPayload();

        if (count($personalRecord) == 0) {
            throw new \Exception("Error: user [{$uniqueId}] does not participate in election with electionId [{$electionId}]. If you are trying to remove a Semi-Finalist or Finalist please use the Results Page to do so.");
        } else {
            $personalRecord = $personalRecord[0];
        }

        //check election Status
        //modifying nomination acceptance (except to 'removed') is not allowed
        //after semi-finalist selection

        if (isset($body['acceptance'])) {
            $electionStatus = $this->getElectionStatus($electionId);

            if ($body['acceptance'] != 'removed' && $personalRecord['nominationAcceptance'] != 'removed' && !in_array($electionStatus,
                    ['New', 'Self Nomination', 'Self Nomination Verification'])) {
                throw new \Exception('Error: modifying nomination acceptance is not allowed');
            }
        }

        if(!isset($body['updateUser'])) {
            //get user's uniqueId from token
            $updateUser = $this->getApiUser()->getUsername();
        } else {
            $updateUser = $body['updateUser'];
            unset($body['updateUser']);
        }

        if (!isset($body['type'])) {
            $body['type'] = 'Nominee';
        }

        if ($body['type'] == 'voter') {
            $body['type'] = 'Voter';
            $body['acceptance'] = '';
            $body['nomineeStatus'] = '';
        } elseif ($body['type'] == 'nominee') {
            $body['type'] = 'Nominee';
            if ($personalRecord['nominationAcceptance'] == null
                && !isset($body['acceptance'])
            ) {
                $body['acceptance'] = 'default';
            }
        }

        //base query
        $sql_query = "
            update facelectmgr.election_member set 
        ";

        //set user params: (optional) - type, acceptance, nomineeStatus
        foreach ($body as $paramName => $paramValue) {
            if ($paramName == 'type' && !is_numeric($paramValue)) {
                $sql_query .= $this->paramName2db($paramName) . ' = case when exists (select facelectmgr.member_type.member_type_id from facelectmgr.member_type where facelectmgr.member_type.member_type_name = ?) then (select facelectmgr.member_type.member_type_id from facelectmgr.member_type where facelectmgr.member_type.member_type_name = ?) else -1 end,';
            } else {
                $sql_query .= $this->paramName2db($paramName) . ' = ?,';
            }
        }
        //set system params: update_user and date_updated
        $sql_query .= "update_user = '" . $updateUser . "', date_updated = sysdate ";
        $sql_query .= "where election_id={$electionId} and uniqueid='{$uniqueId}' and member_type_id = (select member_type_id from facelectmgr.member_type where member_type_name='{$body['type']}')";

        // execute sql query
        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $sql_arr = array();
            foreach ($body as $paramName => $paramValue) {
                if (in_array($paramName, ['type']) && !is_numeric($paramValue)) {
                    array_push($sql_arr, $paramValue);
                }
                array_push($sql_arr, $paramValue);
            }
            //print_r($sql_arr);exit();
            $dbh->perform($sql_query, $sql_arr);
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if (strpos($err, 'parent key not found')) {
                $lPos = strpos($err, 'FACELECTMGR.') + 12;
                $rPos = strpos($err, '_FK) violated - parent key not found');
                $key = substr($err, $lPos, $rPos - $lPos);
                throw new \Exception("parent key not found, in '{$key}'");
            } else {
                throw new \Exception($err);
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     *
     * DELETE:
     *     remove participant
     * 			1, if nominee, set nominee_status to 'removed'
     * 			2, if voters, delete them from db
     *
     * Parameter(s):
     *     @param electionId: single
     *     @param uniqueId: single
     *     @param memberType: single
     *     @param updateUser: single
     *
     * Return(s):
     *     none
     * */

    public function deleteParticipant()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'uniqueId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'memberType' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'updateUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );
        $this->checkParameters($ruleArray, $options);

        //===========================================================
        //================= Get Participant's Info ==================
        //===========================================================

        $participantInfo = $this->callResource('facultyElections.participant.get',
            array(
                'options' => array(
                    'electionId' => $options['electionId'],
                    'uniqueId' => $options['uniqueId'],
                )
            ))->getPayload();

        if ($participantInfo == '' || count($participantInfo) == 0) {
            throw new \Exception("Error: failed to get participant's information");
        }

        $isFound = false;
        foreach ($participantInfo as $info) {
            if (strtolower($info['memberTypeName']) == strtolower($options['memberType'])) {
                $participantInfo = $info;
                $isFound = true;
                break;
            }
        }

        if (!$isFound) {
            $type = strtolower($options['memberType']);
            throw new \Exception("Error: participant [{$options['uniqueId']}] does not exist as {$type}");
        }

        if(!isset($options['updateUser'])) {
            //get user's uniqueId from token
            $updateUser = $this->getApiUser()->getUsername();
        } else {
            $updateUser = $options['updateUser'];
        }

        //===========================================================
        //========= Set nomination_acceptance To 'Removed' ==========
        //====================== If is nominee ======================
        //===========================================================

        if ($participantInfo['memberTypeName'] == 'Nominee') {
            $electionInfo = $this->callResource('facultyElections.election.get',
                array(
                    'options' => array(
                        'electionId' => $options['electionId'],
                    )
                ))->getPayload();

            $electionStatus = $electionInfo[0]['electionStatus'];

            if (!in_array($electionStatus, [
                'Semi-Finalist Verification',
                'Finalist Verification',
                'Closed',
                'Removed',
                'New'
            ])
            ) {
                throw new \Exception("Error: Election is not in the correct status to allow removal of nominees.");

            }
            $sql_update_nomineeStatus = "
			update facelectmgr.election_member
			set nomination_acceptance = 'removed',
				date_updated = sysdate,
				update_user = '{$updateUser}'
			where election_id = {$options['electionId']}
				and uniqueid = '{$options['uniqueId']}'
				and member_type_id = (select member_type_id from facelectmgr.member_type where member_type_name='{$participantInfo['memberTypeName']}')
			";

            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                $dbh->prepare($sql_update_nomineeStatus)->execute();
            } catch (\Exception $e) {
                throw new \Exception("Error: failed to remove nominee [{$options['uniqueId']}]");
            }

            //re-calculate the result
            try {
                if (($electionStatus == 'Semi-Finalist Verification') || ($electionStatus == 'Finalist Verification') || ($electionStatus == 'Closed')) {
                    $roundTypeName = ($electionStatus == 'Semi-Finalist Verification') ? "Semi-Finalists Round" : "Finalists Round";
                    $this->callResource('facultyElections.results.put',
                        array('data' => ['electionId' => $options['electionId'],
                            'roundTypeName' => $roundTypeName ]));
                }

                //for a closed election, do another result update to move the finalist to the winner
                if($electionStatus == 'Closed') {
                    $this->callResource('facultyElections.results.put',
                        array('data' => ['electionId' => $options['electionId'],
                            'roundTypeName' => 'Closed' ]));
                }
            } catch (\Exception $e) {
                throw new \Exception("Error: nominee removed, but failed to re-calculate the result. Reason:". $e->getMessage());
            }

        }

        //===========================================================
        //====== Delete Participant & voter_status If is Voter ======
        //===========================================================

        if ($participantInfo['memberTypeName'] == 'Voter') {
            $sql_delete_voter = "
			delete from facelectmgr.election_member
			where election_id = {$options['electionId']}
				and uniqueid = '{$options['uniqueId']}'
				and member_type_id = (select member_type_id from facelectmgr.member_type where member_type_name='{$participantInfo['memberTypeName']}')
			";

            $sql_delete_voterStatus = "
			delete from facelectmgr.voter_status
			where election_id = {$options['electionId']}
				and uniqueid = '{$options['uniqueId']}'
			";

            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                //delete voter status from voter_status table
                $dbh->prepare($sql_delete_voterStatus)->execute();
                //delete voter from election_member table
                $dbh->prepare($sql_delete_voter)->execute();
            } catch (\Exception $e) {
                throw new \Exception("Error: failed to remove voter [{$options['uniqueId']}]");
            }
        }

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function getUsernameFromToken()
    {
        $user = $this->getApiUser();

        return $user->getUsername();
    }

    private function getElectionStatus($electionId)
    {

        //get record from db for specified user
        $electionGET = $this->callResource('facultyElections.election.get', array(
                'options' => array(
                    'electionId' => $electionId,
                )
            )
        );
        $election = $electionGET->getPayload();

        if (count($election) == 0) {
            throw new \Exception('Error: cannot get election status');
        }

        return $election[0]['electionStatus'];
    }

    /*
     * Helper Method or Building Return for GET
     *
     * Parameters (URL):
     * resultsArray: Array containing the results of the SQL Query
     *
     * Return: Returns an array formatted for JSON return with infomration from the SQL
     *         query.
     */
    private function buildReturnElements($resultsArray)
    {
        $returnArray = array();

        //loop through to collect uniqueIds to be sent off to the person service.
        $uniqueIds = array();
        foreach ($resultsArray as $returnRow) {
            $uniqueIds[] = $returnRow['uniqueid'];
        }
        //$tada = count($uniqueIds);

        $masterPersonPayload = array();

        //call the person service for all uniqueIds
        if ($uniqueIds != '') {//if we have uniqueids to call


            //break the uniqueId list into groups of 500 so as to not break the person service
            $uniqueIdLists = array_chunk($uniqueIds, 100);

            foreach ($uniqueIdLists as $list) {

                $uniqueIdString = implode(',', $list);

                $singlePersonPayload = $this->callResource('person.read.collection.v3',
                    array('options' => array('uniqueId' => $uniqueIdString)))
                    ->getPayload();

                $masterPersonPayload = array_merge($masterPersonPayload,
                    $singlePersonPayload);
            }
        }


            //loop through the database results and construct the return objects. inject the preferred name, too.
            $uniqueIDsSeenBefore = [];
            foreach ($resultsArray as $returnRow) {
                // var_dump($resultsArray);
                //die("here");

                //Get the PreferredName of the Participant
                if (array_search(strtoupper($returnRow['uniqueid']) . " " . $returnRow['member_type_name'] . " " . $returnRow['election_id'],
                        $uniqueIDsSeenBefore) === false) {
                    $uniqueIDsSeenBefore[] = strtoupper($returnRow['uniqueid']) . " " . $returnRow['member_type_name'] . " " . $returnRow['election_id'];

                    $indexForPerson = array_search(strtoupper($returnRow['uniqueid']), array_map('strtoupper', array_column(($masterPersonPayload), 'uniqueId')));


                    if ($indexForPerson !== false or $indexForPerson === 0) {
                        $fullPreferredName = $masterPersonPayload[$indexForPerson]['givenNamePreferred'] . ' ' . $masterPersonPayload[$indexForPerson]['familyName'];
                    } else {
                        $fullPreferredName = '';
                    }

                    //Create Element
                    //Fixed on 9/26/2016 by millse.  The array being used before was causing an issue where a user was only
                    //  getting one node returned for each member type.  The example was schmidee.  She was a voter in two
                    //  elections, but only one of the elections were being returned.  The fix was to NOT use an array key
                    //  when adding the data into the $returnArray.  Instead, use a $temporaryArray to gather up all the data
                    //  out of the database results and THEN add the whole node into the $returnArray.  This prevents the data
                    //  from being overwritten because of duplicate keys.
                    $temporaryArray = array();
                    $temporaryArray['electionId'] = $returnRow['election_id'];
                    $temporaryArray['uniqueId'] = $returnRow['uniqueid'];
                    $temporaryArray['preferredName'] = $fullPreferredName; //inject the preferred name into the object
                    $temporaryArray['memberTypeId'] = $returnRow['member_type_id'];
                    $temporaryArray['memberTypeName'] = $returnRow['member_type_name'];
                    $temporaryArray['nominationAcceptance'] = $returnRow['nomination_acceptance'];
                    $temporaryArray['nomineeStatus'] = $returnRow['nominee_status'];
                    $temporaryArray['createUser'] = $returnRow['create_user'];
                    $temporaryArray['dateCreated'] = $returnRow['date_created'];
                    $temporaryArray['updateUser'] = $returnRow['update_user'];
                    $temporaryArray['dateUpdated'] = $returnRow['date_updated'];
                    if ($returnRow['round_type_name'] != '') {
                        $temporaryArray['voted'][$returnRow['round_type_name']] = $returnRow['voted'];
                    }
                    $returnArray[] = $temporaryArray;
                }


            }


        return array_values($returnArray);
    }

    /*
     * paramNameT->DatabaseName:
     *     used to translate paramName to attribute name in database
     *
     * parameter(s):
     *     @param paramName, name of parameter, string
     *
     * return:
     *     a string, represent name of database attribute
     * */
    private function paramName2db($paramName)
    {
        switch ($paramName) {
            case 'electionId':
                return 'election_id';
            case 'uniqueId':
                return 'uniqueid';
            case 'type':
                return 'member_type_id';
            case 'acceptance':
                return 'nomination_acceptance';
            case 'nomineeStatus':
                return 'nominee_status';
            default:
                throw new \Exception("Error: fail to covert param name, '{$paramName}' doesn't be defined");
        }
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param ruleArr, parameters rule(specify isRequired
     *         and isArray for all parameters), should be array
     *             e.g. $ruleArr = array(
     *                      'electionId' => array(
     *                           'isArray' => true,
     *                           'isRequired' => false,
     *                       ),
     *                       ......
     *                  );
     *     @param paramArr, parameters array send by user, should be an array
     *     @param method, method of request function, either 'GET', 'POST',
     *         'PUT' or 'DELETE'
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameters($ruleArr, $paramArr, $method = 'GET')
    {
        foreach ($ruleArr as $name => $obj) {
            if ($obj['isRequired'] && !isset($paramArr[$name])) {
                throw new \Exception("Error: missing required parameters ({$name})");
            }

            if (isset($paramArr[$name])) {
                if (is_array($paramArr[$name]) && !$obj['isArray']) {
                    throw new \Exception("Error: invalid {$name}, it should be single");
                }

                $this->checkParameterValue($name, $paramArr[$name], $method);

                unset($paramArr[$name]);
            }
        }

        if (count($paramArr) != 0) {
            throw new \Exception('Error: too much parameters');
        }
    }

    /*
     * Parameters value checking controller: (only call by checkParameters())
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param paramName, name of parameter, should be string
     *     @param paramValue, value of parameter, either be single or array
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameterValue($paramName, $paramValue, $method)
    {
        if (!is_array($paramValue)) {
            $paramValue = array($paramValue);
        }
        if ($paramName == 'electionId') {
            foreach ($paramValue as $v) {
                $this->checkParameterNumber($paramName, $v);
            }
        } elseif ($paramName == 'type') {
            foreach ($paramValue as $v) {
                $this->checkParameterType($v);
            }
        } elseif ($paramName == 'uniqueId') {
            foreach ($paramValue as $v) {
                $this->checkParameterUniqueId($v);
            }
        } elseif ($paramName == 'acceptance') {
            foreach ($paramValue as $v) {
                $this->checkParameterAcceptance($v);
            }
        } elseif ($paramName == 'nomineeStatus') {
            foreach ($paramValue as $v) {
                $this->checkParameterNomineeStatus($v);
            }
        } elseif ($paramName == 'memberType') {
            foreach ($paramValue as $v) {
                $this->checkParameterMemberType($v);
            }
        }
    }

    private function checkParameterMemberType($v)
    {
        if (trim($v) == '') {
            throw new \Exception("Error: empty memberType");
        }
        if (!in_array(strtolower($v), ['voter', 'nominee'])) {
            throw new \Exception("Error: invalid memberType, should be either 'voter' or 'nominee'");
        }
    }

    private function checkParameterNumber($key, $value)
    {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!is_numeric($value)) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }

    private function checkParameterType($type)
    {
        if (trim($type) == '') {
            throw new \Exception("Error: empty type");
        }

        if (!in_array(strtolower($type), ['nominee', 'voter'])) {
            throw new \Exception("Error: invalid type, at '{$type}'");
        }
    }

    private function checkParameterAcceptance($acceptance)
    {
        if (trim($acceptance) == '') {
            throw new \Exception("Error: empty acceptance");
        }

        if (!in_array($acceptance, [
            'accepted',
            'declined',
            'default',
            'removed'
        ])
        ) {
            throw new \Exception("Error: invalid acceptance, at '{$acceptance}'");
        }
    }

    private function checkParameterUniqueId($uniqueId)
    {
        if (trim($uniqueId) == '') {
            throw new \Exception("Error: empty uniqueId");
        }

        if (preg_match('/[^a-zA-Z0-9]/', $uniqueId)) {
            throw new \Exception("Error: invalid uniqueId, at '{$uniqueId}'");
        }
    }

    private function checkParameterNomineeStatus($nomineeStatus)
    {
        if (!in_array($nomineeStatus, [
            '',
            'Nominee',
            'Semi-Finalist',
            'Finalist',
            'Winner'
        ])
        ) {
            throw new \Exception("Error: invalid nomineeStatus, at '{$nomineeStatus}'");
        }
    }
}
