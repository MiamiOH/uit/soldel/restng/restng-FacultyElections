<?php
/**
 * -----------------------------------------------------------
 * FILE NAME: Results.class.php
 *
 * Copyright (c) 2016 Miami University, All Rights Reserved.
 *
 * Miami University grants you ("Licensee") a non-exclusive, royalty free,
 * license to use, modify and redistribute this software in source and
 * binary code form, provided that i) this copyright notice and license
 * appear on all copies of the software; and ii) Licensee does not utilize
 * the software in a manner which is disparaging to Miami University.
 *
 * This software is provided "AS IS" and any express or implied warranties,
 * including, but not limited to, the implied warranties of merchantability
 * and fitness for a particular purpose are disclaimed. It has been tested
 * and is believed to work as intended within Miami University's
 * environment. Miami University does not warrant this software to work as
 * designed in any other environment.
 *
 * AUTHOR: Mingchao Liao
 *
 * DESCRIPTION:  This calss is used to handle GET, (POST and PUT) method of
 * "results" service. GET method is used to retrieve information of Semi-Finalists,
 * Finalists and Winners
 *
 * This file is initial version
 * Create on 07/26/2016
 * Written by Mingchao Liao (liaom@miamioh.edu)
 *
 * ENVIRONMENT DEPENDENCIES: PHP, STV.class.php
 * AUDIT TRAIL:
 *
 * DATE        UniqueID
 * 08/10/2016  schmidee  Integrated in STV Class File and error checks for empty
 * ballot case.
 * 08/11/2016  schmidee  Correction for edge case where a person can win having zero
 * votes.
 * 08/17/2016  schmidee  Corrected issue when rerunning elections and when ties
 * return a warning message is returned.
 * 09/02/2016  schmidee  Added Winners type to the recalculation for when a closed
 * election is recalculated.
 */

namespace MiamiOH\FacultyElections\Services;

use Exception;

class Results extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    private $stv;

    // *************************************************************************
    // **********Helper functions that were called by the frame work************
    // **********and create internal datasource and configuration objects*******
    // *************************************************************************
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function setSTV($stv)
    {
        $this->stv = $stv;
    }

    /*
     *
     * GET:
     *     get information of Semi-Finalists, Finalists and Winners
     *
     * Parameter(s):
     *     @param electionId: single - required
     *     @param type: single (enum: Semi-Finalist, Finalist, Winner)
     *
     * Return(s):
     *     @return uniqueId: string
     *     @return preferredName: string
     *     @return type: enum(Semi-Finalist, Finalist, Winner)
     * */
    public function getResults()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        //getting if using paging
        if ($request->isPaged()) {
            $offset = $request->getOffset();
            $limit = $request->getLimit();
        }

        // base sql query, retrieve information of all resultss(pools)
        $sql_query = "
        select uniqueid, nominee_status, facelectmgr.election_status.election_status_name
        from facelectmgr.election_member
        join facelectmgr.election on facelectmgr.election.election_id = facelectmgr.election_member.election_id
        join facelectmgr.election_status on facelectmgr.election.election_status_id = facelectmgr.election_status.election_status_id
        ";

        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'type' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
        );
        $this->checkParameters($ruleArray, $options, 'GET');

        //=============================================================
        //=========== Adding Parameters To SQL WHERE clause ===========
        //=============================================================

        $sql_cond = " where facelectmgr.election_member.election_id = {$options['electionId']}";

        $sql_cond .= " and nomination_acceptance <> 'removed'";

        if (isset($options['type'])) {
            $sql_cond .= " and nominee_status = '{$options['type']}'";
        }

        //===========================================================
        //==================== execute sql query ====================
        //===========================================================

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $results = $dbh->queryall_array($sql_query . $sql_cond);
        } catch (Exception $e) {
            throw new \Exception('Error: retrieve data from database:\n' . $e->getMessage());
        }

        //============================================================
        //============== Check Type Match Election Status ============
        //============================================================

        if (count($results)) {
            $electionStatus = $results[0]['election_status_name'];

            if (($options['type'] == 'Semi-Finalist' && !in_array($electionStatus, [
                        'Semi-Finalist Verification',
                        'Finalist Selection'
                    ]))
                || ($options['type'] == 'Finalist' && !in_array($electionStatus,
                        ['Finalist Verification']))
                || ($options['type'] == 'Winner' && !in_array($electionStatus,
                        ['Closed']))
            ) {
                throw new \Exception("Error: you are not allowed to see [{$options['type']}] in [{$electionStatus}]");
            }
        }

        //============================================================
        //================== Get Name From Person WS =================
        //============================================================

        $uniqueIds = implode(',', array_column($results, 'uniqueid'));

        if ($uniqueIds != '') {
            $personPayload = $this->callResource('person.read.collection.v2',
                array('options' => array('uniqueId' => $uniqueIds)))
                ->getPayload();
        }

        //============================================================
        //==================== Set Return Payload ====================
        //============================================================

        foreach ($results as $result) {
            $tmp = array();

            $tmp['uniqueId'] = $result['uniqueid'];

            $indexForPerson = array_search(strtoupper($result['uniqueid']),
                array_column($personPayload, 'uniqueId'));

            if ($indexForPerson != false or $indexForPerson === 0) {
                $fullPreferredName = $personPayload[$indexForPerson]['givenNamePreferred'] . ' ' . $personPayload[$indexForPerson]['familyName'];
            } else {
                $fullPreferredName = '';
            }

            $tmp['preferredName'] = $fullPreferredName;


            $tmp['type'] = $result['nominee_status'];

            array_push($payload, $tmp);
        }

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setTotalObjects(count($payload));

        if (isset($offset) && isset($limit)) {
            $payload = array_slice($payload, $offset, $limit);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     *
     * PUT:
     *     counting ballots using STV
     *
     * Parameter(s):
     *     @param electionId: single - required
     *     @param roundTypeName: single (enum: 'Semi-Finalists Round', 'Finalists Round')
     *
     * Return(s):
     *     @return uniqueId: string
     *     @return numOfVotes: number
     * */
    public function calculateResults()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        //Warnings from STV Calculation
        $warnings = array();

        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'electionId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'roundTypeName' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
        );
        $this->checkParameters($ruleArray, $body,'PUT');
        $electionId = $body{'electionId'};

        //=============================================================
        //======= Set Finalists to Winners (ONLY For 'Closed') ========
        //=============================================================

        if ($body['roundTypeName'] == 'Closed') {
            $sql_finalists_to_winner = "
			update facelectmgr.election_member
			set nominee_status = 'Winner', UPDATE_USER = 'results_put', DATE_UPDATED = SYSDATE
			where election_id = {$electionId}
				and nomination_acceptance <> 'removed'
				and nominee_status = 'Finalist'
			";

            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                $results = $dbh->queryall_array($sql_finalists_to_winner);
            } catch (\Exception $e) {
                $err = $e->getMessage();
                if ($err != 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                    throw new \Exception('Error: failed to set winner');
                }
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

            return $response;
        }

        //=============================================================
        //================== Run SQL To Get Ballots ===================
        //=============================================================

        // sql query to get ballots
        $sql_query = "
        select ballot_id, nominee_uniqueid, vote_rank from facelectmgr.vote 
        where facelectmgr.vote.election_id={$electionId} 
        and facelectmgr.vote.round_type_id=(select round_type_id from facelectmgr.round_type where facelectmgr.round_type.round_type_name='{$body['roundTypeName']}')
        ";

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $results = $dbh->queryall_array($sql_query);
        } catch (\Exception $e) {
            throw new \Exception('Error: retrieve nominee from database:\n' . $e->getMessage());
        }

        //=============================================================
        //============ Re-format Data To Send To STV Class ============
        //=============================================================

        $data = array();

        foreach ($results as $result) {
            $data[$result['ballot_id']][$result['vote_rank']] = $result['nominee_uniqueid'];
        }

        $data = array_values($data);

        //=============================================================
        //============ Run SQL To Get Num Of SF or Winner =============
        //=============================================================

        $sql_query = "select " . ($body['roundTypeName'] == 'Semi-Finalists Round' ? 'number_of_semi_finalists' : 'number_of_winners') . "
		 from facelectmgr.election where election_id={$electionId}
		";

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $results = $dbh->queryall_array($sql_query);
        } catch (\Exception $e) {
            throw new \Exception('Error: retrieve numOfWinners from database:\n' . $e->getMessage());
        }

        if (!count($results)) {
            throw new \Exception('Error: failed to get number of winners');
        }

        $numOfWinners = array_values($results[0])[0];

        //=============================================================
        //=============== Get Removed Nominees ===============
        //=============================================================

        $sql_query = "select uniqueid from FACELECTMGR.ELECTION_MEMBER where NOMINATION_ACCEPTANCE in ('declined', 'removed')and ELECTION_ID = ?";
        $removedNominees = array();
        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $removedNominees_temp = $dbh->queryall_array($sql_query, $electionId);
            foreach ($removedNominees_temp as $temp) {
                array_push($removedNominees, $temp['uniqueid']);
            }

        } catch (\Exception $e) {
            throw new \Exception('Error: retrieve removed or declined nominees from database:\n' . $e->getMessage());
        }

        //=============================================================
        //===================== Get Valid Nominees ====================
        //=============================================================

        $nomineeCurrentTitle = ($body['roundTypeName'] == 'Semi-Finalists Round' ? "'Nominee', 'Semi-Finalist', 'Finalist', 'Winner'" : "'Semi-Finalist', 'Finalist', 'Winner'");
        $sql_query = "select uniqueid from facelectmgr.election_member where election_id = ? 
                           and nominee_status in ({$nomineeCurrentTitle}) 
                           and NOMINATION_ACCEPTANCE not in ('declined', 'removed')";
        $validNominees = array();
        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $validNominees_temp = $dbh->queryall_array($sql_query, $electionId);
            foreach ($validNominees_temp as $temp) {
                array_push($validNominees, $temp['uniqueid']);
            }

        } catch (\Exception $e) {
            throw new \Exception('Error: retrieve valid nominees from database:\n' . $e->getMessage());
        }

        $winnerUniqueIds = array();

        //=============================================================
        //================ Correct Titles of Nominees ===================
        //=============================================================

        $newNomineeTitle = ($body['roundTypeName'] == 'Semi-Finalists Round' ? "Nominee" : "Semi-Finalist");
        $sql_query = "update facelectmgr.election_member set nominee_status = ?, UPDATE_USER = 'results_put', DATE_UPDATED = SYSDATE  where election_id = ? 
                           and nominee_status in ({$nomineeCurrentTitle}) 
                           and NOMINATION_ACCEPTANCE not in ('declined', 'removed')";

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $dbh->perform($sql_query, $newNomineeTitle, $electionId);

        } catch (\Exception $e) {
            throw new \Exception('Error: update nominees statuses from database:\n' . $e->getMessage());
        }


        //=============================================================
        //=============== Call STV Class To Get Winners ===============
        //=============================================================
        if ($data != array()) {
           //Ballot were actually cast and we need to determine an actual winner
            try {
                $this->stv = new STV($data, $numOfWinners, $validNominees,
                    $removedNominees, false);
                $results = $this->stv->calculate_stv();
                $payload['warnings'] = $this->stv->getWarnings();
                foreach ($results as $uniqueId => $numOfVotes) {
                    array_push($winnerUniqueIds, $uniqueId);
                }
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        } else {
            //No Ballot were cast, nominees all declared winners unless there more nominees than winner spots

            if (count($validNominees) <= $numOfWinners) {
                foreach ($validNominees as $uniqueId) {
                    array_push($winnerUniqueIds, $uniqueId);
                }
            } else {
                throw new \Exception("Unable to calculate winners due to lack of votes and too many nominees.");
            }
        }
        //==============================================================
        //=== Modify Winners' nominee_status in election_member table ==
        //==============================================================

        $winnerUniqueIds = implode("','", $winnerUniqueIds);

        $winnerTitle = ($body['roundTypeName'] == 'Semi-Finalists Round' ? 'Semi-Finalist' : 'Finalist');
        $sql_update = "
		update facelectmgr.election_member  R
		set nominee_status= ?, UPDATE_USER = 'results_put', DATE_UPDATED = SYSDATE
		where election_id= ?
		and nominee_status is not NULL 
		and uniqueid in ('{$winnerUniqueIds}')";

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $dbh->perform($sql_update, $winnerTitle, $electionId);
        } catch (\Exception $e) {
            throw new \Exception('Error: failed to update winners status:\n' . $e->getMessage());
        }


//==============================================================
//================= Set Response Configuration =================
//======================== Send Response =======================
//==============================================================
//        if($this->stv->getWarnings() !== null)
//        {$payload['warnings'] = $this->stv->getWarnings();
//        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        $response->setPayload($payload);

        return $response;
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param ruleArr, parameters rule(specify isRequired
     *         and isArray for all parameters), should be array
     *             e.g. $ruleArr = array(
     *                      'electionId' => array(
     *                           'isArray' => true,
     *                           'isRequired' => false,
     *                       ),
     *                       ......
     *                  );
     *     @param paramArr, parameters array send by user, should be an array
     *     @param method, method of request function, either 'GET', 'POST',
     *         'PUT' or 'DELETE'
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private
    function checkParameters(
        $ruleArr,
        $paramArr,
        $method = 'GET'
    ) {
        foreach ($ruleArr as $name => $obj) {
            if ($obj['isRequired'] && !isset($paramArr[$name])) {
                throw new \Exception("Error: missing required parameters ({$name})");
            }

            if (isset($paramArr[$name])) {
                if (is_array($paramArr[$name]) && !$obj['isArray']) {
                    throw new \Exception("Error: invalid {$name}, it should be single");
                }

                $this->checkParameterValue($name, $paramArr[$name], $method);

                unset($paramArr[$name]);
            }
        }

        if (isset($paramArr['offset'])) {
            unset($paramArr['offset']);
        }
        if (isset($paramArr['limit'])) {
            unset($paramArr['limit']);
        }

        if (count($paramArr) != 0) {
            throw new \Exception('Error: too many parameters');
        }
    }

    /*
     * Parameters value checking controller: (only call by checkParameters())
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param paramName, name of parameter, should be string
     *     @param paramValue, value of parameter, either be single or array
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private
    function checkParameterValue(
        $paramName,
        $paramValue,
        $method
    ) {
        if (!is_array($paramValue)) {
            $paramValue = array($paramValue);
        }
        if ($paramName == 'electionId') {
            foreach ($paramValue as $v) {
                $this->checkParameterNumber($paramName, $v);
            }
        } elseif ($paramName == 'type') {
            foreach ($paramValue as $v) {
                $this->checkParameterType($paramName, $v);
            }
        } elseif ($paramName == 'roundTypeName') {
            foreach ($paramValue as $v) {
                $this->checkParameterRoundTypeName($paramName, $v);
            }
        }
    }

//***********************************************************
//*******************Check Functions Start*******************
//***********************************************************
    private
    function checkParameterNumber(
        $key,
        $value
    ) {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!is_numeric($value)) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }

    private
    function checkParameterType(
        $key,
        $value
    ) {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!in_array($value, ['Semi-Finalist', 'Finalist', 'Winner'])) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }

    private
    function checkParameterRoundTypeName(
        $key,
        $value
    ) {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!in_array($value,
            ['Semi-Finalists Round', 'Finalists Round', 'Closed'])) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }
}
