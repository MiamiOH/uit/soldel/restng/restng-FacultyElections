<?php
/*
-----------------------------------------------------------
FILE NAME: CriteriaGroup.class.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  This calss is used to handle GET, POST and PUT method of "criteriaGroup" service.
GET method is used to retrieve information of criteria group(s)
POST method is used to create an criteria group
PUT method is used to modify an criteria group

This file is initial version
Create on 07/12/2016
Written by liaom@miamioh.edu
*/

namespace MiamiOH\FacultyElections\Services;

class CriteriaGroup extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    // *************************************************************************
    // **********Helper functions that were called by the frame work************
    // **********and create internal datasource and configuration objects*******
    // *************************************************************************
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     *
     * GET:
     *     get information of criteriaGroup(s), which represent Pool table in DB
     *
     * Parameter(s):
     *     @param poolId: list
     *     @param poolName: list
     *
     * Return(s):
     *     @return poolId: string
     *     @return poolName: number
     *     @return criteria: array
     * 			@return id: number
     * 				@return name: string
     * 				@return options: array
     * 					@return id: number
     * 						@return name: string
     *     @return createUser: string
     *     @return updateUser: string
     *     @return dateCreated: date
     *     @return dateUpdated: date
     * */
    public function getCriteriaGroup()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();

        //getting if using paging
        if ($request->isPaged()) {
            $offset = $request->getOffset();
            $limit = $request->getLimit();
        }

        // base sql query, retrieve information of all criteriaGroups(pools)
        $sql_query = "
        	select 
        		facelectmgr.pool.pool_id,
        		facelectmgr.pool.pool_name,
        		facelectmgr.pool.create_user,
        		facelectmgr.pool.update_user,
        		facelectmgr.pool.date_created,
        		facelectmgr.pool.date_updated,
        		facelectmgr.criteria.criteria_id,
        		facelectmgr.criteria.criteria_name,
        		facelectmgr.criteria.filter,
        		facelectmgr.criteria_option.criteria_option_id,
        		facelectmgr.criteria_option.filter_value,
        		facelectmgr.criteria_option.criteria_option_name 
        	from facelectmgr.pool
        	left join facelectmgr.pool_criteria
        	on facelectmgr.pool.pool_id = facelectmgr.pool_criteria.pool_id
        	left join facelectmgr.criteria
        	on facelectmgr.pool_criteria.criteria_id = facelectmgr.criteria.criteria_id
        	left join facelectmgr.criteria_option
        	on facelectmgr.pool_criteria.criteria_option_id = facelectmgr.criteria_option.criteria_option_id
        ";

        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'poolId' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
            'poolName' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
        );
        $this->checkParameters($ruleArray, $options);

        //=============================================================
        //=========== Adding Parameters To SQL WHERE clause ===========
        //=============================================================

        $sql_cond = '';
        if (count($options) != 0) {
            $sql_cond = ' where ';
        }

        $hasSeparator = false;

        // adding poolId to SQL WHERE clause, if exists
        if (isset($options['poolId'])) {
            if ($hasSeparator) {
                $sql_cond .= ' and ';
            }
            if (!is_array($options['poolId'])) {
                $options['poolId'] = array($options['poolId']);
            }
            $hasSeparator = true;
            $sql_cond .= 'facelectmgr.pool.pool_id in (' . implode(",",
                    $options['poolId']) . ')';
        }

        // adding poolName to SQL WHERE clause, if exists
        if (isset($options['poolName'])) {
            if ($hasSeparator) {
                $sql_cond .= ' and ';
            }
            $hasSeparator = true;
            if (!is_array($options['poolName'])) {
                $options['poolName'] = array($options['poolName']);
            }
            $options['poolName'] = array_map(function ($a) {
                return "'{$a}'";
            }, $options['poolName']);
            $sql_cond .= 'facelectmgr.pool.pool_name in (' . implode(",",
                    $options['poolName']) . ')';
        }

        //===========================================================
        //==================== execute sql query ====================
        //===========================================================

        $sql_cond .= ' order by lower(facelectmgr.pool.pool_name) asc';

        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            $results = $dbh->queryall_array($sql_query . $sql_cond);
        } catch (Exception $e) {
            throw new \Exection('Error: retrieve data from database:\n' . $e->getMessage());
        }

        //============================================================
        //==================== Set Return Payload ====================
        //============================================================

        foreach ($results as $row) {
            $payload[$row['pool_id']]['poolId'] = $row['pool_id'];
            $payload[$row['pool_id']]['poolName'] = $row['pool_name'];
            if (!isset($payload[$row['pool_id']]['criteria'])) {
                $payload[$row['pool_id']]['criteria'] = array();
            }
            if ($row['criteria_id'] != '') {
                $payload[$row['pool_id']]['criteria'][$row['criteria_id']]['criteriaFilter'] = $row['filter'];
                $payload[$row['pool_id']]['criteria'][$row['criteria_id']]['criteriaName'] = $row['criteria_name'];
                $payload[$row['pool_id']]['criteria'][$row['criteria_id']]['options'][$row['criteria_option_id']]['criteriaOptionName'] = $row['criteria_option_name'];
                $payload[$row['pool_id']]['criteria'][$row['criteria_id']]['options'][$row['criteria_option_id']]['criteriaFilterValue'] = $row['filter_value'];
            }
            $payload[$row['pool_id']]['createUser'] = $row['create_user'];
            $payload[$row['pool_id']]['updateUser'] = $row['update_user'];
            $payload[$row['pool_id']]['dateCreated'] = $row['date_created'];
            $payload[$row['pool_id']]['dateUpdated'] = $row['date_updated'];
        }

        $payload = array_values($payload);

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setTotalObjects(count($payload));

        if (isset($offset) && isset($limit)) {
            $payload = array_slice($payload, $offset, $limit);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     *
     * POST:
     *     create criteria group
     *
     * Parameter(s):
     *     @param poolName
     *     @param criteria - array
     * 			@param criteriaId
     * 			@param optionIds - array
     *     @param updateUser
     *
     * Return(s):
     *     @return poolId
     * */
    public function createCriteriaGroup()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();

        // base query to insert record to pool table
        $sql_insert_pool = "
        	insert into facelectmgr.pool(pool_id, pool_name, create_user, update_user, date_created, date_updated) 
        	values(FACELECTMGR.FEM_SEQUENCE.NEXTVAL, ?, ?, ?, sysdate, sysdate)
        ";

        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'poolName' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'criteria' => array(
                'isArray' => true,
                'isRequired' => false,
            ),
            'createUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );
        $this->checkParameters($ruleArray, $body);

        //=============================================================
        //============= Insert into Pool table, Get poolId ============
        //=============================================================

        // create pool
        try {
            $dbh = $this->database->getHandle($this->datasource_name);

            if(!isset($body['createUser'])) {
                //get user's uniqueId from token
                $createUser = $this->getApiUser()->getUsername();
            } else {
                $createUser = $body['createUser'];
            }

            $dbh->perform($sql_insert_pool, [$body['poolName'], $createUser, $createUser]);
            $results = $dbh->queryall_array("select pool_id from facelectmgr.pool where pool_name='{$body['poolName']}'");
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if (strpos($err, 'unique constraint')) {
                throw new \Exception("Error: duplicate poolName [{$body['poolName']}]");
            } else {
                throw new \Exception($err);
            }
        }

        if (count($results) == 0) {
            throw new \Exception('Error: fail to get poolId');
        }

        $poolId = $results[0]['pool_id'];

        //============================================================
        //============== insert into pool_criteria table =============
        //============================================================

        $this->insertPoolCriteria($body['criteria'], $body['poolName'], $poolId,
            $createUser);

        //============================================================
        //==================== Set Return Payload ====================
        //============================================================

        $payload['poolId'] = $poolId;

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    /*
     *
     * PUT:
     *     modify criteria group
     *
     * Parameter(s):
     * 		@param poolId
     * 		@param poolName
     * 		@param criteria - array
     * 			@param criteriaId
     * 			@param optionIds - array
     *      @param updateUser
     *
     * Return(s):
     *     @return poolId
     * */
    public function modifyCriteriaGroup()
    {
        //init: get options array of use input
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $payload = array();



        //=============================================================
        //===================== Check Parameters ======================
        //=============================================================

        $ruleArray = array(
            'poolId' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'poolName' => array(
                'isArray' => false,
                'isRequired' => true,
            ),
            'criteria' => array(
                'isArray' => true,
                'isRequired' => true,
            ),
            'updateUser' => array(
                'isArray' => false,
                'isRequired' => false,
            ),
        );

        $this->checkParameters($ruleArray, $body);


        if(!isset($body['updateUser'])) {
            //get user's uniqueId from token
            $updateUser = $this->getApiUser()->getUsername();
        } else {
            $updateUser = $body['updateUser'];
        }


        //=============================================================
        //================= Get Old Pool Properties ===================
        //=============================================================

        $dataGET = $this->callResource('facultyElections.criteriaGroup.get',
            array('options' => array('poolId' => $body['poolId'])));
        $responseData = $dataGET->getPayload();

        // Fixed this since the response is an array of array
        list($oldPool) = $responseData;

        //=============================================================
        //================== Check: Change PoolName ===================
        //=============================================================

        if ($oldPool['poolName'] != $body['poolName']) {
            $sql_query_update_poolName = "
			update facelectmgr.pool
			set pool_name = '{$body['poolName']}',
				date_updated = sysdate,
				update_user = '{$updateUser}'
			where pool_id = {$body['poolId']}
			";

            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                $dbh->queryall_array($sql_query_update_poolName);
            } catch (\Exception $e) {
                $err = $e->getMessage();
                if ($err == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                    //do nothing
                } else {
                    //rollback: delete pre-created pool
                    throw new \Exception("Error: failed to update poolName");
                }
            }
        }

        //=============================================================
        //=================== Delete Old Properties ===================
        //=============================================================

        $sql_delete_old_criteria = "
		delete from facelectmgr.pool_criteria
		where pool_id = {$body['poolId']}
		";

        try {
            $dbh = $this->database->getHandle($this->datasource_name);
            $dbh->queryall_array($sql_delete_old_criteria);
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if ($err == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                //do nothing
            } else {
                //rollback: delete pre-created pool
                throw new \Exception("Error: failed to delete old criteria");
            }
        }

        //============================================================
        //============== insert into pool_criteria table =============
        //============================================================

        $this->insertPoolCriteria($body['criteria'], $body['poolName'],
            $body['poolId'], $updateUser);

        //============================================================
        //================ Set Response Configuration ================
        //======================= Send Response ======================
        //============================================================

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function insertPoolCriteria($criteria, $poolName, $poolId, $uniqueId)
    {
        if (count($criteria)) {
            $sql_insert_poolCriteria = "
			insert into facelectmgr.pool_criteria(pool_id, criteria_id, criteria_option_id, create_user, update_user, date_created, date_updated)
			";

            for ($i = 0; $i < count($criteria); $i++) {
                for ($j = 0; $j < count($criteria[$i]['criteriaOptionId']); $j++) {
                    $str = "{$poolId}, (select criteria_id from facelectmgr.criteria_option where criteria_id = {$criteria[$i]['criteriaId']} and criteria_option_id = {$criteria[$i]['criteriaOptionId'][$j]}), {$criteria[$i]['criteriaOptionId'][$j]}, '{$uniqueId}', '{$uniqueId}', sysdate, sysdate from dual";
                    if ($i == 0 && $j == 0) {
                        $str = ' select ' . $str;
                    } else {
                        $str = ' union all select ' . $str;
                    }
                    $sql_insert_poolCriteria .= $str;
                }
            }

            try {
                $dbh = $this->database->getHandle($this->datasource_name);
                $dbh->queryall_array($sql_insert_poolCriteria);
            } catch (\Exception $e) {
                $err = $e->getMessage();
                if ($err == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                    //do nothing
                } else {
                    //rollback: delete pre-created pool
                    try {
                        $dbh->queryall_array("delete from facelectmgr.pool where pool_name = '{$poolName}'");
                    } catch (\Exception $e2) {
                        if ($e2->getMessage() == 'oci_fetch_array(): ORA-24374: define not done before fetch or execute and fetch') {
                            //do nothing
                        } else {
                            throw new \Exception("Error: cannot delete pre-created pool: {$poolName}");
                        }
                    }

                    if (strpos($err, 'cannot insert NULL into')) {
                        throw new \Exception('Error: criteriaId and criteriaOptionId does not match');
                    } else {
                        throw new \Exception($err);
                    }
                }
            }
        }
    }

    /*
     * Parameters checking controller:
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param ruleArr, parameters rule(specify isRequired
     *         and isArray for all parameters), should be array
     *             e.g. $ruleArr = array(
     *                      'electionId' => array(
     *                           'isArray' => true,
     *                           'isRequired' => false,
     *                       ),
     *                       ......
     *                  );
     *     @param paramArr, parameters array send by user, should be an array
     *     @param method, method of request function, either 'GET', 'POST',
     *         'PUT' or 'DELETE'
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameters($ruleArr, $paramArr, $method = 'GET')
    {
        foreach ($ruleArr as $name => $obj) {
            if ($obj['isRequired'] && !isset($paramArr[$name])) {
                throw new \Exception("Error: missing required parameters ({$name})");
            }

            if (isset($paramArr[$name])) {
                if (is_array($paramArr[$name]) && !$obj['isArray']) {
                    throw new \Exception("Error: invalid {$name}, it should be single");
                }

                $this->checkParameterValue($name, $paramArr[$name], $method);

                unset($paramArr[$name]);
            }
        }

        if (isset($paramArr['offset'])) {
            unset($paramArr['offset']);
        }
        if (isset($paramArr['limit'])) {
            unset($paramArr['limit']);
        }

        if (count($paramArr) != 0) {
            throw new \Exception('Error: too much parameters');
        }
    }

    /*
     * Parameters value checking controller: (only call by checkParameters())
     *     used to check whether parameters are valid
     *
     * parameters:
     *     @param paramName, name of parameter, should be string
     *     @param paramValue, value of parameter, either be single or array
     *
     * return:
     *     none if succeed
     *     throwing exception if fail
     * */
    private function checkParameterValue($paramName, $paramValue, $method)
    {
        if (!is_array($paramValue)) {
            $paramValue = array($paramValue);
        }
        if ($paramName == 'poolId') {
            foreach ($paramValue as $v) {
                $this->checkParameterNumber($paramName, $v);
            }
        } elseif ($paramName == 'poolName') {
            foreach ($paramValue as $v) {
                $this->checkParameterName($paramName, $v);
            }
        } elseif ($paramName == 'criteria') {
            foreach ($paramValue as $v) {
                if ($v == '') {
                    throw new \Exception("Error: empty criteria");
                }
                if (!isset($v['criteriaId'])) {
                    throw new \Exception("Error: missing required parameters (criteriaId)");
                }
                if (isset($v['criteriaId']) && is_array($v['criteriaId'])) {
                    throw new \Exception("Error: invalid criteriaId, it should be single");
                }
                if (isset($v['criteriaId'])) {
                    $this->checkParameterNumber('criteriaId', $v['criteriaId']);
                }
                if (!isset($v['criteriaOptionId'])) {
                    throw new \Exception("Error: missing required parameters (criteriaOptionId)");
                }
                if (isset($v['criteriaOptionId'])) {
                    if (!is_array($v['criteriaOptionId'])) {
                        throw new \Exception("Error: invalid criteriaOptionId, it should be a list");
                    }
                    foreach ($v['criteriaOptionId'] as $id) {
                        $this->checkParameterNumber('criteriaOptionId', $id);
                    }
                }
            }
        }
    }

    //***********************************************************
    //*******************Check Functions Start*******************
    //***********************************************************
    private function checkParameterNumber($key, $value)
    {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}");
        }
        if (!is_numeric($value)) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }

    //should not be empty
    //should not exceed 30 bytes
    //should only contains: letter, digits, parentheses,
    // space, colon, dash, underscore, plus
    private function checkParameterName($key, $value)
    {
        if (trim($value) == '') {
            throw new \Exception("Error: empty {$key}, at '{$value}''");
        }
        if (strlen($value) >= 30) {
            throw new \Exception("Error: {$key} too long, at '{$value}'");
        }
        if (preg_match('/[^A-Za-z0-9() :\-_+"]/', $value)) {
            throw new \Exception("Error: invalid {$key}, at '{$value}'");
        }
    }
}
