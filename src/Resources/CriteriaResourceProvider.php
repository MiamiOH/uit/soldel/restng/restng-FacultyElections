<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class CriteriaResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.criteria";
    private $s_path = "/facultyElections/criteria/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Criteria';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Items.Option',
            'type' => 'object',
            'properties' => array(
                'id' => array('type' => 'number'),
                'name' => array('type' => 'string'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Items',
            'type' => 'object',
            'properties' => array(
                'criteriaId' => array('type' => 'number'),
                'criteriaName' => array('type' => 'string'),
                'criteriaOption' => array(
                    'type' => 'object',
                    'items' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Get.Items.CriteriaOption',
                    )
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Items',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Criteria',
            'class' => $this->bs_path,
            'description' => '....',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'get the information of all criteria',
                'pattern' => $this->s_path,
                'service' => 'Criteria',
                'method' => 'getCriteria',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'all criteria',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}