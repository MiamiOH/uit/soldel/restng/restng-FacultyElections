<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class VotingRoundResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.votingRound";
    private $s_path = "/facultyElections/votingRound/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\VotingRound';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.ElectionId',
            'type' => 'object',
            'properties' => array(
                'electionId' => array('type' => 'number'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.StartDate',
            'type' => 'object',
            'properties' => array(
                'startDate' => array('type' => 'date'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.EndDate',
            'type' => 'object',
            'properties' => array(
                'endDate' => array('type' => 'date'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Type',
            'type' => 'object',
            'properties' => array(
                'type' => array('type' => 'string'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get',
            'type' => 'object',
            'properties' => array(
                'electionId' => array('type' => 'number'),
                'type' => array('type' => 'string'),
                'description' => array('type' => 'string'),
                'createUser' => array('type' => 'string'),
                'startDate' => array('type' => 'number'),
                'endDate' => array('type' => 'number'),
                'dateCreated' => array('type' => 'number'),
                'dateUpdated' => array('type' => 'number')
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Body',
            'type' => 'object',
            'required' => array(
                'electionId',
                'type',
                'startDate'
            ),
            'properties' => array(
                'electionId' => array('type' => 'number'),
                'type' => array('type' => 'number'),
                'description' => array('type' => 'string'),
                'startDate' => array('type' => 'number'),
                'endDate' => array('type' => 'number'),
                'createUser' => array('type' => 'string')
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Put',
            'type' => 'object',
            'properties' => array(// TBA
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Put.Body',
            'type' => 'object',
            'required' => array('electionId', 'type'),
            'properties' => array(
                'electionId' => array('type' => 'number'),
                'type' => array('type' => 'number'),
                'description' => array('type' => 'string'),
                'startDate' => array('type' => 'number'),
                'endDate' => array('type' => 'number'),
                'updateUser' => array('type' => 'string'),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'VotingRound',
            'class' => $this->bs_path,
            'description' => 'this service is used to get information of voting round(s), as well as create and edit it',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'get the information of one voting round',
                'pattern' => $this->s_path,
                'service' => 'VotingRound',
                'method' => 'getVotingRound',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',

                'options' => array(
                    'electionId' => array(
                        'type' => 'list',
                        'required' => false,
                        'description' => 'id of an election'
                    ),
                    'type' => array(
                        'type' => 'list',


                        'required' => false,
                        'description' => 'type of an election, can either be selfNominationRound, semiFinalistRound or finalistRound'
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('view')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'information of aa voting round',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'create',
                'name' => $this->dot_path . '.post',
                'description' => 'create voting round(s) associated to an election',
                'pattern' => $this->s_path,
                'service' => 'VotingRound',
                'method' => 'createVotingRound',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => '...',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Post.Body',
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => '......',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Post',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'update',
                'name' => $this->dot_path . '.put',
                'description' => '......',
                'pattern' => $this->s_path,
                'service' => 'VotingRound',
                'method' => 'modifyVotingRound',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => '...',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Put.Body',
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => '......',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Put',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}