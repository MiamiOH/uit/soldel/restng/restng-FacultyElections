<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ParticipantResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.participant";
    private $s_path = "/facultyElections/participant/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Participant';

    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Participant',
            'class' => $this->bs_path,
            'description' => 'Information about Participants in Elections',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => $this->dot_path . '.get',
            'description' => 'Returns information about participant(s).',
            'pattern' => $this->s_path,
            'service' => 'Participant',
            'method' => 'getParticipant',
            'isPageable' => false,
            'tags' => array($this->tag),
            'returnType' => 'collection',
            'options' => array(
                'electionId' => array(
                    'type' => 'list',
                    'required' => false,
                    'description' => 'ID for an Election'
                ),
                'uniqueId' => array(
                    'type' => 'list',
                    'required' => false,
                    'description' => 'Unique ID of a Person'
                ),
                'type' => array(
                    'type' => 'list',
                    'required' => false,
                    'description' => 'Type of Participant to Retrive'
                ),
                'acceptance' => array(
                    'type' => 'list',
                    'required' => false,
                    'description' => 'Type of Acceptance Status to Retrive'
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'FacultyElection',
                        'key' => array('view')
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'information of about participants',
                    'returns' => array(
                        'type' => 'array',
                        'items' => array(
                            'type' => 'object',
                            'properties' => array(
                                'electionId' => array('type' => 'string'),
                                'uniqueId' => array('type' => 'string'),
                                'preferredName' => array('type' => 'string'),
                                'poolTypeId' => array('type' => 'string'),
                                'nominationAcceptance' => array('type' => 'string'),
                                'nomineeStatus' => array('type' => 'string'),
                                'createUser' => array('type' => 'string'),
                                'dateCreated' => array('type' => 'string'),
                                'updateUser' => array('type' => 'string'),
                                'dateUpdated' => array('type' => 'string'),
                            )
                        ),
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => $this->dot_path . '.post',
            'description' => 'create a participant',
            'pattern' => $this->s_path,
            'service' => 'Participant',
            'method' => 'createParticipant',
            'isPageable' => false,
            'tags' => array($this->tag),
            'returnType' => 'collection',
            'body' => array(
                'description' => 'Create a nominee or a voter.',
                'required' => true,
                'schema' => array(
                    'type' => 'object',
                    'required' => array('uniqueId', 'electionId', 'type'),
                    'properties' => array(
                        'uniqueId' => array(
                            'type' => 'array',
                            'items' => array('type' => 'string')
                        ),
                        'electionId' => array('type' => 'number'),
                        'type' => array(
                            'type' => 'single',
                            'enum' => ['voter', 'nominee']
                        ),
                        'acceptance' => array(
                            'type' => 'single',
                            'enum' => ['default', 'accepted', 'declined', 'removed']
                        ),
                        'nomineeStatus' => array(
                            'type' => 'single',
                            'enum' => [
                                '',
                                'Nominee',
                                'Semi-Finalist',
                                'Finalist',
                                'Winner'
                            ]
                        ),
                        'createUser' => array('type' => 'string')
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'FacultyElection',
                        'key' => array('All')
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'create a participant',
                    'returns' => array(
                        'type' => 'object',
                        'properties' => array(),
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => $this->dot_path . '.put',
            'description' => 'modify a participant',
            'pattern' => $this->s_path,
            'service' => 'Participant',
            'method' => 'modifyParticipant',
            'isPageable' => false,
            'tags' => array($this->tag),
            'returnType' => 'collection',
            'body' => array(
                'description' => '...',
                'required' => true,
                'schema' => array(
                    'type' => 'object',
                    'required' => array('electionId', 'uniqueId'),
                    'properties' => array(
                        'electionId' => array('type' => 'string'),
                        'uniqueId' => array('type' => 'string'),
                        'type' => array('type' => 'string'),
                        'acceptance' => array('type' => 'string'),
                        'nomineeStatus' => array('type' => 'string'),
                        'updateUser' => array('type' => 'string')
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'FacultyElection',
                        'key' => array('view', 'All')
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'modify a participant',
                    'returns' => array(
                        'type' => 'object',
                        'properties' => array(),
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => $this->dot_path . '.delete',
            'description' => 'If nominee, set nominee_status to \'removed\', if voter, delete them',
            'pattern' => $this->s_path,
            'service' => 'Participant',
            'method' => 'deleteParticipant',
            'isPageable' => false,
            'tags' => array($this->tag),
            'returnType' => 'collection',
            'options' => array(
                'electionId' => array(
                    'type' => 'single',
                    'required' => true,
                    'description' => 'ID for an Election'
                ),
                'uniqueId' => array(
                    'type' => 'single',
                    'required' => true,
                    'description' => 'nominee or voter UniqueID'
                ),
                'memberType' => array(
                    'type' => 'single',
                    'required' => true,
                    'description' => 'voter or nominee'
                ),
                'updateUser' => array(
                    'type' => 'single',
                    'required' => false,
                    'description' => 'user that makes the deletion'
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'FacultyElection',
                        'key' => array('All')
                    )
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'information of about participants',
                    'returns' => array(
                        'type' => 'object',
                        'properties' => array(),
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}