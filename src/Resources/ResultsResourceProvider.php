<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ResultsResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.results";
    private $s_path = "/facultyElections/results/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Results';

    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Results',
            'class' => $this->bs_path,
            'description' => 'Get Semi-Finalists, Finalists and Winners',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'get results information',
                'pattern' => $this->s_path,
                'service' => 'Results',
                'method' => 'getResults',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'electionId' => array(
                        'type' => 'single',
                        'required' => true,
                        'description' => 'Single Id of an election'
                    ),
                    'type' => array(
                        'type' => 'single',
                        'required' => true,
                        'description' => 'Semi-Finalist/Finalist/Winner'
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('view')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'information of an election',
                        'returns' => array(
                            'type' => 'array',
                            'items' => array(
                                'type' => 'object',
                                'properties' => array(
                                    'uniqueId' => array('type' => 'string'),
                                    'preferredName' => array('type' => 'string'),
                                    'type' => array('type' => 'string'),
                                ),
                            )
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'update',
                'name' => $this->dot_path . '.put',
                'description' => 'counting ballots using STV',
                'pattern' => $this->s_path,
                'service' => 'Results',
                'method' => 'calculateResults',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'roundTypeName can be "Semi-Finalists Round/Finalists Round/Closed"',
                    'required' => true,
                    'schema' => array(
                        'type' => 'object',
                        'required' => array('electionId', 'roundTypeName'),
                        'properties' => array(
                            'electionId' => array('type' => 'number'),
                            'roundTypeName' => array('type' => 'string'),
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'information of an election',
                        'returns' => array(
                            'type' => 'object',
                            'properties' => array(
                                'uniqueId' => array('type' => 'string'),
                                'numOfVotes' => array('type' => 'number'),
                            )
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}