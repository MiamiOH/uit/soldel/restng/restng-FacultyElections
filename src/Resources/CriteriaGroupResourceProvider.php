<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class CriteriaGroupResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.criteriaGroup";
    private $s_path = "/facultyElections/criteriaGroup/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\CriteriaGroup';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Items.Criteria.Option',
            'type' => 'object',
            'properties' => array(
                'id' => array('type' => 'number'),
                'name' => array('type' => 'string'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Items.Criteria',
            'type' => 'object',
            'properties' => array(
                'id' => array('type' => 'number'),
                'name' => array('type' => 'string'),
                'options' => array(
                    'type' => 'array',
                    '$ref' => '#/definitions/' . $this->dot_path . '.Get.Items.Criteria.Option',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Items',
            'type' => 'object',
            'properties' => array(
                'poolId' => array('type' => 'number'),
                'poolName' => array('type' => 'string'),
                'criteria' => array(
                    'type' => 'object',
                    'items' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Get.Items.Criteria',
                    )
                ),
                'createUser' => array('type' => 'string'),
                'updateUser' => array('type' => 'string'),
                'dateCreated' => array('type' => 'string'),
                'dateUpdated' => array('type' => 'string'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Items',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CriteriaGroup',
            'class' => $this->bs_path,
            'description' => '....',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'get the information of criteria group',
                'pattern' => $this->s_path,
                'service' => 'CriteriaGroup',
                'method' => 'getCriteriaGroup',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'poolId' => array(
                        'type' => 'list',
                        'required' => false,
                        'description' => 'id of a pool'
                    ),
                    'poolName' => array(
                        'type' => 'list',
                        'required' => false,
                        'description' => 'name of a pool'
                    )
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'information of an election',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'create',
                'name' => $this->dot_path . '.post',
                'description' => 'create a criteriaGroup',
                'pattern' => $this->s_path,
                'service' => 'CriteriaGroup',
                'method' => 'createCriteriaGroup',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => '...',
                    'required' => true,
                    'schema' => array(
                        'type' => 'object',
                        'required' => array('poolName', 'criteria'),
                        'properties' => array(
                            'poolName' => array('type' => 'string'),
                            'criteria' => array(
                                'type' => 'array',
                                'items' => array(
                                    'type' => 'object',
                                    'properties' => array(
                                        'criteriaId' => array('type' => 'number'),
                                        'criteriaOptionId' => array(
                                            'type' => 'number',
                                            'name' => 'Array of Criteria Option Ids',
                                        ),
                                    ),
                                ),
                            ),
                            'createUser' => array('type' => 'string')
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'create a criteria group',
                        'returns' => array(
                            'type' => 'object',
                            'properties' => array(
                                'poolId' => array('type' => 'number'),
                            ),
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'update',
                'name' => $this->dot_path . '.put',
                'description' => 'modify a criteriaGroup',
                'pattern' => $this->s_path,
                'service' => 'CriteriaGroup',
                'method' => 'modifyCriteriaGroup',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => '...',
                    'required' => true,
                    'schema' => array(
                        'type' => 'object',
                        'required' => array('poolId', 'poolName', 'criteria'),
                        'properties' => array(
                            'poolId' => array('type' => 'number'),
                            'poolName' => array('type' => 'string'),
                            'criteria' => array(
                                'type' => 'array',
                                'items' => array(
                                    'type' => 'object',
                                    'properties' => array(
                                        'criteriaId' => array('type' => 'number'),
                                        'criteriaOptionId' => array(
                                            'type' => 'array',
                                            'items' => ['type' => 'string'],
                                            'name' => 'Array of Criteria Option Ids',
                                        ),
                                    ),
                                )
                            ),
                            'updateUser' => array('type' => 'string')
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'modify a criteriaGroup',
                        'returns' => array(
                            'type' => 'object',
                            'properties' => [],
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}