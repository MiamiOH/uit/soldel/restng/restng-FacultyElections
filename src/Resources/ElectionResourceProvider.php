<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ElectionResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.election";
    private $s_path = "/facultyElections/election/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Election';

    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Election',
            'class' => $this->bs_path,
            'description' => 'get information of an election; modify an election; create an election; delete an election',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'get the information of election(s) 
            specified by election id or election type',
                'pattern' => $this->s_path,
                'service' => 'Election',
                'method' => 'getElection',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'electionId' => array(
                        'type' => 'list',
                        'required' => false,
                        'description' => 'id of election'
                    ),
                    'electionStatus' => array(
                        'type' => 'single',
                        'enum' => ['open', 'close'],
                        'required' => false,
                        'description' => 'status of an election, either open or close'
                    ),
                    'uniqueId' => array(
                        'type' => 'single',
                        'required' => false,
                        'description' => 'participant UniqueID'
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('view')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'information of an election',
                        'returns' => array(
                            'type' => 'array',
                            'items' => array(
                                'type' => 'object',
                                'properties' => array(
                                    'electionId' => array('type' => 'number'),
                                    'electionName' => array('type' => 'string'),
                                    'numOfWinners' => array('type' => 'number'),
                                    'numOfSemiFinalists' => array('type' => 'number'),
                                    'electionStatus' => array('type' => 'number'),
                                    'isOpen' => array('type' => 'number'),
                                    'voterPoolId' => array('type' => 'number'),
                                    'createUser' => array('type' => 'string'),
                                    'updateUser' => array('type' => 'string'),
                                    'dateCreated' => array('type' => 'string'),
                                    'dateUpdated' => array('type' => 'string'),
                                    'voterPoolName' => array('type' => 'string'),
                                    'nomineePoolId' => array('type' => 'number'),
                                    'nomineePoolName' => array('type' => 'string'),
                                    'divisionFilterValue' => array('type' => 'string'),
                                    'departmentFilterValue' => array('type' => 'string'),
                                    'campusFilterValue' => array('type' => 'string'),
                                    'votingRounds' => array(
                                        'type' => 'array',
                                        'items' => array(
                                            'type' => 'object',
                                            'properties' => array(
                                                'roundTypeName' => array(
                                                    'type' => 'object',
                                                    'properties' => array(
                                                        'id' => array('type' => 'number'),
                                                        'startDate' => array('type' => 'string'),
                                                        'endDate' => array('type' => 'string'),
                                                        'status' => array('type' => 'string'),
                                                        'createUser' => array('type' => 'string'),
                                                        'updateUser' => array('type' => 'string'),
                                                        'dateCreated' => array('type' => 'string'),
                                                        'dateUpdated' => array('type' => 'string'),
                                                    ),
                                                ),
                                            ),
                                        )
                                    )
                                )
                            )
                        ),
                    )
                )
            )
        );

        $this->addResource(array(
                'action' => 'create',
                'name' => $this->dot_path . '.post',
                'description' => 'create an new election',
                'pattern' => $this->s_path,
                'service' => 'Election',
                'method' => 'createElection',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'Create new election',
                    'required' => true,
                    'schema' => array(
                        'type' => 'object',
                        'required' => array('electionName'),
                        'properties' => array(
                            'electionName' => array('type' => 'string'),
                            'nomineePoolId' => array('type' => 'number'),
                            'numOfWinners' => array('type' => 'number'),
                            'numOfSemiFinalists' => array('type' => 'number'),
                            'voterPoolId' => array('type' => 'number'),
                            'divisionFilterValue'=>array('type' => 'string'),
                            'departmentFilterValue'=>array('type' => 'string'),
                            'createUser' => array('type' => 'string'),
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'create an election',
                        'returns' => array(
                            'type' => 'object',
                            'properties' => array(
                                'electionId' => array('type' => 'number'),
                            )
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'update',
                'name' => $this->dot_path . '.put',
                'description' => 'modify an election',
                'pattern' => $this->s_path,
                'service' => 'Election',
                'method' => 'modifyElection',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'modify an election',
                    'required' => true,
                    'schema' => array(
                        'type' => 'object',
                        'required' => array('electionId'),
                        'properties' => array(
                            'electionId' => array('type' => 'number'),
                            'electionName' => array('type' => 'string'),
                            'nomineePoolId' => array('type' => 'string'),
                            'voterPoolId' => array('type' => 'string'),
                            'electionStatus' => array('type' => 'string'),
                            'numOfWinners' => array('type' => 'number'),
                            'numOfSemiFinalists' => array('type' => 'number'),
                            'updateUser' => array('type' => 'string')
                        ),
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'modify an election',
                        'returns' => array(
                            'type' => 'object',
                            'properties' => array(),
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}