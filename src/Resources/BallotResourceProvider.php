<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class BallotResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "FacultyElections.Ballot";
    private $s_path = "/facultyElections/ballot/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Ballot';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.InnerVote',
            'type' => 'object',
            'properties' => array(
                'electionId' => array(
                    'type' => 'number',
                    'description' => 'ID of the election the vote belongs to'
                ),
                'roundTypeName' => array(
                    'type' => 'string',
                    'description' => 'Name of the round that the vote belongs to'
                ),
                'voteRank' => array(
                    'type' => 'number',
                    'description' => 'Rank given to the nominee by the voter on the ballot'
                ),
                'nomineeUniqueid' => array(
                    'type' => 'string',
                    'description' => 'UniqueID of the nominee ranked by the voter on the ballot'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Ballot',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.InnerVote',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Ballot.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Ballot',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Ballot.Collection',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Return',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                    'description' => 'Success message'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Body.Inner',
            'type' => 'object',
            'properties' => array(
                'voteRank' => array(
                    'type' => 'number',
                    'description' => 'Rank at which the voter ranked the nominee'
                ),
                'nomineeUniqueId' => array(
                    'type' => 'string',
                    'description' => 'Nominee ranked by the voter'
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Body.Inner.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Post.Body.Inner',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Body',
            'type' => 'object',
            'required' => array('ballot', 'voter'),
            'properties' => array(
                'ballot' => array(
                    'type' => 'object',
                    'description' => 'Collection of votes',
                    'properties' => array(
                        'electionId' => array(
                            'type' => 'object',
                            'description' => 'ID of the Election for which the Ballots were cast',
                            'properties' => array(
                                'roundTypeName' => array(
                                    'type' => 'array',
                                    'description' => 'Name of the Round during which the Ballots were cast',
                                    '$ref' => '#/definitions/' . $this->dot_path . '.Post.Body.Inner.Collection',
                                ),
                            ),
                        ),
                    ),
                ),
                'voter' => array( 'type' => 'string')
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Ballot',
            'class' => $this->bs_path,
            'description' => 'This service provides resources about Ballots for FacultyElections.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'Return all Ballots (collection of votes) given an ElectionId and RoundTypeName.',
                'pattern' => $this->s_path,
                'service' => 'Ballot',
                'method' => 'getBallot',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'electionId' => array(
                        'type' => 'single',
                        'required' => true,
                        'description' => 'ID of an Election'
                    ),
                    'roundTypeName' => array(
                        'type' => 'single',
                        'required' => true,
                        'description' => 'Name of RoundType'
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Ballots for the given Election and RoundType',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'create',
                'name' => $this->dot_path . '.post',
                'description' => 'Submit a Ballot (collection of votes).',
                'pattern' => $this->s_path,
                'service' => 'Ballot',
                'method' => 'submitBallot',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'Should contain one ballot and one voter',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Post.Body',
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('view')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Successful ballot submission message.',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Post.Return',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}