<?php

namespace MiamiOH\FacultyElections\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class NotificationResourceProvider extends ResourceProvider
{

    private $tag = "FacultyElections";
    private $dot_path = "facultyElections.Notification";
    private $s_path = "/facultyElections/notification/v1";
    private $bs_path = 'MiamiOH\FacultyElections\Services\Notification';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post',
            'type' => 'object',
            'properties' => array(
                'status' => array('type' => 'string'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Post.Body',
            'type' => 'object',
            'required' => array('name', 'electionId', 'type', 'status'),
            'properties' => array(
                'electionId' => array(
                    'type' => 'number',
                    'required' => 'true',
                    'description' => 'Id of the election that the notification will be sent about',
                ),
                'toPool' => array(
                    'type' => 'string',
                    'default' => 'all',
                    'enum' => array(
                        'voters',
                        'nominees',
                        'all',
                        'nomineesNotResponded',
                        'voternotvotedinsfr',
                        'voternotvotedinfr',
                        'semi-finalist'
                    ),
                    'description' => 'Pool of participants who should receive the notification. Either this field or \'to\' is required.'
                ),
                'to' => array(
                    'type' => 'string',
                    'description' => 'List of email addresses that the notification should be sent to.  Either this field or \'toPool\' is required.',
                    'default' => 'user1@miamioh.edu,user2@miamioh.edu',
                ),
                'notificationType' => array(
                    'type' => 'string',
                    'description' => 'Type of notification to be sent. Functions as a sort of email template.  If this field is not used, then \'subject\' and \'body\' must be.',
                    'enum' => array(
                        'ballotConfirmation',
                        'nominationRound',
                        'semiFinalistsRound',
                        'finalistsRound',
                        'results',
                        'nomineesNotResponded',
                        'voternotvotedinsfr',
                        'voternotvotedinfr',
                        'semi-finalist'
                    ),
                ),
                'body' => array(
                    'type' => 'string',
                    'description' => 'Body of the notification to be sent.  Must be used if \'notificationType\' is not.',
                    'default' => 'Email body text',
                ),
                'subject' => array(
                    'type' => 'string',
                    'description' => 'Subject of the notification to be sent.  Must be used if \'notificationType\' is not.',
                    'default' => 'Email subject text',
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Notification',
            'class' => $this->bs_path,
            'description' => 'Information about Notification sent for Faculty Elections',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'create',
                'name' => $this->dot_path . '.post',
                'description' => 'create a notification',
                'pattern' => $this->s_path,
                'service' => 'Notification',
                'method' => 'createNotification',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'Details about notifications that should be sent out',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/' . $this->dot_path . '.Post.Body',
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'FacultyElection',
                            'key' => array('All')
                        )
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Response from an attempt to create a notification',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Post',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}