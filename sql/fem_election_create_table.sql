-- Create ELECTION table

CREATE TABLE FACELECTMGR.ELECTION
   (
      ELECTION_ID NUMBER(7,0) NOT NULL ENABLE,
      ELECTION_NAME VARCHAR2(200) NOT NULL ENABLE,
      NOMINEE_POOL_ID NUMBER(7,0) ,
      VOTER_POOL_ID NUMBER(7,0),
      ELECTION_STATUS_ID NUMBER(7,0),
      NUMBER_OF_WINNERS NUMBER(7,0),
      NUMBER_OF_SEMI_FINALISTS NUMBER(7,0),
      DIVISION_FILTER_VALUE VARCHAR2(500),
      DEPARTMENT_FILTER_VALUE VARCHAR2(500),
      CAMPUS_FILTER_VALUE VARCHAR2(500),
      CREATE_USER VARCHAR2(300),
      UPDATE_USER VARCHAR2(300),
      DATE_CREATED DATE,
      DATE_UPDATED DATE, 
      PRIMARY KEY(ELECTION_ID),
      UNIQUE(ELECTION_NAME),
      FOREIGN KEY(nominee_pool_id) REFERENCES FACELECTMGR.POOL(POOL_ID),
      FOREIGN KEY(election_status_id) REFERENCES FACELECTMGR.ELECTION_STATUS(ELECTION_STATUS_ID),
      FOREIGN KEY (voter_pool_id) REFERENCES FACELECTMGR.POOL(POOL_ID)
   );

-- ELECTION Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."ELECTION_ID" IS 'id for the election table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."ELECTION_NAME" IS 'name of the election';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."NOMINEE_POOL_ID" IS 'id for the nominee pool related to the pool_id from pool table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."ELECTION_STATUS_ID" IS 'id related to the election status table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."NUMBER_OF_WINNERS" IS 'number of winners for the given election';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."NUMBER_OF_SEMI_FINALISTS" IS 'number of semi-finalists for a given election';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."DIVISION_FILTER_VALUE" IS 'value for filtering the nominee and voter pools by division';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."DEPARTMENT_FILTER_VALUE" IS 'value for filtering the nominee and voter pools by department';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."CAMPUS_FILTER_VALUE" IS 'value for fitering the nominee and voter pools by campus';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."UPDATE_USER" IS 'name of the user updated';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."DATE_CREATED" IS 'date when the election status table was created';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."DATE_UPDATED" IS 'date when the election status table was updated';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION"."VOTER_POOL_ID" IS 'id of the voter pool related to the pool_id from pool table';