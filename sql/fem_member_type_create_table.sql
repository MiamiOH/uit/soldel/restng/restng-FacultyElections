-- Create MEMBER_TYPE table

CREATE TABLE FACELECTMGR.MEMBER_TYPE
   (
      MEMBER_TYPE_ID NUMBER(7,0) NOT NULL ENABLE, 
      MEMBER_TYPE_NAME VARCHAR2(300 BYTE), 
      MEMBER_TYPE_DESCRIPTION VARCHAR2(300 BYTE),
      CREATE_USER VARCHAR2(30 BYTE),
      DATE_CREATED DATE,
      PRIMARY KEY (MEMBER_TYPE_ID)
   );
   
-- MEMBER_TYPE table insertions

INSERT INTO FACELECTMGR.MEMBER_TYPE(member_type_id, member_type_name,member_type_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Nominee', 'It contains all the nominees', 'patelah' , SYSDATE);

INSERT INTO FACELECTMGR.MEMBER_TYPE(member_type_id, member_type_name,member_type_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Voter', 'It contains all the voters', 'patelah' , SYSDATE);

-- MEMBER_TYPE Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."MEMBER_TYPE"."MEMBER_TYPE_ID" IS 'id of the member type table';
   COMMENT ON COLUMN "FACELECTMGR"."MEMBER_TYPE"."MEMBER_TYPE_NAME" IS 'the name of the member (Ex: Nominees , Voters)';
   COMMENT ON COLUMN "FACELECTMGR"."MEMBER_TYPE"."MEMBER_TYPE_DESCRIPTION" IS 'description of the member type';
   COMMENT ON COLUMN "FACELECTMGR"."MEMBER_TYPE"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."MEMBER_TYPE"."DATE_CREATED" IS 'date when the member_type was created';