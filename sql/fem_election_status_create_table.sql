-- Create ELECTION_STATUS table

CREATE TABLE FACELECTMGR.ELECTION_STATUS
   (
      ELECTION_STATUS_ID NUMBER(7,0) NOT NULL ENABLE,
      ELECTION_STATUS_NAME VARCHAR2(30),
      ELECTION_IS_OPEN NUMBER(1,0) NOT NULL,
      ELECTION_STATUS_DESCRIPTION VARCHAR2(300),
      CREATE_USER VARCHAR2(300),
      DATE_CREATED DATE,
      PRIMARY KEY(ELECTION_STATUS_ID),
      UNIQUE(ELECTION_STATUS_NAME)
   );


INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'New' ,'1', 'Election has been created with name', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Self Nomination' , '1', 'Accept/Decline/Default step', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Self Nomination Verification' , '1', 'Verification of Self Nomination', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Semi-Finalist Selection' , '1', 'Eligible nominees who have accepted or defaulted are voted upon', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Semi-Finalist Verification' , '1', 'Verification of Semi-Finalists', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Finalist Selection' , '1', 'Semi-finalists are voted upon', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Finalist Verification' , '1', 'Period in which administrators are verifying finalists', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Closed' , '0', 'Winners are set in stone', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ELECTION_STATUS(election_status_id, election_status_name, election_is_open, election_status_description, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Removed' , '0', 'An election that has been deleted', 'patelah', SYSDATE);

-- ELECTION_STATUS Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."ELECTION_STATUS_ID" IS 'id for the election status';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."ELECTION_STATUS_NAME" IS 'name of the election status table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."ELECTION_IS_OPEN" IS 'check whether election is open or closed';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."ELECTION_STATUS_DESCRIPTION" IS 'description of the election status table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_STATUS"."DATE_CREATED" IS 'date when the election status table was created';
   
   