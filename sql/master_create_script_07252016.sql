SET ECHO ON

@fem_create_sequence.sql
@fem_pool_create_table.sql
@fem_member_type_create_table.sql
@fem_criteria_create_table.sql
@fem_criteria_option_create_table.sql
@fem_pool_criteria_create_table.sql
@fem_election_status_create_table.sql
@fem_election_create_table.sql
@fem_election_member_create_table.sql
@fem_round_type_create_table.sql
@fem_election_round_create_table.sql
@fem_vote_create_table.sql
@fem_voter_status_create_table.sql
@fem_grant.sql

