-- Create ELECTION_MEMBER table

CREATE TABLE FACELECTMGR.ELECTION_MEMBER
   (
      ELECTION_ID NUMBER(7,0),
      UNIQUEID VARCHAR2(300) NOT NULL,
      CREATE_USER VARCHAR2(30),
      DATE_CREATED DATE,
      MEMBER_TYPE_ID NUMBER(7,0),
      NOMINATION_ACCEPTANCE VARCHAR2(30),
      NOMINEE_STATUS VARCHAR2(30),
      UPDATE_USER VARCHAR2(30),
      DATE_UPDATED DATE,
      PRIMARY KEY(UNIQUEID,ELECTION_ID,MEMBER_TYPE_ID),
      FOREIGN KEY (member_type_id) REFERENCES FACELECTMGR.MEMBER_TYPE(MEMBER_TYPE_ID),
      FOREIGN KEY (election_id) REFERENCES FACELECTMGR.ELECTION(ELECTION_ID)
   );

-- ELECTION_MEMBER Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."ELECTION_ID" IS 'id for the election member table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."UNIQUEID" IS 'uniqueID for the election member table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."DATE_CREATED" IS 'date when the election member table was created';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."MEMBER_TYPE_ID" IS 'id related to the member_type table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."NOMINATION_ACCEPTANCE" IS 'details whether the nomination was accepted or not';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."NOMINEE_STATUS" IS 'status of the nominee in election member table';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."UPDATE_USER" IS 'name of the user updated';
   COMMENT ON COLUMN "FACELECTMGR"."ELECTION_MEMBER"."DATE_UPDATED" IS 'date when the election member table was updated';