-- Create POOL table

CREATE TABLE FACELECTMGR.POOL
   (	
      POOL_ID NUMBER(7,0) NOT NULL ENABLE, 
      POOL_NAME VARCHAR2(200) NOT NULL ENABLE, 
      DATE_CREATED DATE, 
      DATE_UPDATED DATE, 
      CREATE_USER VARCHAR2(30),
      UPDATE_USER VARCHAR2(30),
      PRIMARY KEY (POOL_ID),
      UNIQUE(POOL_NAME)
   );

-- POOL Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."POOL"."POOL_ID" IS 'id of the pool table';
   COMMENT ON COLUMN "FACELECTMGR"."POOL"."POOL_NAME" IS 'the name of the pool (Ex:All Tenure faculty, All Admins)';
   COMMENT ON COLUMN "FACELECTMGR"."POOL"."DATE_CREATED" IS 'the date when the pool was created';
   COMMENT ON COLUMN "FACELECTMGR"."POOL"."DATE_UPDATED" IS 'the date when the pool was updated';
   COMMENT ON COLUMN "FACELECTMGR"."POOL"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."POOL"."UPDATE_USER" IS 'name of the user updated';