-- Create CRITERIA_OPTION table

CREATE TABLE FACELECTMGR.CRITERIA_OPTION
   (
      CRITERIA_OPTION_ID NUMBER(7,0) NOT NULL ENABLE,
      CRITERIA_ID NUMBER(7,0),
      CRITERIA_OPTION_NAME VARCHAR2(300),
      FILTER_VALUE VARCHAR2(500),
      CREATE_USER VARCHAR2(30),
      DATE_CREATED DATE,
      PRIMARY KEY(CRITERIA_OPTION_ID),
      FOREIGN KEY(criteria_id) REFERENCES FACELECTMGR.CRITERIA(CRITERIA_ID)
   );
   
-- Inserts of default data:
    --Tenure
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Tenure'), 'Tenured', 'T', 'millse', SYSDATE);
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Tenure'), 'Tenure Track', 'TT', 'millse', SYSDATE);
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Tenure'), 'Tenured and Tenure Track', 'T,TT', 'millse', SYSDATE);
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Tenure'), 'Not Tenurable', 'NT', 'millse', SYSDATE);

    --Grad Level
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Grad Level'), 'A', 'GSA,GSAP', 'millse', SYSDATE);
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Grad Level'), 'A or B', 'GSA,GSAP,GSB', 'millse', SYSDATE);

    --Licensed/Clincal Faculty
    INSERT INTO FACELECTMGR.CRITERIA_OPTION (criteria_option_id, criteria_id, criteria_option_name, filter_value, create_user, date_created)
    VALUES (FACELECTMGR.fem_sequence.NEXTVAL, (select criteria_id from FACELECTMGR.CRITERIA where criteria_name = 'Licensed/Clinical Faculty'), 'Yes', 'CF,SC', 'millse', SYSDATE);

-- CRITERIA_OPTION Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."CRITERIA_OPTION_ID" IS 'id of the criteria option table';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."CRITERIA_ID" IS 'id of the criteria coming from criteria table';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."CRITERIA_OPTION_NAME" IS 'the detailed description of the criteria option (Ex: Tenure Level: T, NT, TT; Grad Level: A only, B only, A or B)';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."FILTER_VALUE" IS 'the value that should be passed as a filter for this criteria option';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA_OPTION"."DATE_CREATED" IS 'date when the criteria option was created';