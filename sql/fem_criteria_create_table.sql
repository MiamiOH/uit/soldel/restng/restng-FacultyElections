-- Create CRITERIA table

CREATE TABLE FACELECTMGR.CRITERIA
   (	
      CRITERIA_ID NUMBER(7,0) NOT NULL ENABLE, 
      CRITERIA_NAME VARCHAR2(100) NOT NULL ENABLE, 
      CRITERIA_DESCRIPTION VARCHAR2(300 BYTE),
      FILTER VARCHAR2(500),
      CREATE_USER VARCHAR2(30),
      DATE_CREATED DATE,
      PRIMARY KEY (CRITERIA_ID),
      UNIQUE(CRITERIA_NAME)
   );
   
-- CRITERIA table insertions

INSERT INTO FACELECTMGR.CRITERIA(criteria_id, criteria_name, criteria_description, filter, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Tenure', 'Tenure level to be selected', 'tenureCode', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.CRITERIA(criteria_id, criteria_name, criteria_description, filter, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Grad Level', 'Grad level criteria to be selected', 'gradLevel', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.CRITERIA(criteria_id, criteria_name, criteria_description, filter, create_user, date_created)
VALUES (FACELECTMGR.fem_sequence.NEXTVAL, 'Licensed/Clinical Faculty', 'Whether or not Licensed/Clinical Faculty are included', 'rankCode', 'patelah', SYSDATE);

-- CRITERIA Table column name description:

   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."CRITERIA_ID" IS 'id of the criteria table';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."CRITERIA_NAME" IS 'the name of the criteria (Ex: Tenure , Grad Level)';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."CRITERIA_DESCRIPTION" IS 'description of the criteria';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."FILTER" IS 'filter that this criteria refers to';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."CRITERIA"."DATE_CREATED" IS 'date when the criteria was created';