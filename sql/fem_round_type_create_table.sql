-- Create ROUND_TYPE table

CREATE TABLE FACELECTMGR.ROUND_TYPE
   (
      ROUND_TYPE_ID NUMBER(7,0) NOT NULL ENABLE,
      ROUND_TYPE_NAME VARCHAR2(300) NOT NULL ENABLE,
      ROUND_DESCRIPTION VARCHAR2(300),
      CANDIDATES VARCHAR2(30),
      CREATE_USER VARCHAR2(300),
      DATE_CREATED DATE,
      PRIMARY KEY(ROUND_TYPE_ID),
      UNIQUE(ROUND_TYPE_NAME)
   );
   
-- ROUND_TYPE table insertions

INSERT INTO FACELECTMGR.ROUND_TYPE(round_type_id, round_type_name, round_description, candidates, create_user, date_created)
VALUES (FACELECTMGR.FEM_SEQUENCE.NEXTVAL, 'Self-Nomination Round'  , 'Eligible nominees are able to accept or reject nomination during this round.', NULL, 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ROUND_TYPE(round_type_id, round_type_name, round_description, candidates, create_user, date_created)
VALUES (FACELECTMGR.FEM_SEQUENCE.NEXTVAL, 'Semi-Finalists Round'  , 'Semi-Finalists are selected in this round', 'Nominee', 'patelah', SYSDATE);

INSERT INTO FACELECTMGR.ROUND_TYPE(round_type_id, round_type_name, round_description, candidates, create_user, date_created)
VALUES (FACELECTMGR.FEM_SEQUENCE.NEXTVAL, 'Finalists Round'  , 'Finalists are selected in this round', 'Semi-Finalist', 'patelah', SYSDATE);

-- ROUND_TYPE table column name descriptions:

   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."ROUND_TYPE_ID" IS 'id for the round type table';
   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."ROUND_TYPE_NAME" IS 'name of the round type table(Ex: Self Nomination, Semi-Finalists, Finalists)';
   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."ROUND_DESCRIPTION" IS 'description of the round type name';
   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."CANDIDATES" IS 'Contains the Nominee_status in which a person must be in to be on the ballot for the round.';
   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."CREATE_USER" IS 'name of the user created';
   COMMENT ON COLUMN "FACELECTMGR"."ROUND_TYPE"."DATE_CREATED" IS 'date when the round type was created';