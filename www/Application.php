<?php
/*
* -----------------------------------------------------------------------
*	FILE NAME: Application.php
*
*	Copyright (c) 2013 Miami University, All Rights Reserved.
*
*	Miami University grants you ("Licensee") a non-exclusive, royalty free,
*	license to use, modify and redistribute this software in source and
*	binary code form, provided that i) this copyright notice and license
*	appear on all copies of the software; and ii) Licensee does not utilize
*	the software in a manner which is disparaging to Miami University.
*
*	This software is provided "AS IS" and any express or implied warranties,
*	including, but not limited to, the implied warranties of merchantability
*	and fitness for a particular purpose are disclaimed. It has been tested
*	and is believed to work as intended within Miami University's
*	environment. Miami University does not warrant this software to work as
*	designed in any other environment.
*
*	AUTHOR: Axhay Patel
*
*	DESCRIPTION: Main application
*
*	TABLES/USAGE: none
*
*	INPUT:
*		PARAMETERS: none
*		FILES:      none
*		STDIN:      none
*		CLASSES:    none
*
*	OUTPUT:
*		RETURN VALUES: none
*		FILES:         none
*		STDOUT:        none
*
*	SYSTEM: Debt Projection
*
*	ENVIRONMENT DEPENDENCIES: none
*
*	PROGRAM DEPENDENCIES: none
*
*	AUDIT TRAIL:
*
*	DATE		PRJ-TSK		UniqueID
*
*	06/14/2016				patelah
*	Description:  Initial Draft
*
* --------------------------------------------------------------------------
*/
include 'MU_DS_Manager.php'; //required to use Marmot PHP Datasources
include_once 'config.php';

//start the session for faculty election manager

$fem_sessionname = 'facultyElections';
mu_start_session($fem_sessionname);

//make the user sign in with CAS if they don't already have a session established
// This application does need the proxy to be enabled so changed cas_proxy_enabled to 0.

mu_check_authenticated_user(
	array(
		'application_name' => $fem_sessionname,
		'session_name' => $fem_sessionname,
		'was_enabled' => 0,
		'cas_enabled' => 1,
		'cas_proxy_enabled' => 0,  
	));

if($_SESSION['authenticated_uid']) {
	if(!$_SESSION['authorizations']['FacultyElections']) {
		$auth = new MU_Authorize($_SESSION['authenticated_dn']);

		$_SESSION['authAsAdmin'] = $auth -> check_authorization('FacultyElections', 'SecurityRoles', 'admin');
		if(!isset($_SESSION['isAdmin'])) {
			$_SESSION['isAdmin'] = $_SESSION['authAsAdmin'];
		}
	}
}

//load constant from configMgr
include_once 'lib/configMgr.php';
configMgr();

//load nav bar content to session
include_once 'lib/navBarContentConfig.php';
//$_SESSION['navBarContent'] = $_navBar;

//=========================================================
//==================== Get User Token =====================
//=========================================================
// Commented to enable the CAS5	 authentication
// if (!isset($PHPCAS_CLIENT)) {
// 	phpCAS::proxy(CAS_VERSION_2_0, $_SESSION['wsUrl']['application_cas_proxy'], 443, '/cas', false);
// 	phpCAS::setPGTStorageOCIDb(MU_CAS_PGT_STORAGE_DBHOST, MU_CAS_PGT_STORAGE_DBUSER, _MU_DS_PASSWORD_DECRYPT(MU_CAS_PGT_STORAGE_DBPASS), MU_CAS_PGT_STORAGE_DBTABLE);
//  phpCAS::setNoCasServerValidation();
// 	phpCAS::forceAuthentication();
// }
	$username = $_SESSION['authenticated_uid'];
// 	$serviceUrl = $_SESSION['wsUrl']['application_service_url'];

// 	$service = phpCAS::getProxiedService(PHPCAS_PROXIED_SERVICE_HTTP_POST);
 	
	// $service->setUrl($serviceUrl);
// 	$service->setContentType('application/x-www-form-urlencoded');
// 	$service->setBody('type=cas-proxy');
// 	$service->send();
// //if(!isset($_SESSION['token'])) {

// 	if ($service->getResponseStatusCode() == 200) {
// 		$response = new SimpleXMLElement($service->getResponseBody());
// 		
// 		if($token = (string)$response->token) {
// 			$_SESSION['token'] = $token;
// 		} else {
// 			die("Failed to get token");
// 		}
// 	} else {
// 		die("Failed to get token");
// 	}
// //}




//===================================================================
//==================== Get Entity Account Token =====================
//===================================================================

//if(!isset($_SESSION['entityAccToken'])) {
	$_SESSION['entityAccToken'] = getToken();


//}

function getToken(){

	//Get the username and password for the SFAWS service account:
	$datasourceName  = 'facultyelections_ws';
	//print_r($datasourceName);
	$ds = new MU_DS_Manager('facultyelections_ws');
	$username = $ds->ds_user;
	$password = $ds->ds_password;

	//construct the URL
	$serviceURL = $_SESSION['wsUrl']['application_service_url_json'];

	//create the post data array
	$data = array('type' => 'usernamePassword',
		'username' => $username,
		'password' => $password);
	//print_r($data);
	//initialize the curl object and set some options
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_URL, $serviceURL);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	//execute the call and kill the curl object
	$result = curl_exec($curl);
	curl_close($curl);

	//pull the token out of the results an return it
	$result = json_decode($result, true);

	return $result['token'];
}
