<!--
-----------------------------------------------------------
FILE NAME: token.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: display token

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE

_header('Token'); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================
if(!$_SESSION['sysConfig']['isDebugMode'] || !FacultyElections::isAdmin()) {
	msgBox('error', "Error: you do not have permission to do this", array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->

<div class="col-xs-12"></div>
	<h1 class="col-xs-12">Personal Token: </h1>
	<h2 class="col-xs-12"><?php echo $_SESSION['token']; ?></h2><hr>
	<h1 class="col-xs-12">Entity Account Token: </h1>
	<h2 class="col-xs-12"><?php echo $_SESSION['entityAccToken']; ?></h2>
</div>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
