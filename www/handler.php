<!--
-----------------------------------------------------------
FILE NAME: blank_page.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: This is UI template file for Faculty Election System

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu

AUDIT TRAIL:

DATE		UniqueID
02/15/2017  schmidee
Description:  Corrected Code Injection
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
include_once 'lib/PostHandler.php';
loading(); // DO NOT MODIFY THIS LINE
_header(''); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

$rtnMsg = 'This is work. But you should not reach to this page !';
$rtnType = 'warning';
$rtnBtn = array(
	array('label' => 'Back to home page', 'url' => './index.php'),
);

if (!isset($_POST['handlerName']) && !isset($_GET['handlerName'])) {
	// do nothing
} else {
	if (isset($_POST['handlerName'])) {
		$handlerName = $_POST['handlerName'];
		unset($_POST['handlerName']);
	} elseif (isset($_GET['handlerName'])) {
		$handlerName = $_GET['handlerName'];
		unset($_GET['handlerName']);
	}


    $res = call_user_func('PostHandler::' . preg_replace("/[^a-zA-Z0-9_]+/", "", $handlerName));

    if (is_array($res)
		&& count($res) == 3
		&& in_array($res[0], ['ok', 'warning', 'error'])
		&& is_string($res[1])
		&& is_array($res[2])
	) {

		$rtnMsg = $res[1];
		$rtnType = $res[0];
		$rtnBtn = $res[2];
	}
}

function debug_get_post() {
	echo "<pre>";
	echo '<h1>$_GET</h1>';
	print_r($_GET);
	echo '<hr>';
	echo '<h1>$_POST</h1>';
	print_r($_POST);
	echo "</pre>";
	die();
}
?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->


<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->

<?php
//debug_get_post();

msgBox($rtnType, $rtnMsg, $rtnBtn);
?>

<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
