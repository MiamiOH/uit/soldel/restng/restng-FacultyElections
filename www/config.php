<?php
/*
-----------------------------------------------------------
FILE NAME: config.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this file use to include all helper (method, class) file used in UI

This file is initial version
Create on 07/15/2016
Written by liaom@miamioh.edu
*/

include_once('lib/HTTPReq.php'); // helper class to make http request
include_once ('lib/common.php'); // php helper method, or php base javascript code
include_once('lib/FacultyElections.php'); // helper class to communicate with WS

/*
 * !!! IMPORTANT !!!
 *
 * UI theme controller, implement a new theme file and replace this line to
 * use new theme
 * */
//include_once ('template/sb-admin-2.php');
include_once ('template/mu_theme_1.php');

