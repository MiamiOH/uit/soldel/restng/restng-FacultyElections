<!--
-----------------------------------------------------------
FILE NAME: results.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: This is UI template file for Faculty Election System
 - Semi-Finalist: Admin can edit
 - Finalist: Admin can edit
 - Winner: Admin and Faculty can view

This file is initial version
Create on 07/27/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header('Result'); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================


?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->

<?php
if (!isset($_GET['electionId'])) {
	msgBox('error', $_SESSION['configValue']['resultsMissingId'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (!isset($_GET['electionName'])) {
	msgBox('error', $_SESSION['configValue']['resultsMissingElectionName'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (!isset($_GET['type'])) {
	msgBox('error', $_SESSION['configValue']['resultsMissingType'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (!in_array($_GET['type'], ['Semi-Finalist', 'Finalist', 'Winner'])) {
	msgBox('error', $_SESSION['configValue']['resultsInvalidError'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if ((in_array($_GET['type'], ['Semi-Finalist', 'Finalist']) || isset($_GET['action'])) && !FacultyElections::isAdmin()) {
	msgBox('error', $_SESSION['configValue']['resultsAccessDeniedError'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (isset($_GET['action'])) {
	if(!in_array($_GET['action'], ['remove'])) {
		msgBox('error', $_SESSION['configValue']['resultsInvalidAction'], array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		));
	}
	if(!isset($_GET['uniqueId'])) {
		msgBox('error', $_SESSION['configValue']['resultsMissingUniqueId'], array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		));
	}
	if($_GET['action'] == 'remove') {
		$action = 'removed';
	} else {
		$action = 'accepted';
	}
	$res = FacultyElections::removeParticipant($_GET['electionId'], $_GET['uniqueId']);
	if(isset($res['error'])) {
        msgBox('error',
            "Failed to " . htmlspecialchars($_GET['action']) . " [" . htmlspecialchars($_GET['uniqueId']) . "]: " . $res['error']['message']
            , array(
                array(
                    'label' => 'Go Back',
                    'url' => "results.php?electionId=" . urlencode($_GET['electionId']) . "&electionName=" . urlencode($_GET['electionName']) . "&type=" . urlencode($_GET['type'])
                ),
		));
	} else {
        msgBox('ok', htmlspecialchars($_GET['action']) . "[" . htmlspecialchars($_GET['uniqueId']) . "] successfully",
            array(
                array(
                    'label' => 'Go Back',
                    'url' => "results.php?electionId=" . urlencode($_GET['electionId']) . "&electionName=" . urlencode($_GET['electionName']) . "&type=" . urlencode($_GET['type'])
                ),
		));
	}
}

$results = FacultyElections::getResults($_GET['electionId'], $_GET['type']);

if (isset($results['error'])) {
	msgBox('error', $results['data']['message'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

$uniqueIds = array_map(function($person) {
	return $person['uniqueId'];
}, $results);

$jobInfo = FacultyElections::getParticipantJobInfo($uniqueIds);
if(isset($jobInfo['error'])) errMsg($jobInfo['data']['message']);
?>

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->
<div class="col-xs-12">
	<h2>Election Information:</h2>
	<div id="electionInfoContainer" class="col-xs-12 addBorder containerPadding">
		<div id='electionInfo' class="col-xs-12 ">
			<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                <strong>ID:</strong> <?php echo htmlspecialchars($_GET['electionId']); ?>
			</div>
			<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                <strong>Election Name:</strong> <?php echo htmlspecialchars($_GET['electionName']); ?>
			</div>
			<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                <strong>Result Type:</strong> <?php echo htmlspecialchars($_GET['type']); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12">

	<h2>Results:</h2>
	<div class="col-xs-12 addBorder containerPadding" style="padding:15px 15px;">
		<div id='no-more-tables col-xs-12'>
			<table class='col-md-12 table-bordered table-striped table-condensed cf'>
				<thead class='cf'>
				<tr>
					<th>Preferred Name</th>
					<th>UniqueID</th>
					<th>Title</th>
					<th>Department</th>
					<th>Division</th>
                    <th>Tenure Status</th> <?php
                    if ($_SESSION['isAdmin']) {
                        echo '<th>Grad Level Expire Date</th>';
                    }

                    if ($_GET['type'] != 'Winner') {
						echo '<th>Action</th>';
					} ?>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach ($results as $result) {
					$title = $_SESSION['configValue']['particiNotAvailableNow'];
					$dept = $_SESSION['configValue']['particiNotAvailableNow'];
					$division = $_SESSION['configValue']['particiNotAvailableNow'];
					$gradLevelExpDate = 'N/A';
					$tenureDescription = 'N/A';

					if(isset($jobInfo[$result['uniqueId']])) {
						$title = implode('<br>', $jobInfo[$result['uniqueId']]['title']);
						$dept = implode('<br>', $jobInfo[$result['uniqueId']]['standardizedDepartmentName']);
                        if ($_SESSION['isAdmin']) {
                            $gradLevelExpDate = $jobInfo[$result['uniqueId']]['gradLevelExpDate'];
                        }
						$division = implode('<br>', $jobInfo[$result['uniqueId']]['standardizedDivisionName']);
						$tenureDescription = $jobInfo[$result['uniqueId']]['tenureDescription'];
					}

					echo "
						<tr>
							<td data-title='Preferred Name'>{$result['preferredName']}</td>
							<td data-title='UniqueID'>{$result['uniqueId']}</td>
							<td data-title='Title'>{$title}</td>
							<td data-title='Department'>{$dept}</td>
							<td data-title='Division'>{$division}</td>
							<td data-title='Tenure Status'>{$tenureDescription}</td>
					";
                    if ($_SESSION['isAdmin']) {

                        echo "
							<td data-title='Grad Level Expire Date'>{$gradLevelExpDate}</td>
						";
                    }
					if ($_GET['type'] != 'Winner') {
						echo "
							<td data-title='Action'>
								<a class='btn btn-danger' href='results.php?action=remove&electionId=" . urlencode($_GET['electionId']) .
                            "&uniqueId=" . urlencode($result['uniqueId']) . "&electionName=" . urlencode($_GET['electionName']) .
                            "&type=" . urlencode($_GET['type']) . "'>Remove</a>
							</td>
							";
					}
					echo "
						</tr>
						";
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
		<?php
		if($_GET['type'] == 'Winner' && FacultyElections::isAdmin()) {
			echo "
				
					<div id='removalNotice'><strong>Notice: You are not able to remove a winner in this page, please click 
					<a href='./participant.php?electionId=" . urlencode($_GET['electionId']) . "'>here</a>
					</strong></div>";
		}
		?>

	</div>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
