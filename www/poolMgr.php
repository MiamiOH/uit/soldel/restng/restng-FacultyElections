<!--
-----------------------------------------------------------
FILE NAME: poolMgr.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: used to manage pool (criteria group)
 - create pool
 - modify pool
 - admin ONLY

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header(($_GET['type'] == 'createPool' ? "Create Criteria Group" : "Modify Criteria Group")); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

if (!FacultyElections::isAdmin()) {
	msgBox('error', $_SESSION['configValue']['postHandlerErrorPermission'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}


if (!isset($_GET['type'])) {
	msgBox('error', $_SESSION['configValue']['poolMgrMissingType'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (!in_array($_GET['type'], ['createPool', 'modifyPool'])) {
	msgBox('error', $_SESSION['configValue']['poolMgrInvalidType'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if ($_GET['type'] == 'modifyPool' && !isset($_GET['poolId'])) {
	msgBox('error', $_SESSION['configValue']['poolMgrMissingPoolId'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->
<link rel='stylesheet' href='css/funkyradio.css'/>

<script>
	$(document).ready(function () {
		$('.criteriaOption-btn').click(function () {
			$(this).parent().siblings().children('input[type="checkbox"]').not(this).prop('checked', false);
		});
	});
</script>
<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->
<?php
if (isset($_GET['poolId'])) {
	$currentPool = FacultyElections::getCriteriaGroup($_GET['poolId']);
	if (isset($currentPool['error'])) {
		msgBox('error', $currentPool['data']['message'], array(
			array('label' => 'OK', 'url' => './pool.php'),
		));
	}
	if (count($currentPool) == 0) {
		msgBox('error', $_SESSION['configValue']['poolMgrPoolIdNotFound'], array(
			array('label' => 'OK', 'url' => './pool.php'),
		));
	}
	$currentPool = $currentPool[0];
}
?>
<h2><?php echo(isset($_GET['poolId']) ? 'Modify Criteria Group' : 'Create Criteria Group'); ?></h2>
<br>
<div class="col-sm-12">
	<form class='col-sm-12 form-horizontal' id='createPoolForm' method='POST' action='handler.php?handlerName=poolMgr'>
		<div class='form-group row'>
            <label for='poolName' class='col-sm-2 form-control-label' style='font-size: 16px'>Pool
				Name:</label>
            <div class='col-sm-6'>
				<input type='text' class='form-control' id='poolName' name='poolName' placeholder='Pool Name' value='<?php if (isset($_GET['poolId'])) {
					echo $currentPool['poolName'];
				} ?>'>
			</div>
		</div>

		<div class='form-group row'>
            <label for='criteria' class='col-sm-2 form-control-label'></label>
			<div class='no-more-tables col-lg-12 col-md-12'>

                <table class='col-xs-12 table-bordered table-striped table-condensed cf'
                       style='padding-right: 0px; padding-left: 0px; border: 0px solid #FFFFFF'>
                    <thead class='cf' style='border: 0px solid #FFFFFF'>
					<tr>
                        <th style='border: 0px solid #FFFFFF'>Criteria</th>
                        <th style='border: 0px solid #FFFFFF'>Criteria Options</th>
					</tr>
					</thead>
					<tbody>
					<?php

					$criteriaSet = FacultyElections::getCriteria();
					if (isset($criteriaSet['error'])) {
						msgBox('error', $criteriaSet['data']['message'], array(
							array('label' => 'Back to home page', 'url' => './index.php'),
						));
					}

					foreach ($criteriaSet as $criteria) {


						$criteriaId = $criteria['criteriaId'];
						$criteriaName = $criteria['criteriaName'];
						echo "
						<tr>
							<td  data-title='Criteria Options' style='border: 0px solid #FFFFFF'>{$criteria['criteriaName']}</td>
							<td style='border: 0px solid #FFFFFF'>";
						if ($criteriaName === "Licensed/Clinical Faculty") {
                            //CheckBox
                            echo "	<div class='funkyradio'>";
                            foreach ($criteria['criteriaOption'] as $optionId => $optionName) {
                                $isChecked = '';
                                if (isset($_GET['poolId'])) {
                                    foreach ($currentPool['criteria'] as $criteria) {
                                        foreach ($criteria['options'] as $option) {
                                            if ($option['criteriaOptionName'] == $optionName) {

                                                $isChecked = 'checked';
                                            }
                                        }
                                    }
                                }
                                echo "
									<div class='funkyradio-success'style='width: 220px'>
										<input class='criteriaOption-btn' type='checkbox' name='{$criteriaId}[]' id='{$optionId}' value='{$optionId}' {$isChecked}/>
										<label for='{$optionId}' style='margin-top: 0px;font-size: 14px;'>{$optionName}</label>
									</div>
							";
                            }
                            echo "
								</div>";

                        } else {
                            //Select Box
                            echo "
								<select class='selectpicker' name='{$criteriaId}[]'>
								<option></option>
						";

                            foreach ($criteria['criteriaOption'] as $optionId => $optionName) {
                                $isChecked = '';
                                if (isset($_GET['poolId'])) {
                                    foreach ($currentPool['criteria'] as $criteria) {
                                        foreach ($criteria['options'] as $option) {
                                            if ($option['criteriaOptionName'] == $optionName) {
                                                $isChecked = 'selected';
                                            }
										}
									}
								}
                                echo "
										<option value='{$optionId}' {$isChecked}>{$optionName}</option>
							";
							}
							echo "
								</select>
							</td>
						</tr>
						";
                        }
					}
					?>
					</tbody>
				</table>
			</div>
		</div>

		<div class='form-group row'>
            <div class='col-sm-offset-6 col-sm-10'>
				<?php if (isset($_GET['poolId'])) {
                    echo "<input type='hidden' name='poolId' value='" . htmlspecialchars($_GET['poolId']) . "'/>";
				} ?>
				<input type='hidden' name='type' value='<?php echo(isset($_GET['poolId']) ? "modifyPool" : "createPool"); ?>'/>
				<button type='submit' id='createPool' class='btn btn-secondary'>
                    <?php echo(isset($_GET['poolId']) ? "Modify Critera Group" : "Create Criteria Group"); ?>
				</button>
			</div>
		</div>
	</form>

</div>

<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
