<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name='description' content=''>
	<meta name='author' content=''>
	<link rel='icon' href='../../favicon.ico'>

	<title>Faculty Elections</title>


	<?php include('template/allJsCss.php'); ?>
	<link rel='stylesheet' href='css/mu_theme_1.css'/>
</head>

<body>


<div class='container2'>


	<div class='container1'>
		<div class='col-lg-12'>
			<div class='row' style='margin-top: 3px;'>
				<div class='col-xs-4'>
					<img alt='Miami University' class='logoSmall' src='//miamioh.edu/stylesheets/images/logos/logoSmall4.png' style='margin-top:3px;height: 30px !important;'>
				</div>
                <div id='header-bar-right' class='col-xs-8 no-print'>
					<ul class='navigationTactical' id='navigationTactical'>
						<li>
							<a href='http://mymiami.miamioh.edu' target='_parent' title='myMiami'>myMiami</a>
						</li>
						<li>
							<a href='http://miamioh.edu/cec/support/index.html' target='_parent' title='Support Us'>Support
								Us</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class='col-lg-12 header-div' style='margin-top: 6px; height: 105px !important;'>

			<div class='col-sm-11 col-xs-10' id="header-content" style="padding-left:0px;margin-top:20px;">
				<h1 style='height: 1em; color: white;'>Faculty Elections</h1>
			</div>

            <div class='navbar-header col-sm-1 col-xs-2 no-print' style="margin-top: 20px;">
				<spam style='margin-right:-10px;' type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
					Menu
				</spam>
			</div>

		</div>
	</div>

	<div class='col-lg-12' id="navContainer">

		<nav id='navbar' class='navbar navbar-default navbar-collapse collapse' style="padding-left:0px;">
			<div class='container-fluid'>
				<ul class='nav navbar-nav'>
					<li><a href='./index.php'>Open Elections</a></li>
					<li><a href='./index.php?isClosed=true'>Archived
							Elections</a></li>
					<li>
						<a data-toggle='modal' id='directory-btn' href='#' data-target='#directory-content'>MU
							Directory</a></li>

					<li><a href='./faq.php'>FAQ</a></li>
				</ul>

				<ul class='nav navbar-nav navbar-right'>
					<li class='dropdown'>
						<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='true'>Admin
							Tools <span class='caret'></span></a>
						<ul class='dropdown-menu'>
							<?php

							if (FacultyElections::isAdmin()) {
								echo "
								<li><a href='./electionMgr.php?type=createElection'>Create Election</a></li>
								<li><a href='./pool.php'>Criteria Group</a></li>
								";
							}

							if ($_SESSION['authAsAdmin']) {
								if ($_SESSION['isAdmin']) {
									echo "<li><a href='./handler.php?handlerName=switchView&view=faculty'>Faculty View</a></li>";
								} else {
									echo "<li><a href='./handler.php?handlerName=switchView&view=admin'>Admin View</a></li>";
								}
							}
							?>
						</ul>
					</li>
					<li class='dropdown'>
						<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='true'>
								Welcome, <?php echo $_SESSION['phpCAS']['user']; ?>
							<span class='caret'></span></a>
						<ul class='dropdown-menu'>
							<li>
								<a href='<?php echo $_SESSION['wsUrl']['cas_logout']; ?>'>Logout</a>
							</li>
						</ul>
					</li>

				</ul>

			</div>
		</nav>
	</div>
	<div class='col-xs-12'>
		<div id='errMsg' class='col-xs-12'></div>
<!--		content-->
	</div>

    <div class="col-xs-12 morespace no-print">&nbsp;</div>
    <div class='col-xs-12 siteFooterTertiary footer no-print' style='padding-left: 0px;padding-right:0px;'>
		<div class='col-xs-12'>
			<div class='col-lg-12 divider subSiteOnly'></div>
		</div>
		<div class='col-xs-12'>
			<div class='row siteFooter'>
				<div class='col-xs-12 col-md-7' style='padding-left: 0px;'>
					<div class='copyright'>
						©2016 Miami University. All rights reserved.
					</div>
					<ul class='footerLinks'>
						<li>
							<a href='http://miamioh.edu/about-miami/pubs-policies/privacy-statement/index.html' target='_parent' title='Privacy Statement'>Privacy
								Statement</a></li>
						<li>
							<a href='http://miamioh.edu/about-miami/pubs-policies/consumer-info/index.html' target='_parent' title='Family Consumer Information'>Family
								Consumer Information</a></li>
						<li>
							<a href='https://miamioh.formstack.com/forms/access_barrier_report' target='_parent' title='Accessibility Needs'>Accessibility
								Needs</a></li>
						<li>
							<a href='http://miamioh.edu/includes/problem/index.html' target='_parent' title='Report a Problem With This Website'>Report
								a Problem With This Website</a></li>
						<li>
							<a href='http://miamioh.edu/campus-safety/annual-report/index.html' target='_parent' title='Annual Security and Fire Safety Report'>Annual
								Security and Fire Safety Report</a></li>
					</ul>
				</div>
				<div class='col-xs-12 col-md-5' style='padding-right: 0px;'>
					<ul class='footerLinksSecondary'>
						<li>
							<a href='http://miamioh.edu/a-to-z/index.html' target='_parent' title='A to Z Listing'>A
								to Z Listing</a></li>
						<li>
							<a href='http://www.miamioh.edu/jobs' target='_parent' title='Employment'>Employment</a>
						</li>
					</ul>
					<ul class='footerActions hide-for-small'>
						<li class='printLink'>
							<a href='#' onclick='window.print()'>Print</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>


<div class='modal fade' id='directory-content' tabindex='-1' role='dialog'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
					<span aria-hidden='true'>&times;</span></button>
				<h4 class='modal-title'>MU Directory</h4>
			</div>
			<div id='directoryModalBody' class='modal-body' style='min-height: 80vh;'>
				...
			</div>
		</div>
	</div>
</div>

</body>
</html>