<!--
-----------------------------------------------------------
FILE NAME: vote.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: used to vote in a certain voting round for specified election

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu

AUDIT TRAIL:

DATE		UniqueID
02/15/2017  schmidee
Description:  Corrections for Firefox and Chrome for ballot submission and Code Injection
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header(); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================


if (!isset($_GET['type']) || !isset($_GET['electionId'])) {
    msgBox('error', $_SESSION['configValue']['voteEssentials'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

//access control: only admin and voter for this election can get in this page
if (!FacultyElections::isAdmin() && !FacultyElections::isVoter($_GET['electionId'])) {
    msgBox('error', $_SESSION['configValue']['voteAccessDeniedError'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

$election = FacultyElections::getSingleElection($_GET['electionId']);

if ($election['electionStatus'] != 'Semi-Finalist Selection' &&
    $election['electionStatus'] != 'Finalist Selection'
) {
    msgBox('error', $_SESSION['configValue']['voteAccessDeniedError'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

echo "<script>var emptyBallotErrorMsg = '{$_SESSION['configValue']['emptyBallotErrorMsg']}';</script>";
?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->
<script src="js/vote.js"></script>
<script>
	var isNS = (navigator.appName == "Netscape") ? 1 : 0;

	if(navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN||Event.MOUSEUP);

	function mischandler(){
		return false;
	}

	function mousehandler(e){
		var myevent = (isNS) ? e : event;
		var eventbutton = (isNS) ? myevent.which : myevent.button;
		if((eventbutton==2)||(eventbutton==3)) return false;
	}
	document.oncontextmenu = mischandler;
	document.onmousedown = mousehandler;
	document.onmouseup = mousehandler;

</script>
<style>
    #group .instant::after {
        content: " \2261";
    }

    #group .instant {
        float: right;
    }

    .unselectable {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

	.undraggable {
		user-drag: none;
		user-select: none;
		-moz-user-select: none;
		-webkit-user-drag: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
</style>
<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 unselectable undraggable">
    <h2><?php echo $_SESSION['configValue']['votingPageHeader']; ?></h2>
    <div class='col-xs-12'>
        <h3><?php echo $_SESSION['configValue']['votingInstructionheader']; ?></h3>
        <div id="votingInstructions"><?php echo $_SESSION['configValue']['votingPageInstruction']; ?></div>
    </div>
    <div id="ballotContainer" class="col-xs-12 addBorder containerPadding" style="margin-top:20px;">
        <div class='col-xs-12 col-sm-12 col-md-5 col-lg-5'>
            <h4><?php echo $_SESSION['configValue']['votingSelectNomineeHeader']; ?></h4>

            <div class="col-xs-12" style="margin-bottom: 10px;">
                <input id="search" class='form-control unselectable undraggable' name="search" placeholder="Search here" data-toggle="hideseek"
                       type="text" data-list=".data">
            </div>
            <div class="data" style='padding-left: 0px; padding-right: 0px;'>

                <?php
                $nominees = FacultyElections::getNomineesForVote($_GET['electionId'], $_GET['type']);
                $participantRoundType = '';
                switch ($election['electionStatus']) {
                    case 'Finalist Selection':
                        $participantRoundType = 'Finalists Round';
                        break;
                    case 'Semi-Finalist Selection':
                        $participantRoundType = 'Semi-Finalists Round';
                        break;
                }

                if (isset($nominees['error'])) {
                    die($nominees['data']['message']);
                }
                if (count($nominees) == 0) {
                    die($_SESSION['configValue']['voteNoNominees']);
                }
                foreach ($nominees as $nominee) {
                    echo "
								<div data-toggle='buttons' class='col-xs-12 unselectable undraggable' onmousedown='return false'>
									<label class='btn btn-lg btn-default col-xs-12  unselectable undraggable'>
										<input type='checkbox' class='nominees  unselectable undraggable' value='{$nominee['uniqueId']}'>
										<p style='display:inline;font-size: 1em;' class=' unselectable undraggable'>{$nominee['preferredName']} ({$nominee['uniqueId']})</p>
									</label>
								</div>
								";

                }

                ?>

            </div>
        </div>

        <div class='col-xs-12 col-sm-12 col-md-7 col-lg-7 unselectable undraggable' style="padding-left: 0px;padding-right: 0px;">

            <h4 style="padding-left: 15px"><?php echo $_SESSION['configValue']['votingDragDropHeader']; ?></h4>
            <div class="col-xs-12 undraggable unselectable" id='group' style="padding-left:0px; padding-right:0px;">

            </div>

        </div>

        <div class="col-xs-10"></div>
        <div class="col-xs-2">
            <button type='button' class="btn btn-lg btn-default"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing "
                    id="submitBtn"
                    data-electionId="<?php echo htmlspecialchars($_GET['electionId']); ?>"
                    data-type="<?php echo htmlspecialchars($_GET['type']); ?>"
                    data-header="<?php echo $_SESSION['configValue']['votingConfirmationPromptHeader']; ?>"
                    data-body=<?php echo $_SESSION['configValue']['votingConfirmationPromptBody']; ?>">
                Submit
            </button>
        </div>
    </div>
</div>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); ?>