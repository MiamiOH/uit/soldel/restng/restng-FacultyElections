/*
 -----------------------------------------------------------
 FILE NAME: getElection.js

 Copyright (c) 2016 Miami University, All Rights Reserved.

 Miami University grants you ("Licensee") a non-exclusive, royalty free,
 license to use, modify and redistribute this software in source and
 binary code form, provided that i) this copyright notice and license
 appear on all copies of the software; and ii) Licensee does not utilize
 the software in a manner which is disparaging to Miami University.

 This software is provided "AS IS" and any express or implied warranties,
 including, but not limited to, the implied warranties of merchantability
 and fitness for a particular purpose are disclaimed. It has been tested
 and is believed to work as intended within Miami University's
 environment. Miami University does not warrant this software to work as
 designed in any other environment.

 AUTHOR: Mingchao Liao

 DESCRIPTION:  this is js library for index.php

 Create on 07/15/2016
 Written by liaom@miamioh.edu
 */

$(document).ready(function () {
    // move all open election under 'current election' tag
    // move all closed election under 'closed election' tag
    // move all removed election under 'removed election' tag
    reorderElections();

    $('[data-toggle="tooltip"]').tooltip();

    // admin remove an election
    // alert box will be displayed
    $(".removeBtn").click(function (e) {
        var electionId = $(this).prop('name');
        bootbox.dialog({
            message: "Are you sure ?",
            title: "Warning",
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-cancel",
                    callback: function () {

                    }
                },
                danger: {
                    label: "Delete",
                    className: "btn-danger",
                    callback: function () {
                        window.location = "./handler.php?handlerName=removeElection&electionId=" + electionId;
                    }
                }
            }
        });
    });

    // nominee accept/declined an election
    // alert box will be displayed
    $(".nominee-action-btn").click(function (e) {
        var electionId = $(this).attr('data-electionId');
        var header = $(this).attr('data-title');
        var body = $(this).attr('data-body');

        bootbox.dialog({
            message: body,
            title: header,
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-cancel",
                    callback: function () {

                    }
                },
                success: {
                    label: "Accept",
                    className: "btn-success",
                    callback: function () {
                        window.location = "./handler.php?handlerName=acceptance&acceptance=accepted&electionId=" + electionId;
                    }
                },
                danger: {
                    label: "Declined",
                    className: "btn-danger",
                    callback: function () {
                        window.location = "./handler.php?handlerName=acceptance&acceptance=declined&electionId=" + electionId;
                    }
                },
            }
        });
    });
});

// used to put election under correct tag
//  -   open election under 'current election' tag
//  -   closed election under 'closed election' tag
//  -   removed election under 'removed election' tag
function reorderElections() {
    if ($('#currentElections').length) {
        // for admin view
        $(".open").parent().appendTo($("#currentElections"));
        $(".removed").parent().appendTo($("#removedElections"));
    } else {
        // for faculty view
        $(".nominee").parent().appendTo($("#asNominee"));
        $(".voter").parent().appendTo($("#asVoter"));
    }
    $(".closed").parent().appendTo($("#closedElections"));
}