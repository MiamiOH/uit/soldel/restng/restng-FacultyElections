/*
 -----------------------------------------------------------
 FILE NAME: statistics.js

 Copyright (c) 2016 Miami University, All Rights Reserved.

 Miami University grants you ("Licensee") a non-exclusive, royalty free,
 license to use, modify and redistribute this software in source and
 binary code form, provided that i) this copyright notice and license
 appear on all copies of the software; and ii) Licensee does not utilize
 the software in a manner which is disparaging to Miami University.

 This software is provided "AS IS" and any express or implied warranties,
 including, but not limited to, the implied warranties of merchantability
 and fitness for a particular purpose are disclaimed. It has been tested
 and is believed to work as intended within Miami University's
 environment. Miami University does not warrant this software to work as
 designed in any other environment.

 AUTHOR: Mingchao Liao

 DESCRIPTION:  this is js library for statistics.php

 Create on 09/01/2016
 Written by liaom@miamioh.edu
 */

$(document).ready(function () {
    var a = nomineeStat();
    var b = voterVotedInSFR();
    var c = voterVotedInFR();
});

function nomineeStat() {
    return new Chart($('#nomineeStat'),{
        type: 'pie',
        data: {
            labels: [
                "Default",
                "Accepted",
                "Declined",
                "Removed"
            ],
            datasets: [
                {
                    data: [numOfDefaultNominees,numOfAcceptedNominees,numOfDeclinedNominees,numOfRemovedNominees],
                    backgroundColor: [
                        "#36A2EB",
                        "#37eb64",
                        "#ebbe37",
                        "#eb3737"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#37eb64",
                        "#ebbe37",
                        "#eb3737"
                    ]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Figure 1: Nominee Status'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                        return data.labels[tooltipItem.index] + ": " + currentValue + " people (" + precentage + "%)";
                    }
                }
            }
        }
    });
}

function voterVotedInSFR() {
    return new Chart($('#voterVotedInSFR'),{
        type: 'pie',
        data: {
            labels: [
                "Voted",
                "Not Voted",
            ],
            datasets: [
                {
                    data: [numOfVotersVotedInSFR, numOfVoters - numOfVotersVotedInSFR],
                    backgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Figure 2: Voter Voted In Semi-Finalists Round'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                        return data.labels[tooltipItem.index] + ": " + currentValue + " people (" + precentage + "%)";
                    }
                }
            }
        }
    });
}

function voterVotedInFR() {
    return new Chart($('#voterVotedInFR'),{
        type: 'pie',
        data: {
            labels: [
                "Voted",
                "Not Voted",
            ],
            datasets: [
                {
                    data: [numOfVotersVotedInFR, numOfVoters - numOfVotersVotedInFR],
                    backgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Figure 3: Voter Voted In Finalists Round'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                        return data.labels[tooltipItem.index] + ": " + currentValue + " people (" + precentage + "%)";
                    }
                }
            }
        }
    });
}