/*
 -----------------------------------------------------------
 FILE NAME: common.js

 Copyright (c) 2016 Miami University, All Rights Reserved.

 Miami University grants you ("Licensee") a non-exclusive, royalty free,
 license to use, modify and redistribute this software in source and
 binary code form, provided that i) this copyright notice and license
 appear on all copies of the software; and ii) Licensee does not utilize
 the software in a manner which is disparaging to Miami University.

 This software is provided "AS IS" and any express or implied warranties,
 including, but not limited to, the implied warranties of merchantability
 and fitness for a particular purpose are disclaimed. It has been tested
 and is believed to work as intended within Miami University's
 environment. Miami University does not warrant this software to work as
 designed in any other environment.

 AUTHOR: Mingchao Liao

 DESCRIPTION:  this is a collection of javascript function

 Create on 07/15/2016
 Written by liaom@miamioh.edu
*/

$(document).ready(function () {
    var url = window.location;
    // Will only work if string in href matches with location
    $('.navbar-default .navbar-nav > li > a[href="'+ url +'"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('.navbar-default .navbar-nav > li > a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');


    //open MU directory page
    $('#directory-btn').on('click', function (e) {
        e.preventDefault();
        var url = "https://community.miamioh.edu/phpapps/directory/?query_type=simple";

        $("#directoryModalBody").html('<iframe  frameborder="0" scrolling="yes" allowtransparency="true" src="' + url + '" width="100%" style="min-height:90vh;"></iframe>');
    });

    // close 'loading' icon
    if($('#loading')) {
        $('#loading').remove();
    }
});

//open a alert box
function bootboxAlert(msg, redirect) {

    if (redirect == '') { // stay in current page

        bootbox.alert('<h3>'+msg+'</h3>', function () {
        });


    } else { // redirect to specified page, default is home page
        bootbox.alert('<h3>'+msg+'</h3>', function () {
            window.location = './' + redirect;
        });
    }
}
