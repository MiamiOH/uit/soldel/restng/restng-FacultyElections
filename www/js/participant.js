/*
 -----------------------------------------------------------
 FILE NAME: participant.js

 Copyright (c) 2016 Miami University, All Rights Reserved.

 Miami University grants you ("Licensee") a non-exclusive, royalty free,
 license to use, modify and redistribute this software in source and
 binary code form, provided that i) this copyright notice and license
 appear on all copies of the software; and ii) Licensee does not utilize
 the software in a manner which is disparaging to Miami University.

 This software is provided "AS IS" and any express or implied warranties,
 including, but not limited to, the implied warranties of merchantability
 and fitness for a particular purpose are disclaimed. It has been tested
 and is believed to work as intended within Miami University's
 environment. Miami University does not warrant this software to work as
 designed in any other environment.

 AUTHOR: Mingchao Liao

 DESCRIPTION:  this is js library for participant.php

 Create on 07/15/2016
 Written by liaom@miamioh.edu
*/
$(document).ready(function () {
    // open modal to allow admin to add a nominee
    //  -   uniqueId
    //  -   acceptance: default, accepted, declined, removed
    //  -   nominee status: '', Nominee, Semi-Finalist, Finalist, Winner
    $('#addNomineeBtn').click(function() {
        $('#addParticipantTitle,#addParticipantSubmitBtn').text('Add Nominee');
        $('.notForVoter').show();
        $('.notForVoterInput').prop('disabled', false);
    });

    $('#voter-table').tablesorter();
    $('#nominee-table').tablesorter();

    // open modal to allow admin to add a voter
    //  -   uniqueId
    $('#addVoterBtn').click(function() {
        $('#addParticipantTitle,#addParticipantSubmitBtn').text('Add Voter');
        $('.notForVoter').hide();
        $('.notForVoterInput').prop('disabled', true);
    });

    $('#addParticipantSubmitBtn').click(function() {
        $('#addParticipantForm').submit();
    });

    // handle removing a voter
    // alert box will be displayed
    $('.removeVoterBtn').click(function() {
        var uniqueId = $(this).attr('data-uniqueid');
        bootbox.dialog({
            message: "<h3>Are you sure ?</h3>",
            title: "Remove Voter",
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-cancel",
                    callback: function () {

                    }
                },
                danger: {
                    label: "Remove",
                    className: "btn-danger",
                    callback: function () {
                        window.location = 'handler.php?electionId=' + electionId
                            + '&uniqueId=' + uniqueId
                            + '&handlerName=deleteParticipant&memberType=voter';
                    }
                }
            }
        });
    });


    // this is for admin to set acceptance for nominee
    //      blue: default
    //      green: accepted
    //      yellow: declined
    //      red: removed
    $(".selectbox-nominee-action").change(function () {
        var uniqueId = $(this).attr('data-uniqueid');
        var newSelectedState = $(this).val();
        if (newSelectedState == 'removed') {
            bootbox.dialog({
                message: "<h3>Are you sure you want to remove " + uniqueId + "?</h3>",
                title: "Remove Nominee",
                buttons: {
                    cancel: {
                        label: "Cancel",
                        className: "btn-cancel",
                        callback: function () {

                        }
                    },
                    danger: {
                        label: "Remove",
                        className: "btn-danger",
                        callback: function () {
                            window.location = 'handler.php?electionId=' + electionId
                                + '&uniqueId=' + uniqueId
                                + '&handlerName=deleteParticipant&memberType=nominee';
                        }
                    }
                }
            });
        } else {
            window.location = 'handler.php?electionId=' + electionId
                + '&uniqueId=' + uniqueId
                + '&acceptance=' + newSelectedState
                + '&handlerName=modifyNomineeAcceptance';
        }

    });
});


// use chart.js library to create a pie chart
// to show nominee status
function nomineePieChart(data) {
    return new Chart($('#nomineePieChart'),{
        type: 'pie',
        data: {
            labels: [
                "Default",
                "Accepted",
                "Declined",
                "Removed"
            ],
            datasets: [
                {
                    data: data,
                    backgroundColor: [
                        "#36A2EB",
                        "#37eb64",
                        "#ebbe37",
                        "#eb3737"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#37eb64",
                        "#ebbe37",
                        "#eb3737"
                    ]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Nominee Status'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                        return data.labels[tooltipItem.index] + ": " + currentValue + " people (" + precentage + "%)";
                    }
                }
            }
        }
    });
}

function createPieChart(name, title, data) {
    return new Chart($('#' + name),{
        type: 'pie',
        data: {
            labels: [
                "Voted",
                "Not Voted",
            ],
            datasets: [
                {
                    data: data,
                    backgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#DDDDDD"
                    ]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: title
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                        return data.labels[tooltipItem.index] + ": " + currentValue + " people (" + precentage + "%)";
                    }
                }
            }
        }
    });
}