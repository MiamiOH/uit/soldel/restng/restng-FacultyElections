/*
 -----------------------------------------------------------
 FILE NAME: vote.js

 Copyright (c) 2016 Miami University, All Rights Reserved.

 Miami University grants you ("Licensee") a non-exclusive, royalty free,
 license to use, modify and redistribute this software in source and
 binary code form, provided that i) this copyright notice and license
 appear on all copies of the software; and ii) Licensee does not utilize
 the software in a manner which is disparaging to Miami University.

 This software is provided "AS IS" and any express or implied warranties,
 including, but not limited to, the implied warranties of merchantability
 and fitness for a particular purpose are disclaimed. It has been tested
 and is believed to work as intended within Miami University's
 environment. Miami University does not warrant this software to work as
 designed in any other environment.

 AUTHOR: Mingchao Liao

 DESCRIPTION:  this is js library for vote.php

 Create on 07/15/2016
 Written by liaom@miamioh.edu


 */

// this is used to calculate index for conformation page
// displaying on voter trying to submit ballots
function calIndex() {
    var index = 1;
    $(".badge").each(function () {
        $(this).html(index);
        index++;
    });
}

$(document).ready(function () {
    $('.nominees').change(function () {
        if ($(this).prop("checked") == true) {

            $('#group').append("<div  onmousedown='return false' class='col-xs-12  unselectable undraggable demo-no-swipe' style='padding-left:0px; padding-right:0px; margin-bottom: 5px;'> <a class='col-xs-1 btn btn-lg  unselectable undraggable'><span class='badge  unselectable undraggable'>0</span></a> <button style='margin-bottom:0px;font-size: 1em;' class='col-xs-8 btn btn-lg btn-default selectedNominees unselectable undraggable' id='" + $(this).val() + "'>" + $(this).parent().children('p').text() + "<span class='instant'> </span></button> <a class='col-xs-1 btn btn-lg btn-default up'><i class='fa fa-arrow-up'></i></a> <a class='col-xs-1 btn btn-lg btn-default down'><i class='fa fa-arrow-down'></i></a> <a class='col-xs-1 btn btn-lg btn-default delete'><i class='fa fa-minus'></i></a> </div>");

            //remove nominee from 'rank' side (right-side)
            $(".delete").off('click').click(function () {
                var id = $(this).parent().children('button').prop('id');
                uncheckLeft(id);
                $(this).parent().remove();
                calIndex();
            });

            // move up a nominee in 'rank' side
            $(".up").off('click').click(function () {
                $(this).parent().insertBefore($(this).parent().prev());
                calIndex();
            });

            // move down a nominee in 'rank' side
            $(".down").off('click').click(function () {
                $(this).parent().insertAfter($(this).parent().next());
                calIndex();
            });
        } else {
            $('#' + $(this).val()).parent().remove();
        }

        calIndex();
    });

    var ol = document.getElementById('group');
    ol.addEventListener('slip:beforereorder', function (e) {

        if (/demo-no-reorder/.test(e.target.className)) {
            e.preventDefault();
        }
    }, false);

    ol.addEventListener('slip:beforeswipe', function (e) {
        if (e.target.nodeName == 'INPUT' || /demo-no-swipe/.test(e.target.className)) {
            e.preventDefault();
        }
    }, false);

    ol.addEventListener('slip:beforewait', function (e) {
        if (e.target.className.indexOf('instant') > -1) e.preventDefault();
    }, false);

    ol.addEventListener('slip:afterswipe', function (e) {
        uncheckLeft(e.target.querySelector('button').id);
        //uncheckLeft(e.target.attributes("id"));
        e.target.remove();
        calIndex();
    }, false);

    ol.addEventListener('slip:reorder', function (e) {
        e.target.parentNode.insertBefore(e.target, e.detail.insertBefore);
        calIndex();
        return false;
    }, false);

    new Slip(ol);

    $('#submitBtn').click(function (e) {
        if ($('#group').children().length == 0) {
            bootboxAlert(emptyBallotErrorMsg, '');
            return;
        }

        var electionId = $(this).attr('data-electionId');
        var type = $(this).attr('data-type');
        var header = $(this).attr('data-header');
        var body = $(this).attr('data-body');

        var confirmList = [];
        var uniqueIds = [];

        $('.selectedNominees').each(function () {
            confirmList.push($(this).text());
            uniqueIds.push($(this).prop('id'));
        });

        var confirmationMsg = "<h4>" + body + "</h4><h4><span class='badge'>1</span>&nbsp;&nbsp;" + confirmList[0] + "</h4>";
        for (var i = 1; i < confirmList.length; i++) {
            confirmationMsg += "<h4><span class='badge'>" + (i + 1) + "</span>&nbsp;&nbsp;" + confirmList[i] + "</h4>";
        }

        bootbox.dialog({
            message: confirmationMsg,
            title: header,
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-cancel",
                    callback: function () {
                    }
                },
                success: {
                    label: "Submit",
                    className: "btn-success",
                    callback: function () {
                        //$this.button('loading');
                        var reqData = {};
                        reqData.electionId = electionId;
                        reqData.roundTypeName = type;
                        reqData.ballots = uniqueIds;

                        var form = $("<form action='handler.php?handlerName=submitBallots' method='POST'>" +
                            "<input type='text' name='electionId' value='" + electionId + "' />" +
                            "<input type='text' name='roundTypeName' value='" + type + "' />" +
                            "<input type='text' name='ballots' value='" + uniqueIds + "' /></form>");
                        $(document.body).append(form);
                        form.submit();


                        // $.ajax({
                        //     url: "./handler/callBallotHelper.php",
                        //     type: "POST",
                        //     data: JSON.stringify(reqData),
                        //     contentType: "application/json",
                        //     complete: function(res) {
                        //         res = res.responseJSON;
                        //         if(res.error) {
                        //             bootbox.alert("<h4>"+res.data.message+"</h4>", function() {});
                        //         } else {
                        //             $this.button('reset');
                        //             bootbox.alert("vote successfully !", function() {
                        //                 window.location = "./index.php";
                        //             });
                        //         }
                        //     }
                        // });
                    }
                }
            }
        });
    });
});

function uncheckLeft(id) {
    $(".data input").each(function () {
        if ($(this).prop("value") == id) {
            $(this).prop('checked', false);
            $(this).parent().removeClass('active');
        }
    });
}