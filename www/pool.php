<!--
-----------------------------------------------------------
FILE NAME: pool.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: used to view pool (criteria group)
 - admin ONLY

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header('Criteria Group'); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

if (!FacultyElections::isAdmin()) {
	msgBox('error', $_SESSION['configValue']['postHandlerErrorPermission'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->

<style>
	@media (max-width: 600px) {
		.criteriaOptions {
			display: none;
		}
	}
</style>

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->

<div class="col-xs-12" style="padding-bottom:20px;">
	<a class="btn btn-lg btn-default" href="./poolMgr.php?type=createPool">Create Criteria Group</a>
</div>

<?php
$criteriaGroups = FacultyElections::getCriteriaGroup();
if(isset($criteriaGroups['error'])) {
	msgBox('error', $criteriaGroups['data']['message'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
foreach($criteriaGroups as $criteriaGroup) {
	echo "
	<div class='col-xs-12'>
		<div class='panel panel-default'>
			<div class='panel-heading'>
				<div class='panel-title pull-left'>
					<a class='collapse-header' data-toggle='collapse' data-parent='#accordion' href='#{$criteriaGroup['poolId']}' style='text-decoration: none'>
						{$criteriaGroup['poolName']}
					</a>
					";
	foreach($criteriaGroup['criteria'] as $criteria) {
		$criteriaOptionName = array_values($criteria['options'])[0]['criteriaOptionName'];
		echo "
						<span class='label label-info criteriaOptions'>{$criteriaOptionName}</span>
		";
	}
	echo "
				</div>
				<div class='panel-title pull-right'>
					<a class='btn btn-default' href='poolMgr.php?type=modifyPool&poolId={$criteriaGroup['poolId']}'><i class='fa fa-pencil' style='font-size: 140%;'></i></a>
				</div>
				<div class='clearfix'></div>
			</div>
			<div id='{$criteriaGroup['poolId']}' class='panel-collapse collapse'>
				<div class='panel-body'>
	";
	if(count($criteriaGroup['criteria']) == 0) {
		echo "
					<div>
						<h5>No Criteria</h5>
					</div>
		";
	} else {
		makeTable($criteriaGroup);
	}
	echo "
				</div>
			</div>
		</div>
	</div>
	";
}

function makeTable($criteriaGroup) {
	echo "
	<div id='no-more-tables'>
					<table class='col-md-12 table-bordered table-striped table-condensed cf' style='padding-right: 0px; padding-left: 0px;'>
						<thead class='cf'>
							<tr>
								<th>Criteria</th>
								<th>Criteria Option</th>
							</tr>
						</thead>
						<tbody>
	";
	//panel body start
	foreach($criteriaGroup['criteria'] as $criteriaId => $criteria) {
		$criteriaOptionName = array_values($criteria['options'])[0]['criteriaOptionName'];
		echo "
							<tr>
								<td data-title='Criteria'>{$criteria['criteriaName']}</td>
								<td data-title='Criteria Option' class='criteriaOptionName'>{$criteriaOptionName}</td>
							</tr>
		";
	}
	//panel body end
	echo "
						</tbody>
					</table>
				</div>
	";
}

?>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
