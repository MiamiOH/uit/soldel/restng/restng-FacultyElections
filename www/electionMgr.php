<!--
-----------------------------------------------------------
FILE NAME: electionMgr.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: used to manage election including creating and modifying
 - used by admin ONLY

Create on 06/05/2016
Written by liaom@miamioh.edu

Author:      patelah
Description: Added the code to insert the division for UI.
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header(($_GET['type'] == 'createElection' ? "Create Election" : "Modify Election")); // DO NOT MODIFY THIS LINE

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

if (!FacultyElections::isAdmin()) {
	msgBox('error', "Error: you do not have permission to do this", array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->

<script>
	$(document).ready(function () {
		$("#division").change(function() {
			if($(this).find(":selected").val() == undefined) {
				$("#department").prop('disabled', false);
			} else {
				$("#department").prop('disabled', true);
			}
		});

		$("#department").change(function() {
			if($(this).find(":selected").val() == undefined) {
				$("#division").prop('disabled', false);
			} else {
				$("#division").prop('disabled', true);
			}
		});
	});
</script>

<?php

//url parameters:
//require:
//  GET: type, electionId
//  POST
if ($_SERVER['REQUEST_METHOD'] == 'GET') { // GET method is for modify election
	if (!isset($_GET['type'])) {
		msgBox('error', $_SESSION['configValue']['electionMgrMissingType'], array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		));
	}
	if (!in_array($_GET['type'], ['createElection', 'modifyElection', 'Self-Nomination Round', 'Semi-Finalists Round', 'Finalists Round', 'countingBallotsInSFR', 'countingBallotsInFR'])) {
		msgBox('error', $_SESSION['configValue']['electionMgrInvalidType'], array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		));
	}
	if ($_GET['type'] == 'modifyElection' && !isset($_GET['electionId'])) {
		msgBox('error', $_SESSION['configValue']['electionMgrMissingId'], array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		));
	}

	if ($_GET['type'] == 'modifyElection') {
		$currentElection = FacultyElections::getSingleElectionForModifying($_GET['electionId']);
		if(isset($currentElection['error'])) {
			msgBox('error', $currentElection['data']['message'], array(
				array('label' => 'Back to home page', 'url' => './index.php'),
			));
		}

		echo "<script>var electionId = {$currentElection['electionId']};</script>";
	}
}

$electionId = $_GET['type'] == 'modifyElection' ? $currentElection['electionId'] : "";
$electionName = $_GET['type'] == 'modifyElection' ? $currentElection['electionName'] : "";
$numOfWinners = $_GET['type'] == 'modifyElection' ? $currentElection['numOfWinners'] : "";
$numOfSemiFinalists = $_GET['type'] == 'modifyElection' ? $currentElection['numOfSemiFinalists'] : "";
$selectDivisions = $_GET['type'] == 'modifyElection' ? $currentElection['divisionFilterValue']:"";
$selectDepartment = $_GET['type'] == 'modifyElection' ? $currentElection['departmentFilterValue']:"";

//var_dump($selectDepartment);

//get pool id and name
$pools = FacultyElections::getPoolIdName();
$nomineeOptionString = "<option value=''>Please select nominee pool</option>";
$voterOptionString = "<option value=''>Please select voter pool</option>";
if (isset($pools['error'])) {
	msgBox('error', $pools['data']['message'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
//create dropdown list string for nominee pool and voter pool
foreach ($pools as $pool) {
	$nomineeOptionString .= "<option value='{$pool['poolId']}' " . ($currentElection['nomineePoolId'] == $pool['poolId'] ? "selected" : "") . ">{$pool['poolName']}</option>";
	$voterOptionString .= "<option value='{$pool['poolId']}' " . ($currentElection['voterPoolId'] == $pool['poolId'] ? "selected" : "") . ">{$pool['poolName']}</option>";
}

$divisions=FacultyElections::getDivision();

$divOptionString = "";
if (isset($divisions['error'])) {
	msgBox('error', $divisions['data']['message'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
//create dropdown list string for division and sort
sort($divisions);
$selDivArray = explode(',',$selectDivisions);
foreach ($divisions as $division) {
	if(in_array($division['divisionCode'], $selDivArray)){
		$divOptionString .= "<option value='{$division['divisionCode']}' selected>{$division['divisionCode']}: {$division['divisionDescription']}</option>";
	}
	else{
		$divOptionString .= "<option value='{$division['divisionCode']}' >{$division['divisionCode']}: {$division['divisionDescription']}</option>";
	}
}

$departments=FacultyElections::getDepartment();

$deptOptionString = "";
if (isset($departments['error'])) {
	msgBox('error', $departments['data']['message'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}
//create dropdown list string for department and sort
sort($departments);
$selDeptArray = explode(',',$selectDepartment);
foreach ($departments as $department) {
	if(in_array($department['departmentCode'], $selDeptArray)){
		$deptOptionString .= "<option value='{$department['departmentCode']}' selected>{$department['departmentCode']}: {$department['departmentName']}</option>";
	} else {
		$deptOptionString .= "<option value='{$department['departmentCode']}' >{$department['departmentCode']}: {$department['departmentName']}</option>";
	}
}
?>

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->

<form class='col-sm-12 form-horizontal' id='createElectionForm' method='POST'
      action='handler.php?handlerName=<?php echo urlencode($_GET['type']) . (isset($_GET['electionId']) ? "&electionId=" . urlencode($_GET['electionId']) : ""); ?>'>

	<?php if($_GET['type'] == 'modifyElection') echo "<input type='text' name='electionId' value='{$electionId}' hidden/>"; ?>
	<div class='form-group row'>
		<label for='electionName' class='col-sm-2 form-control-label'>Name:</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' id='electionName' name='electionName' placeholder='Election Name' value="<?php echo $electionName; ?>">
		</div>
	</div>
	<div class='form-group'>
		<label for='nomineePoolId' class='col-sm-2 form-control-label'>Nominee
			Pool</label>
		<div class='col-sm-10 selectContainer'>
			<select id='nomineePoolId' name='nomineePoolId' class='form-control'>
				<?php echo $nomineeOptionString; ?>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label for='voterPoolId' class='col-sm-2 form-control-label'>Voter
			Pool</label>
		<div class='col-sm-10 selectContainer'>
			<select id='voterPoolId' name='voterPoolId' class='form-control'>
				<?php echo $voterOptionString; ?>
			</select>
		</div>
	</div>
	<div class='form-group row'>
		<label for='numOfWinners' class='col-sm-2 form-control-label'>Number
			Of Winners:</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' id='numOfWinners' name='numOfWinners' placeholder='Number Of Winners' value="<?php echo $numOfWinners; ?>">
		</div>
	</div>
	<div class='form-group row'>
		<label for='numOfSemiFinalists' class='col-sm-2 form-control-label'>Number
			Of Semi-Finalists:</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' id='numOfSemiFinalists' name='numOfSemiFinalists' placeholder='Number Of Semi-Finalists' value="<?php echo $numOfSemiFinalists; ?>">
		</div>
	</div>



	<div class='form-group'>
		<label for='divisionId' class='col-sm-2 form-control-label'>Select Division:</label>
		<div class='col-sm-10 selectContainer'>
			<select id='division' name='divisionFilterValue[]' class='form-control selectpicker' data-live-search="true" data-none-selected-text="Please select division(s)" <?php if ($_GET['type'] == 'modifyElection') echo 'disabled';?> multiple>
				<?php echo $divOptionString; ?>
			</select>
		</div>
	</div>

	<div class='form-group'>
		<label class='col-sm-1 form-control-label'>&nbsp;&nbsp;&nbsp;OR</label>
	</div>

	<div class='form-group'>
		<label for='departmentId' class='col-sm-2 form-control-label'>Select Department:</label>
		<div class='col-sm-10 selectContainer'>
			<select id='department' name='departmentFilterValue[]' class='form-control selectpicker' data-live-search="true" data-none-selected-text="Please select department(s)" <?php if ($_GET['type'] == 'modifyElection') echo 'disabled';?> multiple>
				<?php echo $deptOptionString; ?>
			</select>
		</div>
	</div>


	<div class='form-group row'>
		<div class='col-sm-offset-2 col-sm-10'>
            <button type='submit' id='createElectionFormSubmit' class='btn btn-primary btn-responsive'>
				<?php echo ($_GET['type'] == 'createElection' ? "Create Election" : "Modify Election"); ?>
			</button>
		</div>
	</div>
</form>

<?php
if($_GET['type'] == 'modifyElection') {
	$firstRoundStart = 'disabled';
	$secondRoundStart = 'disabled';
	$thirdRoundStart = 'disabled';
	$firstRoundEnd = 'disabled';
	$secondRoundEnd = 'disabled';
	$thirdRoundEnd = 'disabled';
	if(!isset($currentElection['votingRounds'])) {
		$firstRoundStart = '';
		$firstRoundStatus = 'Not Started';
		$secondRoundStatus = 'Not Started';
		$thirdRoundStatus = 'Not Started';
	} elseif(isset($currentElection['votingRounds']['Self-Nomination Round']) && $currentElection['votingRounds']['Self-Nomination Round']['startDate'] != 'none' && $currentElection['votingRounds']['Self-Nomination Round']['endDate'] == 'none') {
		$firstRoundEnd = '';
		$firstRoundStatus = 'In Progress';
		$secondRoundStatus = 'Not Started';
		$thirdRoundStatus = 'Not Started';
	} elseif(isset($currentElection['votingRounds']['Self-Nomination Round']) && $currentElection['votingRounds']['Self-Nomination Round']['startDate'] != 'none' && $currentElection['votingRounds']['Self-Nomination Round']['endDate'] != 'none' && !isset($currentElection['votingRounds']['Semi-Finalists Round'])) {
		$secondRoundStart = '';
		$firstRoundStatus = 'Closed';
		$secondRoundStatus = 'Not Started';
		$thirdRoundStatus = 'Not Started';
	} elseif(isset($currentElection['votingRounds']['Semi-Finalists Round']) && $currentElection['votingRounds']['Semi-Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Semi-Finalists Round']['endDate'] == 'none') {
		$secondRoundEnd = '';
		$firstRoundStatus = 'Closed';
		$secondRoundStatus = 'In Progress';
		$thirdRoundStatus = 'Not Started';
	} elseif(isset($currentElection['votingRounds']['Semi-Finalists Round']) && $currentElection['votingRounds']['Semi-Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Semi-Finalists Round']['endDate'] != 'none' && !isset($currentElection['votingRounds']['Finalists Round'])) {
		$thirdRoundStart = '';
		$firstRoundStatus = 'Closed';
		$secondRoundStatus = 'Closed';
		$thirdRoundStatus = 'Not Started';
	} elseif(isset($currentElection['votingRounds']['Finalists Round']) && $currentElection['votingRounds']['Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Finalists Round']['endDate'] == 'none') {
		$thirdRoundEnd = '';
		$firstRoundStatus = 'Closed';
		$secondRoundStatus = 'Closed';
		$thirdRoundStatus = 'In Progress';
	} else {
		$firstRoundStatus = 'Closed';
		$secondRoundStatus = 'Closed';
		$thirdRoundStatus = 'Closed';
	}

	echo "
	<div class='col-xs-12'>
		<hr>
		<h3>Round Management</h3>
		<div id='no-more-tables'>
			<table class='col-md-12 table-bordered table-striped table-condensed cf' style='padding-right: 0px; padding-left: 0px;'>
				<thead class='cf'>
				<tr>
					<th>Round Type Name</th>
					<th>Start</th>
					<th>End</th>
					<th>Status</th>
				</tr>
				</thead>
				<tbody>
					<tr>
						<td data-title='Round Type Name'>Self-Nomination Round</td>
						<td data-title='Start'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Self-Nomination Round&action=start&electionId={$currentElection['electionId']}' {$firstRoundStart}>Start</a></td>
						<td data-title='End'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Self-Nomination Round&action=end&electionId={$currentElection['electionId']}' {$firstRoundEnd}>End</a></td>
						<td data-title='Status'>{$firstRoundStatus}</td>
					</tr>
					<tr>
						<td data-title='Round Type Name'>Semi-Finalists Round</td>
						<td data-title='Start'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Semi-Finalists Round&action=start&electionId={$currentElection['electionId']}' {$secondRoundStart}>Start</a></td>
						<td data-title='End'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Semi-Finalists Round&action=end&electionId={$currentElection['electionId']}' {$secondRoundEnd}>End</a></td>
						<td data-title='Status'>{$secondRoundStatus}</td>
					</tr>
					<tr>
						<td data-title='Round Type Name'>Finalists Round</td>
						<td data-title='Start'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Finalists Round&action=start&electionId={$currentElection['electionId']}' {$thirdRoundStart}>Start</a></td>
						<td data-title='End'><a class='btn btn-success btn-responsive ' href='handler.php?handlerName=roundMgr&type=Finalists Round&action=end&electionId={$currentElection['electionId']}' {$thirdRoundEnd}>End</a></td>
						<td data-title='Status'>{$thirdRoundStatus}</td>	
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	";
}
?>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); // DO NOT MODIFY THIS LINE ?>
