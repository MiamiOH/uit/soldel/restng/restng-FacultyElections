<!--
-----------------------------------------------------------
FILE NAME: statistics.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: This is used to generate a statistics report
	after an election being closed

This file is initial version
Create on 08/10/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header($_SESSION['configValue']['statisReport']); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

if (!FacultyElections::isAdmin()) {
	msgBox('error', $_SESSION['configValue']['postHandlerErrorPermission'], array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	));
}

if (!isset($_GET['electionId'])) {
    msgBox('error', $_SESSION['configValue']['statisMissingId'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

//get election info
$election = FacultyElections::getSingleElection($_GET['electionId']);
if (isset($election['error'])) {
    msgBox('error', $_SESSION['configValue']['statisticsElectionInformationFailure'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

if ($election['electionStatus'] != 'Closed') {
    msgBox('error', $_SESSION['configValue']['statisticsElectionNotClosed'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

$participants = FacultyElections::getParticipantByType('', $_GET['electionId']);

if (isset($participants['error'])) {
    msgBox('error', $_SESSION['configValue']['statisticsParticipantInforFailure'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

$numOfNominees = 0;
$numOfAcceptedNominees = 0;
$numOfDefaultNominees = 0;
$numOfDeclinedNominees = 0;
$numOfRemovedNominees = 0;
$numOfVoters = 0;
$numOfVotersVotedInSFR = 0;
$numOfVotersVotedInFR = 0;

foreach ($participants as $participant) {
    if ($participant['memberTypeName'] == 'Voter') {
        $numOfVoters++;
        if ($participant['voted']['Semi-Finalists Round']) {
            $numOfVotersVotedInSFR++;
        }
        if ($participant['voted']['Finalists Round']) {
            $numOfVotersVotedInFR++;
        }
    } elseif ($participant['memberTypeName'] == 'Nominee') {
        $numOfNominees++;
        if ($participant['nominationAcceptance'] == 'default') {
            $numOfDefaultNominees++;
        } elseif ($participant['nominationAcceptance'] == 'accepted') {
            $numOfAcceptedNominees++;
        } elseif ($participant['nominationAcceptance'] == 'declined') {
            $numOfDeclinedNominees++;
        } elseif ($participant['nominationAcceptance'] == 'removed') {
            $numOfRemovedNominees++;
        }
    }
}

//get criteria options for each pool: voter, nominee
$criteriaGroups = FacultyElections::getCriteriaGroup($election['nomineePoolId'] . ',' . $election['voterPoolId']);
$nomineePoolCriteriaStr = '';
$voterPoolCriteriaStr = '';
if (isset($criteriaGroups['error'])) {
    $nomineePoolCriteriaStr = 'Unable to load data';
    $voterPoolCriteriaStr = 'Unable to load data';
} else {
    foreach ($criteriaGroups as $criteriaGroup) {
        if ($criteriaGroup['poolId'] == $election['nomineePoolId']) {
            $isFirstRow = true;
            foreach ($criteriaGroup['criteria'] as $criteria) {
                if (!$isFirstRow) {
                    $nomineePoolCriteriaStr .= '<br>';
                }
                $nomineePoolCriteriaStr .= "{$criteria['criteriaName']}: ";
                foreach ($criteria['options'] as $option) {
                    $nomineePoolCriteriaStr .= $option['criteriaOptionName'];
                }
                $isFirstRow = false;
            }
        }

        if ($criteriaGroup['poolId'] == $election['voterPoolId']) {
            $isFirstRow = true;
            foreach ($criteriaGroup['criteria'] as $criteria) {
                if (!$isFirstRow) {
                    $voterPoolCriteriaStr .= '<br>';
                }
                $voterPoolCriteriaStr .= "{$criteria['criteriaName']}: ";
                foreach ($criteria['options'] as $option) {
                    $voterPoolCriteriaStr .= $option['criteriaOptionName'];
                }
                $isFirstRow = false;
            }
        }
    }
}

?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->

<?php

echo "
<script>
	var numOfNominees = {$numOfNominees};
	var numOfAcceptedNominees = {$numOfAcceptedNominees};
	var numOfDefaultNominees = {$numOfDefaultNominees};
	var numOfDeclinedNominees = {$numOfDeclinedNominees};
	var numOfRemovedNominees = {$numOfRemovedNominees};
	var numOfVoters = {$numOfVoters};
	var numOfVotersVotedInSFR = {$numOfVotersVotedInSFR};
	var numOfVotersVotedInFR = {$numOfVotersVotedInFR};
</script>
";
?>

<script src="js/statistics.js"></script>

<div class="col-xs-12">
    <div class="col-xs-12">
        <h2 class="col-xs-12 text-center">
            <?php echo $_SESSION['configValue']['statisticsHeader'] . $election['electionName']; ?></h2>
        <div class="col-xs-12">
            <h3><?php echo $_SESSION['configValue']['statisticsElectionInformationHeader']; ?></h3>
            <div id='electionInfo' class="col-xs-12 addBorder containerPadding">
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>ID: </strong> <?php echo $election['electionId'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Election Name:</strong> <?php echo $election['electionName'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Status:</strong> <?php echo $election['electionStatus'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Nominee Pool:</strong> <?php echo $election['nomineePoolName'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Voter Pool:</strong> <?php echo $election['voterPoolName'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Number of Semi-Finalists:</strong> <?php echo $election['numOfSemiFinalists'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Number of Winners:</strong> <?php echo $election['numOfWinners'] ?>
                </div>
                <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>
                    <strong>Date Created:</strong> <?php echo $election['dateCreated'] ?>
                </div>
            </div>

            <div class="col-xs-12" style="margin-top: 15px;" id="criteraContainer">
                <h3><?php echo $_SESSION['configValue']['statisticsCriteraGroupHeader']; ?></h3>
                <div id='criteraInfo' class="col-xs-12 addBorder" style="padding:10px">

                    <?php
                    createResponsiveTable(array(
                        'Type' => ['Nominee Criteria Group', ' Voter Criteria Group'],
                        'Criteria Group ID' => [$election['nomineePoolId'], $election['voterPoolId']],
                        'Criteria Group Name' => [$election['nomineePoolName'], $election['voterPoolName']],
                        'Criteria(s)' => [$nomineePoolCriteriaStr, $voterPoolCriteriaStr],
                    ));
                    ?>
                </div>
            </div>

            <div class="col-xs-12" id="roundContainer" style="margin-top: 15px;">
                <h3><?php echo $_SESSION['configValue']['statisticsRoundHeader']; ?></h3>
                <div id='roundInformation' class="col-xs-12 addBorder" style="padding:10px">

                    <?php
                    createResponsiveTable(array(
                        'Round Type' => [
                            'Self-Nomination Round',
                            'Semi-Finalists Round',
                            'Finalists Round',
                        ],
                        'Start Date' => [
                            $election['votingRounds']['Self-Nomination Round']['startDate'],
                            $election['votingRounds']['Semi-Finalists Round']['startDate'],
                            $election['votingRounds']['Finalists Round']['startDate'],
                        ],
                        'End Date' => [
                            $election['votingRounds']['Self-Nomination Round']['endDate'],
                            $election['votingRounds']['Semi-Finalists Round']['endDate'],
                            $election['votingRounds']['Finalists Round']['endDate'],
                        ],
                        'Duration' => [
                            date_diff(date_create($election['votingRounds']['Self-Nomination Round']['startDate']),
                                date_create($election['votingRounds']['Self-Nomination Round']['endDate']))->format("%D Days %H Hours %I Minutes %S Seconds"),
                            date_diff(date_create($election['votingRounds']['Semi-Finalists Round']['startDate']),
                                date_create($election['votingRounds']['Semi-Finalists Round']['endDate']))->format("%D Days %H Hours %I Minutes %S Seconds"),
                            date_diff(date_create($election['votingRounds']['Finalists Round']['startDate']),
                                date_create($election['votingRounds']['Finalists Round']['endDate']))->format("%D Days %H Hours %I Minutes %S Seconds"),
                        ],
                    ));
                    ?>
                </div>
            </div>

            <div class="col-xs-12" id="nominationContainer" style="margin-top: 15px;">
                <h3><?php echo $_SESSION['configValue']['statisticsNominationHeader']; ?></h3>
                <div class="col-xs-12 addBorder" style="padding:15px">
                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <p>
                                <strong>
                                    <?php echo $_SESSION['configValue']['statisticsNumberOfNomineeLabel']; ?>
                                </strong>
                                <?php echo $numOfNominees; ?>
                            </p>

                            <p>
                                <strong>
                                    <?php echo $_SESSION['configValue']['statisticsNomineeDefaultLabel']; ?>
                                </strong>
                                <?php echo $numOfDefaultNominees . ' (' . floor((($numOfDefaultNominees / $numOfNominees) * 100) + 0.5) . '%)'; ?>
                            </p>
                            <p>
                                <strong>
                                    <?php echo $_SESSION['configValue']['statisticsNomineeAcceptedLabel']; ?>
                                </strong>
                                <?php echo $numOfAcceptedNominees . ' (' . floor((($numOfAcceptedNominees / $numOfNominees) * 100) + 0.5) . '%)'; ?>
                            </p>
                            <p>
                                <strong>
                                    <?php echo $_SESSION['configValue']['statisticsNomineeDeclinedLabel']; ?>
                                </strong>
                                <?php echo $numOfDeclinedNominees . ' (' . floor((($numOfDeclinedNominees / $numOfNominees) * 100) + 0.5) . '%)'; ?>
                            </p>
                            <p>
                                <strong>
                                    <?php echo $_SESSION['configValue']['statisticsNomineeRemovedLabel']; ?>
                                </strong>
                                <?php echo $numOfRemovedNominees . ' (' . floor((($numOfRemovedNominees / $numOfNominees) * 100) + 0.5) . '%)'; ?>
                            </p>
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <canvas id='nomineeStat'>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12" id="votingContainer" style="margin-top: 15px;">
                <h3><?php echo $_SESSION['configValue']['statisticsVotingHeader']; ?></h3>
                <div class="col-xs-12" id="semiFinalistContainer" style="margin-top: 15px;">
                    <h4><?php echo $_SESSION['configValue']['statisticsSemiFinalistHeader']; ?></h4>
                    <div class="col-xs-12 addBorder" style="padding:15px">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsTotalNumberVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVoters; ?>
                                </p>
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsNumberCastedVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVotersVotedInSFR . ' (' . floor((($numOfVotersVotedInSFR / $numOfVoters) * 100) + 0.5) . '%)' ?>
                                </p>
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsNumberUncastedVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVoters - $numOfVotersVotedInSFR . ' (' . floor(((($numOfVoters - $numOfVotersVotedInSFR) / $numOfVoters) * 100) + 0.5) . '%)' ?>
                                </p>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <canvas id='voterVotedInSFR'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="finaistContainer" style="margin-top: 15px;">
                    <h4><?php echo $_SESSION['configValue']['statisticsFinalistHeader']; ?></h4>
                    <div class="col-xs-12 addBorder" style="padding:15px">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsTotalNumberVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVoters; ?>
                                </p>
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsNumberCastedVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVotersVotedInFR . ' (' . floor((($numOfVotersVotedInFR / $numOfVoters) * 100) + 0.5) . '%)' ?>
                                </p>
                                <p>
                                    <strong>
                                        <?php echo $_SESSION['configValue']['statisticsNumberUncastedVotersLabel']; ?>
                                    </strong>
                                    <?php echo $numOfVoters - $numOfVotersVotedInFR . ' (' . floor(((($numOfVoters - $numOfVotersVotedInFR) / $numOfVoters) * 100) + 0.5) . '%)' ?>
                                </p>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <canvas id='voterVotedInFR'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ================================================================= -->
    <!-- ======================= Page Content End ======================== -->
    <!-- ================================================================= -->

    <?php _footer(); // DO NOT MODIFY THIS LINE ?>