<?php
/*
-----------------------------------------------------------
FILE NAME: blank_template.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: This is a blank template page. Following instructions below
				if you want to implement a new UI theme

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu
*/

include_once "../config.php"; // DO NOT MODIFY THIS LINE

//parameter 'title' is optional, which can be displayed on page or not
function _header($title='') {
	echo "
	<!DOCTYPE html>
	<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name='description' content=''>
		<meta name='author' content=''>
		<link rel='icon' href='../../favicon.ico'>
	
		<title>Faculty Elections</title>
	
	";

	include('allJsCss.php');

	echo "
	<link rel='stylesheet' href='css/mu_theme_1.css'/>
</head>

<body>


<div class='container2'>


	<div class='container1'>
		<div class='col-lg-12'>
			<div class='row' style='margin-top: 3px;'>
				<div class='col-xs-4'>
					<a href='http://www.miamioh.edu/'><img alt='Miami University' class='logoSmall' src='//miamioh.edu/stylesheets/images/logos/logoSmall4.png' style='margin-top:3px;height: 30px'></a>
				</div>
				<div id='header-bar-right' class='col-xs-8 no-print'>
					<ul class='navigationTactical' id='navigationTactical'>
						<li>
							<a href='http://mymiami.miamioh.edu' target='_parent' title='myMiami'>myMiami</a>
						</li>
						<li>
							<a href='http://www.units.miamioh.edu/uit/support-consultation/support-services' target='_parent' title='IT Help'>IT Help</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class='col-lg-12 header-div' style='margin-top: 6px; height: 105px'>

			<div class='col-sm-11 col-xs-10' id=\"header-content\" style=\"padding-left:0px;margin-top:20px;\">
				<h1 style='height: 1em; color: white;'><a href='./index.php'>Faculty Elections</a></h1>
			</div>

			<div class='navbar-header col-sm-1 col-xs-2 no-print' style=\"margin-top: 20px;\">
				<spam style='margin-right:-10px;' type='button' onclick='' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
					Menu
				</spam>
			</div>

		</div>
	</div>

	<div class='col-lg-12' id='navContainer'>
		<nav id='navbar' class='navbar navbar-default navbar-collapse collapse' style=\"padding-left:0px;padding-right:0px;\">
			<div class='container-fluid'>
				<ul class='nav navbar-nav'>
					<li><a href='./index.php'>Open Elections</a></li>
					<li><a href='./index.php?isClosed=true'>Archived
							Elections</a></li>
					<li>
						<a data-toggle='modal' id='directory-btn' href='#' data-target='#directory-content'>MU
							Directory</a></li>

					<li><a href='./faq.php'>FAQ</a></li>
				</ul>

				<ul class='nav navbar-nav navbar-right'>
					<li class='dropdown'>
						<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='true'>Admin
							Tools <span class='caret'></span></a>
						<ul class='dropdown-menu'>
					";
    if (FacultyElections::isAdmin()) {
        echo "
						<li><a href='./electionMgr.php?type=createElection'>Create Election</a></li>
						<li><a href='./pool.php'>Criteria Group</a></li>
						";
    }

    if ($_SESSION['authAsAdmin']) {
        if ($_SESSION['isAdmin']) {
            echo "<li><a href='./handler.php?handlerName=switchView&view=faculty'>Faculty View</a></li>";
        } else {
            echo "<li><a href='./handler.php?handlerName=switchView&view=admin'>Admin View</a></li>";
        }
    }

    echo "
					</ul>
					</li>
					<li class='dropdown'>
						<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='true'>
								Welcome, {$_SESSION['phpCAS']['user']}
							<span class='caret'></span></a>
						<ul class='dropdown-menu'>
							<li>
								<a href='{$_SESSION['wsUrl']['cas_logout']}'>Logout</a>
							</li>
						</ul>
					</li>

				</ul>

			</div>
		</nav>
	</div>
	<div class='col-xs-12 small-device' >
		<div id='errMsg' class='col-xs-12'></div>
	";
}

function _footer() {
	echo "
	</div>
	
		<div class=\"col-xs-12 morespace no-print\">&nbsp;</div>
		<div class='col-xs-12 siteFooterTertiary footer no-print' style='padding-left: 0px;padding-right:0px;'>
			<div class='col-xs-12'>
				<div class='col-lg-12 divider subSiteOnly'></div>
			</div>
			<div class='col-xs-12'>
				<div class='row siteFooter'>
					<div class='col-xs-12 col-md-7' style='padding-left: 0px;'>
						<div class='copyright'>
							©2016 Miami University. All rights reserved.
						</div>
						<ul class='footerLinks'>
							<li>
								<a href='http://miamioh.edu/about-miami/pubs-policies/privacy-statement/index.html' target='_parent' title='Privacy Statement'>Privacy
									Statement</a></li>
							<li>
								<a href='http://miamioh.edu/about-miami/pubs-policies/consumer-info/index.html' target='_parent' title='Family Consumer Information'>Family
									Consumer Information</a></li>
							<li>
								<a href='https://miamioh.formstack.com/forms/access_barrier_report' target='_parent' title='Accessibility Needs'>Accessibility
									Needs</a></li>
							<li>
								<a href='http://miamioh.edu/includes/problem/index.html' target='_parent' title='Report a Problem With This Website'>Report
									a Problem With This Website</a></li>
							<li>
								<a href='http://miamioh.edu/campus-safety/annual-report/index.html' target='_parent' title='Annual Security and Fire Safety Report'>Annual
									Security and Fire Safety Report</a></li>
						</ul>
					</div>
					<div class='col-xs-12 col-md-5' style='padding-right: 0px;'>
						<ul class='footerLinksSecondary'>
							<li>
								<a href='http://miamioh.edu/a-to-z/index.html' target='_parent' title='A to Z Listing'>A
									to Z Listing</a></li>
							<li>
								<a href='http://www.miamioh.edu/jobs' target='_parent' title='Employment'>Employment</a>
							</li>
						</ul>
						<ul class='footerActions hide-for-small'>
							<li class='printLink'>
								<a href='#' onclick='window.print()'>Print</a>
							</li>
						</ul>
					</div>
	
				</div>
			</div>
		</div>
	</div>
	
	
	<div class='modal fade' id='directory-content' tabindex='-1' role='dialog'>
		<div class='modal-dialog modal-lg'>
			<div class='modal-content'>
				<div class='modal-header'>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span></button>
					<h4 class='modal-title'>MU Directory</h4>
				</div>
				<div id='directoryModalBody' class='modal-body' style='min-height: 80vh;'>
					...
				</div>
			</div>
		</div>
	</div>
	
	</body>
	</html>
	";
}

