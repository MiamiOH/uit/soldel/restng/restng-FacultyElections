<!-- Navigation Bar content For 'SB Admin 2' template -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<div>
			<img id="logo" src="img/siteLogo.png">
			<a class="navbar-brand" href="index.php">Faculty Elections</a>
		</div>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">

				<li><a href="./index.php"><i class="fa fa-home fa-fw"></i>&nbsp;&nbsp;&nbsp;Open Elections</a></li>
				<li><a href="./index.php?isClosed=true"><i class="fa fa-times-circle-o fa-fw"></i>&nbsp;&nbsp;&nbsp;Archived Elections</a></li>
				<li><a data-toggle='modal' id='directory-btn' href='#' data-target='#directory-content'><i class='fa fa-phone fa-fw'></i>&nbsp;&nbsp;&nbsp;MU Directory</a></li>

				<li><a href="./faq.php"><i class="fa fa-question fa-fw"></i>&nbsp;&nbsp;&nbsp;FAQ</a></li>
				<li><a href='<?php echo $_SESSION['wsUrl']['cas_logout']; ?>'><i class='fa fa-sign-out fa-fw'></i>&nbsp;&nbsp;&nbsp;Logout</a></li>

				<?php
				if($_SESSION['authAsAdmin']) {
					echo "<hr>";
				}

				if(FacultyElections::isAdmin()) {
					echo "
					<li><a href='./electionMgr.php?type=createElection'><i class='fa fa-plus fa-fw'></i>&nbsp;&nbsp;&nbsp;Create Election</a></li>
					<li><a href='./pool.php'><i class='fa fa-pencil fa-fw'></i>&nbsp;&nbsp;&nbsp;Criteria Group</a></li>
					";
				}

				if($_SESSION['authAsAdmin']) {
					if($_SESSION['isAdmin']) {
						echo "<li><a href='./handler.php?handlerName=switchView&view=faculty'><i class='fa fa-user fa-fw'></i>&nbsp;&nbsp;&nbsp;Faculty View</a></li>";
					} else {
						echo "<li><a href='./handler.php?handlerName=switchView&view=admin'><i class='fa fa-user fa-fw'></i>&nbsp;&nbsp;&nbsp;Admin View</a></li>";
					}
				}
				?>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>