<?php
/*
-----------------------------------------------------------
FILE NAME: blank_template.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: This is a blank template page. Following instructions below
				if you want to implement a new UI theme

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu
*/

/*
 * when you implementing a new UI theme
 * _header and _footer function are must implemented
 *
 * _header() function is html content before 'page content'
 * _footer() function is html content after 'page content'
 *
 * For example:
 *
 * a minimized html file:
 * 		<!DOCTYPE html>
 * 		<html>
 * 			<head>
 * 				<title>Page Title</title>
 * 			</head>
 * 			<body>
 *
 * 				<h1>This is a Heading</h1>
 * 				<p>This is a paragraph.</p>
 *
 * 			</body>
 * 		</html>
 *
 * So, _header() should contains:
 * 		<!DOCTYPE html>
 * 		<html>
 * 			<head>
 * 				<title>Page Title</title>
 * 			</head>
 * 			<body>
 *
 * and _footer() should contains:
 * 			</body>
 * 		</html>
 *
 * page content:
 * 		<h1>This is a Heading</h1>
 * 		<p>This is a paragraph.</p>
 * should put between _header(); and _footer(); in the page
 * */

include_once "../config.php"; // DO NOT MODIFY THIS LINE

//parameter 'title' is optional, which can be displayed on page or not
function _header($title='') {

}

function _footer() {

}

