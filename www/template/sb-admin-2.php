<?php
/*
-----------------------------------------------------------
FILE NAME: sb-admin-2.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: this is an UI template called 'SB Admin 2' from startbootstrap.com

Create on 06/05/2016
Written by liaom@miamioh.edu
*/

include_once "../config.php";

function _header($title='') {
	echo "
	<!DOCTYPE html>
	<html lang='en'>

	<head>

		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta name='description' content='Faculty Election V2'>
		<meta name='author' content='Mingchao Liao'>
		<meta name='UniqueID' content='liaom'>
	
		<title>Faculty Elections</title>

	<!-- CSS and JS lib -->
	";
	include('allJsCss.php');
	echo "
	</head>

	<body>
	
	<div id='wrapper'>
	
		<!-- Navigation Bar -->
	";
	include 'template/sb-admin-2-nav.php';
	echo "
		<div id='page-wrapper' style='padding-left: 10px; padding-right: 10px;'>
			<div class='container-fluid' style='padding-left: 0px; padding-right: 0px;'>
				<div class='row'>
					".($title==''?"":"
					<div class='col-lg-12'>
						<!-- Page Header -->
						<h1 class='page-header'>{$title}
					</div>
					")."
				<!-- Page Content Start -->
				<div class='col-lg-12' style='padding-left: 0px; padding-right: 0px;'>
				<div id='errMsg' class='col-xs-12'></div>
	";
}

function _footer() {
	echo "
				</div>
					<!-- Page Content End -->
				</div>
			</div>
		</div>
	</div>
	
	<div class='modal fade' id='directory-content' tabindex='-1' role='dialog'>
		<div class='modal-dialog modal-lg'>
			<div class='modal-content'>
				<div class='modal-header'>
        			<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        			<h4 class='modal-title'>MU Directory</h4>
      			</div>
				<div id='directoryModalBody' class='modal-body' style='min-height: 80vh;'>
					...
				</div>
			</div>
		</div>
	</div>
	</body>
	</html>
	";
}

