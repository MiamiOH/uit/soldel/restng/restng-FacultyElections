<?php
/*
-----------------------------------------------------------
FILE NAME: allJsCss.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: this is a config file contains all js and css library which will be
			put in UI code

Create on 06/05/2016
Written by liaom@miamioh.edu
*/

$js = array(
    'template/js/jquery.min.1.12.2.js',
    'template/js/bootstrap-select.min1.10.1.js',
    'template/js/moment.min.2.13.0.js',
    'template/js/bootstrap-datetimepicker.min.4.17.37.js',
    'template/js/bootstrap.min.3.3.6.js',
    'template/js/jquery.hideseek.min.0.7.1.js',
    'template/js/bootbox.min.4.4.0.js',
    'template/js/jquery-ui.min.1.11.4.js',
    'template/js/slip.min.2.0.0.js',
    'template/js/jquery.ui.touch-punch.min.0.2.3.js',
    'template/js/metisMenu.min.2.5.2.js',
	'js/common.js',
	'js/sb-admin-2.js',
    'template/js/html5shiv.3.7.0.js', //for IE 9
    'template/js/respond.min.1.4.2.js', //for IE 9
    'template/js/Chart.min.2.2.1.js',
	
);
$css = array(
    'template/css/bootstrap.min.3.3.6.css',
    'template/css/font-awesome.min.4.6.3.css',
    'template/css/bootstrap-datepicker.min.1.6.1.css',
    'template/css/bootstrap-select.min.1.10.0.css',
    'template/css/metisMenu.min.2.5.2.css',

	'css/no-more-tables.css',
	
);
foreach ($js as $v) {
	echo "\t<script src='{$v}'></script>\n\t";
}
foreach ($css as $v) {
	echo "\t<link rel='stylesheet' href='{$v}' />\n\t";
}

