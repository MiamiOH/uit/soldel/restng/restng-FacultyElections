<?php
/*
-----------------------------------------------------------
FILE NAME: FacultyElections.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this is a helper class to connect between UI and WS

Create on 07/15/2016
Written by liaom@miamioh.edu
*/

include_once('HTTPReq.php'); // http request library

class FacultyElections {
	/*
	 * used to get election info (,voting round info, voter's voting status) for faculty
	 *
	 * Parameters:
	 * 		offset: number - optional (default: 1, this is for paging)
	 * 		limit: number - optional (default: 999999, this is for paging)
	 *
	 * Return:
	 * 		an array of election, with voter's voting status and voting round info
	 * */
	public static function getElectionsForFaculty($offset = 1, $limit = 999999, $isClosed = false) {
		//get faculty uniqueId
		$uniqueId = $_SESSION['authenticated_uid'];

		//get election by uniqueId filter
		//only get election the person participated
		if(!$isClosed) {
			$elections = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'uniqueId' => $uniqueId,
				'electionStatus' => 'open',
			));
		} else {
			$elections = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'uniqueId' => $uniqueId,
				'electionStatus' => 'close',
			));
		}


		//for voter
		//get voter's 'voted' status from participant ws
		$participants = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
			'uniqueId' => $uniqueId,
		));

		if ($participants['error']) {
			return $participants;
		}

		if ($elections['error']) {
			return $elections;
		}
		$participants = $participants['data'];
		$elections = $elections['data'];
        $finalReturn = array();

		//put voter's voting status into election arr
		foreach ($elections as &$election) {
            $electionMergedIn = false;
            $temp = $election;
            $participantRoundType = '';
            switch ($election['electionStatus']) {
                case 'Finalist Selection':
                    $participantRoundType = 'Finalists Round';
                    break;
                case 'Semi-Finalist Selection':
                    $participantRoundType = 'Semi-Finalists Round';
                    break;
            }
            if ($election['electionStatus'] != 'Closed') {
                //Election isn't closed, get participant information paired up
                foreach ($participants as $index => $participant) {
                    if ($election['electionId'] == $participant['electionId'] &&
                        ($participantRoundType == '' || isset($participant['voted'][$participantRoundType]))
                    ) {
                        $electionMergedIn = true;
                        $finalReturn[] = array_merge($temp, $participant);
                        unset($participants[$index]);
                    }
                }
            }

            if (!$electionMergedIn) {
                $temp['memberTypeName'] = $temp['participantType'];
                $finalReturn[] = $temp;
            }

		}

        return $finalReturn;
	}

	/*
	 * used to get election info (,voting round info) for admin
	 *
	 * Parameters:
	 * 		offset: number - required (this is for paging)
	 * 		limit: number - required (this is for paging)
	 *
	 * Return:
	 * 		an array of election, with voting round info
	 * */
	public static function getElectionsForAdmin($limit, $offset, $isClosed = false) {
		//get all election whatever close or not
		if(!$isClosed) {
			$elections = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'electionStatus' => 'open',
			));
		} else {
			$elections = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'electionStatus' => 'close',
			));
		}
		if ($elections['error']) {
			return $elections;
		}

		return $elections['data'];
	}

	/*
	 * used to get all criteria group name (poolName) and their id (poolId)
	 *
	 * Return:
	 * 		an array of poolName name and poolId
	 * */
	public static function getPoolIdName() {
		$pool = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['criteriaGroup_ws'], []);
		if ($pool['error']) {
			return $pool;
		}
		return $pool['data'];
	}

    /*
     * used to get all criteria group name (poolName) and their id (poolId)
     *
     * Return:
     * 		an array of poolName name and poolId
     * */
    public static function getDepartment() {
        $pool = HTTPReq::get($_SESSION['wsUrl']['IA_ROOT'] . $_SESSION['wsUrl']['ia_department_ws'], [], false);
        if ($pool['error']) {
            return $pool;
        }
        return $pool['data'];
    }

    /*
    * used to get all criteria group name (poolName) and their id (poolId)
    *
    * Return:
    * 		an array of poolName name and poolId
    * */
    public static function getDivision() {
        $pool = HTTPReq::get($_SESSION['wsUrl']['IA_ROOT'] . $_SESSION['wsUrl']['ia_division_ws'], [], false);
        if ($pool['error']) {
            return $pool;
        }
        return $pool['data'];
    }

	/*
	 * used to get all criteria name and their option name
	 *
	 * Return:
	 * 		an array of criteria name and criteria option name
	 * */
	public static function getCriteria() {
		$criteria = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['criteria_ws'], []);
		if ($criteria['error']) {
			return $criteria;
		}
		return $criteria['data'];
	}

	/*
	 * used to get results of a voting round
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		type: string - required (either 'Semi-Finalist', 'Finalist', 'Winner')
	 *
	 * Return:
	 * 		an array of uniqueId
	 * */
	public static function getResults($electionId, $type) {
		$results = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['results_ws'], array(
			'electionId' => $electionId,
			'type' => $type,
		));
		if ($results['error']) {
			return $results;
		}
		return $results['data'];
	}

	/*
	 * helper method to call to notification WS to send email
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		toPool: string - required (for usages, check notification ws scheme on swagger)
	 * 		notification: string - required (for usages, check notification ws scheme on swagger)
	 * */
	public static function sendEmail($electionId, $toPool, $notificationType) {
		if(!$_SESSION['sysConfig']['isEmailOn']) {
			return "Warning: we are not able to send email in this time, because sending email function has been closed. To change it, please go to configMgr.";
		}
		$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['notification_ws'], array('token' => $_SESSION['entityAccToken']), array(
			'electionId' => $electionId,
			'toPool' => $toPool,
			'notificationType' => $notificationType,
		));

		if ($res['error']) {
			return $_SESSION['configValue']['facelectResendEmailError'];
		}

		return $_SESSION['configValue']['facelectResendEmailMessage'];
	}

	/*
	 * call results ws to run stv
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		roundTypeName: string - required (either 'Semi-Finalists Round' or 'Finalists Round')
	 * */
	public static function runSTV($electionId, $roundTypeName) {
		$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['results_ws'], [], array(
			'electionId' => $electionId,
			'roundTypeName' => $roundTypeName
		));
		if ($res['error']) {
			return $res;
		}
		return $_SESSION['configValue']['facelectSRVRunning'];
	}

	/*
	 * call job ws to get participants' job info
	 *
	 * Parameters:
	 * 		uniqueIds: array - required (a list of participants' uniqueId)
	 *
	 * Return:
	 * 		an array of participants' job info
	 * */
	public static function getParticipantJobInfo($uniqueIdsArr) {
		$employees = array();
		$jobs = array();

		for($i = 0; $i < count($uniqueIdsArr); $i += 200) {
			$uniqueIds = implode(',', array_slice($uniqueIdsArr, $i, 200));

			$jobRes = HTTPReq::get($_SESSION['wsUrl']['WS_URL_ROOT'] . $_SESSION['wsUrl']['job_ws'], array(
				'uniqueId' => $uniqueIds,
				'limit' => 500,
				'token' => $_SESSION['entityAccToken'],
			));

			if ($jobRes['error']) {
				return $jobRes;
			}

			$jobs = array_merge($jobRes['data'], $jobs);

			$employeeRes = HTTPReq::get($_SESSION['wsUrl']['WS_URL_ROOT'] . $_SESSION['wsUrl']['employee_ws'], array(
				'uniqueId' => $uniqueIds,
				'limit' => 500,
				'token' => $_SESSION['entityAccToken'],
			));

			if ($employeeRes['error']) {
				return $employeeRes;
			}

			$employees = array_merge($employeeRes['data'], $employees);
		}

		$rtn = array();
		foreach($jobs as $job) {
			$job['uniqueId'] = strtolower($job['uniqueId']);

			if(isset($rtn[$job['uniqueId']])) {
				array_push($rtn[$job['uniqueId']]['title'], $job['title']);
				array_push($rtn[$job['uniqueId']]['standardizedDepartmentName'], $job['standardizedDepartmentName']);
				array_push($rtn[$job['uniqueId']]['standardizedDivisionName'], $job['standardizedDivisionName']);
				continue;
			}
			$rtn[$job['uniqueId']] = $job;
			$rtn[$job['uniqueId']]['title'] = array($rtn[$job['uniqueId']]['title']);
			$rtn[$job['uniqueId']]['standardizedDepartmentName'] = array($rtn[$job['uniqueId']]['standardizedDepartmentName']);
			$rtn[$job['uniqueId']]['standardizedDivisionName'] = array($rtn[$job['uniqueId']]['standardizedDivisionName']);
			foreach($employees as $key => $employee) {
				$employee['uniqueId'] = strtolower($employee['uniqueId']);
				if($employee['uniqueId'] == $job['uniqueId']) {
					if($employee['gradLevelExpDate'] == '') $employee['gradLevelExpDate'] = 'N/A';
					$rtn[$job['uniqueId']]['gradLevelExpDate'] = $employee['gradLevelExpDate'];

					if($employee['tenureDescription'] == '') $employee['tenureDescription'] = 'N/A';
					$rtn[$job['uniqueId']]['tenureDescription'] = $employee['tenureDescription'];
					unset($employees[$key]);
				}
			}
		}
		
		return $rtn;
	}

	/*
	 * call participant ws to get participants
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		type: string - required (either '' for get all, 'nominee' or 'voter')
	 *
	 * Return:
	 * 		an array of participants info
	 * */
	public static function getParticipantByType($type, $electionId) {
		if ($type == 'nominee') {
			$type = 'semi-finalist,finalist,winner,nominee';
		}

		if ($type == '') {
			$participant = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
				'electionId' => $electionId
			));
		} else {
			$participant = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
				'electionId' => $electionId,
				'type' => $type,
			));
		}

		if ($participant['error']) {
			return $participant;
		}

		return $participant['data'];
	}

	/*
	 * call criteriaGroup ws to get pool info
	 *
	 * Parameters:
	 * 		poolId: number - optional (default: get all pool)
	 *
	 * Return:
	 * 		an array of pool info
	 * */
	public static function getCriteriaGroup($poolId = '') {
		$body = array();
		if ($poolId != '') {
			$body['poolId'] = $poolId;
		}
		$criteriaGroups = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['criteriaGroup_ws'], $body);
		if ($criteriaGroups['error']) {
			return $criteriaGroups;
		}
		return $criteriaGroups['data'];
	}

	/*
	 * call participant ws DELETE method to remove a participant, and re-run STV
	 * when removing a nominee and election is not in self-nomination round
	 * 		- for nominee, mark his/her nomination_acceptance as 'removed'
	 * 		- for voter, just delete form DB
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		uniqueId: string - required
	 * */
	public static function removeParticipant($electionId, $uniqueId, $memberType='nominee') {
        //check election current status
        $election = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
            'electionId' => $electionId,
        ));

        if (!in_array($election['data'][0]['electionStatus'], [
                'Semi-Finalist Verification',
                'Finalist Verification',
                'Closed',
                'Removed',
                'New'
            ]) && $memberType != 'voter'
        ) {
            //Trying to remove nominee during wrong phase.
            $res = "Error: could not remove [{$uniqueId}] due to election is not in the correct status. You cannot remove Nominees during voting.";
            return $res;
        }
        // call to participant ws to delete participant

        $res = HTTPReq::delete($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
            'electionId' => $electionId,
            'uniqueId' => $uniqueId,
            'memberType' => $memberType
        ));


		if ($res['error']) {
			return $res['data']['message'];
		}



		if ($election['error']) {
            $res['error']['message'] = "Error: remove [{$uniqueId}] successfully, but failed to re-run STV";
            return $res;
		}

		$election = $election['data'][0];

		// if election run after semi-finalists selection and is not in finalists selection
		// call results ws PUT method to re-run STV to get new list of winners

        $res = self::runSTV($electionId,
            $election['electionStatus'] == 'Semi-Finalist Verification' ? "Semi-Finalists Round" : "Finalists Round");

        if (isset($res['error'])) {
            $res['error']['message'] = "Error: remove [{$uniqueId}] successfully, but failed to re-run STV. \nReason: " . $res['data']['message'];
            return $res;
        }

		return "Remove [{$uniqueId}] successfully !";

	}


	/*
	 * call to participant ws to modify participants' nomination_acceptance
	 * 		- faculty are not allow to specify a uniqueId
	 * 		- admin can specify a uniqueId
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		acceptance: string - required (either 'default', 'accepted' or 'declined')
	 *		uniqueId: string - optional (admin use ONLY)
	 * */
	public static function electionAcceptance($electionId, $acceptance, $uniqueId = '') {
		$body = array(
			'electionId' => $electionId,
			'acceptance' => $acceptance,
		);

		if ($uniqueId != '') {
			$body['uniqueId'] = $uniqueId;
		}

		$status = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], [], $body);
		if ($status['error']) {
			return $status;
		}

		return $status['data'];
	}

	/*
	 * call election ws PUT method to change electionStatus to 'Removed'
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * */
	public static function removeElection($electionId) {
		$body = array(
			'electionId' => $electionId,
			'electionStatus' => 'Removed',
		);

		$status = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], [], $body);

		if ($status['error']) {
			return $status;
		}

		return $status['data'];
	}

	/*
	 * call ballot ws POST method to submit user ballots
	 *
	 * Parameters:
	 * 		data: array - required
	 * */
	public static function submitBallots($data) {
		$data['ballots'] = explode(',', $data['ballots']);
		if(strtolower($data['roundTypeName']) == 'nominee') {
			$data['roundTypeName'] = 'Semi-Finalists Round';
		} elseif(strtolower(($data['roundTypeName'])) == 'semi-finalist') {
			$data['roundTypeName'] = 'Finalists Round';
		}

		$requestData = array();

		$rank = array();
		foreach ($data["ballots"] as $index => $uniqueId) {
			$rank[$index + 1] = $uniqueId;
		}

		$requestData['electionId'] = $data['electionId'];
		$requestData['roundTypeName'] = $data['roundTypeName'];
		$requestData['votes'] = $rank;
        $ballot = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['ballot_ws'], array(),
             $requestData);

         if ($ballot['error']) {
             return $ballot;
         }


        //Send Email
        $res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['notification_ws'],
            array('token' => $_SESSION['entityAccToken']), array(
                'electionId' => $data['electionId'],
                'to' => phpCAS::getUser() . "@miamioh.edu",
                'notificationType' => 'ballotConfirmation',
            ));

        if ($res['error']) {
            return $res;
        }

        return $ballot['data'];
	}

	/*
	 * restore an removed election
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * */
	public static function restoreElection($electionId) {
		return self::updateElectionStatus($electionId);
	}

	/*
	 * call participant ws GET method to get nominees for specified voting round
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		type: string - required (either 'Nominee' or 'Semi-Finalist')
	 *
	 * Return:
	 * 		an array of nominee
	 * */
	public static function getNomineesForVote($electionId, $type) {
		$nominees = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
			'electionId' => $electionId,
			'acceptance' => 'default,accepted',
			'type' => $type,
		));
		if ($nominees['error']) {
			return $nominees;
		}
		return $nominees['data'];
	}

	/*
	 * call election ws to get one specified election
	 *
	 * Parameters:
	 * 		electionId: number - required
	 * 		isOption: number - optional (either 1 or 0)
	 *
	 * Return:
	 * 		info of an election
	 * */
	public static function getSingleElection($electionId, $isOpen = '') {
		if ($isOpen == '') {
			$election = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'electionId' => $electionId,
			));
		} else {
			$election = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(
				'electionId' => $electionId,
				'electionStatus' => $isOpen,
			));
		}

		if ($election['error']) {
			return $election;
		}
		return $election['data'][0];
	}

	public static function addParticipant($uniqueId, $acceptance, $nomineeStatus, $electionId) {
		if($acceptance != '') {
			$body = array(
				'uniqueId' => $uniqueId,
				'acceptance' => $acceptance,
				'nomineeStatus' => $nomineeStatus,
				'electionId' => $electionId,
			);
			$body['type'] = 'nominee';
		} else {
			$body = array(
				'uniqueId' => $uniqueId,
				'electionId' => $electionId,
			);
			$body['type'] = 'voter';
		}
		$participant = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], [], $body);
		if($participant['error']) return $participant;
		return "Successfully add " . ($acceptance == '' ? 'voter' : 'nominee') . " into election [{$electionId}]";
	}

	/*
	 * call election ws to get an OPEN election
	 *
	 * Parameters:
	 * 		electionId: number - required
	 *
	 * Return:
	 * 		info of an election
	 * */
	public static function getSingleElectionForModifying($electionId) {
		$election = self::getSingleElection($electionId, 'open');

		if (isset($election['error'])) {
			return $election;
		}

		if (count($election) == 0) {
			return array(
				'error' => 1,
				'data' => array('message' => $_SESSION['configValue']['facelectElectionNotExistError'])
			);
		}

		return $election;
	}

	/*
	 * used to start or end a voting round for specified election
	 *
	 * Parameters:
	 * 		roundTypeName: string - required (either 'Self-Nomination Round', 'Semi-Finalists Round' or 'Finalists Round')
	 * 		action: number - optional (either 'start' or 'end')
	 * 		election: number - required
	 *
	 * Return:
	 * 		success message or failed message
	 * */
	public static function roundMgr($roundTypeName, $action, $electionId) {
		$currentElection = self::getSingleElectionForModifying($electionId);
		if (isset($currentElection['error'])) {
			return "Error: failed to {$action} {$roundTypeName}, trace: failed to get election info";
		}
		if ($roundTypeName == 'Self-Nomination Round' && $action == 'start' && !isset($currentElection['votingRounds'])) {
			$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'startDate' => time(),
			));
		} elseif ($roundTypeName == 'Self-Nomination Round' && $action == 'end' && isset($currentElection['votingRounds']['Self-Nomination Round']) && $currentElection['votingRounds']['Self-Nomination Round']['startDate'] != 'none' && $currentElection['votingRounds']['Self-Nomination Round']['endDate'] == 'none') {
			$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'endDate' => time(),
			));
		} elseif ($roundTypeName == 'Semi-Finalists Round' && $action == 'start' && isset($currentElection['votingRounds']['Self-Nomination Round']) && $currentElection['votingRounds']['Self-Nomination Round']['startDate'] != 'none' && $currentElection['votingRounds']['Self-Nomination Round']['endDate'] != 'none' && !isset($currentElection['votingRounds']['Semi-Finalists Round'])) {
			$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'startDate' => time(),
			));
		} elseif ($roundTypeName == 'Semi-Finalists Round' && $action == 'end' && isset($currentElection['votingRounds']['Semi-Finalists Round']) && $currentElection['votingRounds']['Semi-Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Semi-Finalists Round']['endDate'] == 'none') {
			$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'endDate' => time(),
			));

		} elseif ($roundTypeName == 'Finalists Round' && $action == 'start' && isset($currentElection['votingRounds']['Semi-Finalists Round']) && $currentElection['votingRounds']['Semi-Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Semi-Finalists Round']['endDate'] != 'none' && !isset($currentElection['votingRounds']['Finalists Round'])) {
			$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'startDate' => time(),
			));
		} elseif ($roundTypeName == 'Finalists Round' && $action == 'end' && isset($currentElection['votingRounds']['Finalists Round']) && $currentElection['votingRounds']['Finalists Round']['startDate'] != 'none' && $currentElection['votingRounds']['Finalists Round']['endDate'] == 'none') {
			$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['votingRound_ws'], array(), array(
				'electionId' => $currentElection['electionId'],
				'type' => $roundTypeName,
				'endDate' => time(),
			));

		} else {
			return "Error: failed to {$action} {$roundTypeName}, trace: illegal arg";
		}

		if ($res['error']) {
			return "Error: failed to {$action} {$roundTypeName}";
		}

		$res = self::updateElectionStatus($electionId);

		if ($res['error']) {
			return $_SESSION['configValue']['facelectElectionStatusUpdateError'];
		}

		if (!$res['error'] && $action == 'end' && in_array($roundTypeName, [
				'Semi-Finalists Round',
				'Finalists Round'
			])
		) {
			$res = self::runSTV($electionId, $roundTypeName);



			if (isset($res['error'])) {
				return $_SESSION['configValue']['facelectRunningSTVProblem'] . $res['data']['message'];
			}
		}

		return "{$action} {$roundTypeName} successfully";
	}

	/*
	 * call election PUT method to update electionStatus for specified election
	 *
	 * Parameters:
	 * 		election: number - required
	 * 		electionStatus: string - optional
	 * 				if leave blank, ws will automatically update election status
	 * 				or you can specified a electionStatus from following:
	 * 					'New', 'Self Nomination', 'Self Nomination Verification',
	 * 					'Semi-Finalist Selection', 'Semi-Finalist Verification',
	 * 					'Finalist Selection', 'Finalist Verification', 'Closed', 'Removed'
	 *
	 * Return:
	 * 		check election PUT scheme on swagger
	 * */
	public static function updateElectionStatus($electionId, $electionStatus = '') {
		if ($electionStatus != '') {
			$body = array(
				'electionId' => $electionId,
				'electionStatus' => $electionStatus,
			);
		} else {
			$body = array(
				'electionId' => $electionId,
			);
		}

		$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], [], $body);

		if ($res['error']) {
			return $res;
		}

		return $res['data'];
	}

	/*
	 * call election POST method to create a new election
	 *
	 * Parameters:
	 * 		body: array - required
	 * */
	public static function createElection($body) {
		$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array('token' => $_SESSION['entityAccToken']), $body);
		if ($res['error']) {
			return $res;
		}
		return $res['data'];
	}

	/*
	 * call election PUT method to modify an election
	 *
	 * Parameters:
	 * 		body: array - required
	 * */
	public static function modifyElection($body) {
		$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['election_ws'], array(), $body);
		if ($res['error']) {
			return $res;
		}
		return $res['data'];
	}

	/*
	 * call criteriaGroup POST method to create a new pool
	 *
	 * Parameters:
	 * 		body: array - required
	 * */
	public static function createPool($body) {
		$res = HTTPReq::post($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['criteriaGroup_ws'], array(), $body);
		if ($res['error']) {
			return $res;
		}
		return $res['data'];
	}

	/*
	 * call criteriaGroup PUT method to modify a pool
	 *
	 * Parameters:
	 * 		body: array - required
	 * */
	public static function modifyPool($body) {
		$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['criteriaGroup_ws'], array(), $body);
		if ($res['error']) {
			return $res;
		}
		return $res['data'];
	}

	/*
	 * used to switch between faculty view and admin view, this is for admin ONLY
	 *
	 * there are two field (authAsAdmin, isAdmin) in session indicate visitor's privilege
	 * authAsAdmin should never change which always indicate visitor's real privilege, admin or faculty
	 * isAdmin can be change to temporary let admin have faculty view
	 *
	 * Parameters:
	 * 		type: string - required (either 'admin' or 'faculty')
	 * */
	public static function switchView($type) {
		if(!$_SESSION['authAsAdmin']) return false;
		
		if (strtolower($type) == 'admin') {
			if ($_SESSION['authAsAdmin']) {
				$_SESSION['isAdmin'] = 1;
			}
		} else {
			$_SESSION['isAdmin'] = 0;
		}

		return true;
	}

	/*
	 * return whether visitor is a admin or faculty
	 * WARNING:
	 * 		this may not indicate visitor's real privilege
	 * 		when admin is in faculty view, this function will return false
	 * */
	public static function isAdmin() {
		return $_SESSION['isAdmin'] == TRUE;
	}

	/*
	 * call to participant GET method to check whether a participant is nominee or voter
	 *
	 * Parameters:
	 * 		$electionId: number
	 * */
	public static function isVoter($electionId) {
		$voter = HTTPReq::get($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['participant_ws'], array(
			'electionId' => $electionId,
			'uniqueId' => $_SESSION['authenticated_uid'],
			'type' => 'voter',
		));
		if ($voter['error']) {
			return FALSE;
		}
		return count($voter['data']) != 0;
	}
}