<?php
/*
-----------------------------------------------------------
FILE NAME: HTTPReq.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this is a helper class to make HTTP request

This file is initial version
Create on 07/15/2016
Written by liaom@miamioh.edu
*/

class HTTPReq {
	// to use this class, just include this file
	// and use like: HTTPReq::get(...)

	/*
	 * used to make HTTP GET request
	 *
	 * Parameters:
	 * 		url: string - required
	 * 		options: array - required
	 * 				this is the param list, e.g. foo.com?age=20&name='liaom'
	 * 				'options' array should be array('age' => 20, 'name' => 'liaom')
	 *
	 * Return:
	 * 		an array of results
	 * */
	public static function get($url, $options, $autoAddToken = true) {
		return self::handler($url, $options, '', 'GET', $autoAddToken);
	}

	/*
	 * used to make HTTP POST request
	 *
	 * Parameters:
	 * 		url: string - required
	 * 		options: array - required
	 * 				this is the param list, e.g. foo.com?age=20&name='liaom'
	 * 				'options' array should be array('age' => 20, 'name' => 'liaom')
	 * 		body: array - required
	 * 				this parameter hold request body, same format as 'options' array
	 *
	 * Return:
	 * 		an array of results
	 * */
	public static function post($url, $options, $body) {
		return self::handler($url, $options, $body, 'POST');
	}

	/*
	 * used to make HTTP PUT request
	 *
	 * Parameters:
	 * 		same with POST, see above
	 *
	 * Return:
	 * 		an array of results
	 * */
	public static function put($url, $options, $body) {
		return self::handler($url, $options, $body, 'PUT');
	}

	/*
	 * used to make HTTP DELETE request
	 *
	 * Parameters:
	 * 		same with GET, see above
	 *
	 * Return:
	 * 		an array of results
	 * */
	public static function delete($url, $options) {
		return self::handler($url, $options, '', 'DELETE');
	}

	/*
	 * helper function to make Http request using php curl
	 *
	 * Parameters:
	 * 		url: string - required, same with above
	 * 		options: array - required, same with above
	 * 		body: array - required, same with above
	 * 		method: string - required, either 'GET', 'POST', 'PUT' or 'DELETE'
	 * Return:
	 * 		an array of results
	 * */
	private static function handler($url, $options, $body, $method, $autoAddToken = true) {
		$curl = curl_init();

		//auto set personal token if token isn't specified
		//you can pass service account token to 'options' array
		//to override this default token
		if(!isset($options['token']) && $autoAddToken) $options['token'] = $_SESSION['entityAccToken'];
		$configArray = array(
			CURLOPT_URL => $url . '?' . http_build_query($options),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 200,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
			),
		);

		if($method == 'POST' || $method == 'PUT') {
			$configArray[CURLOPT_POSTFIELDS] = json_encode($body, JSON_FORCE_OBJECT);
			array_push($configArray[CURLOPT_HTTPHEADER], "content-type: application/json");
		}

		curl_setopt_array($curl, $configArray);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return array('error' => true, 'data' => array('message' => $err));
		} else {
			//add a error message for 401 code, since RESTng does not return anything
			if($response == '') {
				return array('error' => true, 'data' => array('message' => 'Error: 401 Unauthorized'));
			}

			$response = json_decode($response, true);

			//both 200 and 201 are ok, 401 is unauthorized, above 500 is server error
			if($response['status'] > 300) $response['error'] = true;
			return $response;
		}
	}
}