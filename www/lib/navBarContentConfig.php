<?php
/*
-----------------------------------------------------------
FILE NAME: navBarContentConfig.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this file contains nav bar content and its url. Those value are stored
				in session which will be easy to use when creating a new UI theme

Create on 08/15/2016
Written by liaom@miamioh.edu
*/

$_navBar = array(
	'faculty' => array(
		'allElection' => array(
			'name' => 'All Elections',
			'url' => './index.php',
		),
		'faq' => array(
			'name' => 'FAQ',
			'url' => '#',
		),
		'muDirectory' => array(
			'name' => 'MU Directory',
			'url' => '#',
		),
		'logout' => array(
			'name' => 'Logout',
			'url' => 'https://idptest.miamioh.edu/cas/logout?service=https://webdev.admin.miamioh.edu/phpapps/facultyElectionsWWW.liaom/',
		),
	),
	'admin' => array(
		'createElection' => array(
			'name' => 'Create Election',
			'url' => './electionMgr.php?type=createElection',
		),
		'criteriaGroup' => array(
			'name' => 'Criteria Group',
			'url' => './pool.php',
		),
		'switchView' => array(
			'faculty' => array(
				'name' => 'Faculty View',
				'url' => './index.php?type=switchView&view=faculty',
			),
			'admin' => array(
				'name' => 'Admin View',
				'url' => './index.php?type=switchView&view=admin',
			),
		),
	),
);