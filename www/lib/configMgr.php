<?php
/*
-----------------------------------------------------------
FILE NAME: configMgr.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  this is a helper file to get all config value form configMgr App

Create on 08/15/2016
Written by liaom@miamioh.edu
*/

include_once 'configMgr/configManager.php';

function configMgr() {
	// remove this condition to get config value every time people reload the page
	if (count($_SESSION['configValue']) == 0) {
		// this contains the key from configMgr -> facultyElections -> ApplicationText
		$configKeys = array(
			'facelectResendEmailError',
			'facelectResendEmailMessage',
			'facelectElectionNotExistError',
			'facelectElectionStatusUpdateError',
			'facelectElectionClosedMessage',
			'facelectSRVRunning',
			'facelectSuccessfulSemiFinalistEmail',
			'facelectElectionClosedFailure',
			'facelectSetWinnersFailure',
			'facelectRunningSTVProblem',

			'indexMissingElectionId',
			'indexElectionRemoved',
			'indexElectionRestore',
			'indexSearchText',
			'indexCurrentElectionsNominees',
			'indexCurrentElectionsVoter',
			'indexRoundInformation',
			'indexNoElection',
			'indexVotingRoundInfo',
            'indexCurrentElectionAsNomineeHeader',
            'indexCurrentElectionAsVoterHeader',
            'indexCurrentElectionHeader',
            'indexClosedElectionHeader',
            'indexRemovedElectionHeader',


			'particiNomineeTable',
			'particiVoterTable',
            'participantStatisticHeader',
            'participantEmailNotificationHeader',
            'participantActionHeader',
            'participantCloseElectionLabel',
			'particiUnresponsiveNominees',
			'particiUnresponsiveSFR',
			'particiUnresponsiveFR',
			'particiAccessDenied',
			'particiMissingId',
			'particiMissingUniqueId',
			'particiMissingAcceptance',
			'particiElectionInfoFailure',
			'particiNotAvailableNow',


			'poolMgrAccessDenied',
			'poolMgrInvalidType',
			'poolMgrMissingPoolId',
			'poolMgrMissingPoolName',
			'poolMgrPoolIdNotFound',
			'poolMgrPoolCreateSuccessful',
            'poolMgrMissingType',

            'postHandlerErrorPermission',
            'postHandlerErrorMissingGet',
            'postHandlerErrorInvalidGet',
            'postHandlerSuccessfulSubmitBallot',

			'resultsInvalidError',
			'resultsAccessDeniedError',
			'resultsMissingId',
			'resultsMissingElectionName',
			'resultsMissingType',
			'resultsInvalidAction',
			'resultsMissingUniqueId',

			'voteEssentials',
			'voteAccessDeniedError',
			'voteNoNominees',

			'stvNumberOfWinners',
			'stvNoBallotsCast',
			'stvEnoughNominees',
			'stvVoteTallyAtEnd',
			'stvWinnerList',
			'stvVoteTally',
			'stvVoteTallyAfterTransfer',
			'stvVoteTransfer',
			'stvTransferScore',
			'stvTransferVotes',
			'stvNumberOfVotersAfterTransfer',
			'stvNoLosers',
			'stvLoserFound',
			'stvLoser',
			'stvBallotExhausted',

			'electionMgrCreateElection',
			'electionMgrModifyElection',
			'electionMgrMissingType',
			'electionMgrInvalidAction',
			'electionMgrMissingRequiredParam',
			'electionMgrSelectVoterPool',
			'electionMgrInvalidType',
			'electionMgrMissingId',

			'staticticsElectionInformationFailure',
			'statisticsElectionNotClosed',
            'statisticsHeader',
            'statisticsElectionInformationHeader',
            'statisticsCriteraGroupHeader',
            'statisticsRoundHeader',
            'statisticsNominationHeader',
            'statisticsVotingHeader',
            'statisticsSemiFinalistHeader',
            'statisticsFinalistHeader',
            'statisticsNumberOfNomineeLabel',
            'statisticsNomineeDefaultLabel',
            'statisticsNomineeAcceptedLabel',
            'statisticsNomineeDeclinedLabel',
            'statisticsNomineeRemovedLabel',
            'statisticsTotalNumberVotersLabel',
            'statisticsNumberCastedVotersLabel',
            'statisticsNumberUncastedVotersLabel',
			'statisticsParticipantInforFailure',
			'statisAccessDenied',
			'statisMissingId',
			'statisReport',
            'statisNominationAcceptanceFig',
            'statisVoterVoted',
            'statisSemiFinalRoundFig',
            'statisFinalistRoundFig',

			'votingPageInstruction',
            'votingPageHeader',
            'votingSelectNomineeHeader',
            'votingDragDropHeader',
            'votingConfirmationPromptHeader',
            'votingConfirmationPromptBody',

			'emptyBallotErrorMsg',

            'nominationPromptBodyText',
            'nominationPromptHeaderText',

            'noElectionWarning',
			'electionStatusDescription_new',
			'electionStatusDescription_self_nomination',
			'electionStatusDescription_self_nomination_verification',
			'electionStatusDescription_semi_finalist_selection',
			'electionStatusDescription_semi_finalist_verification',
			'electionStatusDescription_finalist_selection',
			'electionStatusDescription_finalist_verification',
			'electionStatusDescription_closed',
			'electionStatusDescription_removed',
            'faqContent',

		);

		// this contains the key from configMgr -> facultyElections -> WebServiceURLs
		$wsUrlKeys = array(
			'cas_logout',
			'URL_ROOT',
			'employee_ws',
            'IA_ROOT',
			'ballot_ws',
			'criteriaGroup_ws',
			'election_ws',
			'notification_ws',
			'participant_ws',
			'results_ws',
			'votingRound_ws',
			'criteria_ws',
			'employee_ws',
			'job_ws',
			'WS_URL_ROOT',
            'ia_department_ws',
            'ia_division_ws',
			'application_cas_proxy',
			'application_service_url',
			'application_service_url_json',
		);

		getConfigValue('configValue', $configKeys);
		getConfigValue('wsUrl', $wsUrlKeys);
	}

	//load system config value
	$sysConfigKeys = array(
		'isDebugMode',
		'isEmailOn',
	);
	getConfigValue('sysConfig', $sysConfigKeys);
}

function getConfigValue($name, $configKeys) {
	$config = new configManager('FacultyElections');

	$_SESSION[$name] = array();

	foreach ($configKeys as $configKey) {
		$_SESSION[$name][$configKey] = $config->getValue($configKey);
	}
}

