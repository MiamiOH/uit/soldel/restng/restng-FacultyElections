<?php
/*
-----------------------------------------------------------
FILE NAME: common.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  helper files contains some php helper function
				and php base javascript helper function

Create on 07/15/2016
Written by liaom@miamioh.edu
*/

/*
 * used to display a alert message, and redirect page to a specified destination
 *
 * Parameters:
 * 		msg: string - required (message you want to display)
 * 		redirect: string - optional (default is home page, parsing a empty string to stay in current page)
 * */
function alert($msg, $redirect='index.php') {
	ob_start();

	include 'template/allJsCss.php';
	if($redirect == '') { // stay in current page
		echo "
		<script>
			bootbox.alert(\"<h3>{$msg}</h3>\",function(){});
		</script>";

	} else { // redirect to specified page, default is home page
		echo "<script>bootbox.alert(\"<h3>{$msg}</h3>\",function(){
			window.location = \"./{$redirect}\";
		});</script>";
	}

	ob_end_flush();
}

function errMsg($msg) {
	echo "
	<script>
	$('#errMsg').append(\"<div class='alert alert-danger col-xs-12'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <strong>Error: </strong><p style='display: inline;'>{$msg}</p></div>\");
	</script>
	";
}

/*
 * used to display a 'loading' icon and block screen when loading data from WS
 * */
function loading() {
	// check device type, loading icon is only displayed on big screen, not mobile device
	if(preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|bo‌​ost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])) {
		return;
	}

	// loading icon image now is in img/loading.gif, you can put a new gif in 'img' folder and replace its reference
	echo "
	<div id='loading' style='
			position:fixed;
			top: 50%;
			left: 50%;
			width:18em;
			height:18em;
			margin-top: -9em; 
			margin-left: -9em;
	'>
		<img src='img/loading.gif'>
		<div style='background:url(img/loading.gif) no-repeat center center;'></div>
	</div>	
	";

	ob_flush();
	flush();
}

function createResponsiveTable($data) {
	echo "
	<div id='no-more-tables' style='padding-top: 15px'>
		<table class='col-md-12 table-bordered table-striped table-condensed cf' style='padding-right: 0px; padding-left: 0px;'>
			<thead class='cf'>
				<tr>";

	foreach($data as $k => $v) {
		echo "		<th>{$k}</th>";
	}

	echo "		</tr>
			</thead>
			<tbody>";

	for($i = 0; $i < 99999; $i++) {
		$numOfNotSet = 0;
		$rowStr = '';

		foreach ($data as $title => $valueSet) {
			if(isset($valueSet[$i])) {
				$rowStr .= "		<td data-title='{$title}'>{$valueSet[$i]}</td>";
			} else {
				$numOfNotSet++;
			}
		}

		if($numOfNotSet < count($data)) {
			echo "<tr>{$rowStr}</tr>";
		} else {
			break;
		}
	}

	echo "	</tbody>
		</table>
	</div>
	";
}

function debug_print($var, $exit = true) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	if($exit) exit();
}

function accessDenied() {
	header('Location: ./handler.php?handlerName=accessDenied');
	die();
}

function icon($type) {
	switch ($type) {
		case 'ok' :
			echo "<img style='width:75%; margin-left: 15px;' class='icon icons8-Ok' src='img/ok.png'>";
			return;
		case 'error' :
			echo "<img style='width:75%;' class='icon icons8-Cancel' src='img/error.png'>";
			return;
		case 'warning' :
			echo "<img style='width:75%;' class='icon icons8-Attention' src='img/warning.png'>";
			return;
		default:
			echo '';
	}
}

function msgBox($rtnType, $rtnMsg, $rtnBtn) {
	echo "
	<div class='col-xs-12'>
		<div class='col-xs-12 col-sm-8 col-sm-offset-2' style='margin-top: 40px; border: 6px solid #DDDDDD;'>
			<div class='col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-5' style='padding-top: 15px;'>
	";
	icon($rtnType);
	echo "
			</div>
			<div class='col-xs-12'>
				<h2 style='font-size: 1.5em;'>" . $rtnMsg . "</h2>
			</div>
			<hr>
			<div class='col-xs-12 text-center'>
	";
	foreach ($rtnBtn as $btn) {
		if ($btn['url'] == '') { //go back to previous page
			echo "<a href='javascript:history.go(-1)' class='btn btn-lg btn-default' style='margin: 15px;'>{$btn['label']}</a>";
		} else {
			echo "<a href='{$btn['url']}' class='btn btn-lg btn-default' style='margin: 15px;'>{$btn['label']}</a>";
		}

	}
	echo "
			</div>
		</div>
	</div>
	";
    _footer();
	ob_flush();
	flush();
	die();
}
