<?php
include 'config.php';
include_once 'FacultyElections.php';

class PostHandler {
	private static $OK_RTNTYPE = 'ok';
	private static $WARNING_RTNTYPE = 'warning';
	private static $ERROR_RTNTYPE = 'error';

	private static $BACK_HOME_RTNBTN = array(
		array('label' => 'Back to home page', 'url' => './index.php'),
	);

	private static $ACCESS_DENIED = array(
		'error',
		'Error: you do not have permission to do this',
		array(
			array('label' => 'Back to home page', 'url' => './index.php'),
		)
	);

	// ================================================================================
	// ================================= Handler Start ================================
	// ================================================================================

	//switch between admin view and faculty view
	public static function switchView() {
		if (!isset($_GET['view'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['postHandlerErrorMissingGet']);
		}

		if (!in_array($_GET['view'], ['admin', 'faculty'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['postHandlerErrorInvalidGet']);
		}

		$res = FacultyElections::switchView($_GET['view']);

		if ($res) {
            return self::rtn(self::$OK_RTNTYPE, "Successfully switch to " . htmlspecialchars($_GET['view']) . " view");
		} else {
			return self::$ACCESS_DENIED;
		}
	}

	//handle user accept or decline an election
	public static function acceptance() {
		if (isset($_GET['electionId'])) {
			//echo $_GET['acceptance'];exit();
			$res = FacultyElections::electionAcceptance($_GET['electionId'], $_GET['acceptance']);

			if (isset($res['error'])) {
				return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message']);
			} else {
                return self::rtn(self::$OK_RTNTYPE,
                    "You have " . htmlspecialchars($_GET['acceptance']) . " this election");
			}
		} else {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexMissingElectionId']);
		}
	}

	//remove election handling
	public static function removeElection() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (isset($_GET['electionId'])) {
			$res = FacultyElections::removeElection($_GET['electionId']);

			if (isset($res['error'])) {
				return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message']);
			} else {
				return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexElectionRemoved']);
			}
		} else {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexMissingElectionId']);
		}
	}

	//restore removed election
	public static function restoreElection() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexMissingElectionId']);
		}

		$res = FacultyElections::restoreElection($_GET['electionId']);

		if (isset($res['error'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message']);
		} else {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexElectionRestore']);
		}

	}

	//close election and release results
	public static function closeAndRelease() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexMissingElectionId']);
		}

		$electionId = $_GET['electionId'];

		$res = FacultyElections::updateElectionStatus($_GET['electionId'], 'Closed');

		if (isset($res['error'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['facelectElectionClosedFailure']);
		}

		$res = HTTPReq::put($_SESSION['wsUrl']['URL_ROOT'] . $_SESSION['wsUrl']['results_ws'], [], array(
			'electionId' => $electionId,
			'roundTypeName' => 'Closed',
		));

		if ($res['error']) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['facelectSetWinnersFailure']);
		}

		$res = FacultyElections::sendEmail($electionId, 'all', 'results');

		return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['facelectElectionClosedMessage']);
	}

	//send semi-finalists email
	public static function releaseSemiFinalists() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['indexMissingElectionId']);
		}

		$electionId = $_GET['electionId'];

		$res = FacultyElections::sendEmail($electionId, 'semi-finalist', 'semi-finalist');

		return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['facelectResendEmailMessage']);
	}

	//create election
	public static function createElection() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_POST['electionName']) || !isset($_POST['nomineePoolId']) || !isset($_POST['voterPoolId']) || !isset($_POST['numOfWinners']) || !isset($_POST['numOfSemiFinalists'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingRequiredParam'], './electionMgr.php?type=createElection');
		}


		if (isset($_POST['divisionFilterValue']) && $_POST['divisionFilterValue'] == '') {
			unset($_POST['divisionFilterValue']);
		} else if($_POST['divisionFilterValue'] == null) {
			unset($_POST['divisionFilterValue']);
		} else {
			$_POST['divisionFilterValue'] = implode(',',$_POST['divisionFilterValue']);
		}


		if (isset($_POST['departmentFilterValue']) && $_POST['departmentFilterValue'] == '') {
			unset($_POST['departmentFilterValue']);
		} else if($_POST['departmentFilterValue'] == null) {
            unset($_POST['departmentFilterValue']);
        } else {
            $_POST['departmentFilterValue'] = implode(',',$_POST['departmentFilterValue']);
        }

		$_POST['updateUser'] = $_SESSION['phpCAS']['user'];

		$res = FacultyElections::createElection($_POST);

		if (isset($res['error'])) {
			return self::goBack(self::$ERROR_RTNTYPE, $res['data']['message']);
		}

		if (isset($res['messages'])) {
			$msg = 'Warning: ';
			foreach ($res['messages'] as $str) {
				$msg .= '<br>' . $str;
			}
			return self::rtn(self::$WARNING_RTNTYPE, $msg);
		} else {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['electionMgrCreateElection']);
		}
	}

	//modify election
	public static function modifyElection() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_POST['electionName']) || !isset($_POST['nomineePoolId']) || !isset($_POST['voterPoolId']) || !isset($_POST['numOfWinners']) || !isset($_POST['numOfSemiFinalists'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingRequiredParam']);
		}

		$res = FacultyElections::modifyElection($_POST);

		if (isset($res['error'])) {
			return self::goBack(self::$ERROR_RTNTYPE, $res['data']['message']);
		}

		return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['electionMgrModifyElection']);
	}

	public static function addParticipant() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_GET['electionId'])) {
            return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingId'],
                "./participant.php?electionId=" . urlencode($_GET['electionId']));
		}

		if (count($_POST) == 1) { // add voter
			$res = FacultyElections::addParticipant($_POST['uniqueId'], '', '', $_GET['electionId']);
			if (isset($res['error'])) {
                return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message'],
                    "participant.php?electionId=" . urlencode($_GET['electionId']));
			}
            return self::rtn(self::$OK_RTNTYPE, $res, "participant.php?electionId=" . urlencode($_GET['electionId']));
		} elseif (count($_POST) == 3) { // add nominee
			$res = FacultyElections::addParticipant($_POST['uniqueId'], $_POST['nominationAcceptance'], $_POST['nomineeStatus'], $_GET['electionId']);
			if (isset($res['error'])) {
                return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message'],
                    "participant.php?electionId=" . urlencode($_GET['electionId']));
			}
            return self::rtn(self::$OK_RTNTYPE, $res, "participant.php?electionId=" . urlencode($_GET['electionId']));
		} else {
            return self::rtn(self::$ERROR_RTNTYPE, 'Wrong Request',
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}
	}

	public static function resendEmail() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}
		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingId']);
		}
		if (!isset($_GET['to'])) {
            return self::rtn(self::$ERROR_RTNTYPE, "Error: missing 'to'",
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}
		if (!in_array($_GET['to'], [
			'nomineesNotResponded',
            'voternotvotedinsfr',
            'voternotvotedinfr'

        ])
		) {
            return self::rtn(self::$ERROR_RTNTYPE, "Error: invalid 'to'",
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}

		$res = FacultyElections::sendEmail($_GET['electionId'], $_GET['to'], $_GET['to']);
        return self::rtn(self::$OK_RTNTYPE, $res, "participant.php?electionId=" . urlencode($_GET['electionId']));
	}

	public static function deleteParticipant() {
		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingId']);
		}

		if (!isset($_GET['uniqueId'])) {
            return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['particiMissingUniqueId'],
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}
		$res = FacultyElections::removeParticipant($_GET['electionId'], $_GET['uniqueId'], $_GET['memberType']);

		if (strpos($res, 'Error:') === 0) {
            return self::rtn(self::$ERROR_RTNTYPE, $res,
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		} else {
            return self::rtn(self::$OK_RTNTYPE, $res, "participant.php?electionId=" . urlencode($_GET['electionId']));
		}
	}

	public static function modifyNomineeAcceptance() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_GET['electionId'])) {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['electionMgrMissingId']);
		}

		if (!isset($_GET['uniqueId'])) {
            return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['particiMissingUniqueId'],
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}

		if (!isset($_GET['acceptance'])) {
            return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['particiMissingAcceptance'],
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}

		$res = FacultyElections::electionAcceptance($_GET['electionId'], $_GET['acceptance'], $_GET['uniqueId']);
		if (isset($res['error'])) {
            return self::rtn(self::$ERROR_RTNTYPE, $res['data']['message'],
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		} else {
            return self::rtn(self::$OK_RTNTYPE,
                "Successfully set [" . htmlspecialchars($_GET['uniqueId']) . "] acceptance to " . htmlspecialchars($_GET['acceptance']),
                "participant.php?electionId=" . urlencode($_GET['electionId']));
		}
	}

	public static function poolMgr() {
		if (!FacultyElections::isAdmin()) {
			return self::$ACCESS_DENIED;
		}

		if (!isset($_POST['poolName']) || $_POST['poolName'] == '') {
			return self::rtn(self::$ERROR_RTNTYPE, $_SESSION['configValue']['poolMgrMissingPoolName'], "pool.php");
		}
		$type = $_POST['type'];
		unset($_POST['type']);
		$poolName = $_POST['poolName'];
		unset($_POST['poolName']);
		$body = array();
		if (isset($_POST['poolId'])) {
			$body['poolId'] = $_POST['poolId'];
			unset($_POST['poolId']);
		}
		$body['poolName'] = $poolName;
		$body['criteria'] = array();
		foreach ($_POST as $criteriaId => $criteriaOptions) {
            if ($criteriaOptions[0] != '' && $criteriaId != '') {
                $criteria = array();
                $criteria['criteriaId'] = $criteriaId;
                $criteria['criteriaOptionId'] = $criteriaOptions;
                array_push($body['criteria'], $criteria);
            }
		}

		if ($type == 'createPool') {
			$response = FacultyElections::createPool($body);
		} else {
			$response = FacultyElections::modifyPool($body);
		}

		if ($response['error']) {
			return self::goBack(self::$ERROR_RTNTYPE, $response['data']['message']);
		} else {
			return self::rtn(self::$OK_RTNTYPE, 'Pool ' . ($type == 'createPool' ? 'Create' : 'Modify') . ' Successfully');
		}
	}

	public static function submitBallots() {
		$res = FacultyElections::submitBallots($_POST);
		if (isset($res['error'])) {
			return self::goBack(self::$ERROR_RTNTYPE, $res['data']['message']);
		} else {
			return self::rtn(self::$OK_RTNTYPE, $_SESSION['configValue']['postHandlerSuccessfulSubmitBallot']
            );
		}
	}

	public static function roundMgr() {
		if (in_array($_GET['type'], [
				'Self-Nomination Round',
				'Semi-Finalists Round',
				'Finalists Round'
			]) && (!isset($_GET['action']) || !in_array($_GET['action'], [
					'start',
					'end'
				]) || !isset($_GET['electionId']))
		) {
			return array(
				'error',
				$_SESSION['configValue']['electionMgrInvalidAction'],
				array(
					array(
						'label' => 'Back to home page',
						'url' => './index.php'
					),
				)
			);
		}
		if (in_array($_GET['type'], [
			'Self-Nomination Round',
			'Semi-Finalists Round',
			'Finalists Round'
		])) {
			$res = FacultyElections::roundMgr($_GET['type'], $_GET['action'], $_GET['electionId']);
			if (strpos($res, 'Error')) {
				return array(
					'error',
					$res,
					array(
						array('label' => 'Home Page', 'url' => './index.php'),
						array(
							'label' => 'Go Back',
                            'url' => "./electionMgr.php?type=modifyElection&electionId=" . urlencode($_GET['electionId'])
						),
					)
				);
			} else {
				return array(
					'ok',
					$res,
					array(
						array('label' => 'Home Page', 'url' => './index.php'),
						array(
							'label' => 'Go Back',
                            'url' => "./electionMgr.php?type=modifyElection&electionId=" . urlencode($_GET['electionId'])
						),
					)
				);
			}
		}
	}
	// ================================================================================
	// ================================== Handler End =================================
	// ================================================================================

	private static function rtn($type, $msg, $url = './index.php') {
		return array(
			$type,
			$msg,
			array(
				array('label' => 'OK', 'url' => $url),
			)
		);
	}

	private static function goBack($type, $msg, $label = 'Go back') {
		return array(
			$type,
			$msg,
			array(
				array('label' => $label, 'url' => ''),
			)
		);
	}
}