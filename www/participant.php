<!--
-----------------------------------------------------------
FILE NAME: participant.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: used to view participant in specific election
 - view participant
 - modify participant status
 - Admin ONLY

This file is initial version
Create on 06/05/2016
Written by liaom@miamioh.edu

AUDIT TRAIL:

DATE		UniqueID
02/15/2017  schmidee
Description:  Corrections for Finalist Resend button not working and Code Injection Problems.
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header('Election Participant'); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

$nomineeDefaultCount = 0;
$nomineeAcceptedCount = 0;
$nomineeDeclinedCount = 0;
$nomineeRemovedCount = 0;
$numOfVoters = 0;

if (!FacultyElections::isAdmin()) {
    msgBox('error', $_SESSION['configValue']['postHandlerErrorPermission'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

if (!isset($_GET['electionId']) || !is_numeric($_GET['electionId'])) {
    msgBox('error', $_SESSION['configValue']['particiMissingId'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}

$election = FacultyElections::getSingleElection($_GET['electionId']);
if ($election['error']) {
    msgBox('error', $_SESSION['configValue']['particiElectionInfoFailure'], array(
        array('label' => 'Back to home page', 'url' => './index.php'),
    ));
}


?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->
<script src="js/participant.js"></script>
<script src="js/jquery.tablesorter.min.js"></script>

<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->
<div class="col-xs-12 moduleContainer">
    <h2 style="display:inline;">Election Information</h2>
    <div id="electionInfoContainer" class="col-xs-12 addBorder containerPadding">
        <div id='electionInfo' class="col-xs-12 ">
            <?php
            echo "
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>ID:</strong> {$election['electionId']}</div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Election Name:</strong> {$election['electionName']}</div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Status:</strong> {$election['electionStatus']}</div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'>&nbsp;</div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Nominee Pool:</strong> <a href='poolMgr.php?type=modifyPool&poolId={$election['nomineePoolId']}'>{$election['nomineePoolName']}</a></div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Voter Pool:</strong> <a href='poolMgr.php?type=modifyPool&poolId={$election['voterPoolId']}'>{$election['voterPoolName']}</a></div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Number of Semi-Finalists:</strong> {$election['numOfSemiFinalists']}</div>
                        <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Number of Finalists:</strong> {$election['numOfWinners']}</div>
                        ";
            ?>
        </div>
        <div class='col-xs-12  panel-group' id='accordion'>
            <div class="moduleContainer allEData small-device">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title pull-left">
                            <a data-toggle='collapse' data-parent='#accordion' href='#content-nomineeInformation'>
                                <h4><?= $_SESSION['configValue']['particiNomineeTable'] ?></h4>
                            </a>
                        </div>
                        <div class="panel-title pull-right">
                            <button class="btn btn-default btn-responsive" data-toggle="modal"
                                    data-target="#addParticipant"
                                    id="addNomineeBtn">
                                <i class="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                        <div class='clearfix'></div>
                    </div>
                    <div id='content-nomineeInformation' class="panel-collapse collapse">

                        <div id='nominee-div' class="panel-body"><?php createTable('nominee'); ?></div>
                    </div>
                </div>
            </div>

            <div class="moduleContainer allEData small-device">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title pull-left">
                            <a data-toggle='collapse' data-parent='#accordion' href='#content-voterInformation'>
                                <h4><?= $_SESSION['configValue']['particiVoterTable'] ?></h4>
                            </a>
                        </div>
                        <div class="panel-title pull-right">
                            <button class="btn btn-default btn-responsive" data-toggle="modal"
                                    data-target="#addParticipant"
                                    id="addVoterBtn">
                                <i class="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                        <div class='clearfix'></div>
                    </div>
                    <div id='content-voterInformation' class="panel-collapse collapse">

                        <div id='voter-div' class="panel-body"><?php createTable('voter'); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="moduleContainer statisticsContainer">
            <div id='stats-div' class="row">
                <h4><?php echo $_SESSION['configValue']['participantStatisticHeader'] ?></h4>

                <?php createStatistics(); ?>
            </div>
        </div>
    </div>
</div>



<div class="col-xs-12 moduleContainer">

    <h2><?php echo $_SESSION['configValue']['participantEmailNotificationHeader'] ?></h2>
    <div id="emailNotificaitonContainer" class="addBorder containerPadding">
        <div class="row">
            <div
                class="col-md-8 emailNotificationLabel"><?= $_SESSION['configValue']['particiUnresponsiveNominees'] ?></div>
            <div class="col-md-4 emailNotificationButton"><a
                        class="btn  btn-responsive btn-default <?php if ($election['electionStatus'] != 'Self Nomination') {
                        echo 'disabled';
                    } ?>"
                        href="./handler.php?handlerName=resendEmail&to=nomineesNotResponded&electionId=<?php echo urlencode($_GET['electionId']); ?>">Send</a>
            </div>
        </div>
        <div class="row">

            <div class="col-md-8 emailNotificationLabel"><?= $_SESSION['configValue']['particiUnresponsiveSFR'] ?></div>
            <div class="col-md-4 emailNotificationButton"><a
                        class="btn btn-responsive btn-default <?php if ($election['electionStatus'] != 'Semi-Finalist Selection') {
                        echo 'disabled';
                    } ?>"
                        href="./handler.php?handlerName=resendEmail&to=voternotvotedinsfr&electionId=<?php echo urlencode($_GET['electionId']); ?>">Send</a>
            </div>
        </div>
        <div class="row">

            <div class="col-md-8 emailNotificationLabel"><?= $_SESSION['configValue']['particiUnresponsiveFR'] ?></div>
            <div class="col-md-4 emailNotificationButton"><a
                        class="btn btn-responsive btn-default <?php if ($election['electionStatus'] != 'Finalist Selection') {
                        echo 'disabled';
                    } ?>"
                        href="./handler.php?handlerName=resendEmail&to=voternotvotedinfr&electionId=<?php echo urlencode($_GET['electionId']); ?>">Send</a>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 moduleContainer">

    <h2><?php echo $_SESSION['configValue']['participantActionHeader']; ?></h2>
    <div id="actionContainer" class="addBorder containerPadding">
        <div class="row">
            <div
                class="col-md-8 emailNotificationLabel"><?php echo $_SESSION['configValue']['participantCloseElectionLabel']; ?></div>
            <div class="col-md-4 emailNotificationButton"><a
                        class="btn btn-responsive btn-warning <?php echo($election['electionStatus'] == 'Closed' ? "" : "disabled"); ?>"
                        href="handler.php?electionId=<?php echo urlencode($_GET['electionId']); ?>&handlerName=closeAndRelease">Close
                    Election</a></div>
        </div>
    </div>

</div>

<?php
echo "<script>var electionId = '" . htmlspecialchars($_GET['electionId']) . "';</script>";

$numOfVoterVotedInSFR = 0;
$numOfVoterVotedInSFR = 0;
$numOfVoterVotedInFR = 0;
$numOfVoterVotedInFR = 0;
$participant = null;

function createTable($type)
{
    global $participant;
    $participant = FacultyElections::getParticipantByType($type, $_GET['electionId']);
    global $numOfVoters;
    if ($type == 'voter') {
        $numOfVoters = count($participant);
        global $numOfVoterVotedInSFR;
        $numOfVoterVotedInSFR = 0;
        global $numOfVoterVotedInFR;
        $numOfVoterVotedInFR = 0;
    }
    //print_r($participant);exit();}
    if (isset($participant['error'])) {
        msgBox('error', $participant['data']['message'], array(
            array('label' => 'Back to home page', 'url' => './index.php'),
        ));
    }
    if (count($participant) == 0) {
        echo "<div class='{$type}-table'>No {$type} in this election</div>";
        return;
    }
    echo "
	<div id='no-more-tables' class='{$type}-table'>
		<table id='{$type}-table' class='col-md-12 table-bordered table-striped table-condensed cf tablesorter participantTable' style='padding-right: 0px; padding-left: 0px;'>
			<thead class='cf'>
			<tr>
				<th>Name</th>
				<th>UniqueId</th>
				<th>Title</th>
				<th>Department</th>
				<th>Division</th>
				<th>Grad Level Expire Date</th>
				" . ($type == 'nominee' ? "
				<th>Status</th>
				" : "") . "
				<th>Acceptance</th>
			</tr>
			</thead>
			<tbody>
	";
    $uniqueIds = array_map(function ($person) {
        return $person['uniqueId'];
    }, $participant);

    $jobInfo = FacultyElections::getParticipantJobInfo($uniqueIds);

    foreach ($participant as $person) {
        $title = $_SESSION['configValue']['particiNotAvailableNow'];
        $dept = $_SESSION['configValue']['particiNotAvailableNow'];
        $division = $_SESSION['configValue']['particiNotAvailableNow'];
        $gradLevelExpDate = 'N/A';

        if (isset($jobInfo[$person['uniqueId']])) {
            $title = implode('<br>', $jobInfo[$person['uniqueId']]['title']);
            $dept = implode('<br>', $jobInfo[$person['uniqueId']]['standardizedDepartmentName']);
            $gradLevelExpDate = $jobInfo[$person['uniqueId']]['gradLevelExpDate'];
            $division = implode('<br>', $jobInfo[$person['uniqueId']]['standardizedDivisionName']);

        }

        global $nomineeDefaultCount, $nomineeAcceptedCount, $nomineeDeclinedCount, $nomineeRemovedCount;
        if ($person['nominationAcceptance'] == 'default') {
            $nomineeDefaultCount++;
        } elseif ($person['nominationAcceptance'] == 'accepted') {
            $nomineeAcceptedCount++;
        } elseif ($person['nominationAcceptance'] == 'declined') {
            $nomineeDeclinedCount++;
        } elseif ($person['nominationAcceptance'] == 'removed') {
            $nomineeRemovedCount++;
        }

        echo "
				<tr>
					<td data-title='Name'>{$person['preferredName']}</td>
					<td data-title='UniqueId'>{$person['uniqueId']}</td>
					<td data-title='Title'>{$title}</td>
					<td data-title='Department'>{$dept}</td>
					<td data-title='Division'>{$division}</td>
					<td data-title='Grad Level Expire Date'>{$gradLevelExpDate}</td>
		";
        if ($type == 'nominee') {
            echo "
					<td data-title='Status'>" . ($person['nomineeStatus'] != '' ? $person['nomineeStatus'] : "N/A") . "</td>
					<td data-title='Acceptance'>
        				<div class='selectContainer'>
							<select class='selectpicker selectbox-nominee-action'  
								data-width='fit'
								data-uniqueid='{$person['uniqueId']}' 
								data-priorSelectedState='{$person['nominationAcceptance']}'>
								<option value='default' " . ($person['nominationAcceptance'] == 'default' ? 'selected' : '') . " >
									Default
								</option>
								<option value='accepted' " . ($person['nominationAcceptance'] == 'accepted' ? 'selected' : '') . " >
									Accepted
								</option>
								<option value='declined' " . ($person['nominationAcceptance'] == 'declined' ? 'selected' : '') . " >
									Declined
								</option>
								<option value='removed' " . ($person['nominationAcceptance'] == 'removed' ? 'selected' : '') . " >
									Removed
								</option>
							</div>
        				</div>
					</td>
			";
        } else {
            if (isset($person['voted']['Finalists Round'])) {
                if ($person['voted']['Finalists Round']) {
                    $numOfVoterVotedInFR++;
                }
            }

            if (isset($person['voted']['Semi-Finalists Round'])) {
                if ($person['voted']['Semi-Finalists Round']) {
                    $numOfVoterVotedInSFR++;
                }
            }

            echo "
					</td>
					<td data-title='Action'>
						<a class='btn btn-responsive btn-danger removeVoterBtn' data-uniqueid='{$person['uniqueId']}'>Remove</a>
					</td>
			";
        }
        echo "
				</tr>
		";
    }
    echo "
			</tbody>
		</table>
	</div>
	";


}

function createStatistics()
{
    global $participant, $numOfVoterVotedInFR,
           $nomineeDefaultCount, $nomineeAcceptedCount,
           $nomineeDeclinedCount, $nomineeRemovedCount,
           $numOfVoterVotedInSFR, $numOfVoters;
    if (count($participant)) {
        echo "
		<div class='col-md-4 col-xs-12'>
			<canvas id='nomineePieChart'>
		</div>
		<script>
			var nomineePieChart = nomineePieChart([{$nomineeDefaultCount},{$nomineeAcceptedCount},{$nomineeDeclinedCount},{$nomineeRemovedCount}]);
		</script>
		";

        if (isset($participant[0]['voted']['Semi-Finalists Round'])) {
            echo "
			<div class='col-md-4 col-xs-12'>
				<canvas id='votedStaChartInSFR'>
			</div>
			<script>
				var votedStaChartInSFR = createPieChart('votedStaChartInSFR', 'Voter Status In Semi-Finalists Round', [{$numOfVoterVotedInSFR}, " . ($numOfVoters - $numOfVoterVotedInSFR) . "]);
			</script>
			";
        }

        if (isset($participant[0]['voted']['Finalists Round'])) {
            echo "
			<div class='col-md-4 col-xs-12'>
				<canvas id='votedStaChartInFR'>
			</div>
			<script>
				var votedStaChartInFR = createPieChart('votedStaChartInFR', 'Voter Status In Finalists Round', [{$numOfVoterVotedInFR}, " . ($numOfVoters - $numOfVoterVotedInFR) . "]);
			</script>
			";
        }
    } else {
        echo "<div class='col-md-4 col-xs-12'> Sorry, no statistics to be displayed at this time. </div>";
    }
}

?>


<div class="modal fade" id="addParticipant" tabindex="-1" role="dialog"
     aria-labelledby="addParticipantLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="addParticipantTitle">

                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form" method="POST"
                      action="handler.php?handlerName=addParticipant&electionId=<?php echo urlencode($_GET['electionId']); ?>"
                      id="addParticipantForm">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="uniqueid">UniqueID(s)</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="uniqueId" name="uniqueId"
                                      placeholder="UniqueID(s)"></textarea>
                        </div>
                    </div>
                    <div class="form-group notForVoter">
                        <label class="col-sm-2 control-label" for="acceptance">Acceptance</label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control notForVoterInput" id="acceptance"
                                    name="nominationAcceptance">
                                <option value="default">Default</option>
                                <option value="accepted">Accepted</option>
                                <option value="declined">Declined</option>
                                <option value="removed">Removed</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group notForVoter">
                        <label class="col-sm-2 control-label" for="nomineeStatus">Status</label>
                        <div class="col-sm-10">
                            <select type="text" class="form-control notForVoterInput" id="nomineeStatus"
                                    name="nomineeStatus">
                                <option value="Nominee">Nominee</option>
                                <option value="Semi-Finalist">Semi-Finalist</option>
                                <option value="Finalist">Finalist</option>
                                <option value="Winner">Winner</option>
                            </select>
                        </div>
                    </div>
                </form>


            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-responsive btn-default"
                        data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-responsive btn-primary" id="addParticipantSubmitBtn">

                </button>
            </div>
        </div>
    </div>
</div>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); ?>
