<!--
-----------------------------------------------------------
FILE NAME: index.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION: home page of FSVS
 - Admin View
  - All Election
  - Management Btn
  - Detailed view of election info, as well as voting round info
 - Faculty View
  - All participative election
  - voting round info
  - As Nominee: Accept/Declined Btn
  - As Voter: Vote Btn

Create on 06/05/2016
Written by liaom@miamioh.edu
-->
<?php
include 'config.php'; // DO NOT MODIFY THIS LINE
loading(); // DO NOT MODIFY THIS LINE
_header(isset($_GET['isClosed']) && $_GET['isClosed'] == true ? "Archived Elections" : "Open Elections"); // Put title here, Default: no title (DO NOT MODIFY THIS LINE)

//=======================================================================
//========= Put any pre-load handling here. e.g. access control =========
//=======================================================================

?>

<!-- ================================================================ -->
<!-- =================== Put JS and CSS code here =================== -->
<!-- ================================================================ -->
<script src="js/getElection.js"></script>

<style>
    .allElection[style*="display: none"] {
        margin-top: 0px;
    }

</style>
<!-- ================================================================= -->
<!-- ====================== Page Content Start ======================= -->
<!-- ================================================================= -->
<div class='col-sm-12'>
    <div class='panel-group' id='accordion'>
        <input id="search" style='margin-bottom:20px;' class='col-sm-3 form-control' name="search"
               placeholder="Search here, e.g. voter, nominee, election name, close, open, or anything you want"
               data-toggle="hideseek" type="text" data-list=".allEData">

        <?php
        if ($_SESSION['isAdmin']) {
            echo "
			<div id='currentElections'>
				<h2>{$_SESSION['configValue']['indexCurrentElectionHeader']}</h2>
			</div>
	
			<div id='closedElections''>
				<h2 >{$_SESSION['configValue']['indexClosedElectionHeader']}</h2>
			</div>
			
			<div id='removedElections'>
				<h2>{$_SESSION['configValue']['indexRemovedElectionHeader']}</h2>
			</div>
			";
            forAdmin();
        } else {
            echo "
			<div id='asNominee'>
				<h2>{$_SESSION['configValue']['indexCurrentElectionAsNomineeHeader']}</h2>
			</div>
			
			<div id='asVoter'>
				<h2>{$_SESSION['configValue']['indexCurrentElectionAsVoterHeader']}</h2>
			</div>
	
			<div id='closedElections'>
				<h2>{$_SESSION['configValue']['indexClosedElectionHeader']}</h2>
			</div>
			";
            forFaculty();
        }

        function forAdmin()
        {

            if (isset($_GET['isClosed']) && $_GET['isClosed'] == true) {
                $elections = FacultyElections::getElectionsForAdmin(1000, 1, true);
            } else {
                $elections = FacultyElections::getElectionsForAdmin(1000, 1);
            }

            //			echo "<pre>";
            //			print_r($elections);echo "</pre";exit();
            if ($elections['error']) {
                msgBox('error', $elections['data']['message'], array(
                    array('label' => 'Back to home page', 'url' => './index.php'),
                ));
            }

            $hasOpenElection = false;
            $hasCloseElection = false;
            $hasRemovedElection = false;

            foreach ($elections as $election) {
                if ($election['isOpen']) {
                    $hasOpenElection = true;
                } elseif ($election['electionStatus'] == 'Removed') {
                    $hasRemovedElection = true;
                } else {
                    $hasCloseElection = true;
                }

                echo "<div class='allEData col-xs-12 small-device'>
						<div  style='margin-top: 3px;' class='panel panel-default allElection " . ($election['isOpen'] ? "open" : ($election['electionStatus'] == 'Removed' ? "removed" : "closed")) . "'>
						  <div class='panel-heading'>
							<div class='panel-title pull-left'>
							  <a data-toggle='collapse' data-parent='#accordion' href='#content-{$election['electionId']}'>
								{$election['electionName']}
							  </a>
							</div>
							<div class='panel-title pull-right'> 
							  " . ($election['isOpen'] ? "<a class='btn btn-default btn-responsive' href='electionMgr.php?type=modifyElection&electionId={$election['electionId']}'><i class='fa fa-pencil' style='font-size: 140%;'></i></a><a style='margin-left: 10px;' class='btn btn-default removeBtn btn-responsive' name='{$election['electionId']}'><i class='fa fa-trash-o' style='font-size: 140%; color: red;'></i></a>" : ($election['electionStatus'] == 'Removed' ? "<a class='btn btn-success btn-responsive' href='handler.php?handlerName=restoreElection&electionId={$election['electionId']}'>Restore</a>" : "")) . "
							</div>
							<div class='clearfix'></div>
						  </div>
						  <div id='content-{$election['electionId']}' class='panel-collapse collapse'>
							<div class='panel-body'>
							  <div class='col-xs-12' style='border: 1px solid #DDDDDD;'>
								  <h4>Election Information: </h4>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>ID: </strong>{$election['electionId']}</div>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Number Of Semi-Finalists: </strong>{$election['numOfSemiFinalists']}</div>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Voter Pool: </strong><a href='poolMgr.php?type=modifyPool&poolId={$election['voterPoolId']}'>{$election['voterPoolName']}</a></div>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Status: </strong>{$election['electionStatus']}</div>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Number Of Finalists: </strong>{$election['numOfWinners']}</div>
							    <div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Nominee Pool: </strong><a href='poolMgr.php?type=modifyPool&poolId={$election['nomineePoolId']}'>{$election['nomineePoolName']}</a></div>
								<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Campus Filter: </strong>" . ($election['campusFilterValue'] == '' ? "none" : $election['campusFilterValue']) . "</div>
								<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Division Filter: </strong>" . ($election['divisionFilterValue'] == '' ? "none" : $election['divisionFilterValue']) . "</div>
								<div class='col-lg-4 col-md-6 col-sm-12 electionInfo'><strong>Department Filter: </strong>" . ($election['departmentFilterValue'] == '' ? "none" : $election['departmentFilterValue']) . "</div>
								</br>
								<div class='col-xs-12'>
									<div class='buttonContainer floatClear'>
										<a class='btn btn-default' href='participant.php?electionId={$election['electionId']}'>View/Edit Participants</a>
									
								
								" . ($election['electionStatus'] == 'Closed' ? "
								<a class='btn btn-default' href='results.php?electionId={$election['electionId']}&electionName={$election['electionName']}&type=Winner'>Final Result</a>
								<a class='btn btn-default' href='statistics.php?electionId={$election['electionId']}'>Statistics Report</a>
								" : "") . "
								</div>
								</div>
								<div class='col-xs-12'>
								  <hr>
								  <h5>Round Information: </h5>";
                addVotingRoundInfo($election);

                echo($election['electionStatus'] == 'Finalist Verification' ?
                    "<div class='buttonContainer'>
					  <a class='btn btn-default' href='handler.php?electionId={$election['electionId']}&handlerName=closeAndRelease'>Close Election & Release Results</a>
					 </div>" : "");


                echo "
						  <hr>
						</div>
						
						<div class='col-xs-2 floatClear'></div>
						<div class='col-xs-5 dateInformation'><strong>Date Created: </strong>{$election['dateCreated']} ({$election['createUser']})</div>
						<div class='col-xs-5 dateInformation'><strong>Date Updated: </strong>{$election['dateUpdated']} ({$election['updateUser']})</div>
					  </div>
					</div>
				  </div>
				</div></div>
				";
            }

            if (!$hasOpenElection) {
                if (isset($_GET['isClosed']) && $_GET['isClosed'] == true) {
                    echo "<div class='col-xs-12'><h5 class='open'><a href='./index.php'>Show Open Elections</a></h5></div>";
                } else {
                    echo "<div class='col-xs-12'><p class='open'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                }
            } else {
                echo "<div class='col-xs-12' style='height: 15px;'><div class='open'></div></div>"; // padding top 15 px
            }

            if (!$hasCloseElection) {
                if (!isset($_GET['isClosed']) || (isset($_GET['isClosed']) && $_GET['isClosed'] == false)) {
                    echo "<div class='col-xs-12'><h5 class='closed'><a href='./index.php?isClosed=true'>Show Closed Elections</a></h5></div>";
                } else {
                    echo "<div class='col-xs-12'><p class='closed'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                }
            } else {
                echo "<div class='col-xs-12' style='height: 15px;'><div class='closed'></div></div>"; // padding top 15 px
            }

            if (!$hasRemovedElection) {
                if (!isset($_GET['isClosed']) || (isset($_GET['isClosed']) && $_GET['isClosed'] == false)) {
                    echo "<div class='col-xs-12'><h5 class='removed'><a href='./index.php?isClosed=true'>Show Removed Elections</a></h5></div>";
                } else {
                    echo "<div class='col-xs-12'><p class='removed'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                }
            } else {
                echo "<div class='col-xs-12' style='height: 15px;'><div class='removed'></div></div>"; // padding top 15 px
            }
        }

        function forFaculty()
        {
            if (isset($_GET['isClosed']) && $_GET['isClosed'] == true) {
                $elections = FacultyElections::getElectionsForFaculty(1, 1000, true);
            } else {
                $elections = FacultyElections::getElectionsForFaculty(1, 1000);
            }

            if (isset($elections['error'])) {
                msgBox('error', $elections['data']['message'], array(
                    array('label' => 'Back to home page', 'url' => './index.php'),
                ));
            }

            $hasOpenElectionAsNominee = false;
            $hasOpenElectionAsVoter = false;
            $hasCloseElection = false;
            $electionDisplayNumber = 0;
            foreach ($elections as $election) {
                $electionDisplayNumber++;
                echo "
				<div class='allEData small-device'>
					<div  style='margin-top: 3px;' class='panel panel-default allElection " . ($election['isOpen'] ? "open" : "closed") . " {$election['memberTypeName']}'>
						<div class='panel-heading'>
							<div class='panel-title pull-left'>
								<a data-toggle='collapse' data-parent='#accordion' href='#content-{$electionDisplayNumber}'>
									{$election['electionName']}
								</a>
							</div>
							<div class='panel-title pull-right'>
				";

                // ===== title bar start
                echo "&nbsp;&nbsp;";
                if ($election['isOpen']) {
                    //echo "(<p style='color:green;display:inline;'>open</p>)";
                } else {
                    //echo "(<p style='color:red;display:inline;'>closed</p>)";
                    echo "&nbsp;&nbsp;<span><a class='btn btn-default btn-responsive' href='./results.php?electionId={$election['electionId']}&electionName={$election['electionName']}&type=Winner'>Result</a></span>";
                    $hasCloseElection = true;
                }

                if (strtolower($election['memberTypeName']) == 'nominee'
                    && isset($election['votingRounds']['Self-Nomination Round'])
                    && $election['votingRounds']['Self-Nomination Round']['status'] == 'In Progress'
                ) {
                    if ($election['nominationAcceptance'] == 'default') {
                        echo "
						<a class='btn btn-default nominee-action-btn btn-responsive' data-electionId='{$election['electionId']}' data-title='{$_SESSION['configValue']['nominationPromptHeaderText']}' data-body='{$_SESSION['configValue']['nominationPromptBodyText']}' style='color: red;' href='#'>Action Required</a>
						";
                    } elseif ($election['nominationAcceptance'] == 'accepted') {
                        echo "
						<a class='btn btn-success nominee-accept-btn btn-responsive disabled' data-electionId='{$election['electionId']}' href='#'>Accepted Nomination</i></a>
						";
                    } else {
                        if ($election['nominationAcceptance'] == 'declined') {
                            echo "
						<a class='btn btn-danger nominee-declined-btn btn-responsive disabled' data-electionId='{$election['electionId']}' href='#'>Declined Nomination</a>
						";
                        }
                    }
                }

                if (strtolower($election['memberTypeName']) == 'voter'
                    && isset($election['votingRounds']['Self-Nomination Round'])
                ) {
                    $hasVotedForSemiFinalistsRound = ((!isset($election['voted']['Semi-Finalists Round']) || $election['voted']['Semi-Finalists Round'] == 0) ? "" : "disabled");
                    $hasVotedForFinalistsRound = ((!isset($election['voted']['Finalists Round']) || $election['voted']['Finalists Round'] == 0) ? "" : "disabled");
                    if ($election['votingRounds']['Semi-Finalists Round']['status'] == 'In Progress') {
                        echo "
						<a class='btn btn-primary btn-responsive  {$hasVotedForSemiFinalistsRound}' id = 'voteButton' href='./vote.php?electionId={$election['electionId']}&type=nominee'>Vote" . ($hasVotedForSemiFinalistsRound == '' ? "" : " Recorded") . "</a>";
                    } elseif ($election['votingRounds']['Finalists Round']['status'] == 'In Progress') {
                        echo "
						<a class='btn btn-primary btn-responsive {$hasVotedForFinalistsRound}' id = 'voteButton' href='./vote.php?electionId={$election['electionId']}&type=semi-finalist'>Vote" . ($hasVotedForFinalistsRound == '' ? "" : " Recorded") . "</a>
						";
                    }
                }

                if (strtolower($election['memberTypeName']) == 'nominee') {
                    echo "<div class='hidden'>nominee</div>";
                    if ($election['isOpen']) {
                        $hasOpenElectionAsNominee = true;
                    }
                } else {
                    echo "<div class='hidden'>voter</div>";
                    if ($election['isOpen']) {
                        $hasOpenElectionAsVoter = true;
                    }
                }

                // ===== title bar end

                echo "
							</div>
							<div class='clearfix'></div>
						</div>
						<div id='content-{$electionDisplayNumber}' class='panel-collapse collapse'>
							<div class='panel-body'>
							    <h4>Election Information: </h4>
								<div class='col-lg-4 col-md-6 col-sm-12'><strong>Status:</strong> {$election['electionStatus']}</div>
								<div class='col-lg-4 col-md-6 col-sm-12'><strong>Number to be Elected:</strong> {$election['numOfWinners']}</div>
								" . ($election['electionStatus'] == 'Closed' ? "
								<div class='col-lg-4 col-md-6 col-sm-12'><a class='btn btn-default btn-responsive' href='results.php?electionId={$election['electionId']}&electionName={$election['electionName']}&type=Winner'>View Final Results</a></div>
								" : "") . "
								<div class='col-xs-12'>
								  <hr>
								  <h5>Round Information</h5>
				";
                // start collapse content
                addVotingRoundInfo($election);
                // end collapse content
                echo "
								</div>
							</div>
						</div>
					</div>
				</div>
				";
            }

            if (isset($_GET['isClosed']) && $_GET['isClosed'] == true) { //in closed/removed elections page
                if (!$hasCloseElection) {
                    echo "<div class='col-xs-12'><p class='closed'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                } else {
                    echo "<div class='col-xs-12' style='height: 15px;'><div class='closed'></div></div>"; // padding top 15 px
                }
                echo "<div class='col-xs-12'><h5 class='nominee'><a href='./index.php'>Show Open Elections</a></h5></div>";
                echo "<div class='col-xs-12'><h5 class='voter'><a href='./index.php'>Show Open Elections</a></h5></div>";
            } else { // in open elections page
                if (!$hasOpenElectionAsNominee) {
                    echo "<div class='col-xs-12'><p class='nominee'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                } else {
                    echo "<div class='col-xs-12' style='height: 15px;'><div class='nominee'></div></div>"; // padding top 15 px
                }

                if (!$hasOpenElectionAsVoter) {
                    echo "<div class='col-xs-12'><p class='voter'>{$_SESSION['configValue']['noElectionWarning']}</p></div>";
                } else {
                    echo "<div class='col-xs-12' style='height: 15px;'><div class='voter'></div></div>"; // padding top 15 px
                }
                echo "<div class='col-xs-12'><h5 class='closed'><a href='./index.php?isClosed=true'>Show Closed Elections</a></h5></div>";
            }
        }

        function addVotingRoundInfo($election)
        {
            if (isset($election['votingRounds'])) {
                echo "
				  <div id='no-more-tables'>
					  <table class='col-md-12 table-bordered table-striped table-condensed cf roundTable'>
						<thead class='cf'>
							<tr>
								<th>Type</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Status</th>
								" . (FacultyElections::isAdmin() ? "<th>Result</th>" : "") . "
							</tr>
						</thead>
						<tbody>
							" . (isset($election['votingRounds']['Self-Nomination Round']) ? "
								<tr>
									<td data-title='Type'>Self-Nomination Round</td>
									<td data-title='Start Date'>{$election['votingRounds']['Self-Nomination Round']['startDate']}</td>
									<td data-title='End Date'>{$election['votingRounds']['Self-Nomination Round']['endDate']}</td>
									<td data-title='Status'>{$election['votingRounds']['Self-Nomination Round']['status']}</td>
									" . (FacultyElections::isAdmin() ? "<td data-title='Result'>" . ($election['votingRounds']['Self-Nomination Round']['endDate'] != 'none' && strtotime($election['votingRounds']['Self-Nomination Round']['endDate']) < time() ? "<a class='btn btn-default resultBtn btn-responsive " . ($election['electionStatus'] == 'Self Nomination Verification' ? "" : "disabled") . "' href='participant.php?electionId={$election['electionId']}' name='Self-Nomination Round'>Result</a>" : "not complete") . "</td>" : "") . "

								</tr>" : "") . "
							" . (isset($election['votingRounds']['Semi-Finalists Round']) ? "
								<tr>
									<td data-title='Type'>Semi-Finalists Round</td>
									<td data-title='Start Date'>{$election['votingRounds']['Semi-Finalists Round']['startDate']}</td>
									<td data-title='End Date'>{$election['votingRounds']['Semi-Finalists Round']['endDate']}</td>
									<td data-title='Status'>{$election['votingRounds']['Semi-Finalists Round']['status']}</td>
									" . (FacultyElections::isAdmin() ? "<td data-title='Result'>" . ($election['votingRounds']['Semi-Finalists Round']['status'] == 'Closed' ? "<a class='btn btn-default resultBtn btn-responsive " . (!in_array($election['electionStatus'],
                                    [
                                        'Semi-Finalist Verification',
                                        'Finalist Selection'
                                    ]) ? 'disabled' : '') . "' href='results.php?electionId={$election['electionId']}&electionName={$election['electionName']}&type=Semi-Finalist' name='Semi-Finalists Round' >Result</a>&nbsp;&nbsp;<a class='btn btn-success btn-responsive " . ($election['electionStatus'] != 'Semi-Finalist Verification' ? "disabled" : "") . "' href='./handler.php?electionId={$election['electionId']}&handlerName=releaseSemiFinalists'>Send Result</a>" : "not complete") . "</td>" : "") . "
								</tr>" : "") . "
							" . (isset($election['votingRounds']['Finalists Round']) ? "
								<tr>
									<td data-title='Type'>Finalists Round</td>
									<td data-title='Start Date'>{$election['votingRounds']['Finalists Round']['startDate']}</td>
									<td data-title='End Date'>{$election['votingRounds']['Finalists Round']['endDate']}</td>
									<td data-title='Status'>{$election['votingRounds']['Finalists Round']['status']}</td>
									" . (FacultyElections::isAdmin() ? "<td data-title='Result'>" . ($election['votingRounds']['Finalists Round']['endDate'] != 'none' && strtotime($election['votingRounds']['Finalists Round']['endDate']) < time() ? "<a class='btn btn-default resultBtn btn-responsive " . (!in_array($election['electionStatus'],
                                    ['Finalist Verification']) ? 'disabled' : '') . "' href='results.php?electionId={$election['electionId']}&electionName={$election['electionName']}&type=Finalist' name='Self-Nomination Round'>Result</a>" : "not complete") . "</td>" : "") . "
								</tr>" : "") . "		
						</tbody>
					  </table>
				  </div>
				";
            } else {
                echo "<div class='col-xs-12'>No Round Information for this Election.</div>";
            }
        }

        ?>
    </div>
</div>
<!-- ================================================================= -->
<!-- ======================= Page Content End ======================== -->
<!-- ================================================================= -->

<?php _footer(); ?>