<?php

return [
    'resources' => [
        'facultyElections' => [
            MiamiOH\FacultyElections\Resources\BallotResourceProvider::class,
            MiamiOH\FacultyElections\Resources\CriteriaResourceProvider::class,
            MiamiOH\FacultyElections\Resources\CriteriaGroupResourceProvider::class,
            MiamiOH\FacultyElections\Resources\ElectionResourceProvider::class,
            MiamiOH\FacultyElections\Resources\NotificationResourceProvider::class,
            MiamiOH\FacultyElections\Resources\ParticipantResourceProvider::class,
            MiamiOH\FacultyElections\Resources\ResultsResourceProvider::class,
            MiamiOH\FacultyElections\Resources\VotingRoundResourceProvider::class,
        ],
    ]
];