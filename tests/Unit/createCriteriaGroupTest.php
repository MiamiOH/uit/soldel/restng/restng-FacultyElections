<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getCriteriaGroupTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/07/2016  liaom

*/

use MiamiOH\RESTng\App;

class CreateCriteriaGroupTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $criteriaGroup, $queryallRecords, $user, $request, $awardService, $mockedApp, $e;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->mockedApp = $this->createMock(App::class);

        $this->mockedApp->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        //set up the mock dbh:
        $this->e = $this->getMockBuilder('\Exception')
            ->setMethods(array('getMessage'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->criteriaGroup = new \MiamiOH\FacultyElections\Services\CriteriaGroup();
        $this->criteriaGroup->setApp($this->mockedApp);
        $this->criteriaGroup->setApiUser($this->user);
        $this->criteriaGroup->setDatabase($db);
        $this->criteriaGroup->setDatasource($ds);
        $this->criteriaGroup->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testEmptyPoolName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyPoolName'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty poolName",
                $e->getMessage());
        }
    }

    public function testMissingPoolName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMissingPoolName'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameters (poolName)",
                $e->getMessage());
        }
    }

    public function testInvalidPoolName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidPoolName'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid poolName",
                $e->getMessage());
        }
    }

    public function testPoolNameTooLong()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataPoolNameTooLong'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: poolName too long",
                $e->getMessage());
        }
    }

    public function testTooMuchParameters()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'poolName' => 'test pool',
                'criteria' => array(
                    array(
                        'criteriaId' => '1',
                        'criteriaOptionId' => [1],
                    ),
                    array(
                        'criteriaId' => '2',
                        'criteriaOptionId' => [3],
                    ),
                ),
                'hh' => '',
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: too much parameters",
                $e->getMessage());
        }
    }

    public function testCriteriaIdNotSingle()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'poolName' => 'test pool',
                'criteria' => array(
                    array(
                        'criteriaId' => [1, 2],
                        'criteriaOptionId' => [1],
                    ),
                    array(
                        'criteriaId' => '2',
                        'criteriaOptionId' => [3],
                    ),
                )
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid criteriaId, it should be single",
                $e->getMessage());
        }
    }

    public function testMissingCriteriaOptionId()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'poolName' => 'test pool',
                'criteria' => array(
                    array(
                        'criteriaId' => 1,
                    ),
                    array(
                        'criteriaId' => '2',
                        'criteriaOptionId' => [3],
                    ),
                )
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameters (criteriaOptionId)",
                $e->getMessage());
        }
    }

    public function testCriteriaOptionIdNotSingle()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'poolName' => 'test pool',
                'criteria' => array(
                    array(
                        'criteriaId' => 1,
                        'criteriaOptionId' => [3, 4],
                    ),
                    array(
                        'criteriaId' => '2',
                        'criteriaOptionId' => [3],
                    ),
                )
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid criteriaOptionId, it should only contains one value",
                $e->getMessage());
        }
    }

    public function testMultiValidPoolName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMultiValidPoolName'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testEmptyCriteria()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyCriteria'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty criteria",
                $e->getMessage());
        }
    }

    public function testEmptyCriteriaId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyCriteriaId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty criteriaId",
                $e->getMessage());
        }
    }

    public function testMissingCriteriaId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMissingCriteriaId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameters (criteriaId)",
                $e->getMessage());
        }
    }

    public function testInvalidCriteriaId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidCriteriaId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid criteriaId",
                $e->getMessage());
        }
    }

    public function testCriteriaOptionIdNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataCriteriaOptionIdNotArray'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be a list",
                $e->getMessage());
        }
    }

    public function testInvalidCriteriaOptionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidCriteriaOptionId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid criteriaOptionId",
                $e->getMessage());
        }
    }

    public function testFailedToGetPoolIdAfterCreatingIt()
    {
        $this->request->method('getData')
            ->will($this->returnValue(
                array('poolName' => 'pool1234')
            ));

        //tell the dbh what to do when the perform method is called.
        $this->dbh->method('perform')
            ->will($this->returnValue(
                1
            ));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnValue(
                array()
            ));

        try {
            $this->criteriaGroup->createCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: fail to get poolId",
                $e->getMessage());
        }
    }

    public function mockDataEmptyPoolName()
    {
        return array(
            'poolName' => '',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataMissingPoolName()
    {
        return array(
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataInvalidPoolName()
    {
        return array(
            'poolName' => '&*&*^ test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataPoolNameTooLong()
    {
        return array(
            'poolName' => 'asldjflajslfalkjlfakjdlfjaldjflkadjlfkajdlfkjasdkfjlaskdfjlakjdflaksdjflkajf',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataMultiValidPoolName()
    {
        return array(
            'poolName' => ['asdf', 'sdfsdf'],
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataEmptyCriteria()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => '',
        );
    }

    public function mockDataEmptyCriteriaId()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataMissingCriteriaId()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataInvalidCriteriaId()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '&^%&^%',
                    'criteriaOptionId' => [3],
                ),
            ),
        );
    }

    public function mockDataCriteriaOptionIdNotArray()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => '3',
                ),
            ),
        );
    }

    public function mockDataInvalidCriteriaOptionId()
    {
        return array(
            'poolName' => 'test pool',
            'criteria' => array(
                array(
                    'criteriaId' => '1',
                    'criteriaOptionId' => [1],
                ),
                array(
                    'criteriaId' => '2',
                    'criteriaOptionId' => ['**&^'],
                ),
            ),
        );
    }
}