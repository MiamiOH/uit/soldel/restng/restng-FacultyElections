<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
	-----------------------------------------------------------
	FILE NAME: postBallotTest.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Erin Mills

	DESCRIPTION:  Unit Tests for Testing the POST Functionality of the Ballot Web Service

	ENVIRONMENT DEPENDENCIES: PHP Unit

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	10/12/2015       MILLSE
	Description:  Initial Program

*/
use MiamiOH\RESTng\App;

class postBallotTest extends \MiamiOH\RESTng\Testing\TestCase
{

    //////////
    //Set Up//
    //////////
    private $dbh, $ballot, $queryallRecords;
    // 	private $user;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock mockedApp:
        $mockedApp = $this->createMock(App::class);
        $mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $notificationResponse = new \MiamiOH\RESTng\Util\Response();
        $notificationResponse->setPayload(array());
        $notificationResponse->setStatus(App::API_CREATED);
        $mockedApp->method('callResource')->willReturn($notificationResponse);

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'returnResourceParam')));

        $request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptions')));


        //set up the mock data source
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->ballot = new \MiamiOH\FacultyElections\Services\Ballot();
        $this->ballot->setApp($mockedApp);
        $this->ballot->setApiUser($this->user);
        $this->ballot->setDatabase($db);
        $this->ballot->setDatasource($ds);
        $this->ballot->setRequest($request);

    }

    ////////////////////////////////////////
    //                TESTS               //
    ////////////////////////////////////////

    /*
     *	Post a Single Ballot Test
     *	Tests the Post of a Single Ballot
     *	Expected Return: 200 OK Response
     */
    public function testPostSingleBallot()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostBody')));


        //the perform method will call the mockPerform() method
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));

        //the queryall_array method
        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                array(array('round_type_name' => 'Semi-Finalist Nomination')),
                //call to check round type name
                array(array('election_id' => '1234')), //call to check electionid
                array(array('uniqueid' => 'MILLSE')),//call to check nominee validity
                array(array('uniqueid' => 'JOHNSODA')),
                //call to check nominee validity
                array(array('uniqueid' => 'PATELAH')),
                //call to check nominee validity
                array(array('uniqueid' => 'COVERTKA')),
                //call to check nominee validity
                array(array('uniqueid' => 'MILEYWG')),
                //call to check nominee validity
                array(array('uniqueid' => 'MILLSE')),
                //call to check voter eligibility
                array(array('nextval' => '185')), //call to check nominee validity
                array(array('round_type_id' => '12')) //call to check nominee validity
            ));


        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        $response = $this->ballot->submitBallot();
        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals($this->mockSuccessfulPostReturn(), $payload);

    }

    /*
     *	Post an empty Ballot Test
     *	Tests the Post of an empty Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostEmptyBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyPostBody')));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Ballot must be provided.', $e->getMessage());
        }
    }

    /*
     *	Post a non-valid sendEmail value in Ballot Test
     *	Expected Return: 400 Bad Request response
     */
    public function testBadSendEmail()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockBadSendEmailPostBody')));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('If used, sendEmail must contain true or false.',
                $e->getMessage());
        }
    }


    /*
     *	Post a non-valid sendEmail value in Ballot Test
     *	Expected Return: 400 Bad Request response
     */
    public function testEmptySendEmail()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockEmptySendEmailPostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('If used, sendEmail must contain true or false.',
                $e->getMessage());
        }
    }


    /*
     *	Post an empty ElectionId in Ballot Test
     *	Tests the Post of an empty ElectionId in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostEmptyElectionIdBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockEmptyElectionIdPostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('ElectionId must be provided.', $e->getMessage());
        }
    }

    /*
     *	Post an empty RoundName in Ballot Test
     *	Tests the Post of an empty RoundName in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostEmptyRoundNameBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockEmptyRoundNamePostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('RoundTypeName must be provided.', $e->getMessage());
        }
    }

    /*
     *	Post an invalid ElectionId in Ballot Test
     *	Tests the Post of an invalid ElectionId in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostInvalidElectionIdBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidElectionIdPostBody'
            )));

        //the queryall_array method
        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                array(array('round_type_name' => 'Semi-Finalist Nomination')),
                //call to check round name
                $this->returnCallback(array(
                    $this,
                    'mockQueryallArrayEmptyReturn'
                )) //call to check electionid
            ));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('ElectionId is invalid.', $e->getMessage());
        }
    }

    /*
     *	Post an invalid RoundName in Ballot Test
     *	Tests the Post of an invalid RoundName in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostInvalidRoundNameBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidRoundNamePostBody'
            )));

        //the queryall_array method
        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                $this->returnCallback(array(
                    $this,
                    'mockQueryallArrayEmptyReturn'
                )) //call to check round type name
            ));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('RoundTypeName is invalid.', $e->getMessage());
        }
    }

    /*
     *	Post a missing nominee uniqueid in Ballot Test
     *	Tests the Post of a missing nominee uniqueid in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostMissingNomineeBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockMissingNomineePostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Nominee uniqueid must be provided with ranked vote [1].',
                $e->getMessage());
        }
    }

    /*
     *	Post a missing rank in Ballot Test
     *	Tests the Post of a missing rank in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostMissingRankBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockMissingRankPostBody')));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Rank must be provided with vote for [MILLSE].',
                $e->getMessage());
        }
    }

    /*
     *	Post a missing rank and nominee in Ballot Test
     *	Tests the Post of a missing rank in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostMissingRankAndNomineeBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockMissingRankAndNomineePostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Rank and nominee must be provided for each vote.',
                $e->getMessage());
        }
    }


    /*
     *	Post an invalid rank in Ballot Test
     *	Tests the Post of an invalid rank in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostInvalidRankBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockInvalidRankPostBody')));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Rank [x] was invalid; must be numeric.',
                $e->getMessage());
        }
    }


    /*
     *	Post a sql injected nominee in Ballot Test
     *	Tests the Post of sql injection in nominee in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostNomineeSQLInjectionBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockNomineeSqlInjectionPostBody'
            )));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Nominee must be alpha-numeric [delete * from users].',
                $e->getMessage());
        }
    }


    /*
     *	Post an invalid nominee in Ballot Test
     *	Tests the Post of an invalid nominee in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostInvalidNomineeBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidNomineePostBody'
            )));

        //the querryall_array method
        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                array(array('round_type_name' => 'Semi-Finalist Nomination')),
                array(array('election_id' => '1234')),
                $this->returnCallback(array($this, 'mockQueryallArrayEmptyReturn'))
            ));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Nominee uniqueid [77r99idbad] was invalid.',
                $e->getMessage());
        }
    }


    /*
     *	Post a bad ranking in Ballot Test
     *	Tests the Post of a bad ranking in Ballot
     *	Expected Return: 400 Bad Request response
     */
    public function testPostBadRankingBallot()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $this->user->method('getUsername')
            ->will($this->returnValue('MILLSE'));
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockBadRankingPostBody')));

        //the queryall_array method
        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                array(array('round_type_name' => 'Semi-Finalist Nomination')),
                //call to check round type name
                array(array('election_id' => '1234')), //call to check electionid
                array(array('uniqueid' => 'MILLSE')),//call to check nominee validity
                array(array('uniqueid' => 'JOHNSODA')),
                //call to check nominee validity
                array(array('uniqueid' => 'PATELAH')),
                //call to check nominee validity
                array(array('uniqueid' => 'COVERTKA')),
                //call to check nominee validity
                array(array('uniqueid' => 'MILEYWG')) //call to check nominee validity
            ));

        /*** Call the submitBallot and get the payload ***/
        $this->ballot->setRequest($request);
        try {
            $response = $this->ballot->submitBallot();
        } catch (\Exception $e) {
            $this->assertEquals('Rankings submitted were inconsistent/invalid.',
                $e->getMessage());
        }
    }





    ////////////////////////////////////////
    //            MOCK METHODS            //
    ////////////////////////////////////////

    /*
    * Mock queryall_array empty return
    */
    public function mockQueryallArrayEmptyReturn()
    {
        return array();
    }

    /*
    * Mock queryall_array good return
    */
    public function mockQueryallArrayGoodReturn()
    {
    }

    ////////
    // POST BODY METHODS
    ////////

    /*
    *	Method which mocks the POST Body for a valid POST request.
    */
    public function mockPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'true',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the EMPTY POST Body for an empty POST request.
    */
    public function mockEmptyPostBody()
    {
        $payload = array();

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an empty ElectionId in Ballot POST request.
    */
    public function mockEmptyElectionIdPostBody()
    {
        $payload = array(
            'electionId' => '',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an empty RoundName in Ballot POST request.
    */
    public function mockEmptyRoundNamePostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => '',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an invalid ElectionId in Ballot POST request.
    */
    public function mockInvalidElectionIdPostBody()
    {
        $payload = array(
            'electionId' => 'badElectionididi13231',    //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an invalid RoundName in Ballot POST request.
    */
    public function mockInvalidRoundNamePostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Crazy Invalid Roundxxlldkf',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for a missing nominee POST request.
    */
    public function mockMissingNomineePostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => '',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for a missing rank POST request.
    */
    public function mockMissingRankPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for a missing rank and missing nominee POST request.
    */
    public function mockMissingRankAndNomineePostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(                            //votingRoundName
                '' => '',                            //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an invalid rank POST request.
    */
    public function mockInvalidRankPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                'x' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for SQL Injection in the nominee POST request.
    */
    public function mockNomineeSqlInjectionPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'delete * from users',
                //first vote for millse
                '2' => 'JOHNSODA',
                //second vote for johnsoda
                '3' => 'PATELAH',
                //third vote for patelah
                '4' => 'COVERTKA',
                //fourth vote for covertka
                '5' => 'MILEYWG',
                //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for an invalid nominee POST request.
    */
    public function mockInvalidNomineePostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => '77r99idbad',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }

    /*
    *	Method which mocks the POST Body for a bad ranking POST request.
    */
    public function mockBadRankingPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '5' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'false',
            'voter' => 'gengx'
        );

        return $payload;
    }


    public function mockBadSendEmailPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => 'blah',
            'voter' => 'gengx'
        );

        return $payload;
    }

    public function mockEmptySendEmailPostBody()
    {
        $payload = array(
            'electionId' => '1234',                            //electionId
            'roundTypeName' => 'Semi-Finalist Nomination',
            'votes' => array(    //votingRoundName
                '1' => 'MILLSE',                    //first vote for millse
                '2' => 'JOHNSODA',                    //second vote for johnsoda
                '3' => 'PATELAH',                    //third vote for patelah
                '4' => 'COVERTKA',                    //fourth vote for covertka
                '5' => 'MILEYWG',                    //fifth vote for mileywg
            ),
            'sendEmail' => '',
            'voter' => 'gengx'
        );

        return $payload;
    }



    ////////
    // POST RESPONSE METHODS
    ////////


    /*
    *	Mocked version of a succcessful Perform call.
    */
    public function mockPerform()
    {
        return true;
    }

    /*
    *	Mocked version of the expected return of a Single PIDM POST call.
    */
    public function mockSuccessfulPostReturn()
    {
        $expectedReturn = array(
            "message" => "Ballot submission successful.",
        );

        return $expectedReturn;
    }
}