<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: createVotingRoundTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the POST Functionality of the VotingRound Web Service
Detail: (+ : implemented, - : not yet be implemented)
    + single valid electionId
    + multiple valid electionId
    + empty electionId
    + missing electionId
    + single invalid electionId
    + multiple invalid electionId
    + empty type
    + valid type
    + invalid type

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/19/2016  liaom

*/

use MiamiOH\RESTng\App;

class CreateVotingRoundTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $votingRound, $queryallRecords, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock api:
        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('perform', 'queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->votingRound = new \MiamiOH\FacultyElections\Services\VotingRound();
        $this->votingRound->setApp($api);
        $this->votingRound->setApiUser($this->user);
        $this->votingRound->setDatabase($db);
        $this->votingRound->setDatasource($ds);
        $this->votingRound->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSingleValidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidElectionId'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->createVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, $this->mockQuerySuccess());
    }

    public function testSingleValidType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidType'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->createVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, $this->mockQuerySuccess());
    }

    public function testSingleValidStartDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidStartDate'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->createVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, $this->mockQuerySuccess());
    }

    public function testSingleValidEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidEndDate'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->createVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, $this->mockQuerySuccess());
    }

    public function testSingleEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleEmptyElectionId'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testSingleEmptyType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleEmptyType'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty type",
                $e->getMessage());
        }
    }

    public function testSingleEmptyStartDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleEmptyStartDate'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty date",
                $e->getMessage());
        }
    }

    public function testSingleEmptyEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleEmptyEndDate'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty date",
                $e->getMessage());
        }
    }

    public function testUnknownParameters()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsUnknownParameters'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: too much parameters",
                $e->getMessage());
        }
    }

    public function testSingleInvalidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidElectionId'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testSingleInvalidType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidType'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid type",
                $e->getMessage());
        }
    }

    public function testSingleInvalidStartDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidStartDate'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid date",
                $e->getMessage());
        }
    }

    public function testSingleInvalidEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidEndDate'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid date",
                $e->getMessage());
        }
    }

    public function testValidStartEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsValidStartEndDate'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->createVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, $this->mockQuerySuccess());
    }

    public function testInvalidStartEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidStartEndDate'
            )));

        try {
            $this->votingRound->createVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: start date > end date",
                $e->getMessage());
        }
    }

    public function mockOptionsSingleValidElectionId()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleValidType()
    {
        return $this->mockOptionsSingleValidElectionId();
    }

    public function mockOptionsSingleValidStartDate()
    {
        return $this->mockOptionsSingleValidElectionId();
    }

    public function mockOptionsSingleValidEndDate()
    {
        return $this->mockOptionsSingleValidElectionId();
    }

    public function mockOptionsSingleEmptyElectionId()
    {
        $optionsArray = array(
            'electionId' => '',
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleEmptyType()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => '',
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleEmptyStartDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => '',
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleEmptyEndDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => ''
        );

        return $optionsArray;
    }

    public function mockOptionsUnknownParameters()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200,
            'unknown' => 'unknown'
        );

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidElectionId()
    {
        $optionsArray = array(
            'electionId' => 'invalidid123',
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidType()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 'invalidType',
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidStartDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => 'asdfd',
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidEndDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => 'sdfsdf'
        );

        return $optionsArray;
    }

    public function mockOptionsValidStartEndDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time() + 7200
        );

        return $optionsArray;
    }

    public function mockOptionsInvalidStartEndDate()
    {
        $optionsArray = array(
            'electionId' => 1111111,
            'type' => 101,
            'description' => 'hello world',
            'startDate' => time() + 3600,
            'endDate' => time()
        );

        return $optionsArray;
    }

    public function mockQuerySuccess()
    {
        $rtn = array();

        return $rtn;
    }
}