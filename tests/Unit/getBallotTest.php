<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getBallotTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Axhay Patel

DESCRIPTION: 
This php class is used to test the GET method for the Ballot service. Specifically 
Election ID option Usage.

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
FacultyElections/Ballot

TABLE USAGE:
FACELECTMGR.VOTE
FACELECTMGR.ROUND_TYPE

Web Service Usage:
	FacultyElections/Ballot service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/31/2016               PATELAH
Description:       Initial Draft

06/03/2016               PATELAH
Description:       Eliminated test cases for multiple electionIds which are not needed at this time.
			 
-----------------------------------------------------------
*/

use MiamiOH\RESTng\App;

class GetBallotTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $user, $ballot, $queryallRecords;


    // set up method automatically called by PHPUnit before every test method:

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->ballot = new \MiamiOH\FacultyElections\Services\Ballot();
        $this->ballot->setApp($this->api);
        $this->ballot->setApiUser($this->user);
        $this->ballot->setDatabase($db);
        $this->ballot->setDatasource($ds);
        $this->ballot->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
      *   Tests Case in which election Id option is not specified.
      *   Actual: No Election ID option specified.
      *	  Expected Return: At least one election ID must be specified.
      */
    public function testNoElectionID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->ballot->getBallot();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyElectionIDResult(),
                $e->getMessage());
        }

    }

    /*
      *   Tests Case in which election ID option is empty.
      *   Actual: Empty Election ID Given
      * 	Expected Return: 400 Error Message. At least one election ID must be specified.
      */
    public function testEmptyElectionID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyParameters'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->ballot->getBallot();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyElectionIDResult(),
                $e->getMessage());
        }

    }

    /*
    *   Tests when a single Election ID is provided.
      *	  Actual: Single Election ID option is given.
      * 	Expected Return: Results seen in the mockExpectedSingleElectionIDResults.
      */

    public function testSingleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        $resp = $this->ballot->getBallot();
        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

        $this->assertEquals(count($payload), 1);

        $this->assertEquals($payload, $this->mockExpectedSingleElectionIDResults());

    }

    /*
    * 	Tests when invalid Election ID are requested.
      *	  Actual: Single Invalid (Wrong Format) Election ID
      *	  Expected Return: 400 Error.
      */
    public function testInvalidWrongFormatSingleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidWrongFormatSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->ballot->getBallot();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidWrongFormatSingleElectionIDResult(),
                $e->getMessage());
        }
    }

    /*
    * 	Tests when an SQL Injection is attempted
      *	  Actual: Single SQL Injection Attempt
      *	  Expected Return: 400 Error
      */
    public function testSingleSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSQLInjectionSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->ballot->getBallot();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedSQLInjectionSingleElectionIDResult(),
                $e->getMessage());
        }

    }

    //Single mock methods
    public function mockOptionsSingleElectionID()
    {
        $optionsArray = array(
            'electionId' => '1234',
            'roundTypeName' => 'Semi-finalists round'
        );

        return $optionsArray;
    }

    public function mockExpectedSingleElectionIDResults()
    {
// 		$expectedReturn = array(
// 			'1234' => array(                         //election_id (Vote)
// 					"Semi-finalists round" => array(      //round_type_name (Round_Type)
// 						array(//ballot 1
// 							array(
// 								'voteRank' => '1',                
// 								'nomineeUniqueid' => 'MILLSE',
// 							),	//vote 1 
// 							 array(
// 								'voteRank' => '2',
// 								'nomineeUniqueid' => 'SCHMIDEE',
// 							),	//vote 2
// 							array( 
// 								'voteRank' => '3',
// 								'nomineeUniqueid' => 'PATELAH',
// 							),	//vote 3 
// 							array(     
// 								'voteRank' => '4',                
// 								'nomineeUniqueid' => 'JOHNSODA',
// 							),	//vote 4
// 							array(
// 								'voteRank' => '5',
// 								'nomineeUniqueid' => 'LIAOM',
// 							),	//vote 5
// 						),
// 					), // End of 12345 Element
// 				)
// 		);

        $expectedReturn =
            array(
                array(//ballot 1
                    array(
                        'electionId' => '1234',
                        'roundTypeName' => 'Semi-finalists round',
                        'voteRank' => '1',
                        'nomineeUniqueid' => 'MILLSE',
                    ),    //vote 1
                    array(
                        'electionId' => '1234',
                        'roundTypeName' => 'Semi-finalists round',
                        'voteRank' => '2',
                        'nomineeUniqueid' => 'SCHMIDEE',
                    ),    //vote 2
                    array(
                        'electionId' => '1234',
                        'roundTypeName' => 'Semi-finalists round',
                        'voteRank' => '3',
                        'nomineeUniqueid' => 'PATELAH',
                    ),    //vote 3
                    array(
                        'electionId' => '1234',
                        'roundTypeName' => 'Semi-finalists round',
                        'voteRank' => '4',
                        'nomineeUniqueid' => 'JOHNSODA',
                    ),    //vote 4
                    array(
                        'electionId' => '1234',
                        'roundTypeName' => 'Semi-finalists round',
                        'voteRank' => '5',
                        'nomineeUniqueid' => 'LIAOM',
                    ),    //vote 5
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllSingleElectionID()
    {
        return array(
            array(
                'ballot_id' => '1',
                'election_id' => '1234',
                'round_type_name' => 'Semi-finalists round',
                'vote_rank' => '1',
                'nominee_uniqueid' => 'MILLSE',
            ),//Block 1 of 12345
            array(
                'ballot_id' => '1',
                'election_id' => '1234',
                'round_type_name' => 'Semi-finalists round',
                'vote_rank' => '2',
                'nominee_uniqueid' => 'SCHMIDEE',
            ),//Block 1 of 12345
            array(
                'ballot_id' => '1',
                'election_id' => '1234',
                'round_type_name' => 'Semi-finalists round',
                'vote_rank' => '3',
                'nominee_uniqueid' => 'PATELAH',
            ),//Block 1 of 12345
            array(
                'ballot_id' => '1',
                'election_id' => '1234',
                'round_type_name' => 'Semi-finalists round',
                'vote_rank' => '4',
                'nominee_uniqueid' => 'JOHNSODA',
            ),//Block 1 of 12345
            array(
                'ballot_id' => '1',
                'election_id' => '1234',
                'round_type_name' => 'Semi-finalists round',
                'vote_rank' => '5',
                'nominee_uniqueid' => 'LIAOM',
            ),//Block 1 of 12345
        );
    }

    //Wrong format Single election ID
    public function mockOptionsInvalidWrongFormatSingleElectionID()
    {
        $optionsArray = array(
            'electionId' => 'worstElectionIdEver',
            'roundTypeName' => 'Semi-finalist'
        );

        return $optionsArray;
    }

    public function mockExpectedInvalidWrongFormatSingleElectionIDResult()
    {
        return "ElectionId must be numeric.";
    }

    //SQL injection for Single Election ID
    public function mockOptionsSQLInjectionSingleElectionID()
    {
        $optionsArray = array(
            'electionId' => "';--",
            'roundTypeName' => 'Semi-finalists Round'
        );

        return $optionsArray;
    }

    public function mockExpectedSQLInjectionSingleElectionIDResult()
    {
        return "ElectionId must be numeric.";
    }

    //No Election Return (No Parameters)
    public function mockNoParameters()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    //Empty Election ID Return
    public function mockOptionsEmptyParameters()
    {
        $optionsArray = array('electionId' => '', 'roundTypeName' => '');

        return $optionsArray;
    }

    public function mockExpectedEmptyElectionIDResult()
    {
        return "Error: At least one election ID or roundTypeName must be specified.";
    }

    public function mockResourceParams()
    {
        return array();
    }

}