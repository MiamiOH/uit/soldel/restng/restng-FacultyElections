<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getElectionTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Election Web Service
Detail: (+ : implemented, - : not yet be implemented)
    + Single valid electionId
	+ Single invalid electionId

	+ Multiple valid electionId
	+ Multiple invalid electionId

	+ Valid election status
	+ Invalid election status

	+ Empty election status
	+ Empty electionId

	+ Both valid election id and status
	+ Missing both election id and status

	- SQL injection

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/19/2016  liaom

*/

use MiamiOH\RESTng\App;

class GetElectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $election, $user, $request, $api;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'callResource',
                'getPayload'
            ))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->election = new \MiamiOH\FacultyElections\Services\Election();
        $this->election->setApp($this->api);
        $this->election->setApiUser($this->user);
        $this->election->setDatabase($db);
        $this->election->setDatasource($ds);
        $this->election->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSingleValidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidElectionId'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingleValidElectionId'
            )));

        $votingRoundResponse = new \MiamiOH\RESTng\Util\Response();
        $votingRoundResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($votingRoundResponse);

        $resp = $this->election->getElection();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 1);
        //var_dump($this->mockResultSingleValidElectionId());
        //var_dump($payload);
        //exit();
        $this->assertEquals($this->mockResultSingleValidElectionId(), $payload);
    }

    public function testSingleInvalidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidElectionId'
            )));

        try {
            $this->election->getElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testMultipleValidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleValidElectionId'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultipleValidElectionId'
            )));

        $votingRoundResponse = new \MiamiOH\RESTng\Util\Response();
        $votingRoundResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($votingRoundResponse);

        $resp = $this->election->getElection();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(3, count($payload));
        $this->assertEquals($this->mockResultMultipleValidElectionId(), $payload);
    }

    public function testMultipleInvalidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleInvalidElectionId'
            )));

        try {
            $this->election->getElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testValidElectionStatus()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsValidElectionStatus'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryValidElectionStatus'
            )));

        $votingRoundResponse = new \MiamiOH\RESTng\Util\Response();
        $votingRoundResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($votingRoundResponse);

        $resp = $this->election->getElection();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(2, count($payload));
        $this->assertEquals($this->mockResultValidElectionStatus(), $payload);
    }

    public function testInvalidElectionStatus()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidElectionStatus'
            )));

        try {
            $this->election->getElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionStatus",
                $e->getMessage());
        }
    }

    public function testEmptyElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyElectionId'
            )));

        try {
            $this->election->getElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testEmptyElectionStatus()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyElectionStatus'
            )));

        try {
            $this->election->getElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionStatus",
                $e->getMessage());
        }
    }

    public function testBothValid()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsBothValid'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryBothValid')));

        $votingRoundResponse = new \MiamiOH\RESTng\Util\Response();
        $votingRoundResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($votingRoundResponse);

        $resp = $this->election->getElection();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(1, count($payload));
        $this->assertEquals($this->mockResultBothValid(), $payload);
    }

    public function testBothMissing()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsBothMissing'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryBothMissing')));

        $votingRoundResponse = new \MiamiOH\RESTng\Util\Response();
        $votingRoundResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($votingRoundResponse);

        $resp = $this->election->getElection();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(4, count($payload));
        $this->assertEquals($this->mockResultBothMissing(), $payload);
    }

    public function mockOptionsSingleValidElectionId()
    {
        $optionsArray = array('electionId' => '1111111');

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidElectionId()
    {
        $optionsArray = array('electionId' => 'j3qgu7yu');

        return $optionsArray;
    }

    public function mockOptionsMultipleValidElectionId()
    {
        $optionsArray = array(
            'electionId' => array(
                '1111111',
                '2222222',
                '3333333',
            )
        );

        return $optionsArray;
    }

    public function mockOptionsMultipleInvalidElectionId()
    {
        $optionsArray = array(
            'electionId' => array(
                '1111111',
                '',
                'sdfs33',
            )
        );

        return $optionsArray;
    }

    public function mockOptionsValidElectionStatus()
    {
        $optionsArray = array('electionStatus' => 'open');

        return $optionsArray;
    }

    public function mockOptionsInvalidElectionStatus()
    {
        $optionsArray = array('electionStatus' => 'hahaha');

        return $optionsArray;
    }

    public function mockOptionsEmptyElectionId()
    {
        $optionsArray = array("electionId" => '');

        return $optionsArray;
    }

    public function mockOptionsEmptyElectionStatus()
    {
        $optionsArray = array('electionStatus' => '');

        return $optionsArray;
    }

    public function mockOptionsBothValid()
    {
        $optionsArray = array(
            'electionId' => array(
                '1111111',
                '2222222',
            ),
            'electionStatus' => 'open',
        );

        return $optionsArray;
    }

    public function mockOptionsBothMissing()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    public function mockOptionsSqlInjection()
    {

    }

    public function mockQuerySingleValidElectionId()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'election_name' => 'first election',
                'number_of_winners' => '1',
                'number_of_semi_finalists' => '1',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_status_name' => 'open',
                'election_is_open' => '1',
                'voter_pool_id' => '1',
                'nominee_pool_id' => '1',
                'pool_id' => '1',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
        );

        return $dbRtn;
    }

    public function mockQueryMultipleValidElectionId()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'election_name' => 'first election',
                'number_of_winners' => '1',
                'number_of_semi_finalists' => '1',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_status_name' => 'open',
                'election_is_open' => '1',
                'voter_pool_id' => '1',
                'nominee_pool_id' => '1',
                'pool_id' => '1',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '2222222',
                'election_name' => 'second election',
                'number_of_winners' => '2',
                'number_of_semi_finalists' => '2',
                'election_status_name' => 'close',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '2',
                'nominee_pool_id' => '2',
                'pool_id' => '2',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '3333333',
                'election_name' => 'third election',
                'number_of_winners' => '3',
                'number_of_semi_finalists' => '3',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '3',
                'nominee_pool_id' => '3',
                'pool_id' => '3',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
        );

        return $dbRtn;
    }

    public function mockQueryValidElectionStatus()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'election_name' => 'first election',
                'number_of_winners' => '1',
                'number_of_semi_finalists' => '1',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '1',
                'nominee_pool_id' => '1',
                'pool_id' => '1',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '3333333',
                'election_name' => 'third election',
                'number_of_winners' => '3',
                'number_of_semi_finalists' => '3',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '3',
                'nominee_pool_id' => '3',
                'pool_id' => '3',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
        );

        return $dbRtn;
    }

    public function mockQueryBothValid()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'election_name' => 'first election',
                'number_of_winners' => '1',
                'number_of_semi_finalists' => '1',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '1',
                'nominee_pool_id' => '1',
                'pool_id' => '1',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
        );

        return $dbRtn;
    }

    public function mockQueryBothMissing()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'election_name' => 'first election',
                'number_of_winners' => '1',
                'number_of_semi_finalists' => '1',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '1',
                'nominee_pool_id' => '1',
                'pool_id' => '1',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '2222222',
                'election_name' => 'second election',
                'number_of_winners' => '2',
                'number_of_semi_finalists' => '2',
                'election_status_name' => 'close',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '2',
                'nominee_pool_id' => '2',
                'pool_id' => '2',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '3333333',
                'election_name' => 'third election',
                'number_of_winners' => '3',
                'number_of_semi_finalists' => '3',
                'election_status_name' => 'open',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '3',
                'nominee_pool_id' => '3',
                'pool_id' => '3',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
            array(
                'election_id' => '4444444',
                'election_name' => 'fourth election',
                'number_of_winners' => '4',
                'number_of_semi_finalists' => '4',
                'election_status_name' => 'close',
                'division_filter_value' => null,
                'department_filter_value' => null,
                'campus_filter_value' => null,
                'election_is_open' => '1',
                'voter_pool_id' => '4',
                'nominee_pool_id' => '4',
                'pool_id' => '4',
                'pool_name' => 'a',
                'create_user' => 'testuser',
                'update_user' => 'testuser',
                'date_created' => '08-AUG-2016 17:04:01',
                'date_updated' => '08-AUG-2016 17:04:01'
            ),
        );

        return $dbRtn;
    }

    public function mockResultSingleValidElectionId()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'electionName' => 'first election',
                'numOfWinners' => '1',
                'numOfSemiFinalists' => '1',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '1',
                'nomineePoolId' => '1',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            )

        );

        return $funcRtn;
    }

    public function mockResultMultipleValidElectionId()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'electionName' => 'first election',
                'numOfWinners' => '1',
                'numOfSemiFinalists' => '1',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '1',
                'nomineePoolId' => '1',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '2222222',
                'electionName' => 'second election',
                'numOfWinners' => '2',
                'numOfSemiFinalists' => '2',
                'electionStatus' => 'close',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '2',
                'nomineePoolId' => '2',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '3333333',
                'electionName' => 'third election',
                'numOfWinners' => '3',
                'numOfSemiFinalists' => '3',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '3',
                'nomineePoolId' => '3',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
        );

        return $funcRtn;
    }

    public function mockResultValidElectionStatus()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'electionName' => 'first election',
                'numOfWinners' => '1',
                'numOfSemiFinalists' => '1',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '1',
                'nomineePoolId' => '1',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '3333333',
                'electionName' => 'third election',
                'numOfWinners' => '3',
                'numOfSemiFinalists' => '3',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '3',
                'nomineePoolId' => '3',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
        );

        return $funcRtn;
    }

    public function mockResultBothValid()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'electionName' => 'first election',
                'numOfWinners' => '1',
                'numOfSemiFinalists' => '1',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '1',
                'nomineePoolId' => '1',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
        );

        return $funcRtn;
    }

    public function mockResultBothMissing()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'electionName' => 'first election',
                'numOfWinners' => '1',
                'numOfSemiFinalists' => '1',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '1',
                'nomineePoolId' => '1',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '2222222',
                'electionName' => 'second election',
                'numOfWinners' => '2',
                'numOfSemiFinalists' => '2',
                'electionStatus' => 'close',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '2',
                'nomineePoolId' => '2',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '3333333',
                'electionName' => 'third election',
                'numOfWinners' => '3',
                'numOfSemiFinalists' => '3',
                'electionStatus' => 'open',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '3',
                'nomineePoolId' => '3',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            ),
            array(
                'electionId' => '4444444',
                'electionName' => 'fourth election',
                'numOfWinners' => '4',
                'numOfSemiFinalists' => '4',
                'electionStatus' => 'close',
                'divisionFilterValue' => '',
                'departmentFilterValue' => '',
                'campusFilterValue' => '',
                'voterPoolName' => 'a',
                'nomineePoolName' => 'a',
                'isOpen' => true,
                'voterPoolId' => '4',
                'nomineePoolId' => '4',
                'createUser' => 'testuser',
                'updateUser' => 'testuser',
                'dateCreated' => '08/08/2016 17:04:01',
                'dateUpdated' => '08/08/2016 17:04:01'
            )
        );

        return $funcRtn;
    }
}