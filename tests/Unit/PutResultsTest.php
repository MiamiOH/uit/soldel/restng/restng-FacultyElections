<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/**
 * -----------------------------------------------------------
 * FILE NAME: putResultsTest.php
 *
 * Copyright (c) 2016 Miami University, All Rights Reserved.
 *
 * Miami University grants you ("Licensee") a non-exclusive, royalty free,
 * license to use, modify and redistribute this software in source and
 * binary code form, provided that i) this copyright notice and license
 * appear on all copies of the software; and ii) Licensee does not utilize
 * the software in a manner which is disparaging to Miami University.
 *
 * This software is provided "AS IS" and any express or implied warranties,
 * including, but not limited to, the implied warranties of merchantability
 * and fitness for a particular purpose are disclaimed. It has been tested
 * and is believed to work as intended within Miami University's
 * environment. Miami University does not warrant this software to work as
 * designed in any other environment.
 *
 * AUTHOR: Mingchao Liao
 *
 * DESCRIPTION:  Unit Tests for Testing the GET Functionality of
 * the Results Web Service
 *
 * ENVIRONMENT DEPENDENCIES: PHP Unit
 *
 * AUDIT TRAIL:
 *
 * DATE        UniqueID
 * 07/26/2016  liaom
 * 08/10/2016  schmidee
 * Reformatted code and corrected spelling errors. Added in tests to check that STV
 * is being called. Added testNoBallotsTooManyNominees.
 * 08/11/2016  schmidee  Correction for edge case where a person can win having zero
 * votes.
 * 08/17/2016  schmidee  Corrected testCorrectParameters to now handle warning
 * return.
 */

use Exception;
use MiamiOH\RESTng\App;

class PutResultsTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $results, $queryallRecords, $user, $request, $awardService, $mockedApp, $stv;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->mockedApp = $this->createMock(App::class);

        $this->stv = $this->getMockBuilder('MiamiOH\FacultyElections\Services\Results\STV')
            ->disableOriginalConstructor()
            ->setMethods(['stv', 'setData', 'setNumOfWinners'])
            ->getMock();

        $this->mockedApp->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getData',
                'isPaged',
                'getOffset',
                'getLimit'
            ))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->results = new \MiamiOH\FacultyElections\Services\Results();
        $this->results->setApp($this->mockedApp);
        $this->results->setApiUser($this->user);
        $this->results->setSTV($this->stv);
        $this->results->setDatabase($db);
        $this->results->setDatasource($ds);
        $this->results->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /**
     * Too Many Parameters Test
     * Result: Exception thrown with an error saying "Error: too many parameters"
     */
    public function testTooManyParameters()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsTooManyParameters'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: too many parameters",
                $e->getMessage());
        }
    }

    /**
     * Options for Too Many Parameter Test
     *
     * @return array with Options for Too Many Parameter test.
     */
    public function mockOptionsTooManyParameters()
    {
        return array(
            'electionId' => '1234',
            'roundTypeName' => 'Semi-Finalists Round',
            'uniqueId' => 'liaom',
        );
    }


    /**
     *Empty Election ID Test
     * Results: Exception is thrown with an error saying "Error: empty electionId"
     */
    public function testEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyElectionId'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    /**
     * Options for Empty Election ID Test
     *
     * @return array with Options for Empty Election ID test.
     */
    public function mockOptionsEmptyElectionId()
    {
        return array(
            'electionId' => '',
            'roundTypeName' => 'Semi-Finalists Round',
        );
    }


    /**
     * Invalid Election ID Test
     * Result: Exception is thrown with an error saying "Error: invalid electionId"
     */
    public function testInvalidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidElectionId'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    /**
     * Options for Invalid Election ID Test
     *
     * @return array with Options for Invalid Election ID test.
     */
    public function mockOptionsInvalidElectionId()
    {
        return array(
            'electionId' => 'skdjhf',
            'roundTypeName' => 'Semi-Finalists Round',
        );
    }

    /**
     * Missing Election ID Test
     * Result: Exception is thrown with an error saying "Error: missing required"
     */
    public function testMissingElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMissingElectionId'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required",
                $e->getMessage());
        }
    }

    /**
     * Options for Missing Election ID Test
     *
     * @return array with Options for Missing Election ID test.
     */
    public function mockOptionsMissingElectionId()
    {
        return array(
            'roundTypeName' => 'Semi-Finalists Round',
        );
    }

    /**
     * Multiple Election ID Test
     * Result: Exception is thrown with an error saying "it should be single"
     */
    public function testElectionIdNotSingle()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsElectionIdNotSingle'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    /**
     * Options for Multiple Election ID Test
     *
     * @return array with Options for Multiple Election ID test.
     */
    public function mockOptionsElectionIdNotSingle()
    {
        return array(
            'electionId' => ['1234', '5678'],
            'roundTypeName' => 'Semi-Finalists Round',
        );
    }

    /**
     * Empty Round Type Name Test
     * Result: Exception is throw with an error saying "Error: empty roundTypeName"
     */
    public function testEmptyRoundTypeName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyRoundTypeName'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty roundTypeName",
                $e->getMessage());
        }
    }

    /**
     * Options for Empty Round Type Test
     *
     * @return array with Options for Empty Round Type Name test.
     */
    public function mockOptionsEmptyRoundTypeName()
    {
        return array(
            'electionId' => '1234',
            'roundTypeName' => '',
        );
    }

    /**
     * Invalid Round Type Name Test
     * Result: Exception is thrown with an error saying "Error: invalid
     * roundTypeName"
     */
    public function testInvalidRoundTypeName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidRoundTypeName'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid roundTypeName",
                $e->getMessage());
        }
    }

    /**
     * Options for Invalid Round Type Name Test
     *
     * @return array with Options for Invalid Round Type Name test.
     */
    public function mockOptionsInvalidRoundTypeName()
    {
        return array(
            'electionId' => '1234',
            'roundTypeName' => 'aaa',
        );
    }

    /**
     * Missing Round Type Name Test
     * Result: Exception is thrown with an error saying "Error: missing require"
     */
    public function testMissingRoundTypeName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMissingRoundTypeName'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing require",
                $e->getMessage());
        }
    }

    /**
     * Options for Missing Round Type Name Test
     *
     * @return array with Options for Missing Round Type Name Test
     */
    public function mockOptionsMissingRoundTypeName()
    {
        return array(
            'electionId' => '1234',
        );
    }

    /**
     * Multiple Round Type Test
     * Result: Exception is thrown with an error saying "it should be single"
     */
    public function testRoundTypeNameNotSingle()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsRoundTypeNameNotSingle'
            )));

        try {
            $this->results->calculateResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    /**
     * Options for Multiple Round Type Test
     *
     * @return array with Options for Multiple Round Type test.
     */
    public function mockOptionsRoundTypeNameNotSingle()
    {
        return array(
            'electionId' => '1234',
            'roundTypeName' => ['Semi-Finalists Round', 'Finalists Round'],
        );
    }


    /**
     * SQL Injection on Election ID Test
     * Result: Exception is thrown with an error saying "Error: invalid electionId"
     */
    public function testSingleSQLInjectionElectionId()
    {

        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSQLInjectionSingleElectionID'
            )));

        try {
            $resp = $this->results->calculateResults();
        } catch (\Exception $e) {
            $this->assertStringStartsWith($this->mockExpectedSQLInjectionSingleElectionIDResult(),
                $e->getMessage());
        }

    }

    /**
     * Options for SQL Injection on Election ID Test
     *
     * @return array with Options for SQL Injection on Election ID
     */
    public function mockOptionsSQLInjectionSingleElectionID()
    {
        $optionsArray = array('electionId' => array("';--"));

        return $optionsArray;
    }


    /**
     * Expected Results from SQL Injection on Election ID Test
     *
     * @return string with expected error message to be returned.
     */
    public function mockExpectedSQLInjectionSingleElectionIDResult()
    {
        return "Error: invalid electionId";
    }


    /**
     * SQL Injection on Round Type Name Test
     * Result: Exception is thrown with an error saying "Error: invalid
     * roundTypeName"
     */
    public function testSingleSQLInjectionRoundTypeName()
    {

        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSQLInjectionRoundTypeName'
            )));

        try {
            $resp = $this->results->calculateResults();
        } catch (\Exception $e) {
            $this->assertStringStartsWith($this->mockExpectedSQLInjectionRoundTypeNameResult(),
                $e->getMessage());
        }

    }

    /**
     * Options for SQL Injection on Round Type Test
     *
     * @return array with Options for SQL Injection on Round Type
     */
    public function mockOptionsSQLInjectionRoundTypeName()
    {
        $optionsArray = array(
            'electionId' => "1234",
            'roundTypeName' => "';--"
        );

        return $optionsArray;
    }

    /**
     * Expected Results from SQL Injection on Round Type Name Test
     *
     * @return string with expected error message to be returned.
     */
    public function mockExpectedSQLInjectionRoundTypeNameResult()
    {
        return "Error: invalid roundTypeName";
    }

    /**
     * Test Correct Parameters
     * Result: Should return with a 200 OK Response
     */
    public function testCorrectParameters()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsCorrectParameters'
            )));

        //What should be returned by the get ballot query
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetBallot'
            )));

        //What should be returned by the get number of winnners query
        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetNumberOfWinners'
            )));

        //What should be returned by the removed nominees query
        $this->dbh->expects($this->at(2))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryEmptyArray'
            )));

        //What should be returned by the valid nominees query
        $this->dbh->expects($this->at(3))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetNumberValidWinners'
            )));

        //What should be returned by the update nominee status query
        $this->dbh->expects($this->at(0))->method('perform')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryUpdateNomineeStatus'
            )));

        $this->stv->method('stv')
            ->will($this->returnValue(
                array('liaom' => 2)
            ));
        $resp = $this->results->calculateResults();

        $payload = $resp->getPayload();
        $warnings = $payload['warnings'];
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(1, count($payload));
        //$this->assertEquals(2, count($warnings));

        //Can No longer check this due to it is random
        // $this->assertEquals($payload, $this->mockReturnCorrectParameters());

    }


    /**
     * Mock Options for the Correct Parameter Test
     *
     * @return array containing the options for the Correct Parameter test.
     */
    public function mockOptionsCorrectParameters()
    {
        $optionsArray = array(
            'electionId' => "1234",
            'roundTypeName' => 'Finalists Round'
        );

        return $optionsArray;
    }

    /**
     * Mock Query returning Empty
     *
     * @return array returns a empty array
     */
    public function mockQueryEmptyArray()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    /**
     * Mock Query for retrieving Ballots
     *
     * @return array containing the mock return from database when retrieving ballots
     */
    public function mockQueryGetBallot()
    {
        $optionsArray = array(
            array(
                'ballot_id' => "1234",
                'nominee_uniqueid' => "liaom",
                'vote_rank' => "1"
            ),
            array(
                'ballot_id' => "1234",
                'nominee_uniqueid' => "patelah",
                'vote_rank' => "2"
            ),

            array(
                'ballot_id' => "123",
                'nominee_uniqueid' => "millse",
                'vote_rank' => "2"
            ),
            array(
                'ballot_id' => "123",
                'nominee_uniqueid' => "patelah",
                'vote_rank' => "1"
            )
        );

        return $optionsArray;
    }

    /**
     * Mock Query for retrieving number of Winners
     *
     * @return array containing the mock return from database when retrieving number
     *     of winners
     */
    public function mockQueryGetNumberOfWinners()
    {
        $optionsArray = array(
            array(
                'number_of_winners' => "1",
            )
        );

        return $optionsArray;
    }


    /**
     * Mock Query for retrieving Valid Winners
     *
     * @return array containing the mock return from database when retrieving number
     *     of winners
     */
    public function mockQueryGetNumberValidWinners()
    {
        $optionsArray = array(
            array(
                'uniqueid' => "liaom",
            ),
            array(
                'uniqueid' => "patelah",
            ),
            array(
                'uniqueid' => "millse",
            ),
        );

        return $optionsArray;
    }

    /**
     * Mock return for updating a Nominee's Status
     *
     * @return array containing the expected return when updating a Nominee's status
     */
    public function mockQueryUpdateNomineeStatus()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    /**
     * Test for No ballots and too many nominees
     * Result: Error thrown saying "Unable to calculate winners due to lack of votes
     * and too many nominees."
     */
    public function testNoBallotsTooManyNominees()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsCorrectParameters'
            )));

        //What should be returned when calling ballots query
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryEmptyArray'
            )));

        //What should be returned by the get number of winners query
        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetNumberOfWinners'
            )));

        //What should be returned by the removed nominees query
        $this->dbh->expects($this->at(2))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryEmptyArray'
            )));

        //What should be returned by the valid nominees query
        $this->dbh->expects($this->at(2))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetNumberValidWinners'
            )));

        //What should be returned by the valid nominees query
        $this->dbh->expects($this->at(3))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetNumberValidWinners'
            )));

        $this->stv->method('stv')
            ->will($this->returnValue(
                array('liaom' => 2)
            ));

        //What should be returned when the nominee status is updated
        $this->dbh->expects($this->at(0))->method('perform')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryUpdateNomineeStatus'
            )));

        try {
            $resp = $this->results->calculateResults();
            $this->fail("No Ballots, Too Many nominees failed to throw an exception.");
        } catch (Exception $e) {
            $this->assertEquals("Unable to calculate winners due to lack of votes and too many nominees.",
                $e->getMessage());
        }
    }


}
