<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getParticipantUniqueIDTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the Participant service. Specifically 
Unique ID Paramater Usage.

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
FacultyElections/Participant

TABLE USAGE:

Web Service Usage:
	FacultyElections/Participant service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */

use MiamiOH\RESTng\App;

class GetParticipantUniqueIDTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $participant, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockPersonResponse());
        $this->api->method('callResource')->willReturn($personResponse);

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->participant = new \MiamiOH\FacultyElections\Services\Participant();
        $this->participant->setApp($this->api);
        $this->participant->setApiUser($this->user);
        $this->participant->setDatabase($db);
        $this->participant->setDatasource($ds);
        $this->participant->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *   No Unique ID Given
     * 	Tests Case in which empty parameteres given at all. This is technically a clone
     *	of the get multiple elections due to it is using the same mock methods, but it
     *	is testing for a different path in the code.
     *	Expected Return: Returns all Election.
     */
    /* Not Written since its a Duplicate of another Test*/

    /*
     *   Empty Unique ID Given
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error (Error Message mockExpectedEmptyUniqueIDResult)
     */
    public function testEmptyUniqueID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleUniqueID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyUniqueIDResult(),
                $e->getMessage());
        }

    }


    /*
     *	Single Unique ID Test
     * 	Tests when a single Unique ID is requested.
     *	Expected Return: Results seen in the mockSingleUniqueIDResults.
     */
    public function testSingleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockSingleUniqueIDOptions')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleUniqueID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 4);
        $this->assertEquals($payload, $this->mockSingleUniqueIDResults());
    }

    /*
     *	Single Invalid (Not Found) Unique ID
     * 	Tests when invalid Unique ID are requested.
     *	Expected Return: 404 Error (Error Message mockExpectedInvalidNotFoundSingleUniqueIDResult)
     */
    public function testInvalidNotFoundSingleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidNotFoundSingleUniqueID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllInvalidNotFoundSingleUniqueID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload,
            $this->mockExpectedInvalidNotFoundSingleUniqueIDResult());

    }

    /*
     *	Single Invalid (Wrong Format) Unique ID
     * 	Tests when invalid Unique ID are requested.
     *	Expected Return: 400 Error.
     */
    public function testInvalidWrongFormatSingleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidWrongFormatSingleUniqueID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleUniqueID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidWrongFormatSingleUniqueIDResult(),
                $e->getMessage());
        }
    }

    /*
     *	Single SQL Injection Attempt
     * 	Tests when an SQL Injection is attempted
     *	Expected Return: 400 Error
     */
    public function testSingleSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSQLInjectionSingleUniqueID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleUniqueID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedSQLInjectionSingleUniqueIDResult(),
                $e->getMessage());
        }

    }

    /*
     *	Multiple Unique ID
     * 	Tests when  multiple Unique IDs are requested.
     *	Expected Return: Results seen in the mockMultipleUniqueIDResults.
     */
    public function testMultipleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleUniqueID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleUniqueID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 11);
        $this->assertEquals($this->mockMultipleUniqueIDResults(), $payload);
    }


    /*
     *	Multiple Invalid (Wrong Format) Unique ID
     * 	Tests when invalid Unique ID are requested.
     *	Expected Return: 400 Error.
     */
    public function testInvalidWrongFormatMultipleUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidWrongFormatMultipleUniqueIDOptions'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleUniqueID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidWrongFormatMultipleUniqueIDResult(),
                $e->getMessage());
        }
    }

    /*
     *	Multiple SQL Injection Attempt
     * 	Tests when an SQL Injection is attempted
     *	Expected Return: 400 Error
     */
    public function testMultipleSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockSQLInjectionMultipleUniqueIDOptions'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleUniqueID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedSQLInjectionMultipleUniqueIDResult(),
                $e->getMessage());
        }

    }

    //Single
    public function mockSingleUniqueIDOptions()
    {
        $optionsArray = array('uniqueId' => array('testuser'));

        return $optionsArray;
    }

    public function mockSingleUniqueIDResults()
    {
        return array(
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'testuser',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'testuser2',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(

                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '123456789',
                'uniqueId' => 'testuser',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '000000001',
                'uniqueId' => 'testuser3',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            )
        );
    }

    public function mockQueryAllSingleUniqueID()
    {
        return array(
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'testuser',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'testuser2',

                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '123456789',
                'uniqueid' => 'testuser',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '000000001',
                'uniqueid' => 'testuser3',

                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            )
        );
    }

    //Invalid Single
    public function mockOptionsInvalidNotFoundSingleUniqueID()
    {
        $optionsArray = array('uniqueId' => array('testuser32'));

        return $optionsArray;
    }

    public function mockExpectedInvalidNotFoundSingleUniqueIDResult()
    {
        return array();

    }

    public function mockQueryAllInvalidNotFoundSingleUniqueID()
    {
        return array();
    }

    public function mockOptionsInvalidWrongFormatSingleUniqueID()
    {
        $optionsArray = array('uniqueId' => array('12312-3123'));

        return $optionsArray;
    }

    public function mockExpectedInvalidWrongFormatSingleUniqueIDResult()
    {
        return "Unique ID must be alpha numeric and at least 3 characters.";
    }

    public function mockOptionsSQLInjectionSingleUniqueID()
    {
        $optionsArray = array('uniqueId' => array("';--"));

        return $optionsArray;
    }

    public function mockExpectedSQLInjectionSingleUniqueIDResult()
    {
        return "Unique ID must be alpha numeric and at least 3 characters.";
    }

    //Mutiple Mock Methods
    public function mockOptionsMultipleUniqueID()
    {
        $optionsArray = array(
            'uniqueId' => array(
                'testuser',
                'testuser1',
                'testuser2',
                'testuser3'
            )
        );

        return $optionsArray;
    }

    public function mockMultipleUniqueIDResults()
    {
        return array(
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'testuser1',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(//1
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'testuser2',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(//2
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'testuser3',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(//3
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'testuser4',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(//4
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'testuser5',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(//5
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '123456789',
                'uniqueId' => 'testuser1',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '123456789',
                'uniqueId' => 'testuser2',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '123456789',
                'uniqueId' => 'testuser3',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '000000001',
                'uniqueId' => 'testuser1',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ), // End of testuser
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '000000001',
                'uniqueId' => 'testuser2',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'finalist',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),//End of testuser1
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000000003',
                'uniqueId' => 'testuser1',
                'preferredName' => 'Neville Longbottom',

                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'testuser_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'testuser_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ), // End of testuser3
        );
    }

    public function mockQueryAllMultipleUniqueID()
    {
        return array(
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'testuser1',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'testuser2',

                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'testuser3',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(//3
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'testuser4',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ), // End of testuser
            array(//4
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'testuser5',

                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(//5
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '123456789',
                'uniqueid' => 'testuser1',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(//6
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '123456789',
                'uniqueid' => 'testuser2',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),//End of testuser1
            array(//7
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '123456789',
                'uniqueid' => 'testuser3',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(//8
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '000000001',
                'uniqueid' => 'testuser1',

                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(//9
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '000000001',
                'uniqueid' => 'testuser2',

                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'finalist',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000000003',
                'uniqueid' => 'testuser1',

                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'testuser_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'testuser_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ), // End of testuser3
        );
    }

    //Invalid Multiple

    public function mockInvalidWrongFormatMultipleUniqueIDOptions()
    {
        $optionsArray = array(
            'uniqueId' => array(
                'testuser',
                'testuser1',
                'testuser2',
                'testuser3',
                '123-4123123'
            )
        );

        return $optionsArray;
    }

    public function mockExpectedInvalidWrongFormatMultipleUniqueIDResult()
    {
        return "Unique ID must be alpha numeric and at least 3 characters.";
    }

    public function mockSQLInjectionMultipleUniqueIDOptions()
    {
        $optionsArray = array(
            'uniqueId' => array(
                'testuser',
                'testuser1',
                'testuser2',
                'testuser3',
                "';--"
            )
        );

        return $optionsArray;
    }

    public function mockExpectedSQLInjectionMultipleUniqueIDResult()
    {
        return "Unique ID must be alpha numeric and at least 3 characters.";
    }


    //No Election Return (No Parameters)
    public function mockNoParameters()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    //Empty Unique ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('uniqueId' => array(''));

        return $optionsArray;
    }

    public function mockExpectedEmptyUniqueIDResult()
    {
        return "Unique ID must be alpha numeric and at least 3 characters.";
    }


    public function mockResourceParams()
    {
        return array();
    }

    public function mockPersonResponse()
    {
        return array(
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER1',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER2',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER3',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER4',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER5',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER6',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER7',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER8',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER9',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER10',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            )
        );
    }
}