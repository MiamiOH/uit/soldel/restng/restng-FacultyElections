<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: modifyCriteriaGroupTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
08/03/2016  liaom

*/

use MiamiOH\RESTng\App;

class ModifyCriteriaGroupTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $criteriaGroup, $queryallRecords, $user, $request, $awardService, $api, $e;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        //set up the mock dbh:
        $this->e = $this->getMockBuilder('\Exception')
            ->setMethods(array('getMessage'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->criteriaGroup = new \MiamiOH\FacultyElections\Services\CriteriaGroup();
        $this->criteriaGroup->setApp($this->api);
        $this->criteriaGroup->setApiUser($this->user);
        $this->criteriaGroup->setDatabase($db);
        $this->criteriaGroup->setDatasource($ds);
        $this->criteriaGroup->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testMissingPoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyMissingPoolId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->modifyCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing req",
                $e->getMessage());
        }
    }

    public function testInvalidPoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyInvalidPoolId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->modifyCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid poolId",
                $e->getMessage());
        }
    }

    public function testEmptyPoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyPoolId'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->modifyCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty poolId",
                $e->getMessage());
        }
    }

    public function testPoolIdNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyPoolIdNotArray'
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        try {
            $this->criteriaGroup->modifyCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function mockBodyMissingPoolId()
    {
        return array(
            'poolName' => 'aaaa'
        );
    }

    public function mockBodyInvalidPoolId()
    {
        return array(
            'poolId' => 'jkk38d',
            'poolName' => 'aaaa'
        );
    }

    public function mockBodyEmptyPoolId()
    {
        return array(
            'poolId' => '',
            'poolName' => 'aaaa'
        );
    }

    public function mockBodyPoolIdNotArray()
    {
        return array(
            'poolId' => [1234, 2222],
            'poolName' => 'aaaa'
        );
    }
}