<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

use MiamiOH\RESTng\App;

define('DB_NO_ERROR', '');

/*
-----------------------------------------------------------
FILE NAME: createParticipantTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the POST Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/07/2016  liaom

*/

class CreateParticipantTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $participant, $queryallRecords, $user, $request, $awardService, $api, $OCI8;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'prepare'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->OCI8 = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute'))
            ->getMock();

        $this->dbh->method('prepare')
            ->willReturn($this->OCI8);

        $this->OCI8->method('execute')->willReturn(1);

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->participant = new \MiamiOH\FacultyElections\Services\Participant();
        $this->participant->setApp($this->api);
        $this->participant->setApiUser($this->user);
        $this->participant->setDatabase($db);
        $this->participant->setDatasource($ds);
        $this->participant->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSetAcceptanceForVoter()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'uniqueId' => array('liaom', 'patelah'),
                'electionId' => '2',
                'type' => 'voter',
                'acceptance' => 'accepted',
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $electionResponse = new \MiamiOH\RESTng\Util\Response();
        $electionResponse->setPayload(array());
        $this->api->method('callResource')->willReturn($electionResponse);

        try {
            $this->participant->createParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: setting acceptance or nomineeStatus for voter",
                $e->getMessage());
        }
    }

    public function testElectionNotFound()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'uniqueId' => array('liaom', 'patelah'),
                'electionId' => '1',
                'type' => 'nominee',
                'acceptance' => 'accepted',
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $electionResponse = new \MiamiOH\RESTng\Util\Response();
        $electionResponse->setPayload(array());
        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload(array(
            array(
                'uniqueId' => 'liaom'
            ),
            array(
                'uniqueId' => 'patelah'
            )
        ));
        $this->api->method('callResource')->will($this->onConsecutiveCalls($personResponse,
            $electionResponse));

        try {
            $this->participant->createParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("does not exist",
                $e->getMessage());
        }
    }

    public function testClosedElection()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'uniqueId' => array('liaom', 'patelah'),
                'electionId' => '1',
                'type' => 'nominee',
                'acceptance' => 'accepted',
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $electionResponse = new \MiamiOH\RESTng\Util\Response();
        $electionResponse->setPayload(array(
            array(
                'electionStatus' => 'Closed'
            ),
        ));
        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload(array(
            array(
                'uniqueId' => 'liaom'
            ),
            array(
                'uniqueId' => 'patelah'
            )
        ));
        $this->api->method('callResource')->will($this->onConsecutiveCalls($personResponse,
            $electionResponse));

        try {
            $this->participant->createParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("has been closed",
                $e->getMessage());
        }
    }

    public function testExecute()
    {
        $this->request->method('getData')
            ->will($this->returnValue(array(
                'uniqueId' => array('liaom', 'patelah'),
                'electionId' => '1',
                'type' => 'nominee',
                'acceptance' => 'accepted',
            )));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $electionResponse = new \MiamiOH\RESTng\Util\Response();
        $electionResponse->setPayload(array(
            array(
                'electionStatus' => 'Open'
            ),
        ));
        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload(array(
            array(
                'uniqueId' => 'liaom'
            ),
            array(
                'uniqueId' => 'patelah'
            )
        ));
        $this->api->method('callResource')->will($this->onConsecutiveCalls($personResponse,
            $electionResponse));

        $resp = $this->participant->createParticipant();

        $payload = $resp->getPayload();
        $this->assertEquals($payload, []);
    }
}