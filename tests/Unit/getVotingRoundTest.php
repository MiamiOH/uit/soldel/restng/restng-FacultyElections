<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getVotingRoundTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the VotingRound Web Service
Detail: (+ : implemented, - : not yet be implemented)
    + single valid electionId
    + multiple valid electionId
    + empty electionId
    + single invalid electionId
    + multiple invalid electionId
    + empty type
    + valid type
    + invalid type

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/19/2016  liaom

*/
use MiamiOH\RESTng\App;

class GetVotingRoundTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $votingRound, $queryallRecords, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock api:
        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->votingRound = new \MiamiOH\FacultyElections\Services\VotingRound();
        $this->votingRound->setApp($api);
        $this->votingRound->setApiUser($this->user);
        $this->votingRound->setDatabase($db);
        $this->votingRound->setDatasource($ds);
        $this->votingRound->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSingleValidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleValidElectionId'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingleValidElectionId'
            )));


        $resp = $this->votingRound->getVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 3);
        $this->assertEquals($payload, $this->mockResultSingleValidElectionId());
    }

    public function testMultipleValidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleValidElectionId'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultipleValidElectionId'
            )));


        $resp = $this->votingRound->getVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 8);
        $this->assertEquals($payload, $this->mockResultMultipleValidElectionId());
    }

    public function testEmptyElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyElectionId'
            )));

        try {
            $this->votingRound->getVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testSingleInvalidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSingleInvalidElectionId'
            )));

        try {
            $this->votingRound->getVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testMultipleInvalidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleInvalidElectionId'
            )));

        try {
            $this->votingRound->getVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertTrue("Error: empty electionId" == $e->getMessage()
                || "Error: invalid electionId" == $e->getMessage(),
                "Error: invalid or empty electionId");
        }
    }

    public function testEmptyType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsEmptyType')));

        try {
            $this->votingRound->getVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty type",
                $e->getMessage());
        }
    }

    public function testValidType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsValidType'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryValidType')));


        $resp = $this->votingRound->getVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 1);
        $this->assertEquals($payload, $this->mockResultValidType());
    }

    public function testInvalidType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidType'
            )));

        try {
            $this->votingRound->getVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid type",
                $e->getMessage());
        }
    }

    public function mockOptionsSingleValidElectionId()
    {
        $optionsArray = array('electionId' => array('1111111'));

        return $optionsArray;
    }

    public function mockOptionsMultipleValidElectionId()
    {
        $optionsArray = array(
            'electionId' => array(
                '1111111',
                '2222222',
                '3333333',
            )
        );

        return $optionsArray;
    }

    public function mockOptionsEmptyElectionId()
    {
        $optionsArray = array("electionId" => '');

        return $optionsArray;
    }

    public function mockOptionsMissingElectionId()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    public function mockOptionsSingleInvalidElectionId()
    {
        $optionsArray = array('electionId' => array('j3qgu7yu'));

        return $optionsArray;
    }

    public function mockOptionsMultipleInvalidElectionId()
    {
        $optionsArray = array(
            'electionId' => array(
                '1111111',
                '',
                'sdfs33',
            )
        );

        return $optionsArray;
    }

    public function mockOptionsEmptyType()
    {
        $optionsArray = array('electionId' => '1111111', 'type' => '');

        return $optionsArray;
    }

    public function mockOptionsValidType()
    {
        $optionsArray = array('electionId' => '1111111', 'type' => '101');

        return $optionsArray;
    }

    public function mockOptionsInvalidType()
    {
        $optionsArray = array('electionId' => '1111111', 'type' => '67y');

        return $optionsArray;
    }

    public function mockQuerySingleValidElectionId()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'round_type_name' => 'selfNominationRound',
                'start_date' => '1-MAY-16 16:19:35',
                'end_date' => '2-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '1111111',
                'round_type_name' => 'semiFinalistRound',
                'start_date' => '3-MAY-16 16:19:35',
                'end_date' => '4-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '1111111',
                'round_type_name' => 'finalistRound',
                'start_date' => '5-MAY-16 16:19:35',
                'end_date' => '6-MAY-16 16:19:35',
            ),
        );

        return $dbRtn;
    }

    public function mockQueryMultipleValidElectionId()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'round_type_name' => 'selfNominationRound',
                'start_date' => '1-MAY-16 16:19:35',
                'end_date' => '2-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '1111111',
                'round_type_name' => 'semiFinalistRound',
                'start_date' => '3-MAY-16 16:19:35',
                'end_date' => '4-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '1111111',
                'round_type_name' => 'finalistRound',
                'start_date' => '5-MAY-16 16:19:35',
                'end_date' => '6-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '2222222',
                'round_type_name' => 'selfNominationRound',
                'start_date' => '7-MAY-16 16:19:35',
                'end_date' => '8-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '2222222',
                'round_type_name' => 'semiFinalistRound',
                'start_date' => '9-MAY-16 16:19:35',
                'end_date' => '10-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '3333333',
                'round_type_name' => 'selfNominationRound',
                'start_date' => '11-MAY-16 16:19:35',
                'end_date' => '12-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '3333333',
                'round_type_name' => 'semiFinalistRound',
                'start_date' => '13-MAY-16 16:19:35',
                'end_date' => '14-MAY-16 16:19:35',
            ),
            array(
                'election_id' => '3333333',
                'round_type_name' => 'finalistRound',
                'start_date' => '15-MAY-16 16:19:35',
                'end_date' => '16-MAY-16 16:19:35',
            ),
        );

        return $dbRtn;
    }

    public function mockQueryValidType()
    {
        $dbRtn = array(
            array(
                'election_id' => '1111111',
                'round_type_name' => 'finalistRound',
                'start_date' => '5-MAY-16 16:19:35',
                'end_date' => '6-MAY-16 16:19:35',
            ),
        );

        return $dbRtn;
    }

    public function mockResultSingleValidElectionId()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'type' => 'selfNominationRound',
                'startDate' => '1-MAY-16 16:19:35',
                'endDate' => '2-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '1111111',
                'type' => 'semiFinalistRound',
                'startDate' => '3-MAY-16 16:19:35',
                'endDate' => '4-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '1111111',
                'type' => 'finalistRound',
                'startDate' => '5-MAY-16 16:19:35',
                'endDate' => '6-MAY-16 16:19:35',
            ),
        );

        return $funcRtn;
    }

    public function mockResultMultipleValidElectionId()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'type' => 'selfNominationRound',
                'startDate' => '1-MAY-16 16:19:35',
                'endDate' => '2-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '1111111',
                'type' => 'semiFinalistRound',
                'startDate' => '3-MAY-16 16:19:35',
                'endDate' => '4-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '1111111',
                'type' => 'finalistRound',
                'startDate' => '5-MAY-16 16:19:35',
                'endDate' => '6-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '2222222',
                'type' => 'selfNominationRound',
                'startDate' => '7-MAY-16 16:19:35',
                'endDate' => '8-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '2222222',
                'type' => 'semiFinalistRound',
                'startDate' => '9-MAY-16 16:19:35',
                'endDate' => '10-MAY-16 16:19:35',
            ),

            array(
                'electionId' => '3333333',
                'type' => 'selfNominationRound',
                'startDate' => '11-MAY-16 16:19:35',
                'endDate' => '12-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '3333333',
                'type' => 'semiFinalistRound',
                'startDate' => '13-MAY-16 16:19:35',
                'endDate' => '14-MAY-16 16:19:35',
            ),
            array(
                'electionId' => '3333333',
                'type' => 'finalistRound',
                'startDate' => '15-MAY-16 16:19:35',
                'endDate' => '16-MAY-16 16:19:35',
            ),
        );

        return $funcRtn;
    }

    public function mockResultValidType()
    {
        $funcRtn = array(
            array(
                'electionId' => '1111111',
                'type' => 'finalistRound',
                'startDate' => '5-MAY-16 16:19:35',
                'endDate' => '6-MAY-16 16:19:35',
            ),
        );

        return $funcRtn;
    }
}