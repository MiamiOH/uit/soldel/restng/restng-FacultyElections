<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: modifyParticipantTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/07/2016  liaom

*/

use MiamiOH\RESTng\App;

class modifyParticipantTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $participant, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->participant = new \MiamiOH\FacultyElections\Services\Participant();
        $this->participant->setApp($this->api);
        $this->participant->setApiUser($this->user);
        $this->participant->setDatabase($db);
        $this->participant->setDatasource($ds);
        $this->participant->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyElectionId'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testElectionIdNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataElectionIdNotArray'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testInvalidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidElectionId'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testMissingElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMissingElectionId'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameter",
                $e->getMessage());
        }
    }

    public function testUniqueIdNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataUniqueIdNotArray'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testEmptyUniqueId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyUniqueId'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty uniqueId",
                $e->getMessage());
        }
    }

    public function testInvalidUniqueId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidUniqueId'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid uniqueId",
                $e->getMessage());
        }
    }

    public function testTypeNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataTypeNotArray'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testEmptyType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyType'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty type",
                $e->getMessage());
        }
    }

    public function testInvalidType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidType'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid type",
                $e->getMessage());
        }
    }

    public function testAcceptanceNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataAcceptanceNotArray'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testEmptyAcceptance()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataEmptyAcceptance'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty acceptance",
                $e->getMessage());
        }
    }

    public function testInvalidAcceptance()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidAcceptance'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid acceptance",
                $e->getMessage());
        }
    }

    public function testNomineeStatusNotArray()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataNomineeStatusNotArray'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testInvalidNomineeStatus()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataInvalidNomineeStatus'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid nomineeStatus",
                $e->getMessage());
        }
    }

    public function testFacultySetUniqueId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataFacultySetUniqueId'
            )));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(0));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("change these data",
                $e->getMessage());
        }
    }

    public function testFacultySetType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataFacultySetType'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(0));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("change these data",
                $e->getMessage());
        }
    }

    public function testFacultySetNomineeStatus()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataFacultySetNomineeStatus'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(0));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("change these data",
                $e->getMessage());
        }
    }

    public function testUserRecordNotFound()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataFacultyAllCorrect'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultUserRecordNotFound());
        $this->api->method('callResource')->willReturn($personResponse);

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(1));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertContains("does not participate",
                $e->getMessage());
        }
    }

    public function testFacultySetAcceptance2Default()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockDataFacultySetAcceptance2Default'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockResultWithUserRecord());
        $electionResponse = new \MiamiOH\RESTng\Util\Response();
        $electionResponse->setPayload(array(
            array(
                'electionStatus' => 'Self Nomination'
            )
        ));
        $this->api->method('callResource')->will($this->onConsecutiveCalls($personResponse,
            $electionResponse));

        $this->user->method('getUsername')
            ->will($this->returnValue('liaom'));

        $this->user->method('isAuthorized')
            ->will($this->returnValue(0));

        try {
            $this->participant->modifyParticipant();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("you are not able to change your nomination acceptance",
                $e->getMessage());
        }
    }

    public function mockDataEmptyElectionId()
    {
        return array(
            'electionId' => '',
        );
    }

    public function mockDataElectionIdNotArray()
    {
        return array(
            'electionId' => [1, 2, 3],
        );
    }

    public function mockDataInvalidElectionId()
    {
        return array(
            'electionId' => 'bad',
        );
    }

    public function mockDataMissingElectionId()
    {
        return array('acceptance' => 'accepted');
    }

    public function mockDataUniqueIdNotArray()
    {
        return array(
            'electionId' => '1234',
            'uniqueId' => ['liaom', 'patelah'],
        );
    }

    public function mockDataEmptyUniqueId()
    {
        return array(
            'electionId' => '1234',
            'uniqueId' => '',
        );
    }

    public function mockDataInvalidUniqueId()
    {
        return array(
            'electionId' => '1234',
            'uniqueId' => 'klj-=',
        );
    }

    public function mockDataTypeNotArray()
    {
        return array(
            'electionId' => '1234',
            'type' => ['nominee', 'voter'],
        );
    }

    public function mockDataEmptyType()
    {
        return array(
            'electionId' => '1234',
            'type' => '',
        );
    }

    public function mockDataInvalidType()
    {
        return array(
            'electionId' => '1234',
            'type' => 'minio',
        );
    }

    public function mockDataAcceptanceNotArray()
    {
        return array(
            'electionId' => '1234',
            'acceptance' => ['default', 'acceptance'],
        );
    }

    public function mockDataEmptyAcceptance()
    {
        return array(
            'electionId' => '1234',
            'acceptance' => '',
        );
    }

    public function mockDataInvalidAcceptance()
    {
        return array(
            'electionId' => '1234',
            'acceptance' => 'sdfsdf',
        );
    }

    public function mockDataNomineeStatusNotArray()
    {
        return array(
            'electionId' => '1234',
            'nomineeStatus' => ['Nominee', ''],
        );
    }

    public function mockDataInvalidNomineeStatus()
    {
        return array(
            'electionId' => '1234',
            'nomineeStatus' => 'sdlfk',
        );
    }

    public function mockDataFacultySetUniqueId()
    {
        return array(
            'electionId' => '1234',
            'uniqueId' => 'liaom',
        );
    }

    public function mockDataFacultySetType()
    {
        return array(
            'electionId' => '1234',
            'type' => 'voter',
        );
    }

    public function mockDataFacultySetNomineeStatus()
    {
        return array(
            'electionId' => '1234',
            'nomineeStatus' => 'Nominee',
        );
    }

    public function mockDataFacultySetAcceptance2Default()
    {
        return array(
            'electionId' => '1234',
            'acceptance' => 'default',
        );
    }

    public function mockDataFacultyAllCorrect()
    {
        return array(
            'electionId' => 1234,
        );
    }

    public function mockResultWithUserRecord()
    {
        return array(
            array(
                'electionId' => 1234,
                'nominationAcceptance' => 'accepted'
            ),
        );
    }

    public function mockResultUserRecordNotFound()
    {
        return array();
    }

    public function getNewResponse($payload)
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setPayload($payload);
    }

    public function mockResourceParams()
    {
        return array();
    }
}