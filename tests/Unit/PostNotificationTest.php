<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: postNotificationTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the POST method of the Notification service.

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
FacultyElections/Notification

TABLE USAGE:

Web Service Usage:
	FacultyElections/Notification service (POST)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

06/XX/2016               SCHMIDEE
Description:  Initial Draft

07/19/2016               PATELAH
Description:  Included test for notificationType of nominees who have not yet responded.

08/03/2016               PATELAH
Description:  Include test for semifinalist winner to receive notification by including them in toPool and notificationType.
			 
----------------------------------------------------------------------------------------------------------------------------
 */
use MiamiOH\RESTng\App;

class PostNotificationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $notification, $queryallRecords, $user, $request, $awardService, $mockedApp;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->mockedApp = $this->createMock(App::class);
        $this->mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            //->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock user
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            //->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock db
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();
        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the configuration mock
        $config = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();
        $config->method('getConfiguration')->willReturn($this->mockConfigurationValues());

        //set up the service with the mocked out resources:
        $this->notification = new \MiamiOH\FacultyElections\Services\Notification();
        $this->notification->setApp($this->mockedApp);
        $this->notification->setApiUser($this->user);
        $this->notification->setDatabase($db);
        $this->notification->setDatasource($ds);
        $this->notification->setRequest($this->request);
        $this->notification->setConfiguration($config);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *   Empty Body Given
     * 	Tests Case in which body is empty.
     *	  Expected Return: 400 Bad Request
     */
    public function testEmptyBody()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('No data was received.', $e->getMessage());
        }
    }

    public function mockEmptyData()
    {
        return array();
    }


    /*
     *	Nomination Round Notification Test
     * 	Tests when a nominationRound notificatonType is requested
     *	Expected Return: API_OK.
     */
    public function testNominationRoundNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockNominationRoundData')));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockNominationRoundData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'nominees',
            'notificationType' => 'nominationRound'
        );
    }

    /*
   * Notification Type Test for Nominees who have not responded
   * Tests toPool when nomineesNotResponded is requested
   * Tests notificationType when nomineesNotResponded is requested
   *	Expected Return: API_CREATED.
   */
    public function testNomineesNotRespondedNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockNomineesNotRespondedData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockNomineesNotRespondedData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'nomineesNotResponded',
            'notificationType' => 'nomineesNotResponded'
        );
    }

    /*
    * Notification Type Test for voters who have not responded
    * Tests toPool when votersnotvotedinsfr is requested
    * Tests notificationType when votersnotvotedinsfr is requested
    *	Expected Return: API_CREATED.
    */
    public function testVotersNotVotedSemiFinalistRoundNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockVotersNotVotedSemiFinalistRoundData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockVotersNotVotedSemiFinalistRoundData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'voternotvotedinsfr',
            'notificationType' => 'voternotvotedinsfr'
        );
    }

    /*
        * Notification Type Test for voters who have not responded
        * Tests toPool when votersnotvotedinsfr is requested
        * Tests notificationType when votersnotvotedinsfr is requested
        *	Expected Return: API_CREATED.
        */
    public function testVotersNotVotedFinalistRoundNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockVotersNotVotedFinalistRoundData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockVotersNotVotedFinalistRoundData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'voternotvotedinfr',
            'notificationType' => 'voternotvotedinfr'
        );
    }

    /*
       * Notification Type Test for semifinalist
       * Tests toPool when semi-finalist is requested
       * Tests notificationType when semi-finalist is requested
       *	Expected Return: API_CREATED.
       */

    public function testSemiFinalistNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockSemiFinalistData')));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockSemiFinalistData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'semi-finalist',
            'notificationType' => 'semi-finalist'
        );
    }


    /*
    * 	Voting Round Notification Test
    *	Tests when a votingRound notificationType is requested
    *	Expected return: API_OK
    */
    public function testVotingRoundNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockVotingRoundData')));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockVotingRoundData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'voters',
            'notificationType' => 'nominationRound'
        );
    }


    /*
    * 	Ballot Confirmation Notification Test
    *	Tests when a ballotConfirmation notificationType is requested
    *	Expected return: API_OK
    */
    public function testBallotConfirmationNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBallotConfirmationData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockBallotConfirmationData()
    {
        return array(
            'electionId' => '1234',
            'to' => 'testuser@miamioh.edu',
            'notificationType' => 'ballotConfirmation'
        );
    }


    /*
    * 	Election Results Notification Test
    *	Tests when a results notificationType is requested
    *	Expected return: API_OK
    */
    public function testResultsNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBallotConfirmationData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockResultsData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all',
            'notificationType' => 'results'
        );
    }


    /*
    * 	Manual Email Notification Test
    *	Tests when a manual notification is requested
    *	Expected return: API_OK
    */
    public function testManualEmailNotification()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBallotConfirmationData'
            )));

        //mock up the election and participant responses
        $this->mockedApp
            ->method('callResource')
            ->will($this->onConsecutiveCalls(
                $this->getNewResponse($this->mockNominationRoundElectionData()),
                $this->getNewResponse($this->mockNominationRoundParticipantData())
            ));

        //Call the createNotification() method and get the payload
        $resp = $this->notification->createNotification();
        $payload = $resp->getPayload();

        //make assertions on the payload
        $this->assertEquals(App::API_CREATED, $resp->getStatus());
    }

    public function mockManualEmailData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'voters',
            'subject' => 'Test email',
            'body' => 'Test email body'
        );
    }


    /*
    * 	ElectionId SQL Injection Test
    *	Tests when SQL Injection is attempted via the ElectionId field
    *	Expected return: 500
    */
    public function testElectionIdSQLInjection()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockElectionIdSQLInjectionData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('\'; is not a valid electionId value.',
                $e->getMessage());
        }
    }

    public function mockElectionIdSQLInjectionData()
    {
        return array(
            'electionId' => '\';',
            'toPool' => 'voters',
            'subject' => 'Test email',
            'body' => 'Test email body'
        );
    }

    /*
    * ToPool SQL Injection Test
    *	Tests when SQL Injection is attempted via the toPool field
    *	Expected return: 500
    */
    public function testToPoolSQLInjection()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockToPoolSQLInjectionData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('\'; is not a valid toPool value.',
                $e->getMessage());
        }
    }

    public function mockToPoolSQLInjectionData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => '\';',
            'subject' => 'Test email',
            'body' => 'Test email body'
        );
    }


    /*
    * To SQL Injection Test
    *	Tests when SQL Injection is attempted via the to field
    *	Expected return: 500
    */
    public function testToSQLInjection()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockToSQLInjectionData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('\'; is not a valid to value.', $e->getMessage());
        }
    }

    public function mockToSQLInjectionData()
    {
        return array(
            'electionId' => '1234',
            'to' => '\';',
            'subject' => 'Test email',
            'body' => 'Test email body'
        );
    }


    /*
    * 	NotificationType SQL Injection Test
    *	Tests when SQL Injection is attempted via the notificationType field
    *	Expected return: 500
    */
    public function testNotificationTypeSQLInjection()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockNotificationTypeSQLInjectionData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('\'; is not a valid notificationType value.',
                $e->getMessage());
        }
    }

    public function mockNotificationTypeSQLInjectionData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all',
            'notificationType' => '\';'
        );
    }


    /*
    * 	Invalid ElectionId Test
    *	Tests when an electionId has invalid data
    *	Expected return: 500
    */
    public function testInvalidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockInvalidElectionIdData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('invalidElectionID is not a valid electionId value.',
                $e->getMessage());
        }
    }

    public function mockInvalidElectionIdData()
    {
        return array(
            'electionId' => 'invalidElectionID',
            'toPool' => 'all',
            'notificationType' => 'nominationRound'
        );
    }


    /*
    * 	Invalid toPool Test
    *	Tests when a toPool has invalid data
    *	Expected return: 500
    */
    public function testInvalidToPool()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockInvalidToPoolData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('invalidToPool is not a valid toPool value.',
                $e->getMessage());
        }
    }

    public function mockInvalidToPoolData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'invalidToPool',
            'notificationType' => 'nominationRound'
        );
    }


    /*
    * 	Invalid to Test
    *	Tests when a to has invalid data
    *	Expected return: 500
    */
    public function testInvalidTo()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockInvalidToData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('invalidTo is not a valid to value.',
                $e->getMessage());
        }
    }

    public function mockInvalidToData()
    {
        return array(
            'electionId' => '1234',
            'to' => 'invalidTo',
            'notificationType' => 'nominationRound'
        );
    }


    /*
    * 	Invalid notificationType Test
    *	Tests when a notificationType has invalid data
    *	Expected return: 500
    */
    public function testInvalidNotificationType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidNotificationTypeData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('invalidNotificationType is not a valid notificationType value.',
                $e->getMessage());
        }
    }

    public function mockInvalidNotificationTypeData()
    {
        return array(
            'electionId' => '1234',
            'to' => 'testuser@miamioh.edu',
            'notificationType' => 'invalidNotificationType'
        );
    }


    /*
    * 	Missing ElectionId Test
    *	Tests when a to is missing
    *	Expected return: 500
    */
    public function testMissingElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockMissingElectionIdData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('ElectionId is required.', $e->getMessage());
        }
    }

    public function mockMissingElectionIdData()
    {
        return array(
            'toPool' => 'testuser@miamioh.edu',
            'notificationType' => 'ballotConfirmation'
        );
    }


    /*
    * Missing To or ToPool Test
    *	Tests when a toPool is missing
    *	Expected return: 500
    */
    public function testMissingToOrToPool()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockMissingToOrToPoolData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('To or ToPool is required.', $e->getMessage());
        }
    }

    public function mockMissingToOrToPoolData()
    {
        return array(
            'electionId' => '1234',
            'notificationType' => 'ballotConfirmation'
        );
    }


    /*
    * 	Missing NotificationType or Subject and Body Test
    *	Tests when a notificationType or subject and body is missing
    *	Expected return: 500
    */
    public function testMissingNotificationTypeOrSubjectAndBody()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockMissingNotificationTypeOrSubjectAndBodyData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('NotificationType or subject and body is required.',
                $e->getMessage());
        }
    }

    public function mockMissingNotificationTypeOrSubjectAndBodyData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all'
        );
    }


    /*
    * 	Empty ElectionId Test
    *	Tests when a electionId is empty
    *	Expected return: 500
    */
    public function testEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyElectionIdData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('ElectionId is required.', $e->getMessage());
        }
    }

    public function mockEmptyElectionIdData()
    {
        return array(
            'electionId' => '',
            'toPool' => 'all',
            'notificationType',
            'results'
        );
    }


    /*
    * 	Empty toPool test
    *	Tests when a toPool is empty
    *	Expected return: 500
    */
    public function testEmptyToPool()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyToPoolData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('To or ToPool is required.', $e->getMessage());
        }
    }

    public function mockEmptyToPoolData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => '',
            'notificationType',
            'results'
        );
    }


    /*
    * 	Empty to test
    *	Tests when a to is empty
    *	Expected return: 500
    */
    public function testEmptyTo()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyToData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('To or ToPool is required.', $e->getMessage());
        }
    }

    public function mockEmptyToData()
    {
        return array(
            'electionId' => '1234',
            'to' => '',
            'notificationType',
            'results'
        );
    }


    /*
    * 	Empty notificationType test
    *	Tests when a notificationType is empty
    *	Expected return: 500
    */
    public function testEmptyNotificationType()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockEmptyNotificationTypeData'
            )));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('NotificationType or subject and body is required.',
                $e->getMessage());
        }
    }

    public function mockEmptyNotificationTypeData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all',
            'notificationType',
            ''
        );
    }


    /*
    * 	Empty subject test
    *	Tests when a subject is empty
    *	Expected return: 500
    */
    public function testEmptySubject()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptySubjectData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('NotificationType or subject and body is required.',
                $e->getMessage());
        }
    }

    public function mockEmptySubjectData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all',
            'subject' => '',
            'body' => 'body of a message'
        );
    }


    /*
    * 	Empty subject test
    *	Tests when a subject is empty
    *	Expected return: 500
    */
    public function testEmptyEmailBody()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'mockEmptyEmailBodyData')));
        try {
            $resp = $this->notification->createNotification();
        } catch (\Exception $e) {
            $this->assertEquals('NotificationType or subject and body is required.',
                $e->getMessage());
        }
    }

    public function mockEmptyEmailBodyData()
    {
        return array(
            'electionId' => '1234',
            'toPool' => 'all',
            'subject' => 'message subject',
            'body' => ''
        );
    }


    ///////////////////////////
    // Shared helper methods //
    ///////////////////////////
    public function getNewResponse($payload)
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setPayload($payload);

        return $response;
    }

    public function mockConfigurationValues()
    {
        array(
            'Ballot_Confirmation_Body' => 'Your Ballot for the [NAME_COMMITTEE] was successfully recorded. Thank you for taking the time to vote. -Faculty Elections',
            'Ballot_Confirmation_Subject' => 'Ballot for the [NAME_COMMITTEE] Recorded',

            'Finalists_Round_Body' => 'finalists email body contents here',
            'Finalists_Round_Subject' => 'finalists round subject contents here',

            'From_Email' => 'FacultyElections@miamioh.edu',

            'Nomination_Round_Body' => '[NAME_DEPARTMENT_DIVISON] faculty will soon be holding elections to choose [NUMBER_FINALISTS] faculty representative to the [ELECTION_NAME]. [NAME_INCUMBENT] will complete a three year term at the end of [TERM_END_DATE]. The newly elected representative will serve a three year term beginning in [TERM_BEGIN_DATE]. Faculty who have served before and those with more than 75% administrative appointment are ineligible. You are eligible to serve. The nomination round of voting will begin shortly. Please log in to the Faculty Assembly Voting system to indicate if you are willing to serve if elected. Please note: YOUR NAME WILL APPEAR as a candidate UNLESS you remove your name. Please respond as soon as possible but no later than midnight, [ROUND_END_DATE]. Prior to the deadline, you can change your decision by coming back to THIS email and clicking on the appropriate link. Thanks, FacultyElections',
            'Nomination_Round_Subject' => 'Nomination Round Started',

            'Nominee_Not_Responded_Body' => 'nominee not responded email body',
            'Nominee_Not_Responded_Subject' => 'nominee not responded email subject',
            'Results_Body' => 'These results are awesome!!!',
            'Results_Subject' => 'Results for Election [ELECTION_NAME]',
            'SemiFinalists_Round_Body' => 'semi finalists email body contents here',
            'SemiFinalists_Round_Subject' => 'semi finalists email subject contents here',

            'Voters_Not_Responded_Subject' => 'voters email subject contents here',
            'Voters_Not_Responded_Body' => 'voters email body contents here',

            'Semi_Finalist_Winner_Body' => 'This body relates to the winner of semi-finalist',
            'Semi_Finalist_Winner_Subject' => 'This subject relates to the winner of semi-finalist',

            'Initial_Nomination_Round_Nominee_Body' => '[NAME_DEPARTMENT_DIVISON] faculty will soon be holding elections to choose [NUMBER_FINALISTS] faculty representative to the [NAME_COMMITTEE]. [NAME_INCUMBENT] will complete a three year term at the end of [TERM_END_DATE]. The newly elected representative will serve a three year term beginning in [TERM_BEGIN_DATE]. Faculty who have served before and those with more than 75% administrative appointment are ineligible. You are eligible to serve. The nomination round of voting will begin shortly. Please log in to the Faculty Assembly Voting system to indicate if you are willing to serve if elected. Please note: YOUR NAME WILL APPEAR as a candidate UNLESS you remove your name. Please respond as soon as possible but no later than midnight, [ROUND_END_DATE]. Prior to the deadline, you can change your decision by coming back to THIS email and clicking on the appropriate link. Thank, FacultyElections',
            'Initial_Nomination_Round_Nominee_Subject' => 'Nomination Round Started'

        );
    }

    public function mockNominationRoundElectionData()
    {
        return array(
            array(
                'electionId' => '1234',
                'electionName' => 'World Domination Committee',
                'nomineePoolId' => '555',
                'voterPoolId' => '556',
                'electionStatus' => 'Self Nomination Round',
                'numOfWinners' => '3',
                'numOfSemiFinalists' => '7',
                'isOpen' => 'true',
                'createUser' => 'MILLSE',
                'updateUser' => 'MILLSE',
                'dateCreated' => '25-DEC-15',
                'dateUpdated' => '25-DEC-15',
            )
        );
    }

    public function mockNominationRoundParticipantData()
    {
        return array(
            array(
                'electionId' => '1234',
                'uniqueId' => 'testuser1',
                'preferredName' => 'Test User1',
                'poolTypeId' => '555',
                'nominationAcceptance' => 'Default',
                'nomineeStatus' => 'Eligible Nominee',
                'createUser' => 'admin1',
                'dateCreated' => '15-AUG-14',
                'updateUser' => 'admin1',
                'dateUpdated' => '15-AUG-14'
            ),
            array(
                'electionId' => '1234',
                'uniqueId' => 'testuser2',
                'preferredName' => 'Test User2',
                'poolTypeId' => '555',
                'nominationAcceptance' => 'Default',
                'nomineeStatus' => 'Eligible Nominee',
                'createUser' => 'admin1',
                'dateCreated' => '15-AUG-14',
                'updateUser' => 'admin1',
                'dateUpdated' => '15-AUG-14'
            )
        );
    }
}