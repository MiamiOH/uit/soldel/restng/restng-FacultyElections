<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getParticipantElectionIDTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION: 
This php class is used to test the GET method of the Participant service. Specifically 
Election ID Paramater Usage.

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
FacultyElections/Participant

TABLE USAGE:

Web Service Usage:
	FacultyElections/Participant service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/XX/2016               SCHMIDEE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */

use MiamiOH\RESTng\App;

class GetParticipantElectionIDTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $participant, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockPersonResponse());
        $this->api->method('callResource')->willReturn($personResponse);

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->participant = new \MiamiOH\FacultyElections\Services\Participant();
        $this->participant->setApp($this->api);
        $this->participant->setApiUser($this->user);
        $this->participant->setDatabase($db);
        $this->participant->setDatasource($ds);
        $this->participant->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *   No Election ID Given
     * 	Tests Case in which empty parameteres given at all. This is technically a clone
     *	of the get multiple elections due to it is using the same mock methods, but it
     *	is testing for a different path in the code.
     *	Expected Return: Returns all Election.
     */
    public function testNoElectionID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleElectionID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 14);
        $this->assertEquals($payload, $this->mockNoParametersResults());

    }


    /*
     *   Empty Election ID Given
     * 	Tests Case in which empty parameteres given at aall.
     *	Expected Return: 400 Error (Error Message mockExpectedEmptyElectionIDResult)
     */
    public function testEmptyElectionID()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockEmptyParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedEmptyElectionIDResult(),
                $e->getMessage());
        }

    }


    /*
     *	Single Election ID Test
     * 	Tests when a single Election ID is requested.
     *	Expected Return: Results seen in the mockSingleElectionIDResults.
     */
    public function testSingleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockSingleElectionIDOptions'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 7);
        $this->assertEquals($payload, $this->mockSingleElectionIDResults());
    }

    /*
     *	Single Invalid (Not Found) Election ID
     * 	Tests when invalid Election ID are requested.
     *	Expected Return: 404 Error (Error Message mockExpectedInvalidNotFoundSingleElectionIDResult)
     */
    public function testInvalidNotFoundSingleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidNotFoundSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryANotFoundSingleElectionID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
        $this->assertEquals($payload,
            $this->mockExpectedInvalidNotFoundSingleElectionIDResult());

    }

    /*
     *	Single Invalid (Wrong Format) Election ID
     * 	Tests when invalid Election ID are requested.
     *	Expected Return: 400 Error.
     */
    public function testInvalidWrongFormatSingleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidWrongFormatSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidWrongFormatSingleElectionIDResult(),
                $e->getMessage());
        }
    }

    /*
     *	Single SQL Injection Attempt
     * 	Tests when an SQL Injection is attempted
     *	Expected Return: 400 Error
     */
    public function testSingleSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsSQLInjectionSingleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllSingleElectionID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedSQLInjectionSingleElectionIDResult(),
                $e->getMessage());
        }

    }

    /*
     *	Multiple Election ID
     * 	Tests when  multiple Election IDs are requested.
     *	Expected Return: Results seen in the mockMultipleElectionIDResults.
     */
    public function testMultipleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMultipleElectionID'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleElectionID'
            )));

        $resp = $this->participant->getParticipant();
        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 14);
        $this->assertEquals($payload, $this->mockMultipleElectionIDResults());
    }


    /*
     *	Multiple Invalid (Wrong Format) Election ID
     * 	Tests when invalid Election ID are requested.
     *	Expected Return: 400 Error.
     */
    public function testInvalidWrongFormatMultipleElectionID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockInvalidWrongFormatMultipleElectionIDOptions'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleElectionID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedInvalidWrongFormatMultipleElectionIDResult(),
                $e->getMessage());
        }
    }

    /*
     *	Multiple SQL Injection Attempt
     * 	Tests when an SQL Injection is attempted
     *	Expected Return: 400 Error
     */
    public function testMultipleSQLInjection()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockSQLInjectionMultipleElectionIDOptions'
            )));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryAllMultipleElectionID'
            )));

        try {
            $resp = $this->participant->getParticipant();
        } catch (\Exception $e) {
            $this->assertEquals($this->mockExpectedSQLInjectionMultipleElectionIDResult(),
                $e->getMessage());
        }

    }

    //Single
    public function mockSingleElectionIDOptions()
    {
        $optionsArray = array('electionId' => array('99999999'));

        return $optionsArray;
    }

    public function mockSingleElectionIDResults()
    {
        return array(
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER3',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'removed',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER5',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER6',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER4',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'winner',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER2',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            )
        );
    }

    public function mockQueryAllSingleElectionID()
    {
        return array(
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER1',
                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER3',
                'nomination_acceptance' => 'removed',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER5',
                'nomination_acceptance' => 'default',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER6',
                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER4',
                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'winner',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER2',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            )
        );
    }

    //Invalid Single
    public function mockOptionsInvalidNotFoundSingleElectionID()
    {
        $optionsArray = array('electionId' => array('99999999'));

        return $optionsArray;
    }

    public function mockExpectedInvalidNotFoundSingleElectionIDResult()
    {
        return array();

    }

    public function mockQueryANotFoundSingleElectionID()
    {
        return array();
    }

    public function mockOptionsInvalidWrongFormatSingleElectionID()
    {
        $optionsArray = array('electionId' => array('notAnElectionID'));

        return $optionsArray;
    }

    public function mockExpectedInvalidWrongFormatSingleElectionIDResult()
    {
        return "Election ID must be numeric.";
    }

    public function mockOptionsSQLInjectionSingleElectionID()
    {
        $optionsArray = array('electionId' => array("';--"));

        return $optionsArray;
    }

    public function mockExpectedSQLInjectionSingleElectionIDResult()
    {
        return "Election ID must be numeric.";
    }

    //Mutiple Mock Methods
    public function mockOptionsMultipleElectionID()
    {
        $optionsArray = array(
            'electionId' => array(
                '99999999',
                '1234567',
                '000001',
                '00000000'
            )
        );

        return $optionsArray;
    }

    public function mockMultipleElectionIDResults()
    {
        return array(
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER3',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'removed',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER5',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER6',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER4',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'winner',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER2',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '1234567',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '1234567',
                'uniqueId' => 'TESTUSER2',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER7',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER8',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER10',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER3',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            )
        );
    }

    public function mockQueryAllMultipleElectionID()
    {
        return array(
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER1',
                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER3',
                'nomination_acceptance' => 'removed',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),

            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER5',
                'nomination_acceptance' => 'default',
                'nominee_status' => 'semi-finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER6',
                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'finalist',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER4',
                'nomination_acceptance' => 'accepted',
                'nominee_status' => 'winner',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '99999999',
                'uniqueid' => 'TESTUSER2',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '1234567',
                'uniqueid' => 'TESTUSER1',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '1',
                'member_type_name' => 'nominee',
                'election_id' => '1234567',
                'uniqueid' => 'TESTUSER2',
                'nomination_acceptance' => 'default',
                'nominee_status' => 'nominee',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000001',
                'uniqueid' => 'TESTUSER7',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000001',
                'uniqueid' => 'TESTUSER8',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000001',
                'uniqueid' => 'TESTUSER10',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000001',
                'uniqueid' => 'TESTUSER1',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            ),
            array(
                'member_type_id' => '2',
                'member_type_name' => 'voter',
                'election_id' => '000001',
                'uniqueid' => 'TESTUSER3',
                'nomination_acceptance' => '',
                'nominee_status' => '',
                'create_user' => 'TESTUSER_CREATOR',
                'date_created' => 'MAY 20 2016 2:00PM',
                'update_user' => 'TESTUSER_UPDATE',
                'date_updated' => 'MAY 20 2016 2:00PM',
                'round_type_name' => '',
            )
        );
    }

    //Invalid Multiple

    public function mockInvalidWrongFormatMultipleElectionIDOptions()
    {
        $optionsArray = array(
            'electionId' => array(
                '99999999',
                '01234567',
                '00000001',
                '00000000',
                'notAnElectionID'
            )
        );

        return $optionsArray;
    }

    public function mockExpectedInvalidWrongFormatMultipleElectionIDResult()
    {
        return "Election ID must be numeric.";
    }

    public function mockSQLInjectionMultipleElectionIDOptions()
    {
        $optionsArray = array(
            'electionId' => array(
                '99999999',
                '01234567',
                '00000001',
                '00000000',
                "';--"
            )
        );

        return $optionsArray;
    }

    public function mockExpectedSQLInjectionMultipleElectionIDResult()
    {
        return "Election ID must be numeric.";
    }


    //No Election Return (No Parameters)
    public function mockNoParameters()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    public function mockNoParametersResults()
    {
        return array(
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER3',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'removed',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER5',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'semi-finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER6',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'finalist',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER4',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'accepted',
                'nomineeStatus' => 'winner',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '99999999',
                'uniqueId' => 'TESTUSER2',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '1234567',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '1',
                'memberTypeName' => 'nominee',
                'electionId' => '1234567',
                'uniqueId' => 'TESTUSER2',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => 'default',
                'nomineeStatus' => 'nominee',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER7',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER8',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER10',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER1',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            ),
            array(
                'memberTypeId' => '2',
                'memberTypeName' => 'voter',
                'electionId' => '000001',
                'uniqueId' => 'TESTUSER3',
                'preferredName' => 'Neville Longbottom',
                'nominationAcceptance' => '',
                'nomineeStatus' => '',
                'createUser' => 'TESTUSER_CREATOR',
                'dateCreated' => 'MAY 20 2016 2:00PM',
                'updateUser' => 'TESTUSER_UPDATE',
                'dateUpdated' => 'MAY 20 2016 2:00PM',
            )
        );
    }

    //Empty Election ID Return
    public function mockEmptyParameters()
    {
        $optionsArray = array('electionId' => array(''));

        return $optionsArray;
    }

    public function mockExpectedEmptyElectionIDResult()
    {
        return "Election ID must be numeric.";
    }


    public function mockResourceParams()
    {
        return array();
    }

    public function mockPersonResponse()
    {
        return array(
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER1',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER2',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER3',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER4',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER5',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER6',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER7',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER8',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER9',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            ),
            array(
                'plusNumber' => '+01126998',
                'uniqueId' => 'TESTUSER10',
                'pidm' => '1122384',
                'prefix' => ' ',
                'suffix' => ' ',
                'givenNameLegal' => 'Neville',
                'givenNamePreferred' => 'Neville',
                'familyName' => 'Longbottom',
                'middleName' => 'Bilius',
                'age' => '15',
                'birthDate' => '1988-07-30',
                'emailAddress' => 'longbonb@miamioh.edu',
                'genderCode' => 'M',
                'genderDescription' => 'Male'
            )
        );
    }
}