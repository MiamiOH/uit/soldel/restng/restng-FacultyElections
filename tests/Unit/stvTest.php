<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

use Exception;
use MiamiOH\FacultyElections\Services\STV;

/**
 * -----------------------------------------------------------
 * FILE NAME: stvTest.php
 *
 * Copyright (c) 2016 Miami University, All Rights Reserved.
 *
 * Miami University grants you ("Licensee") a non-exclusive, royalty free,
 * license to use, modify and redistribute this software in source and
 * binary code form, provided that i) this copyright notice and license
 * appear on all copies of the software; and ii) Licensee does not utilize
 * the software in a manner which is disparaging to Miami University.
 *
 * This software is provided "AS IS" and any express or implied warranties,
 * including, but not limited to, the implied warranties of merchantability
 * and fitness for a particular purpose are disclaimed. It has been tested
 * and is believed to work as intended within Miami University's
 * environment. Miami University does not warrant this software to work as
 * designed in any other environment.
 *
 * AUTHOR: Emily Schmidt
 *
 * DESCRIPTION:  Unit Tests for Testing the STV Class (STV.php).
 *
 * ENVIRONMENT DEPENDENCIES: PHP Unit
 *
 * AUDIT TRAIL:
 *
 * DATE        UniqueID
 * 08/08/2016  schmidee  Initial File
 * 08/11/2016  schmidee  Added No Vote Winner Test to test for a edge case where a
 * person can win having zero votes.
 * 09/09/2016  schmidee  Updated Unit test to reflect corrected vote tally.
 */
//include_once '../../src/Service/stv.php';

class StvTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //Set up Stuff goes here

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /**
     * Test For Negative Winners
     * This is a test ot confirm what happens when the number of winners is negative.
     * Results: Exception thrown with error "Number of winners must be larger than 0"
     */
    public function testNegativeWinners()
    {
        try {
            $stv = new STV($this->mockBallotTransfer(), 0, array(), array(),
                false);
            $this->fail("STV Class Failed to throw Exception for Negative Winners");
        } catch (Exception $e) {
            $this->assertStringStartsWith("Number of winners must be larger than 0",
                $e->getMessage());
        }
    }


    /**
     * Test For No Ballots
     * This is a test ot confirm what happens when there are no ballot.
     * Results: Exception thrown with error "No Ballots Casted""
     */
    public function testNoBallots()
    {
        try {
            $stv = new STV(array(), 3, array(), array(), false);
            $stv->calculate_stv();
            $this->fail("STV Class Failed to throw Exception for No Ballots Cast");
        } catch (Exception $e) {
            $this->assertStringStartsWith("No Ballots Casted", $e->getMessage());
        }
    }

    /**
     * Test For Not Enough Nominees
     * This is a test to check if not enough Nominees exist to fulfill the number of
     * winners. Results: Return with how ever many nominees it can.
     */
    public function testNotEnoughNominees()
    {
        $stv = new STV($this->mockBallotTransfer(),
            7,
            array('jack', 'steve', 'erik', 'jane'),
            array(),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockNotEnoughNomineesResults(), $results);

    }

    /**
     * Mock method for the expected return from Ballot Transfer test.
     *
     * @return array containing the expected return from the Not Enough Nominees
     *     test.
     */
    public function mockNotEnoughNomineesResults()
    {
        return array(
            "steve" => 3,
            "jack" => 1,
            "jane" => 1,
            "erik" => 0,
        );

    }

    /**
     * Test for Ballot Transfer
     * This is a test to confirm that ballots are being transferred correctly.
     * Results: Return should match the output of mockTransferResults.
     */
    public function testBallotTransfer()
    {
        $stv = new STV($this->mockBallotTransfer(),
            2,
            array('jack', 'steve', 'erik', 'jane'),
            array(),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockTransferResults(), $results);
    }

    /**
     * Mock method for generating the data needed to perform the Ballot Transfer
     * test.
     *
     * @return array containing the ballot information for Ballot Transfer test.
     */
    public function mockBallotTransfer()
    {
        return array(
            array(
                "jack",
                "steve",
                "erik",
            ),
            array(
                "steve",
                "erik",
                "jack",
            ),
            array(
                "jane",
                "erik",
                "steve",
            ),
            array(
                "steve",
                "erik",
            ),
            array(
                "steve",
                "jack",
            ),
        );

    }

    /**
     * Mock method for the expected return from Ballot Transfer test.
     *
     * @return array containing the expected return from the Ballot Transfer test.
     */
    public function mockTransferResults()
    {
        return array(
            "steve" => 4,
            "jack" => 1
        );

    }

    /**
     * Test No Quota Change
     * This is a test to check that the quota remains stable in this example
     * election instead of decreasing. Results: Return should match the output of
     * mockNoQuotaChangeResults.
     */
    public function testNoQuotaChange()
    {
        $stv = new STV($this->mockBallotNoQuotaChange(),
            2,
            array('jack', 'steve', 'erik', 'jane'),
            array(),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockNoQuotaChangeResults(), $results);
    }

    /**
     * Mock method for generating the data needed to perform the No Quota Change
     * test.
     *
     * @return array containing the ballot information for the No Quota Change test.
     */
    public function mockBallotNoQuotaChange()
    {
        return array(
            array(
                "jack",
                "steve",
                "erik",
            ),
            array(
                "jack",
                "jane",
                "erik",
            ),
            array(
                "steve",
                "erik",
                "jack",
            ),
            array(
                "steve",
                "jack",
                "jane"
            ),
            array(
                "jane",
                "erik",
                "steve",
            ),
            array(
                "steve",
                "erik",
                "jack",
            ),
            array(
                "erik",
                "jack",
            ),
            array(
                'jane',
            )
        );

    }

    /**
     * Mock method for the expected return from No Quota Change test.
     *
     * @return array containing the expected return from the No Quota Change test.
     */
    public function mockNoQuotaChangeResults()
    {
        return array(
            "steve" => 3,
            "jack" => 3
        );

    }

    /**
     * Test Large Election
     * This is a test to check all functionality in previous tests and also run a
     * larger election. Results: Return should match the output of
     * mockLargeElectionResults.
     */
    public function testLargeElection()
    {
        $stv = new STV($this->mockBallotLargeElection(),
            2,
            array('A', 'B', 'C', 'D', 'E'),
            array(),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockLargeElectionResults(), $results);
    }

    /**
     * Mock method for generating the data needed to perform the Large Election test.
     *
     * @return array containing the ballot information for the Large Election test.
     */
    public function mockBallotLargeElection()
    {
        return array(
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "D",
                "E",
                "A",
                "B",
                "C",
            ),
            array(
                "B",
            ),
            array(
                "B",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "C",
                "B",
                "E",
                "A",
                "D",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "A",
                "C",
                "D",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "C",
                "D",
                "A",
                "B",
                "E",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "A",
                "E",
                "C",
                "B",
                "D",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
            array(
                "E",
                "D",
                "B",
                "C",
                "A",
            ),
        );

    }

    /**
     * Mock method for the expected return from Large Election test.
     *
     * @return array containing the expected return from the Large Election test.
     */
    public function mockLargeElectionResults()
    {
        return array(
            "E" => 29,
            "C" => 28
        );

    }

    /**
     * Test Remove Nominee (Single)
     * This is a test the remove Nominee function.
     * Results: Return should match the output of mockRemoveNomineeResults.
     */
    public function testRemoveNomineeSingle()
    {
        $stv = new STV($this->mockBallotLargeElection(),
            2,
            array('A', 'B', 'D', 'E'),
            array('C'),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockRemoveNomineeSingleResults(), $results);
    }

    /**
     * Mock method for the expected return from Large Election test.
     *
     * @return array containing the expected return from the Large Election test.
     */
    public function mockRemoveNomineeSingleResults()
    {
        return array(
            "E" => 33,
            "D" => 45
        );

    }

    /**
     * Test Remove Nominee (Multiple)
     * This is a test the remove Nominee function.
     * Results: Return should match the output of mockRemoveNomineeResults.
     */
    public function testRemoveNomineeMultiple()
    {
        $stv = new STV($this->mockBallotLargeElection(),
            2,
            array('A', 'B', 'E'),
            array('C', 'D'),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockRemoveNomineeMultipleResults(), $results);
    }

    /**
     * Mock method for the expected return from Large Election test.
     *
     * @return array containing the expected return from the Large Election test.
     */
    public function mockRemoveNomineeMultipleResults()
    {
        return array(
            "E" => 29,
            "A" => 40
        );

    }

    /**
     * Test No Votes Winner
     * This is a test to check for the case when the number of nominees is such that
     * a person with no votes is declared a winner. Results: Return should match the
     * output of mockNoVotesWinner.
     */
    public function testNoVotesWinner()
    {
        $stv = new STV($this->mockNoVotesWinnerBallots(),
            3,
            array('jack', 'steve', 'erik'),
            array(),
            false);
        $results = $stv->calculate_stv();
        $this->assertEquals($this->mockNoVotesWinner(), $results);
    }

    /**
     * Mock method for generating the data needed to perform the No Votes Winner
     * test.
     *
     * @return array containing the ballot information for the No Votes Winner test.
     */
    public function mockNoVotesWinnerBallots()
    {
        return array(
            array(
                "jack",
                "steve",
            ),
            array(
                "jack",
            ),
            array(
                "steve",
                "jack",
            ),
        );

    }

    /**
     * Mock method for the expected return from Large Election test.
     *
     * @return array containing the expected return from the Large Election test.
     */
    public function mockNoVotesWinner()
    {
        return array(
            "jack" => 2,
            "steve" => 1,
            "erik" => 0

        );

    }
}