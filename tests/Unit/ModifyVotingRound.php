<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: modifyVotingRoundTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of the VotingRound Web Service
Detail: (+ : implemented, - : not yet be implemented)
    + ValidElectionId
    + ValidRoundTypeId
    + ValidStartDate
    + ValidEndDate
    + EmptyElectionId
    + EmptyRoundTypeId
    + EmptyStartDate
    + EmptyEndDate
    + MissingElectionId
    + MissingRoundTypeId

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/19/2016  liaom

*/

class modifyVotingRound extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $votingRound, $queryallRecords, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock mockedApp:
        $mockedApp = $this->createMock(App::class);

        $mockedApp->method('newResponse')->willReturn(new \RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\RESTng\Connector\Database\DBH')
            ->setMethods(array('perform', 'queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->votingRound = new \MiamiOH\FacultyElections\Services\VotingRound();
        $this->votingRound->setApp($mockedApp);
        $this->votingRound->setApiUser($this->user);
        $this->votingRound->setDatabase($db);
        $this->votingRound->setDatasource($ds);
        $this->votingRound->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testAllValid()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyAllValid'
            )));

        //tell the dbh what to do when the perform is called.
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockQuerySuccess')));


        $resp = $this->votingRound->modifyVotingRound();

        $payload = $resp->getPayload();
        $this->assertEquals(RESTng\API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 0);
    }

    public function testEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyElectionId'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testEmptyRoundTypeId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyVotingRoundId'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: empty type",
                $e->getMessage());
        }
    }

    public function testEmptyStartDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyStartDate'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: empty date",
                $e->getMessage());
        }
    }

    public function testEmptyEndDate()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyEndDate'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: empty date",
                $e->getMessage());
        }
    }

    public function testMissingElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyMissingElectionId'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: missing required parameters",
                $e->getMessage());
        }
    }

    public function testMissingRoundTypeId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyMissingRoundTypeId'
            )));

        try {
            $this->votingRound->modifyVotingRound();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertEquals("Error: missing required parameters",
                $e->getMessage());
        }
    }

    public function mockBodyAllValid()
    {
        $body = array(
            'electionId' => '1234',
            'type' => '101',
            'startDate' => '999999999',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyEmptyElectionId()
    {
        $body = array(
            'electionId' => '',
            'type' => '101',
            'startDate' => '999999999',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyEmptyRoundTypeId()
    {
        $body = array(
            'electionId' => '1234',
            'type' => '',
            'startDate' => '999999999',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyEmptyStartDate()
    {
        $body = array(
            'electionId' => '1234',
            'type' => '101',
            'startDate' => '',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyEmptyEndDate()
    {
        $body = array(
            'electionId' => '1234',
            'type' => '101',
            'startDate' => '999999999',
            'endDate' => '',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyMissingElectionId()
    {
        $body = array(
            'type' => '101',
            'startDate' => '999999999',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockBodyMissingRoundTypeId()
    {
        $body = array(
            'electionId' => '1234',
            'startDate' => '999999999',
            'endDate' => '999999999',
            'description' => 'hello',
        );

        return $body;
    }

    public function mockQuerySuccess()
    {
        return array();
    }
}