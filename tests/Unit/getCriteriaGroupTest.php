<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getCriteriaGroupTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/07/2016  liaom

*/

use MiamiOH\RESTng\App;

class GetCriteriaGroupTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $criteriaGroup, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getData',
                'isPaged',
                'getOffset',
                'getLimit'
            ))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->criteriaGroup = new \MiamiOH\FacultyElections\Services\CriteriaGroup();
        $this->criteriaGroup->setApp($this->api);
        $this->criteriaGroup->setApiUser($this->user);
        $this->criteriaGroup->setDatabase($db);
        $this->criteriaGroup->setDatasource($ds);
        $this->criteriaGroup->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSingleEmptyPoolId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataSingleEmptyPoolId'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty poolId",
                $e->getMessage());
        }
    }

    public function testSingleInvalidPoolId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataSingleInvalidPoolId'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid poolId",
                $e->getMessage());
        }
    }

    public function testMultiInvalidPoolId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMultiInvalidPoolId'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty poolId",
                $e->getMessage());
        }
    }

    public function testSingleEmptyPoolName()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataSingleEmptyPoolName'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty poolName",
                $e->getMessage());
        }
    }

    public function testSingleInvalidPoolName()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataSingleInvalidPoolName'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid poolName",
                $e->getMessage());
        }
    }

    public function testMultiInvalidPoolName()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMultiInvalidPoolName'
            )));

        try {
            $this->criteriaGroup->getCriteriaGroup();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid poolName",
                $e->getMessage());
        }
    }

    public function testSingleAllCorrect()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataSingleAllCorrect'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQuerySingleAllCorrect')));


        $resp = $this->criteriaGroup->getCriteriaGroup();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(1, count($payload));
        $this->assertEquals($this->mockResultSingleAllCorrect(), $payload);
    }

    public function testSingleAllCorrectUsingPoolId()
    {
        $this->request->method('getOptions')
            ->will($this->returnValue(array(
                'poolId' => 1234,
                'poolName' => 'pool 7777'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQuerySingleAllCorrect')));


        $resp = $this->criteriaGroup->getCriteriaGroup();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(1, count($payload));
        $this->assertEquals($this->mockResultSingleAllCorrect(), $payload);
    }

    public function testMultiAllCorrect()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMultiAllCorrect'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryMultiAllCorrect')));


        $resp = $this->criteriaGroup->getCriteriaGroup();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(2, count($payload));
        $this->assertEquals($this->mockResultMultiAllCorrect(), $payload);
    }

    public function testPaging()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataMultiAllCorrect'
            )));

        $this->request->method('isPaged')
            ->will($this->returnValue(true));

        $this->request->method('getOffset')
            ->will($this->returnValue(1));

        $this->request->method('getLimit')
            ->will($this->returnValue(1));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryMultiAllCorrect')));


        $resp = $this->criteriaGroup->getCriteriaGroup();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(1, count($payload));
        $this->assertEquals($this->mockResultPaging(), $payload);
    }

    public function mockDataSingleEmptyPoolId()
    {
        return array(
            'poolId' => '',
        );
    }

    public function mockDataSingleInvalidPoolId()
    {
        return array(
            'poolId' => 'asdf',
        );
    }

    public function mockDataMultiInvalidPoolId()
    {
        return array(
            'poolId' => [123, '', 'sdf'],
        );
    }

    public function mockDataSingleEmptyPoolName()
    {
        return array(
            'poolName' => '',
        );
    }

    public function mockDataSingleInvalidPoolName()
    {
        return array(
            'poolName' => '*(&(*($&W(*',
        );
    }

    public function mockDataMultiInvalidPoolName()
    {
        return array(
            'poolName' => ['ajkd', '&*^*&#$'],
        );
    }

    public function mockDataSingleAllCorrect()
    {
        return array(
            'poolName' => 'pool 1234',
        );
    }

    public function mockDataMultiAllCorrect()
    {
        return array(
            'poolId' => ['1234', '5678'],
        );
    }

    public function mockQuerySingleAllCorrect()
    {
        return array(
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'filter' => 'tenureCode',
                'filter_value' => 'a',
                'criteria_id' => '1',
                'criteria_name' => 'tenure',
                'criteria_option_id' => '1',
                'criteria_option_name' => 'tenure',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'filter' => 'tenureCode',
                'filter_value' => 'a',
                'criteria_id' => '1',
                'criteria_name' => 'tenure',
                'criteria_option_id' => '2',
                'criteria_option_name' => 'not tenure',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'filter' => 'tenureCode',
                'filter_value' => 'a',
                'criteria_id' => '2',
                'criteria_name' => 'grad level',
                'criteria_option_id' => '1',
                'criteria_option_name' => 'A',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
        );
    }

    public function mockQueryMultiAllCorrect()
    {
        return array(
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'criteria_id' => '1',
                'filter' => 'tenureCode',
                'filter_value' => 'a',
                'criteria_name' => 'tenure',
                'criteria_option_id' => '1',
                'criteria_option_name' => 'tenure',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'criteria_id' => '1',
                'filter' => 'tenureCode',
                'filter_value' => 'a',
                'criteria_name' => 'tenure',
                'criteria_option_id' => '2',
                'criteria_option_name' => 'not tenure',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '1234',
                'pool_name' => 'pool 1234',
                'criteria_id' => '2',
                'filter' => 'gradLevel',
                'filter_value' => 'a',
                'criteria_name' => 'grad level',
                'criteria_option_id' => '1',
                'criteria_option_name' => 'A',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '5678',
                'pool_name' => 'pool 5678',
                'criteria_id' => '2',
                'filter' => 'gradLevel',
                'filter_value' => 'a',
                'criteria_name' => 'grad level',
                'criteria_option_id' => '1',
                'criteria_option_name' => 'A',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '5678',
                'pool_name' => 'pool 5678',
                'criteria_id' => '2',
                'filter' => 'gradLevel',
                'filter_value' => 'a',
                'criteria_name' => 'grad level',
                'criteria_option_id' => '2',
                'criteria_option_name' => 'B',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
            array(
                'pool_id' => '5678',
                'pool_name' => 'pool 5678',
                'criteria_id' => '2',
                'filter' => 'gradLevel',
                'filter_value' => 'a',
                'criteria_name' => 'grad level',
                'criteria_option_id' => '3',
                'criteria_option_name' => 'C',
                'create_user' => 'liaom',
                'update_user' => 'liaom',
                'date_created' => '15-JUN-16 10:22:33',
                'date_updated' => '15-JUN-16 10:22:33',
            ),
        );
    }

    public function mockResultSingleAllCorrect()
    {
        return array(
            array(
                'poolId' => '1234',
                'poolName' => 'pool 1234',
                'criteria' => array(
                    '1' => array(
                        'criteriaFilter' => 'tenureCode',
                        'criteriaName' => 'tenure',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'tenure',
                                'criteriaFilterValue' => 'a'
                            ),
                            '2' => array(
                                'criteriaOptionName' => 'not tenure',
                                'criteriaFilterValue' => 'a'
                            )
                        )
                    ),
                    '2' => array(
                        'criteriaFilter' => 'tenureCode',
                        'criteriaName' => 'grad level',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'A',
                                'criteriaFilterValue' => 'a'
                            ),
                        )
                    ),
                ),
                'createUser' => 'liaom',
                'updateUser' => 'liaom',
                'dateCreated' => '15-JUN-16 10:22:33',
                'dateUpdated' => '15-JUN-16 10:22:33',
            ),
        );
    }

    public function mockResultMultiAllCorrect()
    {
        return array(
            array(
                'poolId' => '1234',
                'poolName' => 'pool 1234',
                'criteria' => array(
                    '1' => array(
                        'criteriaFilter' => 'tenureCode',
                        'criteriaName' => 'tenure',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'tenure',
                                'criteriaFilterValue' => 'a'
                            ),
                            '2' => array(
                                'criteriaOptionName' => 'not tenure',
                                'criteriaFilterValue' => 'a'
                            )
                        )
                    ),
                    '2' => array(
                        'criteriaFilter' => 'gradLevel',
                        'criteriaName' => 'grad level',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'A',
                                'criteriaFilterValue' => 'a'
                            ),
                        )
                    ),
                ),
                'createUser' => 'liaom',
                'updateUser' => 'liaom',
                'dateCreated' => '15-JUN-16 10:22:33',
                'dateUpdated' => '15-JUN-16 10:22:33',
            ),
            array(
                'poolId' => '5678',
                'poolName' => 'pool 5678',
                'criteria' => array(
                    '2' => array(
                        'criteriaFilter' => 'gradLevel',
                        'criteriaName' => 'grad level',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'A',
                                'criteriaFilterValue' => 'a'
                            ),
                            '2' => array(
                                'criteriaOptionName' => 'B',
                                'criteriaFilterValue' => 'a'
                            ),
                            '3' => array(
                                'criteriaOptionName' => 'C',
                                'criteriaFilterValue' => 'a'
                            ),
                        )
                    )
                ),
                'createUser' => 'liaom',
                'updateUser' => 'liaom',
                'dateCreated' => '15-JUN-16 10:22:33',
                'dateUpdated' => '15-JUN-16 10:22:33',
            ),
        );
    }

    public function mockResultPaging()
    {
        return array(
            array(
                'poolId' => '5678',
                'poolName' => 'pool 5678',
                'criteria' => array(
                    '2' => array(
                        'criteriaFilter' => 'gradLevel',
                        'criteriaName' => 'grad level',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'A',
                                'criteriaFilterValue' => 'a'
                            ),
                            '2' => array(
                                'criteriaOptionName' => 'B',
                                'criteriaFilterValue' => 'a'
                            ),
                            '3' => array(
                                'criteriaOptionName' => 'C',
                                'criteriaFilterValue' => 'a'
                            ),
                        )
                    )
                ),
                'createUser' => 'liaom',
                'updateUser' => 'liaom',
                'dateCreated' => '15-JUN-16 10:22:33',
                'dateUpdated' => '15-JUN-16 10:22:33',
            ),
        );
    }
}