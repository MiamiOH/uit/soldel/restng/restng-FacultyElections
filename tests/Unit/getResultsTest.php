<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getResultsTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the GET Functionality of
              the Results Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/26/2016  liaom

*/
use MiamiOH\RESTng\App;

class GetResultsTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $results, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getData',
                'isPaged',
                'getOffset',
                'getLimit'
            ))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->results = new \MiamiOH\FacultyElections\Services\Results();
        $this->results->setApp($this->api);
        $this->results->setApiUser($this->user);
        $this->results->setDatabase($db);
        $this->results->setDatasource($ds);
        $this->results->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testTooManyParameters()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsTooManyParameters'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: too many parameters",
                $e->getMessage());
        }
    }

    public function testEmptyElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyElectionId'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testInvalidElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidElectionId'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testMissingElectionId()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMissingElectionId'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required",
                $e->getMessage());
        }
    }

    public function testElectionIdNotSingle()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsElectionIdNotSingle'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testEmptyType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsEmptyType'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty type",
                $e->getMessage());
        }
    }

    public function testInvalidType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsInvalidType'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid type",
                $e->getMessage());
        }
    }

    public function testMissingType()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsMissingType'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing require",
                $e->getMessage());
        }
    }

    public function testTypeNotSingle()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsTypeNotSingle'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringEndsWith("it should be single",
                $e->getMessage());
        }
    }

    public function testGetSemiFinalistInSFR()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetSemiFinalistInSFR'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetSemiFinalistInSFR'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: you are not allowed to see [Semi-Finalist] in [Semi-Finalist Selection]",
                $e->getMessage());
        }
    }

    public function testGetFinalistInFR()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetFinalistInFR'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetFinalistInFR'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: you are not allowed to see [Finalist] in [Finalist Selection]",
                $e->getMessage());
        }
    }

    public function testGetWinnerInSFR()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetWinnerInSFR'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetWinnerInSFR'
            )));

        try {
            $this->results->getResults();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: you are not allowed to see [Winner] in [Semi-Finalist Selection]",
                $e->getMessage());
        }
    }

    public function testGetSemiFinalistInSFV()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetSemiFinalistInSFV'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetSemiFinalistInSFV'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockQueryFromPersonWS());
        $this->api->method('callResource')->willReturn($personResponse);

        $resp = $this->results->getResults();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultGetSemiFinalistInSFV());
    }

    public function testPreferredNameNotFound()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetSemiFinalistInSFV'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetSemiFinalistInSFV'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockQueryFromPersonWSNameNotFound());
        $this->api->method('callResource')->willReturn($personResponse);

        $resp = $this->results->getResults();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultPreferredNameNotFound());
    }

    public function testPaging()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsPaging'
            )));

        $this->request->method('isPaged')
            ->will($this->returnValue(true));

        $this->request->method('getOffset')
            ->will($this->returnValue(1));

        $this->request->method('getLimit')
            ->will($this->returnValue(2));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryPaging'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockQueryFromPersonWS());
        $this->api->method('callResource')->willReturn($personResponse);

        $resp = $this->results->getResults();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultPaging());
    }

    public function testGetFinalistInFV()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetFinalistInFV'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetFinalistInFV'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockQueryFromPersonWS());
        $this->api->method('callResource')->willReturn($personResponse);

        $resp = $this->results->getResults();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultGetFinalistInFV());
    }

    public function testGetWinnerInC()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockOptionsGetWinnerInC'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryGetWinnerInC'
            )));

        $personResponse = new \MiamiOH\RESTng\Util\Response();
        $personResponse->setPayload($this->mockQueryFromPersonWS());
        $this->api->method('callResource')->willReturn($personResponse);

        $resp = $this->results->getResults();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultGetWinnerInC());
    }

    public function mockOptionsTooManyParameters()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Semi-Finalist',
            'uniqueId' => 'liaom',
        );
    }

    public function mockOptionsEmptyElectionId()
    {
        return array(
            'electionId' => '',
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsInvalidElectionId()
    {
        return array(
            'electionId' => 'skdjhf',
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsMissingElectionId()
    {
        return array(
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsElectionIdNotSingle()
    {
        return array(
            'electionId' => ['1234', '5678'],
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsEmptyType()
    {
        return array(
            'electionId' => '1234',
            'type' => '',
        );
    }

    public function mockOptionsInvalidType()
    {
        return array(
            'electionId' => '1234',
            'type' => 'aaa',
        );
    }

    public function mockOptionsMissingType()
    {
        return array(
            'electionId' => '1234',
        );
    }

    public function mockOptionsTypeNotSingle()
    {
        return array(
            'electionId' => '1234',
            'type' => ['Finalist', 'Winner'],
        );
    }

    public function mockOptionsGetSemiFinalistInSFR()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsGetFinalistInFR()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Finalist',
        );
    }

    public function mockOptionsGetWinnerInSFR()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Winner',
        );
    }

    public function mockOptionsGetSemiFinalistInSFV()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Semi-Finalist',
        );
    }

    public function mockOptionsPaging()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Semi-Finalist',
            'offset' => 1,
            'limit' => 2,
        );
    }

    public function mockOptionsGetFinalistInFV()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Finalist',
        );
    }

    public function mockOptionsGetWinnerInC()
    {
        return array(
            'electionId' => '1234',
            'type' => 'Winner',
        );
    }

    public function mockQueryGetSemiFinalistInSFR()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Nominee',
                'election_status_name' => 'Semi-Finalist Selection',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Nominee',
                'election_status_name' => 'Semi-Finalist Selection',
            ),
        );
    }

    public function mockQueryGetFinalistInFR()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Finalist Selection',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Finalist Selection',
            ),
        );
    }

    public function mockQueryGetWinnerInSFR()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Nominee',
                'election_status_name' => 'Semi-Finalist Selection',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Nominee',
                'election_status_name' => 'Semi-Finalist Selection',
            ),
        );
    }

    public function mockQueryGetSemiFinalistInSFV()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Semi-Finalist Verification',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Semi-Finalist Verification',
            ),
        );
    }

    public function mockQueryGetFinalistInFV()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Finalist',
                'election_status_name' => 'Finalist Verification',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Finalist',
                'election_status_name' => 'Finalist Verification',
            ),
        );
    }

    public function mockQueryPaging()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Semi-Finalist Verification',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Semi-Finalist Verification',
            ),
            array(
                'uniqueid' => 'schmidee',
                'nominee_status' => 'Semi-Finalist',
                'election_status_name' => 'Semi-Finalist Verification',
            ),
        );
    }

    public function mockQueryGetWinnerInC()
    {
        return array(
            array(
                'uniqueid' => 'liaom',
                'nominee_status' => 'Winner',
                'election_status_name' => 'Closed',
            ),
            array(
                'uniqueid' => 'millse',
                'nominee_status' => 'Winner',
                'election_status_name' => 'Closed',
            ),
        );
    }

    public function mockResultGetSemiFinalistInSFV()
    {
        return array(
            array(
                'uniqueId' => 'liaom',
                'preferredName' => 'Mingchao Liao',
                'type' => 'Semi-Finalist',
            ),
            array(
                'uniqueId' => 'millse',
                'preferredName' => 'Erin Mills',
                'type' => 'Semi-Finalist',
            ),
        );
    }

    public function mockResultPaging()
    {
        return array(
            array(
                'uniqueId' => 'millse',
                'preferredName' => 'Erin Mills',
                'type' => 'Semi-Finalist',
            ),
            array(
                'uniqueId' => 'schmidee',
                'preferredName' => 'S E',
                'type' => 'Semi-Finalist',
            ),
        );
    }

    public function mockResultGetFinalistInFV()
    {
        return array(
            array(
                'uniqueId' => 'liaom',
                'preferredName' => 'Mingchao Liao',
                'type' => 'Finalist',
            ),
            array(
                'uniqueId' => 'millse',
                'preferredName' => 'Erin Mills',
                'type' => 'Finalist',
            ),
        );
    }

    public function mockResultPreferredNameNotFound()
    {
        return array(
            array(
                'uniqueId' => 'liaom',
                'preferredName' => 'Mingchao Liao',
                'type' => 'Semi-Finalist',
            ),
            array(
                'uniqueId' => 'millse',
                'preferredName' => '',
                'type' => 'Semi-Finalist',
            ),
        );
    }

    public function mockResultGetWinnerInC()
    {
        return array(
            array(
                'uniqueId' => 'liaom',
                'preferredName' => 'Mingchao Liao',
                'type' => 'Winner',
            ),
            array(
                'uniqueId' => 'millse',
                'preferredName' => 'Erin Mills',
                'type' => 'Winner',
            ),
        );
    }

    public function mockQueryFromPersonWS()
    {
        return array(
            array(
                'uniqueId' => 'MILLSE',
                'prefix' => '',
                'suffix' => '',
                'givenNameLegal' => 'Erin',
                'givenNamePreferred' => 'Erin',
                'familyName' => 'Mills',
                'middleName' => 'Elizabeth',
            ),
            array(
                'uniqueId' => 'SCHMIDEE',
                'prefix' => '',
                'suffix' => '',
                'givenNameLegal' => 'E',
                'givenNamePreferred' => 'S',
                'familyName' => 'E',
                'middleName' => '',
            ),
            array(
                'uniqueId' => 'LIAOM',
                'prefix' => '',
                'suffix' => '',
                'givenNameLegal' => 'Mingchao',
                'givenNamePreferred' => 'Mingchao',
                'familyName' => 'Liao',
                'middleName' => '',
            ),
        );
    }

    public function mockQueryFromPersonWSNameNotFound()
    {
        return array(
            array(
                'uniqueId' => 'SCHMIDEE',
                'prefix' => '',
                'suffix' => '',
                'givenNameLegal' => 'E',
                'givenNamePreferred' => 'S',
                'familyName' => 'E',
                'middleName' => '',
            ),
            array(
                'uniqueId' => 'LIAOM',
                'prefix' => '',
                'suffix' => '',
                'givenNameLegal' => 'Mingchao',
                'givenNamePreferred' => 'Mingchao',
                'familyName' => 'Liao',
                'middleName' => '',
            ),
        );
    }
}