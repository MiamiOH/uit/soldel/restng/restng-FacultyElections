<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: createElectionTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the POST Functionality of
              the Election Web Service

Detail: (+ : implemented, - : not yet be implemented)
    + testSingleEmptyVoterPoolId
    + testSingleEmptyElectionName
    + testSingleEmptyNomineePoolId
    + testSingleEmptyVoterPoolId
    + testSingleEmptyNumOfWinners
    + testSingleEmptyNumOfSemiFinalists
    + testSingleInvalidElectionId
    + testSingleInvalidElectionName
    + testSingleInvalidNomineePoolId
    + testSingleInvalidVoterPoolId
    + testSingleInvalidNumOfWinners
    + testSingleInvalidNumOfSemiFinalists
    + testSingleMissingElectionName

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/20/2016  liaom

09/06/2016  millse  Added unit tests for the divisionFilterValue, departmentFilterValue and campusFilterValue

*/

use MiamiOH\RESTng\App;

class CreateElectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $election, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock mockedApp:
        $mockedApp = $this->createMock(App::class);

        $mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $employeeResponse = new \MiamiOH\RESTng\Util\Response();
        $employeeRequest = new \MiamiOH\RESTng\Util\Request();
        $employeeRequest->setLimit('500');
        $employeeRequest->setOffset('1');
        $employeeResponse->setRequest($employeeRequest);
        $employeeResponse->setPayload($this->mockEmployeeResponse());
        $criteriaGroupResponse = new \MiamiOH\RESTng\Util\Response();
        $criteriaGroupResponse->setPayload($this->mockCriteriaGroupResponse());
        $participantResponse = new \MiamiOH\RESTng\Util\Response();
        $participantResponse->setPayload($this->mockParticipantResponse());
//        $divisionResponse = new \MiamiOH\RESTng\Util\Response();
//        $divisionResponse->setPayload($this->mockDivisionResponse());
//        $departmentResponse = new \MiamiOH\RESTng\Util\Response();
//        $departmentResponse->setPayload($this->mockDepartmentResponse());
//        $campusResponse = new \MiamiOH\RESTng\Util\Response();
//        $campusResponse->setPayload($this->mockCampusResponse());
        $mockedApp->method('callResource')
            ->will($this->onConsecutiveCalls($criteriaGroupResponse,
                $employeeResponse, $participantResponse,
                $criteriaGroupResponse, $employeeResponse, $participantResponse)
            );

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('perform', 'queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        $config = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->election = new \MiamiOH\FacultyElections\Services\Election();
        $this->election->setApp($mockedApp);
        $this->election->setApiUser($this->user);
        $this->election->setDatabase($db);
        $this->election->setDatasource($ds);
        $this->election->setRequest($this->request);
        $this->election->setConfiguration($config);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testSingleEmptyElectionName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleEmptyElectionName'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionName",
                $e->getMessage());
        }
    }

    public function testSingleEmptyNomineePoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleEmptyNomineePoolId'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty nomineePoolId",
                $e->getMessage());
        }
    }

    public function testSingleEmptyVoterPoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleEmptyVoterPoolId'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty voterPoolId",
                $e->getMessage());
        }
    }

    public function testSingleEmptyNumOfWinners()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleEmptyNumOfWinners'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty numOfWinners",
                $e->getMessage());
        }
    }

    public function testSingleEmptyNumOfSemiFinalists()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleEmptyNumOfSemiFinalists'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty numOfSemiFinalists",
                $e->getMessage());
        }
    }

    public function testSingleInvalidElectionName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleInvalidElectionName'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionName",
                $e->getMessage());
        }
    }

    public function testSingleInvalidNomineePoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleInvalidNomineePoolId'
            )));

        try {
            $this->election->createElection();
            //$this->assertTrue(FALSE, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid nomineePoolId",
                $e->getMessage());
        }
    }

    public function testSingleInvalidVoterPoolId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleInvalidVoterPoolId'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid voterPoolId",
                $e->getMessage());
        }
    }

    public function testSingleInvalidNumOfWinners()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleInvalidNumOfWinners'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid numOfWinners",
                $e->getMessage());
        }
    }

    public function testSingleInvalidNumOfSemiFinalists()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleInvalidNumOfSemiFinalists'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid numOfSemiFinalists",
                $e->getMessage());
        }
    }

    public function testSingleMissingElectionName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodySingleMissingElectionName'
            )));

        try {
            $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameters",
                $e->getMessage());
        }
    }

    public function testDivisionEmpty()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDivisionEmpty'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function mockBodyDivisionEmpty()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => '',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testDivisionGood()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDivisionGood'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    public function mockBodyDivisionGood()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CSE',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testDivisionBad()
    {//will include SQL Injection cases. should error.
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDivisionBad'
            )));

        try {
            $resp = $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid divisionFilterValue",
                $e->getMessage());
        }
    }

    public function mockBodyDivisionBad()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'delete * from all_tables;',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testDepartmentEmpty()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDepartmentEmpty'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function mockBodyDepartmentEmpty()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => '',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testDepartmentGood()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDepartmentGood'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function mockBodyDepartmentGood()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testDepartmentBad()
    { //will include SQL Injection cases. should error.
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyDepartmentBad'
            )));

        try {
            $resp = $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid departmentFilterValue",
                $e->getMessage());
        }
    }

    public function mockBodyDepartmentBad()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => 'delete * from all_tables;',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testCampusEmpty()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyCampusEmpty'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function mockBodyCampusEmpty()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => '',
        );

        return $body;
    }

    public function testCampusGood()
    { //expect this to pass through as a successful post
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyCampusGood'
            )));

        $resp = $this->election->createElection();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function mockBodyCampusGood()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'O',
        );

        return $body;
    }

    public function testCampusBad()
    { //will include SQL Injection cases. should error.
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyCampusBad'
            )));

        try {
            $resp = $this->election->createElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid campusFilterValue",
                $e->getMessage());
        }
    }

    public function mockBodyCampusBad()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '1',
            'numOfSemiFinalists' => '2',
            'divisionFilterValue' => 'CEC',
            'departmentFilterValue' => 'MCE',
            'campusFilterValue' => 'delete * from all_tables;',
        );

        return $body;
    }

    public function mockBodySingleEmptyElectionName()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => '',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleEmptyNomineePoolId()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleEmptyVoterPoolId()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleEmptyNumOfWinners()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleEmptyNumOfSemiFinalists()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '',
        );

        return $body;
    }

    public function mockBodySingleInvalidElectionId()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '111sd31111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleInvalidElectionName()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'b,l,a,-*&bla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleInvalidNomineePoolId()
    {
        $body = array(
            'createUser' => 'liaom',
            //'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => 'sdf%3sd',
            'voterPoolId' => '222222',
            'numOfWinners' => '2',
            'numOfSemiFinalists' => '3',
        );

        return $body;
    }

    public function mockBodySingleInvalidVoterPoolId()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => 's%d3d',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleInvalidNumOfWinners()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => 'sd3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockBodySingleInvalidNumOfSemiFinalists()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'electionName' => 'blabla',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2as2',
        );

        return $body;
    }

    public function mockBodySingleMissingElectionName()
    {
        $body = array(
            'createUser' => 'liaom',
            'electionId' => '1111111',
            'nomineePoolId' => '2222222',
            'voterPoolId' => '3333333',
            'numOfWinners' => '3',
            'numOfSemiFinalists' => '2',
        );

        return $body;
    }

    public function mockEmployeeResponse()
    {
        return array(
            array(
                'pidm' => 237663,
                'uniqueId' => 'COVERTKA',
                'plusNumber' => '+00237663',
                'startDate' => '1991-09-19',
                'classificationCode' => 'AF',
                'classificationDescription' => 'Administrative Full-time',
                'fullTimeCode' => 'FT',
                'fullTimeDescription' => 'Full-Time Employee',
                'professionalLibrarian' => 'false',
                'rankCode' => null,
                'rankDescription' => null,
                'tenureCode' => null,
                'tenureDescription' => null,
                'CIPCode' => null,
                'CIPDescription' => null,
                'gradLevelCode' => null,
                'gradLevelDescription' => null,
                'gradLevelCertDate' => null,
                'gradLevelExpDate' => null,
            ),
            array(
                "pidm" => "1225918",
                "uniqueId" => "GENGX",
                "plusNumber" => "+01213207",
                "startDate" => "2011-01-10",
                "classificationCode" => "AF",
                "classificationDescription" => "Administrative Full-time",
                "fullTimeCode" => "FT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "false",
                "rankCode" => null,
                "rankDescription" => null,
                'tenureCode' => null,
                'tenureDescription' => null,
                'CIPCode' => null,
                'CIPDescription' => null,
                'gradLevelCode' => null,
                'gradLevelDescription' => null,
                'gradLevelCertDate' => null,
                'gradLevelExpDate' => null,
            )
        );
    }

    public function mockCriteriaGroupResponse()
    {
        return array(
            array(
                'poolId' => '1234',
                'poolName' => 'pool 1234',
                'criteria' => array(
                    '1' => array(
                        'criteriaName' => 'tenure',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'tenure',
                                'criteriaFilterValue' => 'filterValue1'
                            ),
                            '2' => array(
                                'criteriaOptionName' => 'not tenure',
                                'criteriaFilterValue' => 'filterValue1'
                            )
                        ),
                        'criteriaFilter' => 'tenureCode'
                    ),
                    '2' => array(
                        'criteriaName' => 'grad level',
                        'options' => array(
                            '1' => array(
                                'criteriaOptionName' => 'A',
                                'criteriaFilterValue' => 'filterValue1'
                            )
                        ),
                        'criteriaFilter' => 'tenureCode'
                    ),
                ),
                'createUser' => 'liaom',
                'updateUser' => 'liaom',
                'dateCreated' => '15-JUN-16 10:22:33',
                'dateUpdated' => '15-JUN-16 10:22:33',
            ),
        );
    }

    public function mockParticipantResponse()
    {
        return array();
    }

    public function mockDivisionResponse()
    {
        return array(
            array(
                'divisionCode' => 'CEC',
                'divisionName' => 'College of Engineering & Computing'
            )
        );
    }

    public function mockDepartmentResponse()
    {
        return array(
            array(
                'departmentCode' => 'MCE',
                'departmentName' => 'Mechanical Engineering',
                'divisionCode' => 'CEC',
                'divisionName' => 'College of Engineering & Computing'
            )
        );
    }

    public function mockCampusResponse()
    {
        return array(
            array(
                'campusCode' => 'O',
                'campusName' => 'Oxford',
            )
        );
    }
}