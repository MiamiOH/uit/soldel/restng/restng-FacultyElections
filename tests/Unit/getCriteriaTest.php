<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getCriteriaTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Participant Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
07/15/2016  liaom

*/

use MiamiOH\RESTng\App;

class GetCriteriaTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $criteria, $queryallRecords, $user, $request, $awardService, $api;

    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $resourceResponses = array();

    private $callResourceMockResponse = array();

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')
            ->willReturn(new \MiamiOH\RESTng\Util\Response());


        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('getUsername', 'isAuthorized'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->criteria = new \MiamiOH\FacultyElections\Services\Criteria();
        $this->criteria->setApp($this->api);
        $this->criteria->setApiUser($this->user);
        $this->criteria->setDatabase($db);
        $this->criteria->setDatasource($ds);
        $this->criteria->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetCriteria()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array(
                $this,
                'mockDataGetCriteria'
            )));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryGetCriteria')));


        $resp = $this->criteria->getCriteria();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($payload), 2);
        $this->assertEquals($payload, $this->mockResultGetCriteria());
    }

    public function mockDataGetCriteria()
    {
        return array();
    }

    public function mockQueryGetCriteria()
    {
        return array(
            array(
                'criteria_id' => 1,
                'criteria_name' => 'tenure',
                'criteria_option_id' => 1,
                'criteria_option_name' => 'tenure'
            ),
            array(
                'criteria_id' => 1,
                'criteria_name' => 'tenure',
                'criteria_option_id' => 2,
                'criteria_option_name' => 'not tenure'
            ),
            array(
                'criteria_id' => 2,
                'criteria_name' => 'grad level',
                'criteria_option_id' => 1,
                'criteria_option_name' => 'A'
            ),
            array(
                'criteria_id' => 2,
                'criteria_name' => 'grad level',
                'criteria_option_id' => 2,
                'criteria_option_name' => 'B'
            ),
            array(
                'criteria_id' => 2,
                'criteria_name' => 'grad level',
                'criteria_option_id' => 3,
                'criteria_option_name' => 'C'
            ),
        );
    }

    public function mockResultGetCriteria()
    {
        return array(
            array(
                'criteriaId' => 1,
                'criteriaName' => 'tenure',
                'criteriaOption' => array(
                    '1' => 'tenure',
                    '2' => 'not tenure',
                ),
            ),
            array(
                'criteriaId' => 2,
                'criteriaName' => 'grad level',
                'criteriaOption' => array(
                    '1' => 'A',
                    '2' => 'B',
                    '3' => 'C',
                ),
            ),
        );
    }
}
