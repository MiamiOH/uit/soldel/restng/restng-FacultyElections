<?php

namespace MiamiOH\FacultyElections\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: modifyElectionTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Mingchao Liao

DESCRIPTION:  Unit Tests for Testing the PUT Functionality of
              the Election Web Service

Detail: (+ : implemented, - : not yet be implemented)
    -

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
05/20/2016  liaom

*/

use MiamiOH\RESTng\App;

class ModifyElectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $election, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {


        //set up the mock api:
        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('perform', 'queryall_array'))
            ->getMock();

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->election = new \MiamiOH\FacultyElections\Services\Election();
        $this->election->setApp($api);
        $this->election->setApiUser($this->user);
        $this->election->setDatabase($db);
        $this->election->setDatasource($ds);
        $this->election->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testInvalidElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyInvalidElectionId'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionId",
                $e->getMessage());
        }
    }

    public function testInvalidElectionName()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyInvalidElectionName'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid electionName",
                $e->getMessage());
        }
    }

    public function testInvalidNumberOfWinners()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyInvalidNumberOfWinners'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid numOfWinners",
                $e->getMessage());
        }
    }

    public function testInvalidNumberOfSemiFinalists()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyInvalidNumberOfSemiFinalists'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: invalid numOfSemiFinalists",
                $e->getMessage());
        }
    }

    public function testEmptyElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyElectionId'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty electionId",
                $e->getMessage());
        }
    }

    public function testEmptyNumberOfWinners()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyNumberOfWinners'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty numOfWinners",
                $e->getMessage());
        }
    }

    public function testEmptyNumberOfSemiFinalists()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyEmptyNumberOfSemiFinalists'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: empty numOfSemiFinalists",
                $e->getMessage());
        }
    }

    public function testMissingElectionId()
    {
        $this->request->method('getData')
            ->will($this->returnCallback(array(
                $this,
                'mockBodyMissingElectionId'
            )));

        try {
            $this->election->modifyElection();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Error: missing required parameters",
                $e->getMessage());
        }
    }

    public function mockBodyInvalidElectionId()
    {
        $body = array(
            'electionId' => '78dyu3',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyInvalidElectionName()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => "[/,#$%!@ - _ '",
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyInvalidNomineePoolId()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => 'sd',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyInvalidElectionStatus()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => 'sdf',
        );

        return $body;
    }

    public function mockBodyInvalidNumberOfWinners()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => 'sd',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyInvalidNumberOfSemiFinalists()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => 'sd1',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyInvalidVoterPoolId()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => 'sd',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyEmptyElectionId()
    {
        $body = array(
            'electionId' => '',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyEmptyNomineePoolId()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyEmptyElectionStatus()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '',
        );

        return $body;
    }

    public function mockBodyEmptyNumberOfWinners()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyEmptyNumberOfSemiFinalists()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyEmptyVoterPoolId()
    {
        $body = array(
            'electionId' => '1234567',
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockBodyMissingElectionId()
    {
        $body = array(
            'electionName' => 'abc ABC : () - _ "',
            'nomineePoolId' => '11',
            'voterPoolId' => '11',
            'numOfWinners' => '11',
            'numOfSemiFinalists' => '11',
            'electionStatus' => '11',
        );

        return $body;
    }

    public function mockQueryInvalidElectionStatus()
    {
    }

    public function mockQueryInvalidNomineePoolId()
    {
    }

    public function mockQueryInvalidVoterPoolId()
    {
    }
}