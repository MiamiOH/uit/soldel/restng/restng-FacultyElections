<?php

Logger::configure(array(
    'rootLogger' => array(
        'appenders' => array('apiLogFile'),
        'level' => 'warn',
    ),

    'loggers' => array(
        'RESTng\Service\CITest\CITest' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'debug',
        ),

    ),

    'appenders' => array(
        'apiLogFile' => array(
            'class' => 'LoggerAppenderFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%level %date [%logger] %message%newline%ex'
                )
            ),
            'params' => array(
                'file' => '/var/log/restng/api.log'
            )
        ),
    )
));